﻿<?xml version="1.0" encoding="utf-8"?>
<GridProps xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="http://www.orchestrade.com">
  <ClassName>Ship</ClassName>
  <Categories>
    <Category Name="Identity">
      <Items>
        <CategoryItem FieldName="Id"  AllowEdit="false"/>
        <CategoryItem FieldName="Version"  AllowEdit="false"/>
        <CategoryItem FieldName="Source" ToolTip="Name of the Master System (if any)"/>
        <CategoryItem FieldName="SourceReference" Display="Source Reference" ToolTip="Reference in Master System to be used in reconciliations"/>
      </Items>
    </Category>
    <Category Name="Ship Info">
      <Items>
        <Category Name="Identification">
          <Items>
            <Category Name="IdentifierInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="Imo" Display="IMO" ToolTip="International Maritime Organization (IMO)"/>
                <CategoryItem FieldName="Name" Display="Name" />
                <CategoryItem FieldName="OldName" Display="Ex-Name" />
                <CategoryItem FieldName="HullNumber" Display="Hull Number"/>
              </Items>
            </Category>
            <Category Name="PurposeInfo" IsMultiRow="true">
              <Items> 
                <CategoryItem FieldName="Purpose" Display="Purpose" ExcludeValues="None"/>
                <CategoryItem FieldName="PreOwned" Display="Pre-Owned" EditAs="Bool"/>
              </Items>
            </Category>
            <Category Name="FlagInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="Flag" EditAs="Country"/>
                <CategoryItem FieldName="PrimaryRegistry" Display="Primary Registry" EditAs="Country" />
                <CategoryItem FieldName="OwnerId" Display="Owner" EditAs="Party" Role="CounterParty|FacilityBorrower"/>
              </Items>
            </Category>
            <Category Name="ManagerIDInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="TechnicalManagerId" Display="Technical Manager/Yacht Manager" EditAs="Party"/>
                <CategoryItem FieldName="CommercialManagerId" Display="Commercial Manager" EditAs="Party"/>
              </Items>
            </Category>
            <Category Name="DomicileInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="TechnicalDomicile" Display="Technical Manager/Yacht Manager Domicile" AllowEdit="false"/>
                <CategoryItem FieldName="CommercialDomicile" Display="Commercial Manager Domicile" AllowEdit="false"/>
              </Items>
            </Category>
            <Category Name="IndependentInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="IndependentManager" Display="Independent Manager" EditAs="Bool"/>
              </Items>
            </Category>
          </Items>
        </Category>
        
        <Category Name="Technical">
          <Items>
            <Category Name="UsageInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="Usage" Display="Usage Type" ExcludeValues="None" />
                <CategoryItem FieldName="ShipType" Display="Ship Type" />
                <CategoryItem FieldName="ShipSubType" Display="Ship Subtype" />
              </Items>
            </Category>
            <Category Name="CapacityInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="CapacityMetricAmount" Display="Capacity Value" EditAs="Amount" ToolTip="Capacity amount in Capacity Metric units" />
                <CategoryItem FieldName="CapacityMetricUnit" Display="Capacity Metric" Domain="Ship:CapacityUnit" />
                <CategoryItem FieldName="Capacity" Display="Capacity (DWT)" EditAs="Amount" />
              </Items>
            </Category>
            <Category Name="BuildInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="DeliveryStatus" Display="Delivery Status" Domain="Ship:DeliveryStatus"/>
                <CategoryItem FieldName="BuildMonthYear" Display="Build Month/Year" EditAs="MonthYear"/>
                <CategoryItem FieldName="YearOfDelivery" Display="Year Of Delivery"/>
              </Items>
            </Category>
            <Category Name="YardInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="Yard" Display="Yard" Domain="Ship:Yard"/>
                <CategoryItem FieldName="YardCountry" Display="Yard Country" EditAs="Country"/>
                <CategoryItem FieldName="CaptainId" Display="Captain" EditAs="Party"/>
                <CategoryItem FieldName="ConstructionPrice" Display="Construction Price (USD)" EditAs="Amount"/>
                <CategoryItem FieldName="ShipDesign" Display="Design"/>             
              </Items>
            </Category>
            <Category Name="OtherTechnicalInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="Class" Display="Ice Class" Domain="Ship:IceClass"/>
                <CategoryItem FieldName="Gearing" Domain="Ship:Gearing"/>
                <CategoryItem FieldName="OtherSpecs" Display="Other Specs" EditAs="Memo" />
              </Items>
            </Category>
            <Category Name="PrivateUsageTechnicalInfo1" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="Length" Display="LOA (m)" EditAs="Amount" ToolTip="Specific to Usage=Private"/>
                <CategoryItem FieldName="Beam" Display="BOA (m)" EditAs="Amount" ToolTip="Specific to Usage=Private"/>
                <CategoryItem FieldName="Draft" Display="Draft (m)" EditAs="Amount" ToolTip="Specific to Usage=Private"/>
              </Items>
            </Category>
            <Category Name="PrivateUsageTechnicalInfo2" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="HullType" Display="Hull" Domain="Ship:HullType" ToolTip="Specific to Usage=Private"/>
                <CategoryItem FieldName="Superstructure" Display="Superstructure" Domain="Ship:Superstructure"  ToolTip="Specific to Usage=Private"/>
                <CategoryItem FieldName="NavalArchitecture" Display="Naval Architecture" ToolTip="Specific to Usage=Private"/>
                <CategoryItem FieldName="ExteriorDesigner" Display="Exterior Designer" ToolTip="Specific to Usage=Private"/>
                <CategoryItem FieldName="InteriorDesigner" Display="Interior Designer" ToolTip="Specific to Usage=Private"/>
              </Items>
            </Category>
          </Items>
        </Category>

        <Category Name="Worldcheck, Inspections and Surveys / DD, MII and MAP">
          <Items>
            <Category Name="WorldheckInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="LastWorldcheckDate" Display="Last Worldcheck Date" />
                <CategoryItem FieldName="LastWorldcheckComment" Display="Worldcheck Hit/Comments" EditAs="Memo" />
              </Items>
            </Category>
            <Category Name="InspectionInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="LastInspectionDate" Display="Last Inspection Date" />
                <CategoryItem FieldName="LastInspectionBy" Display="Last Inspection By" />
                <CategoryItem FieldName="LastInspectionComment" Display="Findings/Comments" EditAs="Memo" />
              </Items>
            </Category>
            <Category Name="SurveyInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="LastSurveyDate" Display="Last Special Survey Date" />
                <CategoryItem FieldName="NextEstSurveyDate" Display="Next Estimated Special Survey Date" />
                <CategoryItem FieldName="LastSurveyComment" Display="Comments" EditAs="Memo" />
              </Items>
            </Category>
            <Category Name="MortgageeInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="MortgageeInterestInsurance" Display="Mortgagee Interest Insurance (MII)" Domain="Ship:MII"/>
                <CategoryItem FieldName="MortgageeAdditionalPerilsPollution" Display="Mortgagee AdditionalPerils Pollution (MAP)" Domain="Ship:MAP"/>
              </Items>
            </Category>
            <Category Name="ClassificationInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="ClassificationIssueDate" Display="Classification Issue Date" />
                <CategoryItem FieldName="ClassificationExpiryDate" Display="Classification Expiry Date" />
                <CategoryItem FieldName="ClassificationSociety" Display="Classification Society" Domain="Ship:ClassificationSociety" />
              </Items>
            </Category>
          </Items>
        </Category>
        
        <Category Name="Ship Valuations">
          <Items>
            <Category Name="Broker1Info" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="Broker1Name" Display="Broker Appraiser 1" Domain="Ship:Broker"/>
                <CategoryItem FieldName="Broker1Value" Display="Valuation 1" EditAs="Amount" DisableMouseSpin="true"/>
                <CategoryItem FieldName="Broker1Currency" Display="Currency 1" Domain="Currency"/>
                <CategoryItem FieldName="Broker1ValueDate" Display="Valuation Date 1" />
              </Items>
            </Category>
            <Category Name="Broker2Info" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="Broker2Name" Display="Broker Appraiser 2" Domain="Ship:Broker"/>
                <CategoryItem FieldName="Broker2Value" Display="Valuation 2" EditAs="Amount" DisableMouseSpin="true"/>
                <CategoryItem FieldName="Broker2Currency" Display="Currency 2" Domain="Currency"/>
                <CategoryItem FieldName="Broker2ValueDate" Display="Valuation Date 2" />
              </Items>
            </Category>
            <Category Name="Broker3Info" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="Broker3Name" Display="Broker Appraiser 3" Domain="Ship:Broker"/>
                <CategoryItem FieldName="Broker3Value" Display="Valuation 3" EditAs="Amount" DisableMouseSpin="true"/>
              <CategoryItem FieldName="Broker3Currency" Display="Currency 3" Domain="Currency"/>
              <CategoryItem FieldName="Broker3ValueDate" Display="Valuation Date 3" />
              </Items>
            </Category>
            <Category Name="BrokerAvgInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="BrokerAverageValue" Display="Average Ship Valuation" EditAs="Amount" AllowEdit="false" ToolTip="Average calculated when all the brokers have same currency."/>
                <CategoryItem FieldName="BrokerAverageCurrency" Display="Average Ship Valuation Currency" Domain="Currency" AllowEdit="false"/>
                <CategoryItem FieldName="BrokerAverageValueDate" Display="Average Ship Valuation Date" AllowEdit="false"/>
              </Items>
            </Category>
            <Category Name="PriceInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="AskingPriceFrom" Display="Asking Price (From)" EditAs="Amount"/>
                <CategoryItem FieldName="AskingPriceTo" Display="Asking Price (To)" EditAs="Amount"/>
              </Items>
            </Category>
              <Category Name="ExternalSourceInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="ExtSourceName" Display="Imported Market Valuation (Source)"/>
                <CategoryItem FieldName="ExtSourceValue" Display="Imported Market Valuation (USD)" EditAs="Amount" DisableMouseSpin="true"/>
                <CategoryItem FieldName="ExtSourceValueDate" Display="Imported Market Valuation (Date)"/>
              </Items>
            </Category>
            <Category Name="ExternalDemolitionInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="ExtDemolitionSourceName" Display="Imported Demolition Valuation (Source)"/>
                <CategoryItem FieldName="ExtDemolitionSourceValue" Display="Imported Demolition Valuation (USD)" EditAs="Amount" DisableMouseSpin="true"/>
                <CategoryItem FieldName="ExtDemolitionSourceValueDate" Display="Imported Demolition Valuation (Date)"/>
              </Items>
            </Category>
            <Category Name="PurchaseInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="PurchasePrice" Display="Purchase Price" EditAs="Amount"/>
                <CategoryItem FieldName="ProductCurrency" Display="Currency" Domain="Currency"/>
                <CategoryItem FieldName="PurchaseDate" Display="Purchase Date"/>
                <CategoryItem FieldName="PurchaseComment" Display="Comment" EditAs="Memo" />
              </Items>
            </Category>
          </Items>
        </Category>
        <Category Name="Visual">
          <Items>
            <CategoryItem FieldName="Logo" EditAs="Icon" ToolTip="Click on data area and use right-click menu options to update image"/>
          </Items>
        </Category>
      </Items>
    </Category>
    
    <Category Name="Operating Expenses">
      <Items>
        <Category Name="Client OPEX (all amounts in USD p. d.)" >
          <Items>
            <Category Name ="ClientOpexInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="OpexTotal" Display="Total OPEX" EditAs="Amount" DisableMouseSpin="true"/>
                <CategoryItem FieldName="OpexLastUpdate" Display="Last Updated"/>
              </Items>
            </Category>
            <Category Name ="ClientOpexCosts" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="OpexDailyRunningCost" Display="Daily Running Costs" EditAs="Amount" DisableMouseSpin="true"/>
                <CategoryItem FieldName="OpexManagementFee" Display="G&amp;A/Management Fees" EditAs="Amount" DisableMouseSpin="true"/>
                <CategoryItem FieldName="OpexProvision" Display="Provisions/Retentions for Drydock/Survey" EditAs="Amount" DisableMouseSpin="true"/>
              </Items>
            </Category>
          </Items>
        </Category>
        <Category Name="Standard OPEX (all amounts in USD p. d.)" >
          <Items>
            <Category Name ="StandardOpexInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="ImportedOpexValue" Display="Imported OPEX" EditAs="Amount" AllowEdit="false"/>
                <CategoryItem FieldName="ImportedOpexSource" Display="Reference Data Series (OPEX)"/>
                <CategoryItem FieldName="ImportedOpexLastUpdate" Display="Last Updated" AllowEdit="false"/>
              </Items>
            </Category>
          </Items>
        </Category>
      </Items>
    </Category>
    
    <Category Name="Employment">
      <Items>
        <Category Name="Market Rate (all amounts in USD p. d.)" >
          <Items>
            <Category Name ="StandardOpexInfo" IsMultiRow="true">
              <Items>
                <CategoryItem FieldName="EarningsValue" Display="One Year TC" EditAs="Amount" AllowEdit="false"/>
                <CategoryItem FieldName="EarningsSource" Display="Reference Data Series (Earnings)"/>
                <CategoryItem FieldName="EarningsLastUpdate" Display="Last Updated" AllowEdit="false"/>
              </Items>
            </Category>
          </Items>
        </Category>
      </Items>
    </Category>
      
    
    <Category Name="Additional Info" Collapsed="true">
      <Items>
        <CategoryItem FieldName="Engine" EditAs="List" List="Diesel, Gasoline"/>
        <CategoryItem FieldName="FuelCapacity" Display="Fuel Capacity"/>
        <CategoryItem FieldName="Cabin"/>
        <CategoryItem FieldName="Crew"/>
        <CategoryItem FieldName="Construction"/> 
        <CategoryItem FieldName="CruisingSpeed" Display="Cruising Speed"/>
        <Category Name="BuilderInfo" IsMultiRow="true">
          <Items>
            <CategoryItem FieldName="Builder" />
            <CategoryItem FieldName="BuilderCountry" Display="Builder Country" EditAs="Country"/>
          </Items>
        </Category>
        <Category Name="Commercial" IsMultiRow="false">
          <Items>
            <CategoryItem FieldName="CompensatedGrossTonnage" Display="Compensated Gross Tonnage" EditAs="Amount"/>
            <CategoryItem FieldName="GrossTonnage" Display="Gross Tonage" EditAs="Amount"/>
            <CategoryItem FieldName="BreakHorsePower" Display="Break Horse Power" EditAs="Amount"/>
            <CategoryItem FieldName="Coated" EditAs="List" List="Stainless Steel, Adv. Polymer,Zinc, Poly, Epoxy"/>
          </Items>
        </Category>
        <Category Name="Private" IsMultiRow="false">
          <Items>
            <CategoryItem FieldName="Sailing" Domain="ShipSailingType" ToolTip="Specific to Usage=Private"/>
            <CategoryItem FieldName="Motor" Domain="ShipMotorType" ToolTip="Specific to Usage=Private"/>
          </Items>
        </Category>
      </Items>
    </Category>
    <Category Name="Info">
      <Items>
        <CategoryItem FieldName="IsIlliquid" Display="Is Illiquid" EditAs="Bool"/>
        <CategoryItem FieldName="ProductCodes" Display="Codes" EditAs="Map" Domain="ProductCode" ProductType="Ship"/>
      </Items>
    </Category>
  </Categories>
</GridProps>
