﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrchestradeCommon.Risk;

namespace GemCorpAnalytics.OrchestradeLayer
{
    public class RiskProgress : IAnalysisProgress
    {
        public bool IsCanceled { get; set; }

        public IList<Exception> Exceptions { get; set; }

        public void AnalysisDone()
        {
            Console.WriteLine("");
        }

        public bool AnalysisException(Exception exception, string msg)
        {
            return true;
        }

        public void AnalysisInit(long total, string message)
        {
            Console.WriteLine("");
        }

        public void AnalysisProgress(long current, string message)
        {
            Console.WriteLine("");
        }

        public void OnCompleted(AnalysisReport report)
        {
            Console.WriteLine("");
        }
    }
}
