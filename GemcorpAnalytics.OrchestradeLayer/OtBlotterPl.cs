﻿namespace GemCorpAnalytics.OrchestradeLayer
{
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.Enums;
    using OrchestradeClient;
    using OrchestradeClient.Gui;
    using OrchestradeClient.Gui.Blotters;
    using OrchestradeClient.Gui.Util;
    using OrchestradeCommon.Contracts;
    using OrchestradeCommon.MarketData;
    using OrchestradeCommon.Pricing;
    using OrchestradeCommon.RefData;
    using OrchestradeCommon.Reporting;
    using OrchestradeCommon.Risk;
    using OrchestradeCommon.Trading;
    using OrchestradeCommon.Util;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.IO;

    /// <summary>Orchestrade blotter pl.</summary>
    public class OtBlotterPl
    {
        // make this class a singleton

        #region Fields

        /// <summary>The username.</summary>
        private readonly string _userName;

        /// <summary>The environment.</summary>
        private readonly string _environment;

        /// <summary>The password.</summary>
        private readonly string _pwd;

        /// <summary>The args.</summary>
        private readonly string[] _args;

        /// <summary>The session.</summary>
        private ClientSession _session;

        #endregion

        #region Constructor

        public OtBlotterPl(string userName, string environment, string pwd)
        {
            _userName = userName;
            _environment = environment;
            _pwd = pwd;
            //_args = new string[] { $"-env {_environment }", $"-user {_userName}",  $"-pwd {_pwd}" };

            //-config Default -setup Official -filter Macro|Book -configItems ConsoRisk

            _args = new[] { "RTBlotter.exe", "-env", $"{_environment}", "-user", $"{_userName}", "-pwd", $"{_pwd}", "-config", "Default", 
                "-setup","Official","-time","20180228", "-filter","Macro Book", "-configItems","ConsoRisk"};

        }

        #endregion

        #region Public Methods

        /// <summary>Creates an Orchestrade connection.</summary>
        public void ConnnectToOrchestrade()
        {
            var envName = Env.GetOption("-env", _args);
            var userName = Env.GetOption("-user", _args);
            var pwd = Env.GetOption("-pwd", _args);
            _session = new ClientSession(_args);
            _session.StartAndConnect(userName, "RunRisk", envName, pwd);

        }

        /// <summary>Runs report.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="reportType">The report type.</param>
        /// <param name="configName">The config name.</param>
        /// <returns>A set of <see cref="PLEntry"/>.</returns>
        public List<PlEntry> RunReport(DateTime valueDate, string reportType, string configName)
        {

            try
            {

                if (_session == null)
                    throw new Exception("Unable to run report as session has not begun.");

                var user = Env.Current.User;

                var report = (PlBlotterPanel)ReportHelper.Find(reportType);

                var prevDate = valueDate.AddDays(-1); // This needs to be previous business day
                prevDate = prevDate.AddHours(-7);
                prevDate = prevDate.AddMinutes(1);

                //var config = Env.Current.Admin.GetUserConfig(user, reportType, configName) ?? Env.Current.Admin.GetUserConfig(UserConfig.Shared, reportType, configName);
                var config = Env.Current.Admin.GetUserConfig(UserConfig.Shared, reportType, configName);
                config.SetProperty<DateTime>("FromDate", prevDate); // this should be previous business day
                config.SetProperty<DateTime>("ToDate", valueDate);
                //config.SetProperty<string>("FilterName", "All:TestBook"); // Exclude this line to use GlobalStrategy

                var exceptionsLog = new List<Exception>();

                report.UseConfig(config, valueDate, (IList<Exception>)exceptionsLog, new OrchestradeCommon.RefData.Task.Task());

                //var panel = new PlBlotterPanel();
                var panel = (PlBlotterPanel)report;
                panel.BuildConfig(config);

                var entries = panel.Entries;

                var setOutput = new Set<PLEntry> { entries };

                var partyData = Env.Current.StaticData.GetAllPartyLike("%", "Book");

                var output = CreatePlEntryObjects(setOutput, partyData);

                /*
                using (var outFile = new System.IO.StreamWriter("C:\\temp\\PlBotterOutput_v7.txt"))
                {
                    foreach (var entry in output)
                    {
                        var outLine = $"{entry.UnderlierInfo},{entry.ParentFund},{entry.Entity},{entry.AssetClass},{entry.Book},{entry.Strategy},{entry.Currency}," +
                            $"{entry.TradeId},{entry.StartFxPrice},{entry.StartQuote},{entry.EndFxPrice},{entry.EndQuote},{entry.StartNominal},{entry.EndNominal},{entry.StartPv},{entry.EndPv}," +
                            $"{entry.PlDaily},{entry.PlWeek},{entry.PlMonth}," +
                            $"{entry.PlYtd},{entry.PlInception},{entry.CountryOfRisk},{entry.Description}";
                        outFile.WriteLine(outLine);
                    }
                }
                */

                var noCountryOfRisk = output.Where(t => string.IsNullOrEmpty(t.CountryOfRisk)).ToList();

                return output;
            }catch(Exception e)
            {
                throw e;
            }
      
        }

        public List<ValueAtRiskEntry> RunVaRReport(DateTime valueDate, string configName, string filterName, string analysisConfigName)
        {

            var analysisSet = Env.Current.Admin.GetAnalysisSet(configName);
            // One of the alaysis set values contains data for conso risk too
            var analysisItems = analysisSet.Items.FirstOrDefault(i => i.AnalysisConfigName.Equals(analysisConfigName));
            var analysis = AnalysisSetItem.AnalysisFromName(analysisItems.AnalysisName);
            var config = Env.Current.Admin.GetAnalysisConfig(analysisItems.AnalysisConfigName, analysisItems.AnalysisName);
            config.SetProperty("UseRealTime", "false");

            var setupName = "Official-Curves"; // or Official
            var filter = Env.Current.Trade.GetFilter(filterName);
            var byPosition = true;
            var split = false;

            var riskProgress = new RiskProgress();

            var pricingSetup = Env.Current.MarketData.GetPricingSetup(setupName);
            var market = new Market { Setup = pricingSetup, Time = valueDate };
            var report = analysis.Compute(filter, valueDate, valueDate, market, config, riskProgress, byPosition, split);

            var varReport = report as VaRReport;

            var output = new List<ValueAtRiskEntry>();

            var bookIds = varReport.AllItems.Select(r => r.BookId).ToList();

            var parentId = Env.Current.StaticData.GetPartiesByIds(bookIds);

            foreach (var data in varReport.AllItems.Zip(varReport.Result, (a,b)=> new Tuple<TradeData, VaRTradeResult>(a,b)))
            {

                var party = parentId.FirstOrDefault(p => string.Equals(p.Name, data.Item1.Book));
                var countryOfRisk = string.Empty;
                if (!(data.Item1.TradeProperties == null))
                {
                    var countryOfRiskFlag = data.Item1.TradeProperties.TryGetValue("CountryOfRisk", out countryOfRisk);
                }
                var productType = data.Item1.ProductType;
                var assetClass = data.Item1.AssetClass;
                var description = data.Item1.Description;
                var strategy = string.Empty;
                var parentFund = string.Empty;
                if (party != null)
                { 
                    var strategFound = party.Properties.TryGetValue("Strategy", out strategy); 
                    var parentFundFound = party.Properties.TryGetValue("Parent Fund", out parentFund); 
                }
                var varEntry = new ValueAtRiskEntry.Builder { 
                    CountryOfRisk = countryOfRisk,
                    VaRResults = data.Item2.Result,
                    UnderlierInfo = data.Item1.UnderlierInfo,
                    Currency = data.Item1.Currency,
                    Book = data.Item1.Book,
                    Entity = data.Item1.Entity,
                    VaR = data.Item2.Var,
                    MVaR = data.Item2.MVar,
                    CVaR = data.Item2.CVar,
                    Pv = data.Item2.Pv,
                    PvLocal = data.Item2.PvLocal,
                    Description = description,
                    Strategy = strategy,
                    AssetClass = assetClass,
                    ParentFund = parentFund,
                    ProductType = productType
                }.Build();
                output.Add((ValueAtRiskEntry)varEntry);
            }

            var fundBreakdown = Utils.Utils.CalculateFundLevelValueAtRisk(output, 5); // need to add this to the output object

            using (var outFile = File.AppendText("C:\\temp\\VaROuputDaily_v2.txt"))
            { 
                foreach(var tmp in fundBreakdown)
                {
                    outFile.WriteLine($"{valueDate: yyyy/MM/dd},{tmp.Key},{tmp.Value.Item1},{tmp.Value.Item2}");
                }
            }

            return output;
        }

        public IConsoRiskReportResult RunConsoRiskReport(DateTime valueDate, string configName, string filterName, string analysisConfigName)
        {
            // The code will be the same as above, the inputs and data object will be different
            var analysisSet = Env.Current.Admin.GetAnalysisSet(configName);
            // One of the alaysis set values contains data for conso risk too
            var analysisItems = analysisSet.Items.FirstOrDefault(i => i.AnalysisConfigName.Equals(analysisConfigName)
            && i.FilterName.Equals(filterName));
            var analysis = AnalysisSetItem.AnalysisFromName(analysisItems.AnalysisName);
            var config = Env.Current.Admin.GetAnalysisConfig(analysisItems.AnalysisConfigName, analysisItems.AnalysisName);
            config.SetProperty("UseRealTime", "false");

            var setupName = "Official"; // or Official
            var filter = Env.Current.Trade.GetFilter(analysisItems.FilterName);
            var byPosition = true;
            var split = false;

            var riskProgress = new RiskProgress();

            var pricingSetup = Env.Current.MarketData.GetPricingSetup(setupName);
            var market = new Market { Setup = pricingSetup, Time = valueDate };
            var report = analysis.Compute(filter, valueDate, valueDate, market, config, riskProgress, byPosition, split);

            var scenarioReport = report as ScenarioReport;

            var products = scenarioReport.Perturbations.First().AllUnderliers.Select(p =>BuildProduct(p));

            var bookIds = scenarioReport.Trades.Select(r => r.BookId).ToList();

            var parentId = Env.Current.StaticData.GetPartiesByIds(bookIds);

            var trades = scenarioReport.Trades.Select(t => {

                var party = parentId.FirstOrDefault(p => p.Id == t.BookId);
                var product = products.FirstOrDefault(pr => pr.ProductId == t.Product.Id);

                if(product == null)
                { 
                    product = BuildProduct(t.Product);
                }

                var countryOfRisk = string.Empty;
                if (t.Properties != null)
                {
                    var countryOfRiskFlag = t.Properties.TryGetValue("CountryOfRisk", out countryOfRisk);
                }

                return new Data.Trade.Builder
                {
                    TradeDate = t.TradeTime,
                    Name = t.AuditName,
                    Description = t.Product.Description,
                    BuySell = t.BuySell,
                    TradeId = (int)t.Id,
                    Book = party.Name,
                    BookId = (int)t.BookId,
                    Entity = party.GetEntity().Name,
                    ParentFund = party.Properties["Parent Fund"],
                    Strategy = party.Properties["Strategy"],
                    Quantity = t.Quantity,
                    Price = t.Price,
                    Product  = product,
                    CountryOfRisk = countryOfRisk,
                    TradeSettleCurrency = t.SettleCurrency,
                    Notional = Math.Abs(t.SettleAmount),
                    IsPosition = t.IsPosition
                }.Build();
                }
            );

            var perturbations = new List<Data.Perturbation>();

            foreach(var p in scenarioReport.Perturbations)
            {

                var pert = new Data.Perturbation.Builder
                {
                    Description = p.Description,
                    Name = p.Name,
                    PerturbationType = p.PerturbationType,
                    TradeValue = p.TradeValue,

                };

                if (p is QuotePerturbation qp)
                {
                    pert.TradeDeltaBase = qp.TradeDeltaBase;
                    pert.TradeGammaBase= qp.TradeGammaBase;
                    pert.VegaByTrade = qp.VegaBaseByTrade;
                    pert.BumpType = qp.BumpType.ToString();
                    pert.QuoteName = qp.QuoteName;
                    pert.QuoteValue = qp.QuoteValue;
                }

                if(p is GenericVolatilityPerturbation gvp)
                {
                    pert.VegaByTrade = gvp.VegaBaseByTrade;
                    pert.BumpType = gvp.BumpType.ToString();
                    pert.QuoteName = gvp.QuoteName;
                }

                if (p is InterestPerturbation ip)
                {
                    pert.TradeBaseResults = ip.TradeValue;
                    pert.BumpType = ip.BumpType.ToString();
                    pert.QuoteName = ip.QuoteName;
                    pert.CurveCurrency = ip.CurveCurrency;
                    pert.CurveDescription = ip.CurveDescription;
                    pert.CurveId = ip.CurveId;
                }

                if (p is CreditPerturbation crp)
                {
                    pert.TradeBaseResults = crp.TradeValue;
                    pert.BumpType = crp.BumpType.ToString();
                    pert.QuoteName = crp.QuoteName;
                }

                if (p is VolatilityCommodityPerturbation vcp) 
                {
                    pert.TradeBaseResults = vcp.TradeValue;
                    pert.VegaByTrade = vcp.VegaBaseByTrade;
                    pert.BumpType = vcp.BumpType.ToString();
                    pert.QuoteName = vcp.QuoteName;
                }

                if (p is StressPerturbation sp) 
                {
                    // this perturbation includes the stress scenarios.
                    pert.TradeBaseResults = sp.TradeBaseResults;
                    pert.TradeDeltaBase = sp.TradeValue;
                    pert.BumpType = sp.Shift.BumpType.ToString();
                    pert.ShiftValue = sp.Shift.FirstBump;
                }

                if (p is BasePerturbation bp) 
                {
                    pert.TradeBaseResults = bp.TradeValue;
                }

                if (p is CommodityPerturbation cp) 
                {
                    pert.TradeBaseResults = cp.TradeValue;
                    pert.BumpType = cp.BumpType.ToString();
                    pert.QuoteName = cp.QuoteName;
                }

                perturbations.Add(pert.Build());
            };

            // QuoteName
            // Orchestrade.Common.Risk.Perturbation, and then results are stored by trade id

            /*
             * This will have commodity, commodity vol, commodity ladder, commodity vol ladder
             */

            // keys in bond spreads are product ids. Get the country of risk for product id and then group by the countries 

            var bondSpreads = scenarioReport.Perturbations.First().BondZspreads
                .Select(b=>new Tuple<long, double>(b.Key.Id, b.Value.Mid))
                .ToDictionary(x=>x.Item1, y=>y.Item2);

            var spreadByProduct = bondSpreads.Select(kvp =>
            {
                var product = products.First(p => p.ProductId == kvp.Key);
                return new KeyValuePair<string, double>(product.Name, kvp.Value);
            }).ToDictionary(x => x.Key, y => y.Value);

            // country of risk, z-spread, trade notional 

            var tmpList = new List<Tuple<string, double, double>>();

            foreach(var bondSpread in bondSpreads)
            {
                var product = products.First(p => p.ProductId == bondSpread.Key);
                var tradesForProduct = trades.Where(t => t.Product.ProductId == product.ProductId).ToList();
                var z_spread = bondSpread.Value;
                var countryOfRisk = product.CountryOfRisk;
                foreach (var tradeForProduct in tradesForProduct)
                {
                    var ntnl = tradeForProduct.Notional;
                    var tmpTuple = new Tuple<string, double, double>(countryOfRisk, z_spread, ntnl);
                    tmpList.Add(tmpTuple);
                }
            }

            var countriesOfRisk = tmpList.Select(x => x.Item1).Distinct();

            var aggSpreadCountryOfRisk = countriesOfRisk.Select(c =>
            {
                // need to weight spread by notional to get weighted average spread
                var spreads = tmpList.Where(y => string.Equals(y.Item1, c)).ToList();

                if (spreads.Count() == 1)
                    return new KeyValuePair<string, double>(c, spreads.First().Item2);

                var sumNtnl = spreads.Select(s => s.Item3).Sum();
                var tmp = spreads.Select(s => s.Item2 * s.Item3).Sum();

                return new KeyValuePair<string, double>(c, tmp/sumNtnl);
            }).ToDictionary(x => x.Key, y => y.Value);

            var consoRiskReportResult = new ConsoRiskReportResult.Builder
            {
                ValueDate = valueDate,
                BondZSpreads = bondSpreads,
                Perturbations = perturbations,
                Trades = trades.ToList(),
                Products = products.ToList(),
                ZSpreadsByCountry = aggSpreadCountryOfRisk
            }.Build();

            var outputName = perturbations.Select(x => x.Name).Distinct();

            return consoRiskReportResult;

        }

        public ComplianceLimitCollection RunComplianceLimits(DateTime valueDate, DateTime loadDate, string complianceLimitName= "Country Of Risk - GCF1 - Net", string setupName = "Official")
        {
            // CountryOfRisk - Net
            var complianceLimit = Env.Current.Admin.GetComplianceLimitByName(complianceLimitName);
            var pricingSetup = Env.Current.MarketData.GetPricingSetup(setupName);
            var market = new Market { Setup = pricingSetup, Time = valueDate };
            var exceptionList = new List<Exception>();

            complianceLimit.Compute(market, loadDate, valueDate, exceptionList);

            var output = new Dictionary<string, double>();
            var complianceLimitCollection = new ComplianceLimitCollection();

            foreach (var result in complianceLimit.LimitItems)
            {
                var countryOfRisk = result.Properties.Values.First();
                var tradeIds = result.Trades.Select(t => (int)t.Value.Id).ToList();
                var complianceLimitEntry = new ComplianceLimit(countryOfRisk, result.Description, result.Output, result.RuleLimit, result.Remaining, tradeIds);
                complianceLimitCollection.Add(complianceLimitEntry);
            }

            return complianceLimitCollection;
        }

        public List<ICurve> GetCurves(List<KeyValuePair<DateTime, int>> curveIds, string configName, string filterName, string analysisConfigName, string setupName = "Official")
        {

            // The code will be the same as above, the inputs and data object will be different
            //var marketData = Env.Current.MarketData.GetCurves("interest", 11051, DateTime.Today, DateTime.Today); use this instead of the codes below
            var analysisSet = Env.Current.Admin.GetAnalysisSet(configName);
            // One of the anlaysis set values contains data for conso risk too
            var analysisItems = analysisSet.Items.FirstOrDefault(i => i.AnalysisConfigName.Equals(analysisConfigName)
            && i.FilterName.Equals(filterName));
            var analysis = AnalysisSetItem.AnalysisFromName(analysisItems.AnalysisName);
            var config = Env.Current.Admin.GetAnalysisConfig(analysisItems.AnalysisConfigName, analysisItems.AnalysisName);
            config.SetProperty("UseRealTime", "false");

            var filter = Env.Current.Trade.GetFilter(analysisItems.FilterName);
            var byPosition = true;
            var split = false;

            var riskProgress = new RiskProgress();
            var pricingSetup = Env.Current.MarketData.GetPricingSetup(setupName);

            var output = new List<ICurve>();

            foreach (var curveId in curveIds)
            {
                // 11051 - curve id
                var valueDate = curveId.Key;
                var market = new Market { Setup = pricingSetup, Time = valueDate };
                var report = analysis.Compute(filter, valueDate, valueDate, market, config, riskProgress, byPosition, split);

                var otCurve = report.Market.GetCurve(curveId.Value);
                var builderOutput = otCurve.BuilderOutput as StandardBuilderOutput;
                var df = builderOutput.DF;
                var X = df.Select(kvp => kvp.Key / 360.0).ToArray();
                var Y = df.Select(kvp => kvp.Value).ToArray();
                var curve = new Data.Curve(curveId.Value.ToString(), DayCountConvention.Act360, "USD", valueDate, X, Y); // should really avoid any hardcoding for future proofing
                output.Add(curve);
            }

            // log df's then interpolate, then calculate the weekly fwd rates in increments of 7 days upto about 10 years

            return output;
        }

        /// <summary>Closes the orchestrade connection.</summary>
        public void CloseConnection()
        {
            PubSubSessionFactory.CleanUp();
        }

        #endregion

        #region Methods

        /// <summary>Buils GemcorpAnalytics product</summary>
        /// <param name="product">The product.</param>
        /// <returns>The product.</returns>
        private Product BuildProduct(OrchestradeCommon.Products.Product product)
        {
            var countryOfRisk = product.GetProperty("CountryOfRisk");
            var optionType = string.Empty;

            if (product.AccountingType == "ListedOption")
            {
                var prod = product as OrchestradeCommon.Products.ListedOption;
                optionType = prod.OptionType.ToString();
            }

            return new Product
          .Builder
            {
                ProductId = (int)product.Id,
                ProductType = product.AccountingType,
                Name = product.UnderlierInfo,
                AssetClass = product.UnderlyingAsset == null ? product.PricingType : product.UnderlyingAsset.ToString(),
                Description = product.Description,
                OptionType = optionType,
                Currency = product.Currency,
                CountryOfRisk = countryOfRisk
            }.Build();
        }

        private List<PlEntry> CreatePlEntryObjects(Set<PLEntry> plEntryData, IList<Party> partyData)
        {
            var output = new List<PlEntry>();

            foreach(var data in plEntryData)
            {
                // need to find strategy and verify entity
                // mis labelling of variable book, this should be renamed to trade
                var book = data.Trade;

                // this is very performance expensive, make a single call
                var parentId = Env.Current.StaticData.GetPartiesByIds(new[] { book.BookId });

                var party = partyData.FirstOrDefault(p => string.Equals(p.Name, data.Book));

                var bookStrategy = string.Empty;
                var parentFundFound = parentId.First().Properties.TryGetValue("Parent Fund", out var parentFund);
                var bookAssetClass = string.Empty;
                var entity = data.Entity;

                if (!book.Properties.TryGetValue("CountryOfRisk", out var countryOfRisk))
                    countryOfRisk = string.Empty;

                if (party != null)
                {
                    var strategy = party.Properties.TryGetValue("Strategy", out bookStrategy);
                    var fund = party.Properties.TryGetValue("Parent Fund", out parentFund);
                    entity = party.Parent.Name;
                    // this can at times be empty
                    var assetClass = party.Properties.TryGetValue("AssetClass", out bookAssetClass);
                }

                //if (string.IsNullOrEmpty(parentFund))
                //{
                //    Console.WriteLine("");
                //}

                
                // strategy is in Party object
                var plEntry = new PlEntry.Builder {
                    CountryOfRisk = countryOfRisk,
                    Description = data.Description,
                    UnderlierInfo = data.UnderlierInfo,
                    Book = data.Book,
                    Currency = data.Currency,
                    ProductId = (int)data.ProductId,
                    TradeId = (int)data.TradeId,
                    AssetClass = bookAssetClass,
                    Entity = entity,
                    ParentFund = parentFund,
                    Strategy = bookStrategy,
                    TradeDate = data.StartDate,
                    PlInception = data.PlInceptionBase,
                    PlDaily = data.PlBaseComputed,
                    PlMonth = data.PlMonthBaseComputed,
                    PlYtd = data.PlYearBaseComputed,
                    PlWeek = data.PlWeekBaseComputed,
                    StartFxPrice = data.StartFXPrice,
                    StartQuote = data.StartQuote,
                    EndFxPrice = data.EndFXPrice,
                    EndQuote = data.EndQuote,
                    StartNominal = data.StartNominal,
                    StartPv = data.StartPV,
                    EndNominal = data.EndNominal,
                    EndPv = data.EndPV
                }.Build();
                output.Add((PlEntry)plEntry);
            }

            return output;
        }

        #endregion
    }
}
