﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Timers;
using OrchestradeClient;
using OrchestradeCommon.Contracts;
using OrchestradeCommon.MarketData;
using OrchestradeCommon.MarketData.Streaming;
using OrchestradeCommon.Messaging;
using OrchestradeCommon.Pricing;
using OrchestradeCommon.Products;
using OrchestradeCommon.Risk;
using OrchestradeCommon.Trading;
using OrchestradeCommon.Util;

namespace GemcorpAnalytics.OrchestradeLayer
{
    public class CustomRealTimeBlotter : IDisposable
    {
        private static readonly OrchestradeCommon.Util.Log Logger = OrchestradeCommon.Util.Log.GetLogger(nameof(CustomRealTimeBlotter));
        private bool _rtMarketData = true;
        private readonly Set<PLEntry> _pendingEntries = new Set<PLEntry>();
        private readonly Set<PLEntry> _toRemove = new Set<PLEntry>();
        private readonly ConcurrentDictionary<string, IMarketData> _pendingMarketDatas = new ConcurrentDictionary<string, IMarketData>();
        private readonly ConcurrentSet<Quote> _pendingQuotes = new ConcurrentSet<Quote>();
        private readonly System.Timers.Timer _updatePositionTimer = new System.Timers.Timer();
        private readonly int timerRefreshSeconds = 5;
        private readonly object _computeLock = new object();
        private readonly PlBlotter _blotter;
        private readonly PLConfig _plConfig;
        private readonly Filter _filter;
        private readonly bool _computeEffects;
        private IMarketDataStreamingService _realTimeMarketService;
        private int _isUpdatingMarketSheet;
        private Market _market;
        private PricingSetup _pricingSetup;
        private IMarketDataConsumer _marketSheetListener;
        private PlGreekPerturbation _plGreek;
        private ScenarioReport _plGreekOutput;
        private readonly IPlPlugin _plugIn;
        private IPubSubSession _sessionTrade;
        private IPubSubSession _sessionPos;
        private IList<TradeGroup> _tradeGroups;
        private readonly bool _isLoading;
        private ComputeCompletedCallBack _onComputedCompleted;

        public Set<PLEntry> Entries
        {
            get
            {
                return this._blotter.Entries;
            }
        }

        public CustomRealTimeBlotter(string configName, DateTime time, IList<Exception> exceptions, string filterName, bool rtMarketData, ComputeCompletedCallBack OnComputeCompleted, string analysisConfigName, int timerRefresh = 5)
        {
            this._onComputedCompleted = OnComputeCompleted;

            AnalysisSet analysisSet = Env.Current.Admin.GetAnalysisSet(configName);

            if (analysisSet == null)
                return;
            //AnalysisSetItem analysisSetItem = analysisSet.Items.FirstOrDefault<AnalysisSetItem>((Func<AnalysisSetItem, bool>)(item => item.AnalysisName.Equals("PLAnalysis")));
            AnalysisSetItem analysisSetItem =
                analysisSet.Items.Where(x => x.AnalysisConfigName == analysisConfigName).FirstOrDefault();
            if (analysisSetItem == null)
                return;
            Filter filter1 = Env.Current.Trade.GetFilter(analysisSetItem.FilterName ?? analysisSet.FilterName);
            if (filterName != null)
                filter1 = Env.Current.Trade.GetFilter(filterName);
            Market market = analysisSetItem.GetMarket(analysisSet.PricingSetupName, time);
            this._market = market;
            this._rtMarketData = rtMarketData;
            Analysis analysis = AnalysisSetItem.AnalysisFromName(analysisSetItem.AnalysisName);
            PLConfig config = (PLConfig)(Env.Current.Admin.GetAnalysisConfig(analysisSetItem.AnalysisConfigName,
                analysisSetItem.AnalysisName));
            //?? analysis.Config);
            this._plConfig = config;
            DateTime startTime = PLAnalysis.PreviousTime(config, market.Date(time));
            this._filter = filter1;
            DateTime startUndoTime = startTime;
            DateTime endUndoTime = time;
            if (!config.UseUndo)
            {
                startUndoTime = DateTime.MinValue;
                endUndoTime = DateTime.MinValue;
            }

            this._plugIn = PlPluginHelper.Find();

            this._blotter = new PlBlotter(filter1, market, startTime, time, startUndoTime, endUndoTime, PLAnalysis.ComputeYtd(config), PLAnalysis.ComputeMtd(config), PLAnalysis.ComputeWtd(config), PLAnalysis.ComputeQtd(config))
            {
                ValueFxCash = PLAnalysis.ValueFxCash(config),
                AggregateEntries = config.AggregateEntries,
                IntradayPl = false,
                PlByTrade = config.PlByTrade,
                ComputeNav = PLAnalysis.ComputeNav(config),
                ComputeYearToDate = PLAnalysis.ComputeYtd(config),
                ComputeQuarterToDate = PLAnalysis.ComputeQtd(config),
                ComputeMonthToDate = PLAnalysis.ComputeMtd(config),
                ComputeWeekToDate = PLAnalysis.ComputeWtd(config),
                ComputeInceptionToDate = PLAnalysis.ComputeItd(config),
                InceptionToDate = PLAnalysis.InceptionToDate(config),

                UseTradingDay = config.UseTradingDay,
                ByPosition = true,
                ComputeOtcPrices = config.ComputeOtcPrices,
                UseTasks = config.CanUseTask

            };
            this._computeEffects = config.ComputeEffects;
            this._blotter.ValueFxCash = PLAnalysis.ValueFxCash(config);
            this._blotter.ComputeNav = PLAnalysis.ComputeNav(config);
            this._blotter.UseSavedPrices = config.UseSavedPrices;
            this._blotter.PlFromSavedNav = config.PlFromSavedNav;
            this._blotter.UseEndSavedPrices = config.UseEndSavedPrices;
            this._blotter.CombineExternalPrices = config.CombineExternalMeasures;
            this._blotter.UseLiquidationRealized = config.UseLiquidationForRealized;
            this._blotter.UseCurrencyPair = config.UseCurrencyPair;
            this._blotter.FundAdminMode = config.FundAdminMode;
            this._blotter.FxSweepOnTradeDate = config.FxSweepOnTradeDate;
            this._blotter.AggregateEntries = config.AggregateEntries;
            this._blotter.IntradayPl = false;
            this._blotter.PlByTrade = config.PlByTrade;
            this._blotter.PreComputeMode = config.PreComputeMode;
            this._blotter.FundingPl = config.FundingPl;
            this._blotter.PlByLegCurrency = config.PlByLegCurrency;
            this._blotter.SeggregateClosedPositions = config.SeggregateClosedPositions;
            this._blotter.IntradayPl = true;

            ProgressRisk progressRisk = new ProgressRisk()
            {
                Exceptions = exceptions
            };
            this.timerRefreshSeconds = timerRefresh;
            this._updatePositionTimer.Elapsed += new ElapsedEventHandler(this.TimerEventProcessor);
            ((ClientSession)Env.Current).InitializeRealTime();
            //this.UpdateTimer();
            this._isLoading = true;
            double timeToLoadPos;
            double timeToLoadTrades;
            try
            {
                this._blotter.Load(time, out timeToLoadPos, out timeToLoadTrades, (IAnalysisProgress)progressRisk);
            }
            catch (Exception e)
            {
                //TODO
            }

            this._blotter.InitForPricing(true, exceptions, time, false);
            this._isLoading = false;
            Filter filter2 = (Filter)filter1.Clone();
            filter2.Remove(FilterElementType.OpenQuantity);
            this._tradeGroups = Env.Current.Trade.GetTradeGroups(filter2, this._blotter.ValuationDate);
            List<string> stringList = new List<string>();
            foreach (TradeGroup tradeGroup in (IEnumerable<TradeGroup>)this._tradeGroups)
            {
                if (!stringList.Contains(tradeGroup.GroupingType))
                    stringList.Add(tradeGroup.GroupingType);
            }
            MarketDataUsageSet set = new MarketDataUsageSet();
            if (!this.IsRealTimeOn)
                this._blotter.AllMarketData(set, true, exceptions);
            this._blotter.AllMarketData(new MarketDataUsageSet(), false, exceptions);
        }
        private void TimerEventProcessor(object myObject, EventArgs myEventArgs)
        {
            try
            {
                if (this._updatePositionTimer == null)
                    return;
                this.TimerEventProcessor2(myObject, myEventArgs);
            }
            catch (Exception ex)
            {
                CustomRealTimeBlotter.Logger.Error((object)"Timer", ex);
            }
        }

        public void DoCompute(bool computeEndOnly = false)
        {
            if (this.Market == null || this._blotter == null)
                return;
            this._updatePositionTimer.Stop();
            lock (this._pendingEntries)
            {
                try
                {
                    this.AddMarketDataUpdates();
                }
                finally
                {
                    this._pendingEntries.Clear();
                }
            }
            try
            {
                this.RunCompute(this._plConfig.Name, (IList<Exception>)new List<Exception>(), true, computeEndOnly);
            }
            catch (Exception ex)
            {
            }
        }

        private void RunCompute(string riskConfig, IList<Exception> exceptions = null, bool verbose = true, bool computeEndOnly = false)
        {
            try
            {
                this._blotter.UseTasks = this._plConfig.CanUseTask;
                this._blotter.ComputeOtcPrices = this._plConfig.ComputeOtcPrices;
                bool canUseTask = this._plConfig.CanUseTask;
                DateTime now1 = DateTime.Now;
                IList<Exception> exceptions1 = exceptions ?? (IList<Exception>)new List<Exception>();
                lock (this._computeLock)
                {
                    ExceptionProgress exceptionProgress = new ExceptionProgress()
                    {
                        Exceptions = exceptions1
                    };
                    this._blotter.Compute(this._plConfig.ComputeEffects, (IAnalysisProgress)exceptionProgress, exceptions1, computeEndOnly);


                    DateTime now2 = DateTime.Now;
                    CustomRealTimeBlotter.Logger.Info((object)("*** Time to Compute PL  " + (object)(now2 - now1).TotalMilliseconds + " Used Memory " + (object)(GC.GetTotalMemory(false) / 1048576L) + " / " + (object)(Process.GetCurrentProcess().WorkingSet64 / 1048576L)));
                    try
                    {
                        this.RunRisk(riskConfig, exceptions1, canUseTask, (Set<PLEntry>)null, (IAnalysisProgress)null);
                    }
                    catch (Exception ex)
                    {
                        exceptions1.Add(ex);
                    }
                    try
                    {
                        if (this._plGreek != null)
                        {
                            DateTime now3 = DateTime.Now;
                            this._plGreek.UpdateQuotes(this._blotter.Market, this._blotter.ValuationTime, (IAnalysisProgress)exceptionProgress);
                            CustomRealTimeBlotter.Logger.Info((object)("Pl Greek Compute done in " + (object)(DateTime.Now - now3).TotalMilliseconds));
                        }
                        else
                            this.RunPlGreekRisk(riskConfig, exceptions1, canUseTask);
                    }
                    catch (Exception ex)
                    {
                        exceptions1.Add(ex);
                    }
                    if (this._plConfig.FxExposure)
                    {
                        try
                        {
                            PLAnalysis.ComputeFxExposure(this._blotter.Entries.ToList(), (PLReport)null, this._blotter.ValuationTime, this._blotter.Market, (IAnalysisProgress)exceptionProgress, canUseTask);
                        }
                        catch (Exception ex)
                        {
                            exceptions1.Add(ex);
                        }
                    }
                    if (this._plugIn == null)
                        return;
                    try
                    {
                        IList<string> names = this._plugIn.Names;
                        this._plugIn.Compute((IEnumerable<PLEntry>)this._blotter.Entries, this._blotter.Market, this._blotter, names, exceptions);
                        foreach (PLEntry entry in this._blotter.Entries)
                        {
                            entry.CustomValues = (IDictionary<string, double>)new Dictionary<string, double>();
                            entry.CustomLabels = (IDictionary<string, string>)new Dictionary<string, string>();
                            foreach (string index in (IEnumerable<string>)names)
                            {
                                if (this._plugIn.IsNumeric(index))
                                {
                                    double num = this._plugIn.Value(index, this._blotter.Market, entry, this._blotter);
                                    entry.CustomValues[index] = num;
                                }
                                else
                                {
                                    string str = this._plugIn.Label(index, this._blotter.Market, entry, this._blotter);
                                    entry.CustomLabels[index] = str;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        exceptions.Add(ex);
                    }
                }
            }
            finally
            {
                if (this.IsRealTimeMarket())
                {
                    this._updatePositionTimer.Enabled = true;
                    this._updatePositionTimer.Start();
                }
            }
        }

        private ScenarioConfig GetPlGreekConfig(string configName)
        {
            ScenarioConfig scenarioConfig = string.IsNullOrEmpty(configName) ? (ScenarioConfig)null : (ScenarioConfig)Env.Current.Admin.GetAnalysisConfig(configName, "ScenarioAnalysis");
            if (scenarioConfig == null)
                return (ScenarioConfig)null;
            if (scenarioConfig.Items.OfType<PlGreekScenarioItem>().FirstOrDefault<PlGreekScenarioItem>() != null)
                return scenarioConfig;
            return (ScenarioConfig)null;
        }

        private ScenarioConfig GetRiskScenarioConfig(string configName, bool useTasks, IList<Exception> exceptions, IAnalysisProgress progress = null)
        {
            ScenarioConfig scenarioConfig1 = string.IsNullOrEmpty(configName) ? (ScenarioConfig)null : (ScenarioConfig)Env.Current.Admin.GetAnalysisConfig(configName, "ScenarioAnalysis");
            BaseScenarioItem baseScenarioItem1 = scenarioConfig1 == null ? (BaseScenarioItem)null : scenarioConfig1.Items.OfType<BaseScenarioItem>().FirstOrDefault<BaseScenarioItem>();
            if (baseScenarioItem1 == null)
            {
                BaseScenarioItem baseScenarioItem2 = new BaseScenarioItem();
                baseScenarioItem2.Name = "Default";
                baseScenarioItem2.MeasureNames = (IList<string>)new List<string>();
                baseScenarioItem2.IsDeltaOneSided = false;
                baseScenarioItem2.ShowBaseFxDeltaForAllTrades = false;
                baseScenarioItem1 = baseScenarioItem2;
            }
            BaseScenarioItem baseScenarioItem3 = (BaseScenarioItem)baseScenarioItem1.Clone();
            baseScenarioItem3.MeasureNames = baseScenarioItem3.MeasureNames == null ? (IList<string>)new List<string>() : (IList<string>)new List<string>((IEnumerable<string>)baseScenarioItem3.MeasureNames);
            baseScenarioItem3.MeasureNames = (IList<string>)new List<string>((IEnumerable<string>)Measure.RiskMeasures);
            if (baseScenarioItem3.MeasureNames == null || baseScenarioItem3.MeasureNames.Count == 0)
                return (ScenarioConfig)null;
            List<string> stringList = new List<string>();
            if (baseScenarioItem3.MeasureNames.Contains("PV01"))
                stringList.Add("Warning : PV01 is slowing the PL BLotter consider using IrDelta instead");
            if (baseScenarioItem3.MeasureNames.Contains("Credit01"))
                stringList.Add("Warning : Credit01 is slowing the PL BLotter consider using CrDelta instead");
            if (baseScenarioItem3.MeasureNames.Contains("Fixed DV01"))
                stringList.Add("Warning : Fixed DV01 is slowing the PL BLotter consider using IrFixedDV01 instead");
            if (baseScenarioItem3.MeasureNames.Contains("ModDuration"))
                stringList.Add("Warning : ModDuration is slowing the PL BLotter consider using IrModDuration instead");
            if (stringList.Count > 0)
            {
                foreach (string message in stringList)
                {
                    if (progress != null)
                        progress.AnalysisException(new Exception(message), "Run Risk " + message);
                    else if (exceptions != null)
                        exceptions.Add(new Exception(message));
                    else
                        CustomRealTimeBlotter.Logger.Warn((object)("RunRisk " + message));
                }
            }
            ScenarioConfig scenarioConfig2 = scenarioConfig1 == null ? new ScenarioConfig() : (ScenarioConfig)scenarioConfig1.Clone();
            scenarioConfig2.Items = (IList<ScenarioItem>)new List<ScenarioItem>()
      {
        (ScenarioItem) baseScenarioItem3
      };
            scenarioConfig2.UseTasks = useTasks;
            scenarioConfig2.CompressTrades = false;
            return scenarioConfig2;
        }

        private void RunPlGreekRisk(string configName, IList<Exception> exceptions, bool useTasks)
        {
            DateTime now1 = DateTime.Now;
            this._plGreek = (PlGreekPerturbation)null;
            this._plGreekOutput = (ScenarioReport)null;
            Market market = this._blotter.Market;
            DateTime valuationTime = this._blotter.ValuationTime;
            ExceptionProgress exceptionProgress = new ExceptionProgress()
            {
                Exceptions = exceptions ?? (IList<Exception>)new List<Exception>()
            };
            ScenarioReport plGreek = PLAnalysis.ComputePlGreek(this.GetPlGreekConfig(configName), this._blotter.Entries.ToList(), valuationTime, this._blotter.StartTime, market, (IAnalysisProgress)exceptionProgress, useTasks);
            if (plGreek != null)
            {
                this._plGreek = plGreek.Perturbations.FirstOrDefault<Perturbation>() as PlGreekPerturbation;
                this._plGreekOutput = plGreek;
            }
            DateTime now2 = DateTime.Now;
            CustomRealTimeBlotter.Logger.Info((object)("*** Time to Compute Pl Greeks " + (object)(now2 - now1).TotalMilliseconds));
        }

        private void TimerEventProcessor2(object myObject, EventArgs myEventArgs)
        {
            try
            {
                DateTime now1 = DateTime.Now;
                Set<PLEntry> entries = new Set<PLEntry>();
                List<Exception> exceptionList = new List<Exception>();
                bool flag1 = false;
                bool flag2 = false;
                lock (this._pendingEntries)
                {
                    Set<long> set = new Set<long>();
                    if (this._blotter != null && this._blotter.ComputeInceptionToDate)
                    {
                        foreach (PLEntry pendingEntry in this._pendingEntries)
                        {
                            if (pendingEntry.NeedInception && !set.Contains(pendingEntry.BookId))
                                set.Add(pendingEntry.BookId);
                        }
                        foreach (long bookId in set)
                            this.AddInceptionToDateEntries(bookId);
                    }
                    this.AddMarketDataUpdates();
                    foreach (PLEntry pendingEntry in this._pendingEntries)
                    {
                        if (!pendingEntry.IsRemoved)
                            entries.Add(pendingEntry);
                        else
                            this._toRemove.Add(pendingEntry);
                    }
                    this._pendingEntries.Clear();
                    foreach (PLEntry t in this._toRemove)
                    {
                        flag2 = true;
                        this._blotter.Entries.Remove(t);
                    }
                    this._toRemove.Clear();
                }
                if (entries.Count != 0)
                {
                    flag1 = true;
                    lock (this._computeLock)
                    {
                        if (this._blotter.ComputeEffects || this._blotter.MinimalEffects)
                            this._blotter.CleanBaseMarketForEffects();
                        this._blotter.Market = this.Market;
                        ExceptionProgress exceptionProgress = new ExceptionProgress()
                        {
                            Exceptions = (IList<Exception>)exceptionList
                        };
                        foreach (PLEntry plEntry in entries)
                            plEntry.PlBasePrevious = plEntry.PlBaseComputed;
                        this._blotter.Compute(entries, this._blotter.ComputeEffects, (IAnalysisProgress)exceptionProgress, (IList<Exception>)exceptionList, true, false);
                        this.RunIncrementalRisk(this._plConfig.GreekConfig, entries, this._plConfig.FxExposure, (IAnalysisProgress)exceptionProgress);
                        if (this._plugIn != null)
                        {
                            try
                            {
                                IList<string> names = this._plugIn.Names;
                                this._plugIn.Compute((IEnumerable<PLEntry>)this._blotter.Entries, this._blotter.Market, this._blotter, names, (IList<Exception>)exceptionList);
                                foreach (PLEntry entry in this._blotter.Entries)
                                {
                                    entry.CustomValues = (IDictionary<string, double>)new Dictionary<string, double>();
                                    entry.CustomLabels = (IDictionary<string, string>)new Dictionary<string, string>();
                                    foreach (string index in (IEnumerable<string>)names)
                                    {
                                        if (this._plugIn.IsNumeric(index))
                                        {
                                            double num = this._plugIn.Value(index, this._blotter.Market, entry, this._blotter);
                                            entry.CustomValues[index] = num;
                                        }
                                        else
                                        {
                                            string str = this._plugIn.Label(index, this._blotter.Market, entry, this._blotter);
                                            entry.CustomLabels[index] = str;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                exceptionList.Add(ex);
                            }
                        }
                        if (this._plGreek != null)
                        {
                            DateTime now2 = DateTime.Now;
                            this._plGreek.UpdateQuotes(this._blotter.Market, this._blotter.ValuationTime, (IAnalysisProgress)exceptionProgress);
                            CustomRealTimeBlotter.Logger.Info((object)("Pl Greek Update done in " + (object)(DateTime.Now - now2).TotalMilliseconds));
                        }
                    }
                }
                DateTime now3 = DateTime.Now;
                if (flag1 || flag2)
                {
                    CustomRealTimeBlotter.Logger.Info((object)("***** PLBlotter Timer " + (object)entries.Count + " Entries in " + (object)(now3 - now1).TotalMilliseconds + " ms"));
                    this._onComputedCompleted(this.Entries);
                }
                else
                    CustomRealTimeBlotter.Logger.Info((object)"***** PLBlotter Timer nothing to compute");
            }
            catch (Exception ex)
            {
                CustomRealTimeBlotter.Logger.Error((object)"Timer", ex);
            }
        }

        private void RunIncrementalRisk(string configName, Set<PLEntry> entries, bool computeFxExposure, IAnalysisProgress progress)
        {
            List<Exception> exceptionList = new List<Exception>();
            this.RunRisk(configName, (IList<Exception>)exceptionList, this._plConfig.CanUseTask, entries, progress);
            if (!computeFxExposure)
                return;
            IAnalysisProgress analysisProgress = progress;
            if (analysisProgress == null)
                analysisProgress = (IAnalysisProgress)new ExceptionProgress()
                {
                    Exceptions = (IList<Exception>)exceptionList
                };
            progress = analysisProgress;
            PLAnalysis.ComputeFxExposure(entries == null ? this._blotter.Entries.ToList() : entries.ToList(), (PLReport)null, this._blotter.ValuationTime, this._blotter.Market, progress, this._plConfig.CanUseTask);
        }

        private void RunRisk(string configName, IList<Exception> exceptions, bool useTasks, Set<PLEntry> entries = null, IAnalysisProgress progress = null)
        {
            try
            {
                DateTime now1 = DateTime.Now;
                ScenarioConfig riskScenarioConfig = this.GetRiskScenarioConfig(configName, this._plConfig.CanUseTask, exceptions, progress);
                if (riskScenarioConfig == null)
                    return;
                BaseScenarioItem baseItem = riskScenarioConfig.Items.OfType<BaseScenarioItem>().FirstOrDefault<BaseScenarioItem>();
                entries = entries ?? this._blotter.Entries;
                Market market = (Market)this._blotter.Market.Clone();
                market.Setup = (PricingSetup)market.Setup.Clone();
                IAnalysisProgress analysisProgress = progress;
                if (analysisProgress == null)
                    analysisProgress = (IAnalysisProgress)new ExceptionProgress()
                    {
                        Exceptions = (exceptions ?? (IList<Exception>)new List<Exception>())
                    };
                progress = analysisProgress;
                PLAnalysis.ComputeRisk(baseItem, riskScenarioConfig, (PLReport)null, entries.ToList(), this._blotter.ValuationTime, market, progress, this._blotter.GetSavedMeasures(this._blotter.ValuationTime, market));
                DateTime now2 = DateTime.Now;
                CustomRealTimeBlotter.Logger.Info((object)("*** Time to Compute Risk " + (object)(now2 - now1).TotalMilliseconds));
            }
            catch (Exception ex)
            {
                if (progress != null)
                    progress.AnalysisException(ex, "Run Risk " + ex.Message);
                else if (exceptions != null)
                    exceptions.Add(ex);
                else
                    CustomRealTimeBlotter.Logger.Error((object)nameof(RunRisk), ex);
            }
        }

        private void UpdateTimer()
        {
            if (this._updatePositionTimer != null)
                this._updatePositionTimer.Stop();
            lock (this._pendingEntries)
            {
                if (!this._isLoading)
                    this._pendingEntries.Clear();
            }
            this._pendingMarketDatas.Clear();
            this._pendingQuotes.Clear();
            if (this._sessionTrade == null)
            {
                try
                {
                    this._sessionTrade = PubSubSessionFactory.Current.Topic("Trade", new PubSubMessageConsumer(this.OnNewMsg), false);
                    this._sessionPos = PubSubSessionFactory.Current.Topic("Position", new PubSubMessageConsumer(this.OnNewMsg), false);
                }
                catch (Exception ex)
                {
                    CustomRealTimeBlotter.Logger.Error((object)"Can not start PubSub", ex);
                }
            }
            if (this.IsRealTimeMarket() && this._updatePositionTimer != null)
            {
                this._updatePositionTimer.Interval = (double)(1000 * this.timerRefreshSeconds);
                this._updatePositionTimer.Enabled = true;
                this._updatePositionTimer.Start();
                this._updatePositionTimer.Enabled = true;
            }
            this.UpdateMarketSheet();
        }

        private void AddInceptionToDateEntries(long bookId)
        {
            if (this._blotter == null || !this._blotter.ComputeInceptionToDate)
                return;
            Set<PLEntry> set = this._blotter.PlEntryForBookId(bookId);
            if (set == null)
                return;
            foreach (PLEntry t in set)
                this._pendingEntries.Add(t);
        }

        private void OnNewMsg(IPubSubMessage obj)
        {
            TradeMessage data1 = obj.Data as TradeMessage;
            if (data1 != null)
            {
                if (data1.PreviousBookId == 0L)
                    this.NewTradeMethod(data1.Trade);
                else if (data1.PreviousBookId == data1.Trade.BookId && data1.Trade.Product.Id == data1.PreviousProductId)
                {
                    if (data1.PreviousAggregableDescription != data1.Trade.Product.AggregableDescription.ToString())
                    {
                        Trade oldTrade = data1.GetOldTrade();
                        oldTrade.Status = "Canceled";
                        this.NewTradeMethod(oldTrade);
                        this.NewTradeMethod(data1.Trade);
                    }
                    else if (data1.PreviousFXBasedProduct is IFXBased && data1.Trade.Product is IFXBased)
                    {
                        IFXBased previousFxBasedProduct = data1.PreviousFXBasedProduct as IFXBased;
                        IFXBased product = data1.Trade.Product as IFXBased;
                        bool flag = false;
                        if (previousFxBasedProduct is FXSwap && product is FXSwap)
                        {
                            FXSwap fxSwap1 = previousFxBasedProduct as FXSwap;
                            FXSwap fxSwap2 = product as FXSwap;
                            if (fxSwap1.NearPrimaryDate != fxSwap2.NearPrimaryDate || fxSwap1.FarPrimaryDate != fxSwap2.FarPrimaryDate)
                                flag = true;
                        }
                        if (previousFxBasedProduct.Primary == product.Primary && previousFxBasedProduct.Quoting == product.Quoting && (data1.Trade.SettlementDate == data1.PreviousSettlementDate && !flag))
                        {
                            this.NewTradeMethod(data1.Trade);
                        }
                        else
                        {
                            Trade oldTrade = data1.GetOldTrade();
                            oldTrade.Status = "Canceled";
                            this.NewTradeMethod(oldTrade);
                            this.NewTradeMethod(data1.Trade);
                        }
                    }
                    else
                        this.NewTradeMethod(data1.Trade);
                }
                else
                {
                    Trade oldTrade = data1.GetOldTrade();
                    oldTrade.Status = "Canceled";
                    this.NewTradeMethod(oldTrade);
                    this.NewTradeMethod(data1.Trade);
                }
            }
            else
            {
                PositionMessage data2 = obj.Data as PositionMessage;
                if (data2 == null)
                    return;
                this.NewPositionMethod(data2.Position);
            }
        }

        private void NewTradeMethod(Trade trade)
        {
            try
            {
                if (this._blotter == null)
                    return;
                PLEntry entry = this._blotter.FindEntry(trade);
                if (entry != null && entry.Trade.Version > trade.Version || this._blotter.SeggregateClosedPositions & entry == null && ProductHelper.IsMatured(trade, this._blotter.StartDate + 1, this._blotter.StartTime))
                    return;
                bool flag = AccessHelper.CheckTradeAccess(Env.Current.Admin.GetUserByName(Env.Current.User), trade, false, false);
                lock (this._pendingEntries)
                {
                    if (!flag)
                        return;
                    IList<Exception> exceptions = (IList<Exception>)new List<Exception>();
                    this._blotter.ValuationTime = DateTime.Now;
                    List<Trade> trades = this._blotter.NewTradeByCurrency(trade);
                    foreach (var trade1 in trades)
                    {
                        PLEntry t1 = this._blotter.Entries.Get(new PLEntry((IPlBlotter)this._blotter)
                        {
                            IsTrade = true,
                            Position = (IPosition)trade
                        });
                        if (t1 != null && t1.Trade.Version > trade.Version)
                            return;
                        PLEntry t2 = this._blotter.NewTrade(trade, exceptions, this._pendingEntries);
                        if (t2 != null)
                            this._pendingEntries.Add(t2);
                        else if (t1 != null)
                            this._toRemove.Add(t1);
                        if (trade1 == null)
                            return;
                        PLEntry t3 = this._blotter.Entries.Get(new PLEntry((IPlBlotter)this._blotter)
                        {
                            IsTrade = true,
                            Position = (IPosition)trade1
                        });
                        PLEntry t4 = this._blotter.NewTrade(trade1, exceptions, this._pendingEntries);
                        if (t4 != null)
                        {
                            this._pendingEntries.Add(t4);
                        }
                        else
                        {
                            if (trade.Product.IsMultiplyTraded || t3 == null)
                                return;
                            this._toRemove.Add(t3);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CustomRealTimeBlotter.Logger.Error((object)("NewTrade " + ex.Message), ex);
            }
        }

        private void NewPositionMethod(Position pos)
        {
            try
            {
                if (this._blotter == null)
                    return;
                bool flag = AccessHelper.CheckPositionAccess(Env.Current.Admin.GetUserByName(Env.Current.User), pos, false, false);
                lock (this._pendingEntries)
                {
                    if (!flag)
                        return;
                    IList<Exception> exceptions = (IList<Exception>)new List<Exception>();
                    this._blotter.ValuationTime = DateTime.Now;
                    PLEntry t = this._blotter.NewPosition(pos, exceptions);
                    if (t == null)
                        return;
                    this._pendingEntries.Add(t);
                }
            }
            catch (Exception ex)
            {
                CustomRealTimeBlotter.Logger.Error((object)("NewPosition " + ex.Message), ex);
            }
        }

        public bool IsRealTimeOn
        {
            get
            {
                return this.IsRealTimeMarket();
            }
        }

        public PricingSetup PricingSetup
        {
            get
            {
                if (this._pricingSetup != null)
                    return this._pricingSetup;
                this._pricingSetup = Env.Current.MarketData.GetPricingSetup(this._market.Setup.ClosingName);
                return this._pricingSetup;
            }
        }

        public DateTime SelectedTime
        {
            get
            {
                string tenor = this._plConfig.Tenor;
                string calendar = this._plConfig.Calendar;
                if (tenor != null && tenor.Equals("Specific"))
                {
                    int num = this._plConfig.SpecificStartDate.IsNull ? 1 : 0;
                }
                string timeZone = this._plConfig.TimeZone;
                TimeZoneInfo info = timeZone == null ? (TimeZoneInfo)null : Utilities.FindTimeZone(timeZone);
                SimpleDate specificStartDate = this._plConfig.SpecificStartDate;
                int executionTime = this._plConfig.ExecutionTime;
                return specificStartDate.ToDateTime(executionTime / 10000, executionTime % 10000 / 100, executionTime % 100, info, 0);
            }
        }

        private void AddMarketDataUpdates()
        {
            if (this._pendingQuotes.Count == 0 && this._pendingMarketDatas.Count == 0 || this._blotter == null)
                return;
            Set<QuoteId> quotes = new Set<QuoteId>();
            Set<long> marketDatas = new Set<long>();
            Set<Quote> quoteObjs = new Set<Quote>();
            Set<long> tidsSet = new Set<long>();
            Set<IMarketData> marketDataObjs = new Set<IMarketData>();
            List<TradePricingData> listTradePricingData = new List<TradePricingData>();
            foreach (Quote pendingQuote in this._pendingQuotes)
            {
                quoteObjs.Add(pendingQuote);
                quotes.Add(new QuoteId(pendingQuote.CloseName, pendingQuote.Name, pendingQuote.QuoteDate));
            }
            this._pendingQuotes.Clear();
            foreach (IMarketData t in (IEnumerable<IMarketData>)this._pendingMarketDatas.Values)
            {
                marketDataObjs.Add(t);
                marketDatas.Add(t.Id);
            }
            this._pendingMarketDatas.Clear();
            Set<PLEntry> set = this._blotter.Notify(this.Market, quotes, quoteObjs, marketDatas, marketDataObjs, quoteObjs, tids: tidsSet, tradePricingDatas: listTradePricingData);
            if (set == null)
                return;
            foreach (PLEntry t in set)
                this._pendingEntries.Add(t);
        }

        public bool IsRealTimeMarket()
        {
            return this._rtMarketData;
        }

        private void UnsubscribeMarketSheet()
        {
            if (this._realTimeMarketService == null)
                return;
            CustomRealTimeBlotter.Logger.DebugFormat("Unsubscribing from MarketSheet {0}", this._realTimeMarketService.Config != null ? (object)this._realTimeMarketService.Config.Name : (object)string.Empty);
            if (this._marketSheetListener != null)
            {
                this._realTimeMarketService.Unsubscribe(this._marketSheetListener);
                this._marketSheetListener = (IMarketDataConsumer)null;
            }
            this._realTimeMarketService = (IMarketDataStreamingService)null;
        }

        private void SubscribeMarketSheet()
        {
            if (this._realTimeMarketService == null || this._marketSheetListener != null)
                return;
            this._marketSheetListener = (IMarketDataConsumer)new MarketDataConsumerSurrogate(new Action<IList<Quote>, List<IMarketData>>(this.InternalMarketUpdate));
            CustomRealTimeBlotter.Logger.DebugFormat("Subscribing to MarketSheet {0}", this._realTimeMarketService.Config != null ? (object)this._realTimeMarketService.Config.Name : (object)string.Empty);
            this._realTimeMarketService.Subscribe(this._marketSheetListener, true, false);
        }

        private void UpdateMarketSheet()
        {
            if (Interlocked.CompareExchange(ref this._isUpdatingMarketSheet, 1, 0) == 1)
                return;
            try
            {
                PricingSetup pricingSetup = this.PricingSetup;
                IMultiMarketDataStreamingService marketDataStreaming = ((ClientSession)Env.Current).MultiMarketDataStreaming;
                if (this.IsRealTimeOn && this._updatePositionTimer != null && (marketDataStreaming != null && pricingSetup != null))
                {
                    IMarketDataStreamingService byPricingSetup = marketDataStreaming.FindByPricingSetup(pricingSetup.Name);
                    if (byPricingSetup != this._realTimeMarketService)
                    {
                        this.UnsubscribeMarketSheet();
                        this._realTimeMarketService = byPricingSetup;
                    }
                }
                else
                    this.UnsubscribeMarketSheet();
                this.SubscribeMarketSheet();
            }
            catch (Exception ex)
            {
                CustomRealTimeBlotter.Logger.Error((object)nameof(UpdateMarketSheet), ex);
            }
            finally
            {
                this._isUpdatingMarketSheet = 0;
            }
        }

        public Market Market
        {
            get
            {
                if (this._market != null && this._pricingSetup != null)
                    return this._market;
                if (this.IsRealTimeMarket())
                {
                    this.UpdateMarketSheet();
                    if (this._realTimeMarketService != null)
                    {
                        Market marketCopy = this._realTimeMarketService.GetMarketCopy();
                        if (marketCopy != null)
                        {
                            this._market = marketCopy;
                            return this._market;
                        }
                    }
                }
                this._market = new Market()
                {
                    Setup = this.PricingSetup,
                    Time = this.SelectedTime
                };
                return this._market;
            }
        }

        private void InternalMarketUpdate(IList<Quote> quotes, List<IMarketData> curves)
        {
            if (!this.IsRealTimeMarket())
                return;
            Market market = this.Market;
            if (market == null || market.Setup == null || this._blotter == null)
                return;
            foreach (IMarketData curve in curves)
                this._pendingMarketDatas[CustomRealTimeBlotter.GetKey(curve)] = curve;
            foreach (Quote quote in (IEnumerable<Quote>)quotes)
                this._pendingQuotes.Add(quote);
        }

        public void NewQuoteUpdate(Quote q)
        {
            if (this.IsRealTimeMarket())
            {
                this.NewQuote(q);
            }
            else
            {
                Market market = this.Market;
                if (market == null || market.Setup == null)
                    return;
                this.NewQuote(q);
            }
        }

        public void NewQuote(Quote q)
        {
            Market market = this.Market;
            if (market == null || market.Setup == null || this._blotter == null)
                return;
            this._pendingQuotes.Add(q);
        }

        public void NewMarketData(IMarketData im)
        {
            Market market = this.Market;
            if (market == null || market.Setup == null || (this._blotter == null || new SimpleDate(this._blotter.ValuationTime) != im.Date))
                return;
            this._pendingMarketDatas[CustomRealTimeBlotter.GetKey(im)] = im;
        }

        public void NewMarketDataUpdate(IMarketData im)
        {
            if (this.IsRealTimeMarket())
            {
                this.NewMarketData(im);
            }
            else
            {
                Market market = this.Market;
                if (market == null || market.Setup == null)
                    return;
                this.NewMarketData(im);
            }
        }

        private static string GetKey(IMarketData im)
        {
            return im.GetType().Name + ":" + (object)im.Id;
        }

        public void Dispose()
        {
            try
            {
                if (this._sessionTrade != null)
                {
                    try
                    {
                        this._sessionTrade.Dispose();
                    }
                    catch
                    {
                    }
                }
                this._sessionTrade = (IPubSubSession)null;
                if (this._sessionPos != null)
                {
                    try
                    {
                        this._sessionPos.Dispose();
                    }
                    catch
                    {
                    }
                }
                this._sessionPos = (IPubSubSession)null;
            }
            catch (Exception ex)
            {
                CustomRealTimeBlotter.Logger.Error((object)"BeforeDispose", ex);
            }
        }
        
    }

    internal class MarketDataConsumerSurrogate : IMarketDataConsumer
    {
        private readonly Action<IList<Quote>, List<IMarketData>> _action;

        public MarketDataConsumerSurrogate(Action<IList<Quote>, List<IMarketData>> marketUpdateCallback)
        {
            if (marketUpdateCallback == null)
                throw new ArgumentNullException(nameof(marketUpdateCallback));
            this._action = marketUpdateCallback;
        }

        public void RemoveQuote(Quote q)
        {
        }

        public void NewQuoteUpdate(Quote q)
        {
        }

        public void NewMarketDataUpdate(IMarketData m)
        {
        }

        public void PricingSetupChange(string name)
        {
        }

        public bool IsRealTimeOn
        {
            get
            {
                return true;
            }
        }

        public void MarketUpdate(IList<Quote> quotes, List<IMarketData> curves)
        {
            this._action(quotes, curves);
        }
    
    }
}
