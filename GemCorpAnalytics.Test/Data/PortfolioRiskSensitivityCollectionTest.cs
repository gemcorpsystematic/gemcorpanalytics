﻿namespace GemCorpAnalytics.Test.Data
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataCollections;
    using System.Xml;
    using System.Runtime.Serialization;
    using System.IO;

    [TestFixture]
    public class PortfolioRiskSensitivityCollectionTest
    {

        [Test]
        public void PortfolioRiskSensitivityCollection_SerialiseAndDesrialise_ReturnsCollection()
        {
            // Arrange
            var portfolioRiskSensitivityCollection = new PortfolioRiskSensitivityCollection { 
                new PortfolioRiskSensitivity.Builder{ 
                    Book = "TestBook1",
                    Underlier = "TestUnderlier1",
                    RiskType = "TestRiskType1",
                    ScenarioName = "TestScenarioName1",
                    Entity = "TestEntity1",
                    ProductType = "TestProduct",
                    Delta = 0.0,
                    DeltaBase = 0.0,
                    Vega = 0.0
                }.Build(),
                new PortfolioRiskSensitivity.Builder{
                    Book = "TestBook2",
                    Underlier = "TestUnderlier2",
                    RiskType = "TestRiskType2",
                    ScenarioName = "TestScenarioName2",
                    Entity = "TestEntity2",
                    ProductType = "TestProduct2",
                    Delta = 0.0,
                    DeltaBase = 0.0,
                    Vega = 0.0
                }.Build()
            };

            // Act
            // The files need to be stored in the bin folder, especially as we move towards using a continuous integration tool 
            var filePath = "C:\\temp\\PortfolioRiskSensitivityCollection.xml";
            var xmlWriter = XmlWriter.Create(filePath);
            var dataContractSerializer = new DataContractSerializer(typeof(PortfolioRiskSensitivityCollection));
            dataContractSerializer.WriteObject(xmlWriter, portfolioRiskSensitivityCollection);
            xmlWriter.Close();

            var fs = new FileStream(filePath, FileMode.Open);
            var reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            var serialiser = new DataContractSerializer(typeof(PortfolioRiskSensitivityCollection));
            var readObject = (PortfolioRiskSensitivityCollection)serialiser.ReadObject(reader, true);
            reader.Close();
            fs.Close();

            // Assert

            foreach(var tup in portfolioRiskSensitivityCollection.Zip(readObject,(a,b)=> new Tuple<PortfolioRiskSensitivity, PortfolioRiskSensitivity>(a, b)))
            {
                Assert.That(tup.Item1.Book, Is.EqualTo(tup.Item2.Book));
                Assert.That(tup.Item1.Entity, Is.EqualTo(tup.Item2.Entity));
                Assert.That(tup.Item1.RiskType, Is.EqualTo(tup.Item2.RiskType));
                Assert.That(tup.Item1.ScenarioName, Is.EqualTo(tup.Item2.ScenarioName));
                Assert.That(tup.Item1.Underlier, Is.EqualTo(tup.Item2.Underlier));
                Assert.That(Math.Abs(tup.Item1.Delta-tup.Item2.Delta), Is.LessThanOrEqualTo(1e-6));
                Assert.That(Math.Abs(tup.Item1.DeltaBase-tup.Item2.DeltaBase), Is.LessThanOrEqualTo(1e-6));
                Assert.That(Math.Abs(tup.Item1.Vega-tup.Item2.Vega), Is.LessThanOrEqualTo(1e-6));
            }

        }

    }
}
