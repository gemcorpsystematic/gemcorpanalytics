﻿

namespace GemCorpAnalytics.Test.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using NUnit.Framework;
    using GemCorpAnalytics.Data;


    [TestFixture]
    public class RiskDirectionTest
    {
        private RiskDirection riskDirection;

        [SetUp]
        public void Setup()
        {
            riskDirection = new RiskDirection.Builder 
            {
                AssetClass = Enums.AssetClass.Fx,
                RiskGreekDirection = Enums.RiskGreekDirection.Delta,
                Name = "FX:GBP:USD",
                RiskAmount = 1.0,
                TimeSeriesMapping = "GBPUSD_SPOT"
            }.Build();
        }

        [Test]
        public void RiskDirection_ToString_ConvertsBackToItsOriginalForm()
        {
            // Arrange

            // Act
            var stringRepresentation = riskDirection.ToString();

            // Assert
            Assert.That(stringRepresentation.Length, Is.GreaterThan(0));
        }

    }
}
