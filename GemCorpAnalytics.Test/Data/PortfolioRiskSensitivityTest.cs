﻿namespace GemCorpAnalytics.Test.Data
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using GemCorpAnalytics.Data;
    using NUnit.Framework;


    [TestFixture]
    public class PortfolioRiskSensitivityTest
    {
        [Test]
        public void PortfolioRiskSensitivity_SerialiseThenDeserialise_ReturnsSameObject()
        {
            // Arrange
            var portfolioRiskSensitivity = new PortfolioRiskSensitivity.Builder { 
                Underlier = "FX:GBP:USD",
                Book = "TestBook",
                Entity = "TestEntity",
                ScenarioName = "TestScenario",
                RiskType = "TestRiskType",
                ProductType = "TestProduct",
                Delta = 0.0,
                DeltaBase = 0.0,
                Vega = 0.0
            }.Build();

            // Act
            var filePath = "C:\\temp\\PortfolioRiskSensitivity.xml";
            var xmlWriter = XmlWriter.Create(filePath);
            var dataContractSerializer = new DataContractSerializer(typeof(PortfolioRiskSensitivity));
            dataContractSerializer.WriteObject(xmlWriter, portfolioRiskSensitivity);
            xmlWriter.Close();

            var fs = new FileStream(filePath, FileMode.Open);
            var reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            var serialiser = new DataContractSerializer(typeof(PortfolioRiskSensitivity));
            var readObject = (PortfolioRiskSensitivity)serialiser.ReadObject(reader, true);
            reader.Close();
            fs.Close();
            
            // Assert
            Assert.That(string.Equals(portfolioRiskSensitivity.Book, readObject.Book), Is.True);
            Assert.That(string.Equals(portfolioRiskSensitivity.Underlier, readObject.Underlier), Is.True);
            Assert.That(string.Equals(portfolioRiskSensitivity.Entity, readObject.Entity), Is.True);
            Assert.That(string.Equals(portfolioRiskSensitivity.ScenarioName, readObject.ScenarioName), Is.True);
            Assert.That(string.Equals(portfolioRiskSensitivity.RiskType, readObject.RiskType), Is.True);
            Assert.That(Math.Abs(portfolioRiskSensitivity.Delta - readObject.Delta) < 1e-6, Is.True);
            Assert.That(Math.Abs(portfolioRiskSensitivity.DeltaBase - readObject.DeltaBase) < 1e-6, Is.True);
            Assert.That(Math.Abs(portfolioRiskSensitivity.Vega - readObject.Vega) < 1e-6, Is.True);
        }

    }
}
