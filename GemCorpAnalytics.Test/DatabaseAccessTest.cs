﻿namespace GemCorpAnalytics.Test
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using GemCorpAnalytics.Data;
    using DataAccess.Requests;
    using NUnit.Framework;
    using GemCorpAnalytics.DataAccess.Factories;
    using GemCorpAnalytics.Enums;


    [TestFixture]
    public class DatabaseAccessTest
    {

        [Test]
        public void DatabaseAccess_Create_UpdateTable()
        {
            // Arrange
            var timeSeriesPoint = new TimeSeriesPoint.Builder{
                Date = new DateTime(2021, 4, 12, 16, 30, 0), Name="Naphtha CIF NWE", Ticker="", Value=537.00, Currency="USD", 
                Contributor="Platts", Unit="mt"}.Build();
            var timeSeries = new TimeSeries(new[] { timeSeriesPoint });
            var request = new GetUpdateTimeSeriesTableRequest(timeSeries);
            var handler = UpdateTimeSeriesTableRequestHandlerFactory.GetHandler("UAT");

            // Act
            handler.Handle(request);

            // Assert
        }

        [Test]
        public void DatabaseAccess_GetTimeSeries_GetsTimeSeries()
        {
            // Arrange
            var names = new List<KeyValuePair<string, TimeSeriesAttributes>> { 
                new KeyValuePair<string, TimeSeriesAttributes>("Fueloil 1% FOB MED", TimeSeriesAttributes.Close), 
                new KeyValuePair<string, TimeSeriesAttributes>("Dated Brent", TimeSeriesAttributes.Close), 
                new KeyValuePair<string, TimeSeriesAttributes>("Gasoil 0.1% FOB MED", TimeSeriesAttributes.Close), 
                new KeyValuePair<string, TimeSeriesAttributes>("Naphtha CIF NWE", TimeSeriesAttributes.Close) 
            };
            var request = new GetTimeSeriesRequest(new DateTime(2017, 1, 1));
            request.Names.ToList().AddRange(names);
            var handler = TimeSeriesRequestHandlerFactory.GetHandler("UAT", TimeSeriesType.FairValue1630Snap);

            // Act
            var timeSeries = handler.Handle(request);

            // Assert

            Assert.That(timeSeries.Keys.Count()==names.Count(),$"Number of time series requested: {names.Count()}, time series returned: {timeSeries.Keys.Count()}");
            

        }


    }
}
