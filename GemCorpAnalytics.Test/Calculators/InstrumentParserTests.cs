﻿namespace GemCorpAnalytics.Test.Calculators
{
    using GemCorpAnalytics.Calculators;
    using NUnit.Framework;


    [TestFixture]
    public class InstrumentParserTests
    {

        [Test]
        [TestCase("ANGOL 9.5 11/12/25 REGS","ANGOL25")]
        [TestCase("OMAN 7.375 10/28/32 REGS","OMAN32")]
        public void ParseBond_ParseBondString_ReturnsBondString(string inputString, string result)
        {
            // Act
            var parsedString = InstrumentParser.ParseBond(inputString);

            // Assert
            Assert.That(parsedString, Is.EqualTo(result));
        }

        [Test]
        [TestCase("2-Year U.S. Treasury Note Jun21", "U.S.TreasuryNote,2-Year")]
        public void ParseBondFuture_ParseBondFuture_ReturnsFutureString(string inputString, string result)
        {
            // Act
            var parsedString = InstrumentParser.ParseBondFuture(inputString);

            // Assert
            Assert.That(parsedString, Is.EqualTo(result));
        }

        [Test]
        [TestCase("BRL P CDI-1D/R3.1775 % 3/Jan/22 USD","BRL")]
        [TestCase("CLP P 0.65 %/RCAMARA-1D 13/Oct/22 USD","CLP")]
        public void ParseSwap_ParseSwapString_ReturnsSwapString(string inputString, string result)
        {
            // Act
            var parsedString = InstrumentParser.ParseNonDeliverableSwap(inputString);

            // Assert
            Assert.That(parsedString, Is.EqualTo(result),$"The parsed string is:{parsedString}, expected result is:{result}");
        }

        

    }

}
