﻿namespace GemCorpAnalytics.Test.Context
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Enums;

    [TestFixture]
    public class TimeSeriesRepositoryTest
    {

        private TimeSeriesRepository _timeSeriesRepository;

        [SetUp]
        public void Setup()
        {
            var timeSeriesPoint = new TimeSeriesPoint.Builder {
                Ticker = "Test",
                TimeSeriesAttribute = TimeSeriesAttributes.Close
            }.Build();

            var timeSeries = new TimeSeries(new[] { timeSeriesPoint});

            _timeSeriesRepository = new TimeSeriesRepository(new Dictionary<ITimeSeriesRepositoryEntry, TimeSeries> { 
                { new TimeSeriesRepositoryEntry("Test",TimeSeriesAttributes.Close), timeSeries } 
            }
            );

        }

        [Test]
        public void TimeSeriesRepository_GetTimeSeries_ReturnsSeries() 
        {
            // Arrange
            var timeSeriesRepositoryEntry = new TimeSeriesRepositoryEntry("Test", TimeSeriesAttributes.Close);

            // Act
            var timeSeries = _timeSeriesRepository.GetTimeSeries(timeSeriesRepositoryEntry);

            // Assert
            Assert.That(timeSeries.Count, Is.GreaterThan(0));
        }


    }
}
