﻿using System.Reflection;

namespace GemCorpAnalytics.DataAccess.Factories
{
    using System;
    using System.IO;
    using RequestHandlers;
    using Requests;
    using Newtonsoft.Json.Linq;
    using Npgsql;

    /// <summary>The update time series table request handler factory.</summary>
    public static class UpdateTimeSeriesTableRequestHandlerFactory
    {

        /// <summary>Gets handler.</summary>
        /// <param name="environment">The environment.</param>
        /// <returns>The handler.</returns>
        public static IRequestHandler<GetUpdateTimeSeriesTableRequest, int> GetHandler(string environment)
        {
            // get the json config file and read in connection strings
            var configFile = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\GemTimeSeriesDB.json"));

            if (environment != "PROD" && environment != "UAT")
                throw new Exception($"Unable to handle environment type: {environment}, Can only handle PROD, UAT");

            var configs = configFile[environment];
            var server = (string)configs["Server"];
            var port = (Int32)configs["Port"];
            var database = (string)configs["Database"];
            var username = (string)configs["Username"];
            var pwd = (string)configs["Password"];
            
            var connection = new NpgsqlConnection($"Server={server}; Port={port}; Database={database}; User Id={username}; Password={pwd}");

            return new GetUpdateTimeSeriesTableRequestHandler(connection);
        }

    }
}
