﻿namespace GemCorpAnalytics.DataAccess.Factories
{
    using System;
    using System.IO;
    using Npgsql;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.DataAccess.RequestHandlers;
    using Newtonsoft.Json.Linq;

    /// <summary>The scenario ladder request handler factory.</summary>
    public static class ScenarioLadderRequestHandlerFactory
    {
        /// <summary>The handler.</summary>
        /// <param name="environment">The environment.</param>
        /// <returns>The <see cref="IRequestHandler{T, R}"/>.</returns>
        public static IRequestHandler<GetScenarioLadderRequest, ScenarioLadderCollection> GetHandler(string environment) 
        {
            var configFile = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\GemTimeSeriesDB.json"));

            if (environment != "PROD" && environment != "UAT")
                throw new Exception($"Unable to handle environment type: {environment}, Can only handle PROD, UAT");

            var configs = configFile[environment]["RiskSensitivities"];
            var server = (string)configs["Server"];
            var port = (Int32)configs["Port"];
            var database = (string)configs["Database"];
            var username = (string)configs["Username"];
            var pwd = (string)configs["Password"];

            var connection = new NpgsqlConnection($"Server={server}; Port={port}; Database={database}; User Id={username}; Password={pwd};CommandTimeout=0;");

            return new GetScenarioLadderRequestHandler(connection);
        }

    }
}
