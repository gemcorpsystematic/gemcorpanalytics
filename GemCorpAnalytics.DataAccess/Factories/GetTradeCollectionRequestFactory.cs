﻿namespace GemCorpAnalytics.DataAccess.Factories
{
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.DataAccess.RequestHandlers;
    using Newtonsoft.Json.Linq;
    using Npgsql;
    using System.IO;
    using System;

    /// <summary>The get trade collection request factory.</summary>
    public static class GetTradeCollectionRequestFactory
    {

        /// <summary>Gets the handler.</summary>
        /// <param name="environment">The environment.</param>
        /// <returns>The <see cref="IRequestHandler{T, R}"/>.</returns>
        public static IRequestHandler<GetTradePopulationRequest, TradeCollection> GetHandler(string environment)
        {
            var fileLocation = File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\GemTimeSeriesDB.json");
                        
            var configFile = JObject.Parse(fileLocation);

            if (environment != "PROD" && environment != "UAT")
                throw new Exception($"Unable to handle environment type: {environment}, Can only handle PROD, UAT");

            var configs = configFile[environment]["RiskSensitivities"];
            var server = (string)configs["Server"];
            var port = (Int32)configs["Port"];
            var database = (string)configs["Database"];
            var username = (string)configs["Username"];
            var pwd = (string)configs["Password"];

            var connection = new NpgsqlConnection($"Server={server}; Port={port}; Database={database}; User Id={username}; Password={pwd};CommandTimeout=0;");

            return new GetTradePopulationRequestHandler(connection);
            
        }
    }
}
