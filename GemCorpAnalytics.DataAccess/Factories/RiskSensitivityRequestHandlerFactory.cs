﻿namespace GemCorpAnalytics.DataAccess.Factories
{

    using System;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.DataAccess.RequestHandlers;
    using GemCorpAnalytics.DataAccess.Requests;
    using Newtonsoft.Json.Linq;
    using Npgsql;
    using System.IO;

    /// <summary>The risk sensitivity request handler factory.</summary>
    public static class RiskSensitivityRequestHandlerFactory
    {
        /// <summary>Gets handler.</summary>
        /// <param name="environment">The environment.</param>
        /// <returns>The <see cref="IRequestHandler{T, R}"/>.</returns>
        public static IRequestHandler<GetRiskSensitivityCollection, PortfolioRiskSensitivityCollection> GetHandler(string environment) 
        {

            var fileLocation = $"{AppDomain.CurrentDomain.BaseDirectory}\\GemTimeSeriesDB.json";

            var configFile = JObject.Parse(File.ReadAllText(fileLocation));

            if (environment != "PROD" && environment != "UAT")
                throw new Exception($"Unable to handle environment type: {environment}, Can only handle PROD, UAT");

            var configs = configFile[environment]["RiskSensitivities"];
            var server = (string)configs["Server"];
            var port = (Int32)configs["Port"];
            var database = (string)configs["Database"];
            var username = (string)configs["Username"];
            var pwd = (string)configs["Password"];

            var connection = new NpgsqlConnection($"Server={server}; Port={port}; Database={database}; User Id={username}; Password={pwd};CommandTimeout=0;");

            return new GetRiskSensitivityRequestHandler(connection);
        }

    }
}
