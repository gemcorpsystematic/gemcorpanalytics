﻿namespace GemCorpAnalytics.DataAccess.Factories
{
    using System;
    using Newtonsoft.Json.Linq;
    using Npgsql;
    using System.IO;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.DataAccess.RequestHandlers;
    using GemCorpAnalytics.DataCollections;

    /// <summary>The stress test request handler factory.</summary>
    public static class StressTestRequestHandlerFactory
    {
        /// <summary>Gets the request handler.</summary>
        /// <param name="environment">The environment.</param>
        /// <returns>The <see cref="IRequestHandler{T, R}"/>.</returns>
        public static IRequestHandler<GetStressTestRequest, StressTestCollection> GetHandler(string environment)
        {
            var configFile = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\GemTimeSeriesDB.json"));

            if (environment != "PROD" && environment != "UAT")
                throw new Exception($"Unable to handle environment type: {environment}, Can only handle PROD, UAT");

            var configs = configFile[environment]["RiskSensitivities"];
            var server = (string)configs["Server"];
            var port = (Int32)configs["Port"];
            var database = (string)configs["Database"];
            var username = (string)configs["Username"];
            var pwd = (string)configs["Password"];

            var connection = new NpgsqlConnection($"Server={server}; Port={port}; Database={database}; User Id={username}; Password={pwd};CommandTimeout=0;");

            return new GetStressTestRequestHandler(connection);

        }

    }
}
