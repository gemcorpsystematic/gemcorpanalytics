﻿namespace GemCorpAnalytics.DataAccess.Factories
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using Data;
    using RequestHandlers;
    using Requests;
    using Newtonsoft.Json.Linq;
    using Npgsql;
    using GemCorpAnalytics.Enums;
    using GemCorpAnalytics.Context;

    /// <summary>The time series request handler factory.</summary>
    public static class TimeSeriesRequestHandlerFactory
    {
        /// <summary>Gets the handler.</summary>
        /// <param name="environment">The environment.</param>
        /// <param name="timeSeriesType">The time series type.</param>
        /// <returns>The handler.</returns>
        public static IRequestHandler<GetTimeSeriesRequest, IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries>> GetHandler(
            string environment,  TimeSeriesType timeSeriesType)
        {
            // get the json config file and read in connection strings
            var configFile = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\GemTimeSeriesDB.json"));

            if (environment != "PROD" && environment != "UAT")
                throw new Exception($"Unable to handle environment type: {environment}, Can only handle PROD, UAT");
            
            var configs = configFile[environment][timeSeriesType.ToString()];
            var server = (string)configs["Server"];
            var port = (Int32)configs["Port"];
            var database = (string)configs["Database"];
            var username = (string)configs["Username"];
            var pwd = (string)configs["Password"];

            var connection = new NpgsqlConnection($"Server={server}; Port={port}; Database={database}; User Id={username}; Password={pwd}");

            switch(timeSeriesType)
            {
                case TimeSeriesType.Daily:
                    return new DailyTimeSeriesTypeRequestHandler(connection);
                case TimeSeriesType.FairValue1630Snap:
                    return new FairValueSnapTimeSeriesTypeRequestHandler(connection);
                default:
                    throw new NotImplementedException($"Time series type is not recognised:{timeSeriesType}");
            }

        }

    }
}
