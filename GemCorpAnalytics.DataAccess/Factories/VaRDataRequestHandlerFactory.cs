﻿namespace GemCorpAnalytics.DataAccess.Factories
{
    using System;
    using System.IO;
    using System.Data;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.DataAccess.RequestHandlers;
    using Npgsql;
    using Newtonsoft.Json.Linq;

    /// <summary>The var data request handler factory.</summary>
    public static class VaRDataRequestHandlerFactory
    {
        /// <summary>Gets the handler.</summary>
        /// <param name="environment">The environment.</param>
        /// <param name="themesTable">The themes table.</param>
        /// <param name="themesMapper">The themes mapper.</param>
        /// <returns>The handler.</returns>
        public static IRequestHandler<GetVaRDataRequest, ValueAtRiskEntryCollection> GetHandler(
            string environment,
            ThemesCollection themesTable,
            Func<string, string, string, ThemesCollection,string> themesMapper)
        {
            var configFile = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\GemTimeSeriesDB.json"));

            if (environment != "PROD" && environment != "UAT")
                throw new Exception($"Unable to handle environment type: {environment}, Can only handle PROD, UAT");

            var configs = configFile[environment]["DailyPnL"];
            var server = (string)configs["Server"];
            var port = (Int32)configs["Port"];
            var database = (string)configs["Database"];
            var username = (string)configs["Username"];
            var pwd = (string)configs["Password"];

            var connection = new NpgsqlConnection($"Server={server}; Port={port}; Database={database}; User Id={username}; Password={pwd}");

            return new GetVaRDataRequestHandler(connection, themesTable, themesMapper);
        }

    }
}
