﻿namespace GemCorpAnalytics.DataAccess.Factories
{
    using System;
    using System.IO;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.DataAccess.RequestHandlers;
    using GemCorpAnalytics.DataAccess.Requests;
    using Npgsql;
    using Newtonsoft.Json.Linq;
    
    /// <summary>The themes request handler factory.</summary>
    public static class ThemesRequestHandlerFactory
    {

        /// <summary>Gets request handler.</summary>
        /// <param name="environment">The environment.</param>
        /// <returns>The handler.</returns>
        public static IRequestHandler<GetThemesRequest, ThemesCollection> GetHandler(string environment) 
        {
            var configFile = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\GemTimeSeriesDB.json"));

            if (environment != "PROD" && environment != "UAT")
                throw new Exception($"Unable to handle environment type: {environment}, Can only handle PROD, UAT");

            var configs = configFile[environment]["DailyPnL"];
            var server = (string)configs["Server"];
            var port = (Int32)configs["Port"];
            var database = (string)configs["Database"];
            var username = (string)configs["Username"];
            var pwd = (string)configs["Password"];

            var connection = new NpgsqlConnection($"Server={server}; Port={port}; Database={database}; User Id={username}; Password={pwd}");

            return new GetThemesRequestHandler(connection);
        }

    }
}
