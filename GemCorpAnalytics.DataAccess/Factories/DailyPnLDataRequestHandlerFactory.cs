﻿namespace GemCorpAnalytics.DataAccess.Factories
{
    using System;
    using System.IO;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.DataAccess.RequestHandlers;
    using Newtonsoft.Json.Linq;
    using Npgsql;

    /// <summary>The daily pnl data request handler factory.</summary>
    public static class DailyPnLDataRequestHandlerFactory
    {

        /// <summary>Gets handler.</summary>
        /// <param name="environment">The environment.</param>
        /// <param name="themesTable">The themes table.</param>
        /// <param name="themeMapper">The theme mapper.</param>
        /// <returns>The <see cref="IRequestHandler{T, R}"/>.</returns>
        public static IRequestHandler<GetPnLDataRequest, PlEntryCollection> GetHandler(
            string environment,
            ThemesCollection themesTable,
            Func<string, string, string, ThemesCollection, string> themeMapper)
        {
            var configFile = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\GemTimeSeriesDB.json"));

            if (environment != "PROD" && environment != "UAT")
                throw new Exception($"Unable to handle environment type: {environment}, Can only handle PROD, UAT");

            var configs = configFile[environment]["DailyPnL"];
            var server = (string)configs["Server"];
            var port = (Int32)configs["Port"];
            var database = (string)configs["Database"];
            var username = (string)configs["Username"];
            var pwd = (string)configs["Password"];

            var connection = new NpgsqlConnection($"Server={server}; Port={port}; Database={database}; User Id={username}; Password={pwd}");

            return new GetDailyPnLDataRequestHandler(connection, themesTable, themeMapper);
        }

    }
}
