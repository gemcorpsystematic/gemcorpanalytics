﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using System.Collections.ObjectModel;
    using System.Data;
    using System;
    using System.Collections.Generic;
    using Data;
    using Requests;
    using Npgsql;
    using GemCorpAnalytics.Context;

    /// <summary>The get time series request handler.</summary>
    public class GetTimeSeriesRequestHandler : IRequestHandler<GetTimeSeriesRequest, IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries>>
    {
        #region Fields

        /// <summary>The connection.</summary>
        private readonly NpgsqlConnection _connection;
        
        #endregion

        #region Constructor

        /// <summary>Initialize an instance of the get time series request handler.</summary>
        /// <param name="connection">The connection.</param>
        public GetTimeSeriesRequestHandler(NpgsqlConnection connection)
        {
            _connection = connection;
        }


        #endregion


        #region Public Methods

        /// <summary>Handles the get time series request.</summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="IReadOnlyDictionary{TKey,TValue}"/>.</returns>
        public IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> Handle(GetTimeSeriesRequest request)
        {
            try
            {
                var output = new Dictionary<ITimeSeriesRepositoryEntry, TimeSeries>();
                
                _connection.Open();
                
                var cmd = new NpgsqlCommand {Connection = _connection};
                
                foreach (var name in request.Names)
                {
                    cmd.CommandText = $"Select * from CommodityTimeSeriesTable where _Name = '{name}' and _date >= '{request.FromDate:yyyyMMdd}' and _date <= '{request.ToDate:yyyyMMdd}'";
                    
                    var rdr = cmd.ExecuteReader();

                    var timeSeriesPoints = new List<TimeSeriesPoint>();

                    while (rdr.Read())
                    {
                        var ticker = string.IsNullOrEmpty(rdr.GetString(2)) ? string.Empty : rdr.GetString(2);
                        var timeStamp = rdr.GetTimeStamp(0);
                        var date = new DateTime(timeStamp.Year, timeStamp.Month, timeStamp.Day, timeStamp.Hour,
                            timeStamp.Minute, timeStamp.Second);
                        timeSeriesPoints.Add(new TimeSeriesPoint.Builder{Date=date, Name=rdr.GetString(1), Ticker=ticker,
                            Value=Convert.ToDouble(rdr.GetString(3)), Currency=rdr.GetString(4), Contributor=rdr.GetString(5),
                            Unit=rdr.GetString(6)}.Build());
                    }

                    rdr.Close();

                    var timeSeriesRepositoryEntry = new TimeSeriesRepositoryEntry(name.Key, name.Value);

                    output[timeSeriesRepositoryEntry] = new TimeSeries(timeSeriesPoints);
                    
                }

                _connection.Close();
                
                return new ReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries>(output);
            }
            catch (Exception e)
            {
                if(_connection.State == ConnectionState.Open) _connection.Close();
                throw e;
            }

        }

        #endregion

    }
}
