﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using System;
    using System.Data;
    using Requests;
    using Npgsql;
    using System.Threading.Tasks;

    /// <summary>The update time series table request handler.</summary>
    public class GetUpdateTimeSeriesTableRequestHandler : IRequestHandler<GetUpdateTimeSeriesTableRequest, int>
    {

        #region Fields

        /// <summary>The connection.</summary>
        private readonly NpgsqlConnection _connection;

        #endregion

        #region Constructor

        /// <summary>Initialize an instance of the update time series table request handler.</summary>
        /// <param name="connection">The connection.</param>
        public GetUpdateTimeSeriesTableRequestHandler(NpgsqlConnection connection)
        {
            // create db connection to 
            _connection = connection;
        }

        #endregion

        #region Public Methods

        /// <summary>Handles the update time series table request.</summary>
        /// <param name="request">The request.</param>
        /// <returns>The number of records stored.</returns>
        public int Handle(GetUpdateTimeSeriesTableRequest request)
        {
            var successfulEntries = 0;

            try
            {
                _connection.Open();
                var cmd = new NpgsqlCommand {Connection = _connection};
                
                foreach (var timeSeriesPoint in request.TimeSeries)
                {
                    cmd.CommandText =
                        "Insert into CommodityTimeSeriesTable(date, name, ticker, price, currency, contributor, unit, instrument_id) values " +
                        $"('{timeSeriesPoint.Date.ToString("yyyyMMdd HH:mm:ss")}'," +
                        $" '{timeSeriesPoint.Name}', " +
                        $"'{timeSeriesPoint.Ticker}', {timeSeriesPoint.Value}, '{timeSeriesPoint.Currency}', '{timeSeriesPoint.Contributor}', '{timeSeriesPoint.Unit}'," +
                        $"{timeSeriesPoint.InstrumentId})"; // Have to check if instrument id is null, do we want to only persist data with an instrument id? 

                    successfulEntries += cmd.ExecuteNonQuery();
                }

                _connection.Close();
                
                return successfulEntries;
            }
            catch (Exception e)
            {
                if(_connection.State == ConnectionState.Open) _connection.Close();
                throw e;
            }

        }

        #endregion
    }
}
