﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataAccess.Requests;
    using Npgsql;
    using System.Data;
    using System;
    using System.Linq;
    using System.Text.RegularExpressions;

    /// <summary>The scenario ladder request handler.</summary>
    public class GetScenarioLadderRequestHandler : IRequestHandler<GetScenarioLadderRequest, ScenarioLadderCollection>
    {

        #region Fields

        /// <summary>The stored proc.</summary>
        private readonly string _storedProc = "select * from sp_RiskLadders('{0}','{1}')";

        /// <summary>The sql connection.</summary>
        private readonly NpgsqlConnection _connection;

        #endregion

        #region Constructor

        /// <summary>Initialize an instance of the get scenario ladder request handler.</summary>
        /// <param name="connection"></param>
        public GetScenarioLadderRequestHandler(
            NpgsqlConnection connection)
        {
            _connection = connection;
        }

        #endregion

        #region Public Methods

        /// <summary>Handles the get scenario ladder request.</summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="ScenarioLadderCollection"/>.</returns>
        public ScenarioLadderCollection Handle(GetScenarioLadderRequest request)
        {
            try {
                var sqlQuery = CreateQuery(request, _storedProc);

                using (var sqlCommand = new NpgsqlCommand { Connection = _connection, CommandText = sqlQuery })
                {
                    _connection.Open();
                    var output = CreateScenarioLadderCollection(sqlCommand);
                    _connection.Close();
                    return output;
                }

            }catch(Exception e)
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
                var msg = e.InnerException == null ? e.Message : e.InnerException.Message;
                throw new Exception(msg);
            }
            finally 
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
            }
        }

        #endregion

        #region Methods

        /// <summary>Creates query.</summary>
        /// <param name="requests">The request.</param>
        /// <param name="storedProc">The stored proc.</param>
        /// <returns>The query.</returns>
        private string CreateQuery(GetScenarioLadderRequest request, string storedProc)
        {
            var requestDates = request.Date.Select(r => $"{r:yyyy-MM-dd}");
            var requestStrategies = request.Entity.Select(r => r);

            var sqlQuery = string.Format(storedProc,
                "{" + $"{string.Join(",", requestDates)}" + "}",
                "{" + $"{string.Join(",", requestStrategies)}" + "}");

            return sqlQuery;
        }

        /// <summary>Parses scenario.</summary>
        /// <param name="scenario">The scenario.</param>
        /// <returns>The scenario.</returns>
        private double ParseScenario(string scenario, string riskType)
        {
            var tokens = scenario.Split(' ');

            int numIdx = 0;
            foreach (var token in tokens)
            {
                if (token.Any(Char.IsDigit))
                    break;
                numIdx += 1;
            }

            // the rate ladders have a tag dv250 (+)

            var number = tokens[numIdx];
            number = number.TrimEnd(new[] { '%', '$', '+'});
            number = number.TrimStart(new[] { '%', '$', '+'});
            var numString = Regex.Match(number, @"[+-]?\d+?\d*").Value;
            var num = Int32.Parse(numString);
            var numAdj = Convert.ToDouble(num) / 100.0;

            if (riskType.ToLower().Contains("interest rate"))
            {
                var sign = tokens.Last();
                return sign.Contains('-') ? -numAdj : numAdj;
            }

            return numAdj;
        }

        /// <summary>Creates the scenrio ladder collection.</summary>
        /// <param name="cmd">The command.</param>
        /// <returns>The <see cref="ScenarioLadderCollection"/>.</returns>
        private ScenarioLadderCollection CreateScenarioLadderCollection(NpgsqlCommand cmd)
        {
            var rdr = cmd.ExecuteReader();

            var output = new ScenarioLadderCollection();

            while(rdr.Read())
            {
                var name = rdr.GetString(0);
                var date = rdr.GetDateTime(1);
                var riskType = rdr.GetString(2);
                var scenarioName = rdr.GetString(3); 
                var strategy = rdr.GetString(4);
                var entity = rdr.GetString(5);
                var scenarioValue = rdr.GetDecimal(6);

                output.Add(new ScenarioLadder.Builder {
                    Name = name,
                    ScenarioDate = date,
                    RiskType = riskType,
                    ScenarioName = scenarioName,
                    Strategy = strategy,
                    Entity = entity,
                    ScenarioValue = Convert.ToDouble(scenarioValue)
                }.Build()); 
            }

            return output;
        }

        #endregion

    }
}
