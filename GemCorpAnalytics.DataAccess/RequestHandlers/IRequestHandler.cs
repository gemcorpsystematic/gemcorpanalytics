﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using GemCorpAnalytics.DataAccess.Requests;

    /// <summary>The request handler.</summary>
    /// <typeparam name="T">The request.</typeparam>
    /// <typeparam name="R">The return type.</typeparam>
    public interface IRequestHandler<T, R> where T:IRequest
    {
        R Handle(T request);

    }
}
