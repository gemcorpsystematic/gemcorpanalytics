﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Linq;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.DataAccess.Requests;
    using Npgsql;

    /// <summary>The get var data request handler.</summary>
    public class GetVaRDataRequestHandler : IRequestHandler<GetVaRDataRequest, ValueAtRiskEntryCollection>
    {

        #region Fields

        /// <summary>The npg sql connection.</summary>
        private readonly NpgsqlConnection _connection;

        /// <summary>The themes table.</summary>
        private readonly ThemesCollection _themesTable;

        /// <summary>The themes mapper.</summary>
        private readonly Func<string, string, string, ThemesCollection, string> _themesMapper;

        #endregion

        #region Constructor

        /// <summary>Initialize an instance of the dta arequets handler.</summary>
        /// <param name="connection">The connection.</param>
        /// <param name="themesTable">The themes table.</param>
        /// <param name="themesMapper">The themes mapper.</param>
        public GetVaRDataRequestHandler(
            NpgsqlConnection connection,
            ThemesCollection themesTable,
            Func<string, string, string, ThemesCollection, string> themesMapper)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
            _themesTable = themesTable;
            _themesMapper = themesMapper;
        }

        #endregion

        #region Public Methods

        /// <summary>Handles the request.</summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="DataTable"/>.</returns>
        public ValueAtRiskEntryCollection Handle(GetVaRDataRequest request)
        {

            try
            {
                // this needs to be in a stored proc on the database
                var sqlQuery = $"select rp.underlierinfo, rp.entity, rp.description, rp.book, rp.currency, rt.tradeid, rt.pv, rt.pvlocal, rt.cvar, rt.var, rt.mvar, rt.result, rt.tradedate " +
                    $"from otrawvarresulttrade as rt inner join otrawdatavarreport as rp on rt.tradeid = rp.tradeid where rt.tradedate = '{request.ValueDate:yyyy-MM-dd HH:mm:ss}' " +
                    $"and rp.tradingdate = '{request.ValueDate:yyyy-MM-dd HH:mm:ss}' order by rt.tradeid asc";

                _connection.Open();

                var cmd = new NpgsqlCommand
                {
                    Connection = _connection,
                    CommandText = sqlQuery
                };

                var rdr = cmd.ExecuteReader();

                var output = new ValueAtRiskEntryCollection();

                while (rdr.Read())
                {
                    var valueAtRiskEntry = new ValueAtRiskEntry.Builder {
                        UnderlierInfo = string.IsNullOrEmpty(rdr.GetString(0)) ? string.Empty : rdr.GetString(0),
                        Entity = string.IsNullOrEmpty(rdr.GetString(1)) ? string.Empty : rdr.GetString(1),
                        Description = string.IsNullOrEmpty(rdr.GetString(2)) ? string.Empty : rdr.GetString(2),
                        Book = string.IsNullOrEmpty(rdr.GetString(3)) ? string.Empty : rdr.GetString(3),
                        Currency = string.IsNullOrEmpty(rdr.GetString(4)) ? string.Empty : rdr.GetString(4),
                        TradeId = rdr.GetInt32(5),
                        Pv = Convert.ToDouble(rdr.GetDecimal(6)),
                        PvLocal = Convert.ToDouble(rdr.GetDecimal(7)),
                        CVaR = Convert.ToDouble(rdr.GetDecimal(8)),
                        VaR = Convert.ToDouble(rdr.GetDecimal(9)),
                        MVaR = Convert.ToDouble(rdr.GetDecimal(10)),
                        Result = string.IsNullOrEmpty(rdr.GetString(11)) ? string.Empty : rdr.GetString(11),
                        TradeDate = rdr.GetDateTime(12),
                        Theme = string.Empty
                    }.Build();
                    
                    output.Add((ValueAtRiskEntry)valueAtRiskEntry);
                }

                if (_themesMapper != null)
                {
                    var tempOutput = new List<IValueAtRiskEntry>(output);
                    output.Clear();

                    foreach(ValueAtRiskEntry entry in tempOutput)
                    {
                        var theme = _themesMapper(entry.Book, string.Empty, entry.UnderlierInfo, _themesTable);
                        var entryBuilder = entry.ToBuilder();
                        entryBuilder.Theme = theme;
                        output.Add((ValueAtRiskEntry)entryBuilder.Build());
                    }

                    if (output.Count(p => p.Theme.ToLower() == "missing theme") > 0)
                        PaddMissingThemes(ref output);

                }

                FilterDuplicates(ref output);

                _connection.Close();

                return output;
            
            }catch(Exception e) 
            {
                var errMsg = e.InnerException==null ? e.Message : e.InnerException.Message;
                throw new Exception(errMsg);
            }

        }

        #endregion


        #region Methods

        /// <summary>Filters duplicates.</summary>
        /// <param name="valueAtRiskEntryCollection">The value at risk entry collection.</param>
        private void FilterDuplicates(ref ValueAtRiskEntryCollection valueAtRiskEntryCollection)
        {
            var tradeIds = valueAtRiskEntryCollection.Select(r => r.TradeId).Distinct().ToList();

            var dataCopy = new List<IValueAtRiskEntry>(valueAtRiskEntryCollection);

            valueAtRiskEntryCollection.Clear();

            foreach(var tradeId in tradeIds)
            {
                var entry = dataCopy.First(r => r.TradeId == tradeId);
                valueAtRiskEntryCollection.Add(entry);
            }

        }

        /// <summary>Padds missing themes.</summary>
        /// <param name="plEntries">The pl entries.</param>
        private void PaddMissingThemes(ref ValueAtRiskEntryCollection plEntries)
        {

            var missingThemes = new List<IValueAtRiskEntry>(plEntries.Where(p => p.Theme.ToLower() == "missing theme"));
            var entriesWithThemes = new List<IValueAtRiskEntry>(plEntries.Where(p => p.Theme.ToLower() != "missing theme"));
            plEntries.Clear();

            var matchFound = false;

            foreach (var missingTheme in missingThemes)
            {
                var possibleMatches = entriesWithThemes.Count(e => e.Book.ToLower() == missingTheme.Book.ToLower() && e.Currency.ToLower() == missingTheme.Currency.ToLower());
                if (possibleMatches < 1)
                {
                    plEntries.Add(missingTheme);
                    continue;
                }

                var possibleMatch = entriesWithThemes.Where(e => e.Book.ToLower() == missingTheme.Book.ToLower() && e.Currency.ToLower() == missingTheme.Currency.ToLower());

                foreach (var match in possibleMatch)
                {
                    var matches = String.Compare(missingTheme.UnderlierInfo, match.UnderlierInfo, StringComparison.OrdinalIgnoreCase);
                    if (matches > 0)
                    {
                        var adjustedPlEntry = ((ValueAtRiskEntry)missingTheme).ToBuilder();
                        adjustedPlEntry.Theme = match.Theme;
                        plEntries.Add(adjustedPlEntry.Build());
                        matchFound = true;
                        break;
                    }
                }

                if (!matchFound)
                    plEntries.Add(missingTheme);
                matchFound = false;
            }

            plEntries.AddRange(entriesWithThemes);
        }

        #endregion




    }
}
