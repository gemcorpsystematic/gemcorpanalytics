﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using System;
    using System.IO;
    using System.Data;
    using System.Linq;
    using System.Collections.Generic;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataCollections;
    using Npgsql;
    using Newtonsoft.Json.Linq;
    using System.Threading.Tasks;

    /// <summary>The themes request handler.</summary>
    public class GetThemesRequestHandler : IRequestHandler<GetThemesRequest, ThemesCollection>
    {

        #region Fields

        /// <summary>The npg sql connection.</summary>
        private readonly NpgsqlConnection _connection;

        #endregion

        #region Constructor

        /// <summary>Initialize an instance of the request handler.</summary>
        /// <param name="connection">The npg sql connection.</param>
        public GetThemesRequestHandler(NpgsqlConnection connection)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
        }

        #endregion

        #region Public Methods

        public ThemesCollection Handle(GetThemesRequest request)
        {
            try {

                var output = new ThemesCollection();

                if (!request.MappingFileConfig.ToLower().Contains("macro"))
                {

                    var sqlQuery = "select * from themes";

                    _connection.Open();

                    var cmd = new NpgsqlCommand
                    {
                        Connection = _connection,
                        CommandText = sqlQuery
                    };

                    var rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        var themeEntry = new Themes.Builder { 
                            CountryOfRisk = string.IsNullOrEmpty(rdr.GetString(0)) ? string.Empty : rdr.GetString(0),
                            Theme = string.IsNullOrEmpty(rdr.GetString(1)) ? string.Empty : rdr.GetString(1),
                            Region = string.IsNullOrEmpty(rdr.GetString(2)) ? string.Empty : rdr.GetString(2)
                        }.Build();
                        output.Add(themeEntry);
                    }
                    _connection.Close();
                }
                OverrideThemes(request.MappingFileConfig, ref output);

                return output;

            }catch(Exception e) {
                var errMsg = e.InnerException == null ? e.Message : e.InnerException.Message;
                throw new Exception(errMsg);
            }

        }

        #endregion

        #region Methods

        /// <summary>Override themes.</summary>
        /// <param name="mappingFileConfig">The mapping file config.</param>
        /// <param name="themesTable">The themes table.</param>
        private void OverrideThemes(string mappingFileConfig, ref ThemesCollection themesCollection)
        {
            var configFile = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\ThemeMappingOverrides.json"));
            var themeOverrides = configFile[mappingFileConfig];

            //var copyOfThemesCollection = new List<Themes>(themesCollection);

            //themesCollection.Clear();

            foreach(JProperty mapping in themeOverrides)
            {
                var key = mapping.Name;
                var value = (string)mapping.Value;

                var existingThemes = themesCollection.Count(r => r.CountryOfRisk == key);

                if(existingThemes > 0)
                {
                    var themesToOverride = themesCollection.FirstOrDefault(r => r.CountryOfRisk == key);
                    var themeBuilder = ((Themes)themesToOverride).ToBuilder();
                    themeBuilder.CountryOfRisk = key;
                    themeBuilder.Theme = value;
                    themesCollection.Remove(themesToOverride);
                    themesCollection.Add(themeBuilder.Build());
                    
                    continue;
                }

                var theme = new Themes.Builder
                {
                    CountryOfRisk = key,
                    Theme = value,
                    Region = string.Empty
                }.Build();

                themesCollection.Add(theme);

            }

        }

        #endregion

    }
}
