﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using System;
    using System.Data;
    using System.Threading.Tasks;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.DataCollections;
    using Npgsql;

    /// <summary>The get trade population request handler.</summary>
    public class GetTradePopulationRequestHandler : IRequestHandler<GetTradePopulationRequest, TradeCollection>
    {
        #region Fields

        /// <summary>The stored proc.</summary>
        private const string _storedProc = "select * from sp_GetTradePopulation('{0}')";

        /// <summary>The connection</summary>
        private readonly NpgsqlConnection _connection;

        #endregion


        #region Constructor

        /// <summary>Initialize an instance of the get trade population request handler.</summary>
        /// <param name="connection">The connection.</param>
        public GetTradePopulationRequestHandler(
            NpgsqlConnection connection)
        {
            _connection = connection;
        }

        #endregion

        #region Public Methods

        /// <summary>Handles the get trade population request.</summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="TradeCollection"/>.</returns>
        public TradeCollection Handle(GetTradePopulationRequest request)
        {
            try
            {
                // read nav data from 
                var queryString = string.Format(_storedProc, $"{request.Date:yyyy-MM-dd HH:mm:ss}");

                using (var cmd = new NpgsqlCommand(queryString, _connection))
                {
                    _connection.Open();
                    var output = CreateTradeCollection(cmd);
                    _connection.Close();
                    return output;
                }

            }
            catch (Exception e)
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
                var msg = e.InnerException == null ? e.Message : e.InnerException.Message;
                throw new Exception(msg);
            }
            finally
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
            }

        }

        #endregion

        #region Methods

        /// <summary>Creates trade collection.</summary>
        /// <param name="command">The command.</param>
        /// <returns>The <see cref="TradeCollection"/>.</returns>
        private TradeCollection CreateTradeCollection(NpgsqlCommand command)
        {
            var output = new TradeCollection();

            var rdr = command.ExecuteReader();

            while (rdr.Read())
            {
                var nav = new Trade.Builder
                {
                    TradeDate = rdr.GetDateTime(0),
                    Quantity = rdr.GetDouble(1),
                    Price = Convert.ToDouble(rdr.GetDecimal(2)),
                    Name = rdr.GetString(3),
                    //Ticker = rdr.GetString(4),
                    Description = rdr.GetString(5)
                }.Build();

                output.Add(nav);
            }

            return output;
        }

        #endregion

    }
}
