﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using System;
    using System.Data;
    using System.Collections.Generic;
    using System.Linq;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.DataAccess.Requests;
    using Npgsql;
    using System.Threading.Tasks;

    /// <summary>Get daily pnl data request handler.</summary>
    public class GetDailyPnLDataRequestHandler : IRequestHandler<GetPnLDataRequest, PlEntryCollection>
    {
        #region Fields

        /// <summary>The sql connection.</summary>
        private readonly NpgsqlConnection _connection;

        /// <summary>The themes table.</summary>
        private readonly ThemesCollection _themesTable;

        /// <summary>The theme mapper.</summary>
        private readonly Func<string, string, string, ThemesCollection, string> _themeMapper;

        #endregion


        #region Constructor

        /// <summary>Initialize an instance of the request handler.</summary>
        /// <param name="connection">The connection.</param>
        /// <param name="themesTable">The themes table.</param>
        /// <param name="themeMapper">The theme mapper.</param>
        public GetDailyPnLDataRequestHandler(
            NpgsqlConnection connection,
            ThemesCollection themesTable,
            Func<string, string, string, ThemesCollection, string> themeMapper)
        {
            _connection = connection ?? throw new ArgumentNullException(nameof(connection));
            _themesTable = themesTable;
            _themeMapper = themeMapper;
        }

        #endregion

        #region Public Methods

        /// <summary>Handles the request.</summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="DataTable"/>.</returns>
        public PlEntryCollection Handle(GetPnLDataRequest request)
        {
            try
            {
                var sqlQuery = $"select * from plentryrawotdata where tradedate = '{request.ValueDate:yyyy-MM-dd}' and log = (select Max(log) from plentryrawotdata where tradedate= '{request.ValueDate:yyyy-MM-dd}') order by tradeid asc";

                _connection.Open();

                var cmd = new NpgsqlCommand()
                {
                    Connection = _connection,
                    CommandText = sqlQuery
                };

                var rdr = cmd.ExecuteReader();

                var output = new PlEntryCollection();

                while (rdr.Read())
                {
                    var plEntry = new PlEntry.Builder {
                        CountryOfRisk = rdr.GetString(0) == string.Empty ? string.Empty : rdr.GetString(0),
                        Description = rdr.GetString(1) == string.Empty ? string.Empty : rdr.GetString(1),
                        UnderlierInfo = rdr.GetString(2) == string.Empty ? string.Empty : rdr.GetString(2),
                        Book = rdr.GetString(3) == string.Empty ? string.Empty : rdr.GetString(3),
                        ProductId = rdr.GetInt32(4),
                        TradeId = rdr.GetInt32(5),
                        AssetClass = rdr.GetString(6) == string.Empty ? string.Empty : rdr.GetString(6),
                        ParentFund = rdr.GetString(7) == string.Empty ? string.Empty : rdr.GetString(7),
                        TradeDate = rdr.GetDateTime(9),
                        PlMonth = Convert.ToDouble(rdr.GetDecimal(11)),
                        PlInception = Convert.ToDouble(rdr.GetDecimal(12)),
                        PlYtd = Convert.ToDouble(rdr.GetDecimal(13)),
                        PlWeek = Convert.ToDouble(rdr.GetDecimal(15)),
                        Currency = rdr.GetString(16) == string.Empty ? string.Empty : rdr.GetString(16),
                        PlDaily = Convert.ToDouble(rdr.GetDecimal(17)),
                        Theme = string.Empty
                    }.Build();

                    output.Add(plEntry);
                }

                if (_themeMapper != null)
                {
                    var tempOutput = new List<IPlEntry>(output);
                    output.Clear();
                    
                    foreach(PlEntry entry in tempOutput)
                    {
                        var theme = _themeMapper(entry.Book, entry.CountryOfRisk, entry.UnderlierInfo, _themesTable);
                        var builder = entry.ToBuilder();
                        builder.Theme = string.IsNullOrEmpty(theme) ? string.Empty : theme;
                        output.Add(builder.Build());
                    }

                    if(output.Count(p => p.Theme.ToLower() == "missing theme") > 0)
                        PaddMissingThemes(ref output);
                    
                }    
                
                FilterDuplicates(ref output);

                _connection.Close();

                return output;
            } catch (Exception e)
            {
                var exceptionMessage = string.IsNullOrEmpty(e.InnerException.Message) ? e.Message : e.InnerException.Message;
                throw new Exception(exceptionMessage);
            }
        }

        #endregion


        #region Methods

        /// <summary>Filters duplicates.</summary>
        /// <param name="dailyPnlTable">The daily pnl table.</param>
        private void FilterDuplicates(ref PlEntryCollection plEntryCollection)
        {
            var distinctDailyPnlTable = plEntryCollection.Select(pl => pl.ToString());

            var filterDistinctValues = new List<string>(distinctDailyPnlTable.Distinct());

            plEntryCollection.Clear();

            foreach (var plEntry in filterDistinctValues)
            {
                var splitData = plEntry.Split('<');
                var newPlEntry = new PlEntry.Builder {
                    CountryOfRisk = splitData[0] == string.Empty ? string.Empty : splitData[0],
                    Description = splitData[1] == string.Empty ? string.Empty : splitData[1],
                    UnderlierInfo = splitData[2] == string.Empty ? string.Empty : splitData[2],
                    Book = splitData[3] == string.Empty ? string.Empty : splitData[3],
                    Currency = splitData[4] == string.Empty ? string.Empty : splitData[4],
                    ProductId = Convert.ToInt32(splitData[5]),
                    TradeId = Convert.ToInt32(splitData[6]),
                    AssetClass = splitData[7] == string.Empty ? string.Empty : splitData[7],
                    ParentFund = splitData[8] == string.Empty ? string.Empty : splitData[8],
                    TradeDate = Convert.ToDateTime(splitData[9]),
                    PlInception = Convert.ToDouble(splitData[10]),
                    PlDaily = Convert.ToDouble(splitData[11]),
                    PlWeek = Convert.ToDouble(splitData[12]),
                    PlMonth = Convert.ToDouble(splitData[13]),
                    PlYtd = Convert.ToDouble(splitData[14]),
                    Theme = splitData[15] == string.Empty ? string.Empty : splitData[15]
                }.Build();

                plEntryCollection.Add(newPlEntry);
            }

        }
        
        /// <summary>Padds missing themes.</summary>
        /// <param name="plEntries">The pl entries.</param>
        private void PaddMissingThemes(ref PlEntryCollection plEntries)
        {

            var missingThemes = new List<IPlEntry>(plEntries.Where(p => p.Theme.ToLower() == "missing theme"));
            var entriesWithThemes = new List<IPlEntry>(plEntries.Where(p => p.Theme.ToLower() != "missing theme"));
            plEntries.Clear();

            var matchFound = false;

            foreach (var missingTheme in missingThemes)
            {
                var possibleMatches = entriesWithThemes.Count(e => e.Book.ToLower() == missingTheme.Book.ToLower() && e.Currency.ToLower() == missingTheme.Currency.ToLower());
                if (possibleMatches < 1)
                {
                    plEntries.Add(missingTheme);
                    continue;
                }

                var possibleMatch = entriesWithThemes.Where(e => e.Book.ToLower() == missingTheme.Book.ToLower() && e.Currency.ToLower() == missingTheme.Currency.ToLower());

                foreach (var match in possibleMatch)
                {
                    var matches = String.Compare(missingTheme.UnderlierInfo, match.UnderlierInfo, StringComparison.OrdinalIgnoreCase);
                    if (matches > 0)
                    {
                        var adjustedPlEntry = ((PlEntry)missingTheme).ToBuilder();
                        adjustedPlEntry.Theme = match.Theme;
                        plEntries.Add(adjustedPlEntry.Build());
                        matchFound = true;
                        break;
                    }
                }

                if (!matchFound)
                    plEntries.Add(missingTheme);
                matchFound = false;
            }

            plEntries.AddRange(entriesWithThemes);
        }

        #endregion


    }
}
