﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataAccess.Requests;
    using System.Data;
    using Npgsql;
    using System.Threading.Tasks;

    /// <summary>The get risk sensitivity request handler.</summary>
    public class GetRiskSensitivityRequestHandler : IRequestHandler<GetRiskSensitivityCollection, PortfolioRiskSensitivityCollection>
    {
        #region Fields

        /// <summary>The sql query string.</summary>
        private const string RiskSensitivityStoredProc = "select * from sp_RiskSensitivity('{0}','{1}','{2}','{3}')";

        /// <summary>The sql connection.</summary>
        private readonly NpgsqlConnection _connection;

        #endregion


        #region Constructor

        /// <summary>Initialize an instance of the get risk sensitivity requeest handler.</summary>
        /// <param name="connection">The connection.</param>
        public GetRiskSensitivityRequestHandler(
            NpgsqlConnection connection)
        {
            _connection = connection;
        }

        #endregion

        #region Public Methods

        /// <summary>Handles the request.</summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="PortfolioRiskSensitivityCollection"/>.</returns>
        public PortfolioRiskSensitivityCollection Handle(GetRiskSensitivityCollection request)
        {

            try
            {
                // need to clean up the mess below. Call a single stored proc which returns result sets rather than opening and closing connections
                var sqlQuery = CreateQuery(request, RiskSensitivityStoredProc);
                using (var cmd = new NpgsqlCommand(sqlQuery, _connection))
                {
                    _connection.Open();
                    var riskSensitivityCollection = GenerateRiskSensitivityCollection(cmd);
                    _connection.Close();
                    return riskSensitivityCollection;
                }

            }
            catch (Exception e)
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
                var msg = e.InnerException == null ? e.Message : e.InnerException.Message;
                throw new Exception(msg);
            }
            finally 
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
            }


        }

        #endregion

        #region Methods

        /// <summary>Creates query.</summary>
        /// <param name="requests">The request.</param>
        /// <param name="storedProc">The stored proc.</param>
        /// <returns>The query.</returns>
        private string CreateQuery(IEnumerable<GetRiskSensitivityRequest> requests, string storedProc)
        {
            var requestDates = requests.Select(r => $"{r.ValueDate:yyyy-MM-dd}");
            var requestEntities = requests.Select(r => r.Entity);
            var requestScenarioNames = requests.Select(r => r.ScenarioName);
            var requestRiskTypes = requests.Select(r => r.RiskType);

            var sqlQuery = string.Format(storedProc,
                "{" + $"{string.Join(",", requestDates)}" + "}",
                "{" + $"{string.Join(",", requestEntities)}" + "}",
                "{" + $"{string.Join(",", requestScenarioNames)}" + "}",
                "{" + $"{string.Join(",", requestRiskTypes)}" + "}");
            
            return sqlQuery;
        }

        /// <summary>Generates risk sensitivity collection.</summary>
        /// <param name="command">The npg sql command.</param>
        /// <returns>The <see cref="PortfolioRiskSensitivityCollection"/>.</returns>
        private PortfolioRiskSensitivityCollection GenerateRiskSensitivityCollection(NpgsqlCommand command)
        {
            
            var rdr = command.ExecuteReader();

            try
            {
                var output = new PortfolioRiskSensitivityCollection();
                while (rdr.Read())
                {
                    var portfolioRiskSensitivity = new PortfolioRiskSensitivity.Builder
                    {
                        Underlier = rdr.GetString(0) == string.Empty ? string.Empty : rdr.GetString(0),
                        Book = rdr.GetString(1) == string.Empty ? string.Empty : rdr.GetString(1),
                        Entity = rdr.GetString(2) == string.Empty ? string.Empty : rdr.GetString(2),
                        Strategy = rdr.GetString(3) == string.Empty ? string.Empty : rdr.GetString(3),
                        AssetClass = rdr.GetString(4) == string.Empty ? string.Empty : rdr.GetString(4),
                        CountryOfRisk = rdr.GetString(5) == string.Empty ? string.Empty : rdr.GetString(5),
                        CurveCurrency = rdr.GetString(6) == string.Empty ? string.Empty : rdr.GetString(6),
                        UnderlyingTenor = rdr.GetString(7) == string.Empty ? string.Empty : rdr.GetString(7),
                        ScenarioName = rdr.GetString(8) == string.Empty ? string.Empty : rdr.GetString(8),
                        RiskType = rdr.GetString(9) == string.Empty ? string.Empty : rdr.GetString(9),
                        ProductType = rdr.GetString(10) == string.Empty ? string.Empty : rdr.GetString(10),
                        Delta = Convert.ToDouble(rdr.GetDecimal(11)),
                        DeltaBase = Convert.ToDouble(rdr.GetDecimal(12)),
                        Vega = Convert.ToDouble(rdr.GetDecimal(13)),
                        PlBase = Convert.ToDouble(rdr.GetDecimal(14)),
                        ZSpread = Convert.ToDouble(rdr.GetDecimal(15))
                    }.Build();

                    output.Add(portfolioRiskSensitivity);
                }

                return output;
            }
            finally {
                rdr.Dispose();            
            }
        }

        #endregion

    }
}
