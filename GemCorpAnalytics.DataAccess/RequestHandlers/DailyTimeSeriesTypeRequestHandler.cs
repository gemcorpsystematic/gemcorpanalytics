﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.Enums;
    using Npgsql;


    /// <summary>The daily time series type request handler.</summary>
    public class DailyTimeSeriesTypeRequestHandler : IRequestHandler<GetTimeSeriesRequest, IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries>>
    {

        #region Fields

        /// <summary>The npg sql connection.</summary>
        private readonly NpgsqlConnection _connection;

        #endregion

        #region Constructor

        /// <summary>Initialize an instance of the daily time series request handler.</summary>
        /// <param name="connection">The connection.</param>
        public DailyTimeSeriesTypeRequestHandler(NpgsqlConnection connection)
        {
            _connection = connection;
        }

        #endregion

        #region Public Methods

        public IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> Handle(GetTimeSeriesRequest request)
        {

            var output = new Dictionary<ITimeSeriesRepositoryEntry, TimeSeries>();

            foreach (var name in request.Names)
            {
                
                // join the data point table with the instrument ids
                var sqlQuery = $"select df.code, df.bloomberg_code, df.instrument_id, df.feed_type, dp.date, dp.value, dp.data_point_type, df.source from data_feeds as df inner join data_points as dp on df.instrument_id = dp.instrument_id where df.code in ('{name.Key}')" +
                    $"and dp.data_point_type in ('{MapTimeSeriesAttributeToString(name.Value)}') and dp.date >='{request.FromDate:dd/MM/yyyy}' and dp.date <='{request.ToDate:dd/MM/yyyy}' order by dp.date";


                _connection.Open();

                var cmd = new NpgsqlCommand() { Connection = _connection };

                cmd.CommandText = sqlQuery;

                var timeSeriesPoints = new List<ITimeSeriesPoint>();
                var rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {

                    var ticker = string.IsNullOrEmpty(rdr.GetString(1)) ? string.Empty : rdr.GetString(1);
                    var timeStamp = rdr.GetDate(4);
                    var date = new DateTime(timeStamp.Year, timeStamp.Month, timeStamp.Day);
                    timeSeriesPoints.Add(new TimeSeriesPoint.Builder
                    {
                        Date = date,
                        Name = rdr.GetString(0),
                        Ticker = ticker,
                        Value = Convert.ToDouble(rdr.GetString(5)),
                        Currency = "", // it would be nice to have a currency
                        Contributor = rdr.GetString(7),
                        InstrumentId = rdr.GetInt32(2),
                        TimeSeriesAttribute = MapStringToTimeSeriesAttributes(rdr.GetString(6))
                    }.Build());
                }
                rdr.Close();

                var data = timeSeriesPoints.Where(p => p.Name == name.Key).ToList();

                if (data.Count > 0)
                {
                    var timeSeriesRepositoryEntry = new TimeSeriesRepositoryEntry(name.Key, name.Value);
                    output[timeSeriesRepositoryEntry] = new TimeSeries(data);
                }

                _connection.Close();
            }
            return output;
        }

        #endregion

        #region Methods

        /// <summary>Maps time series attribute to string.</summary>
        /// <param name="timeSeriesAttributes">The time series attribute.</param>
        /// <returns>The mapped string.</returns>
        private string MapTimeSeriesAttributeToString(TimeSeriesAttributes timeSeriesAttributes)
        {
            switch(timeSeriesAttributes)
            {
                case TimeSeriesAttributes.Close:
                case TimeSeriesAttributes.High:
                case TimeSeriesAttributes.Low:
                case TimeSeriesAttributes.Open:
                case TimeSeriesAttributes.Volume:
                case TimeSeriesAttributes.Aum:
                case TimeSeriesAttributes.Nav:
                case TimeSeriesAttributes.Shares_Outstanding:
                case TimeSeriesAttributes.Short_Interest:
                case TimeSeriesAttributes.Volatility_180D:
                case TimeSeriesAttributes.Volatility_180D_ImpVol_100_Mny_Df:
                case TimeSeriesAttributes.Volatility_30D:
                case TimeSeriesAttributes.Volatility_30D_ImpVol_100_Mny_Df:
                case TimeSeriesAttributes.Volatility_30D_50Delta:
                case TimeSeriesAttributes.Volatility_60D:
                case TimeSeriesAttributes.Volatility_60D_ImpVol_100_Mny_Df:
                case TimeSeriesAttributes.Volatility_90D:
                case TimeSeriesAttributes.Z_Sprd_Mid:
                    return timeSeriesAttributes.ToString().ToUpper();
                case TimeSeriesAttributes.OHLC:
                    return "OPEN, HIGH, LOW, CLOSE";
                default:
                    throw new NotImplementedException($"Unable to handle attribute type: {timeSeriesAttributes}");
            }

        }

        /// <summary>Maps string to time series attributes.</summary>
        /// <param name="timeSeriesAttribute">The time series attribute.</param>
        /// <returns>The <see cref="TimeSeriesAttributes"/>.</returns>
        private TimeSeriesAttributes MapStringToTimeSeriesAttributes(string timeSeriesAttribute) 
        {
            switch(timeSeriesAttribute.ToLower())
            {
                case "close":
                    return TimeSeriesAttributes.Close;
                case "open":
                    return TimeSeriesAttributes.Open;
                case "high":
                    return TimeSeriesAttributes.High;
                case "low":
                    return TimeSeriesAttributes.Low;
                case "volume":
                    return TimeSeriesAttributes.Volume;
                case "aum":
                    return TimeSeriesAttributes.Aum;
                case "nav":
                    return TimeSeriesAttributes.Nav;
                case "shares_outstanding":
                    return TimeSeriesAttributes.Shares_Outstanding;
                case "short_interest":
                    return TimeSeriesAttributes.Short_Interest;
                case "z_sprd_mid":
                    return TimeSeriesAttributes.Z_Sprd_Mid;
                case "volatility_30d":
                    return TimeSeriesAttributes.Volatility_30D;
                case "call_1m_imp_vol_50delta":
                    return TimeSeriesAttributes.Volatility_30D_50Delta;
                case "volatility_30d_50delta":
                    return TimeSeriesAttributes.Volatility_30D_ImpVol_100_Mny_Df;
                case "volatility_60d":
                    return TimeSeriesAttributes.Volatility_60D;
                case "volatility_60d_impvol_100_mny_df":
                    return TimeSeriesAttributes.Volatility_60D_ImpVol_100_Mny_Df;
                case "volatility_180d":
                    return TimeSeriesAttributes.Volatility_180D;
                case "volatility_180d_impvol_100_mny_df":
                    return TimeSeriesAttributes.Volatility_180D_ImpVol_100_Mny_Df;
                case "volatility_90d":
                    return TimeSeriesAttributes.Volatility_90D;
                default:
                    throw new NotImplementedException($"Unable to match string to {timeSeriesAttribute} to TimeSeriesAttribute Enum.");
            }
        }

        #endregion

    }
}
