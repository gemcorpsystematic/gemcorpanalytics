﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using System;
    using System.Data;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.DataAccess.Requests;
    using Npgsql;
    using System.Threading.Tasks;

    /// <summary>The get nav request handler.</summary>
    public class GetNavRequestHandler : IRequestHandler<GetNavRequest, NavCollection>
    {

        #region Fields

        /// <summary>The stored proc.</summary>
        private const string _storedProc = "select * from sp_GetNAV('{0}')";

        /// <summary>The npg sql connection.</summary>
        private readonly NpgsqlConnection _connection;

        #endregion

        #region Constructor

        /// <summary>Initialize an instance of the get nav request handler.</summary>
        /// <param name="connection">The connection.</param>
        public GetNavRequestHandler(NpgsqlConnection connection)
        {
            _connection = connection;
        }

        #endregion

        #region Public Methods

        /// <summary>Handles the nav request.</summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="NavCollection"/>.</returns>
        public NavCollection Handle(GetNavRequest request)
        {
            try
            {
                // read nav data from 
                var queryString = string.Format(_storedProc, $"{request.Entity}");

                using (var cmd = new NpgsqlCommand(queryString, _connection))
                {
                    _connection.Open();
                    var output = CreateNavCollection(cmd);
                    _connection.Close();
                    return output;
                }

            }
            catch (Exception e)
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
                var msg = e.InnerException == null ? e.Message : e.InnerException.Message;
                throw new Exception(msg);
            }
            finally {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
            }
            
        }

        #endregion

        #region Methods

        /// <summary>Creates the nav collection.</summary>
        /// <param name="command">The command.</param>
        /// <returns>The <see cref="NavCollection"/>.</returns>
        private NavCollection CreateNavCollection(NpgsqlCommand command)
        {
            var output = new NavCollection();

            var rdr = command.ExecuteReader();

            while (rdr.Read())
            {
                var nav = new Nav.Builder {
                    Date = rdr.GetDateTime(0),
                    Entity = rdr.GetString(1),
                    Amount = Convert.ToDouble(rdr.GetDecimal(2))
                }.Build();

                output.Add(nav);
            }

            return output;
        }

        #endregion

    }
}
