﻿namespace GemCorpAnalytics.DataAccess.RequestHandlers
{
    using System;
    using System.Data;
    using System.Threading.Tasks;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.DataCollections;
    using Npgsql;

    /// <summary>The stress test request handler.</summary>
    public class GetStressTestRequestHandler : IRequestHandler<GetStressTestRequest, StressTestCollection>
    {

        #region Fields

        /// <summary>The sql sonnection.</summary>
        private readonly NpgsqlConnection _connection;

        /// <summary>The stored procedure</summary>
        private const string _storedProc = "select * from sp_getscenarios()";

        #endregion

        #region Constructor
    
        /// <summary>The stress test request handler.</summary>
        /// <param name="connection">The connection.</param>
        public GetStressTestRequestHandler(
            NpgsqlConnection connection)
        {
            _connection = connection;
        }

        #endregion

        #region Public Methods

        /// <summary>Handles the stress test request.</summary>
        /// <param name="request">The request.</param>
        /// <returns>The <see cref="StressTestCollection"/>.</returns>
        public StressTestCollection Handle(GetStressTestRequest request)
        {
            try
            {
                using(var cmd = new NpgsqlCommand(_storedProc, _connection))
                {
                    _connection.Open();
                    var output = CreateCollection(cmd, request.ValueDate);
                    _connection.Close();
                    return output;
                }
            }
            catch (Exception e)
            {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
                var msg = e.InnerException == null ? e.Message : e.InnerException.Message;
                throw new Exception(msg);
            }
            finally {
                if (_connection.State == ConnectionState.Open)
                    _connection.Close();
            }
        }

        #endregion

        #region Methods

        /// <summary>Creates the stress test collection.</summary>
        /// <param name="cmd">The command.</param>
        /// <param name="valueDate">The value date.</param>
        /// <returns>The <see cref="StressTestCollection"/>.</returns>
        private StressTestCollection CreateCollection(NpgsqlCommand cmd, DateTime valueDate)
        {

            var rdr = cmd.ExecuteReader();

            var output = new StressTestCollection();

            while(rdr.Read())
            {
                var stressTest = new StressTest.Builder
                {
                    Name = rdr.GetString(0),
                    StartDate = rdr.GetDateTime(1),
                    EndDate = rdr.GetDateTime(2),
                    ValueDate = valueDate
                }.Build();
                output.Add(stressTest);
            }

            return output;
        }

        #endregion

    }
}
