﻿namespace GemCorpAnalytics.DataAccess.Services
{
    using GemCorpAnalytics.DataAccess.RequestHandlers;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;


    /// <summary>The time series service.</summary>
    public class TimeSeriesService : ITimesSeriesService
    {

        #region Fields

        /// <summary>The request handler.</summary>
        private readonly IRequestHandler<GetTimeSeriesRequest, IReadOnlyDictionary<string, TimeSeries>> _requestHandler;

        #endregion

        #region Constructor

        /// <summary>Initialize an instance of the time series service.</summary>
        /// <param name="requestHandler">The request handler.</param>
        public TimeSeriesService(
            IRequestHandler<GetTimeSeriesRequest, IReadOnlyDictionary<string, TimeSeries>> requestHandler)
        {
            _requestHandler = requestHandler;
        }

        #endregion

        #region Public Methods

        /// <summary>Gets the time series.</summary>
        /// <param name="ticker">The ticker.</param>
        /// <param name="fromDate">The from date.</param>
        /// <param name="toDate">The to date.</param>
        /// <returns>The <see cref="TimeSeries"/>.</returns>
        public IReadOnlyDictionary<string, TimeSeries> GetTimeSeries(
            IEnumerable<KeyValuePair<string, TimeSeriesAttributes>> ticker, 
            DateTime? fromDate, 
            DateTime? toDate)
        {
            try
            {
                var request = new GetTimeSeriesRequest(fromDate, toDate);
                request.Names.ToList().AddRange(ticker);
                return _requestHandler.Handle(request);
            }catch(Exception e)
            {
                var errMsg = e.InnerException == null ? e.Message : e.InnerException.Message;
                throw new Exception(errMsg);
            }

        }

        #endregion
    }
}
