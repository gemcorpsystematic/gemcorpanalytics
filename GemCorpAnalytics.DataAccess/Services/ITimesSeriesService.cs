﻿namespace GemCorpAnalytics.DataAccess.Services
{
    using System;
    using System.Collections.Generic;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Enums;
    
    /// <summary>The times series service.</summary>
    public interface ITimesSeriesService
    {
        /// <summary>Gets the time series.</summary>
        /// <param name="ticker">The ticker.</param>
        /// <param name="fromDate">The from date.</param>
        /// <param name="toDate">The to date.</param>
        /// <returns>The <see cref="TimeSeries"/>.</returns>
        IReadOnlyDictionary<string, TimeSeries> GetTimeSeries(
            IEnumerable<KeyValuePair<string, TimeSeriesAttributes>> ticker, 
            DateTime? fromDate, 
            DateTime? toDate);

    }
}
