﻿namespace GemCorpAnalytics.DataAccess.Requests
{
    using System;
    
    /// <summary>The get trade population request.</summary>
    public class GetTradePopulationRequest : IRequest
    {

        #region Constructor

        /// <summary>Initialize an instance of the get trade population request.</summary>
        /// <param name="date">The date.</param>
        public GetTradePopulationRequest(
            DateTime date)
        {
            if (date == null) throw new ArgumentNullException(nameof(date));
            Date = date;
        }

        #endregion

        #region Public Properties

        /// <summary>The date.</summary>
        public DateTime Date { get; protected set; }

        #endregion

    }
}
