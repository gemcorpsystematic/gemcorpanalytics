﻿namespace GemCorpAnalytics.DataAccess.Requests
{
    using System;
    
    /// <summary>Gets the pnl data request.</summary>
    public class GetPnLDataRequest : IRequest
    {

        #region Constructor

        /// <summary>Initialise an instance of the get pnl data request.</summary>
        /// <param name="valuedate">The value date.</param>
        public GetPnLDataRequest(DateTime? valuedate=null)
        {
            ValueDate = valuedate;
        }

        #endregion


        #region Public Properties

        /// <summary>The value date.</summary>
        public DateTime? ValueDate { get; }

        #endregion

    }
}
