﻿namespace GemCorpAnalytics.DataAccess.Requests
{
    using System;

    /// <summary>The risk sensitivity request.</summary>
    public class GetRiskSensitivityRequest : IRequest
    {

        #region Constructor
        
        /// <summary>Initialize an instance of the get risk sensitivity request.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="underlierInfo">The underlier info.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="scenarioName">The scenario name.</param>
        /// <param name="riskType">The risk type.</param>
        public GetRiskSensitivityRequest(
            DateTime valueDate,
            string underlierInfo,
            string entity,
            string scenarioName,
            string riskType)
        {
            ValueDate = valueDate;
            UnderlierInfo = underlierInfo;
            Entity = entity;
            ScenarioName = scenarioName;
            RiskType = riskType;
        }

        #endregion

        #region Public Properties

        /// <summary>The value date.</summary>
        public DateTime ValueDate { get; }

        /// <summary>The underlier info.</summary>
        public string UnderlierInfo { get; }

        /// <summary>The entity.</summary>
        public string Entity { get; }

        /// <summary>The scenario name.</summary>
        public string ScenarioName { get; }

        /// <summary>The risk type.</summary>
        public string RiskType { get; }

        #endregion

    }
}
