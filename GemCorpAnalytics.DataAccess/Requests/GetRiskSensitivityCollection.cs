﻿namespace GemCorpAnalytics.DataAccess.Requests
{
    using System.Collections.Generic;

    /// <summary>The risk sensitivity collection.</summary>
    public class GetRiskSensitivityCollection : List<GetRiskSensitivityRequest>, IRequest
    {
    }
}
