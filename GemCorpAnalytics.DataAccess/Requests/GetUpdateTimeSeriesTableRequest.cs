﻿namespace GemCorpAnalytics.DataAccess.Requests
{
    using Data;

    /// <summary>The update time series table request.</summary>
    public class GetUpdateTimeSeriesTableRequest : IRequest
    {

        #region Constructor

        /// <summary>Initialise an instance of the update time series table request.</summary>
        /// <param name="timeSeries"></param>
        public GetUpdateTimeSeriesTableRequest(
            TimeSeries timeSeries)
        {
            TimeSeries = timeSeries;
        }

        #endregion

        #region Public Properties

        /// <summary>The time series.</summary>
        public TimeSeries TimeSeries { get; }

        #endregion

    }
}
