﻿
namespace GemCorpAnalytics.DataAccess.Requests
{

    using System;

    /// <summary>The stress test request.</summary>
    public class GetStressTestRequest : IRequest
    {

        #region Constrcutor

        /// <summary>Initialize an instance of the get stress test request.</summary>
        /// <param name="valueDate">The value date.</param>
        public GetStressTestRequest(
            DateTime valueDate)
        {
            ValueDate = valueDate;
        }

        #endregion

        #region Public Properties

        /// <summary>The value date.</summary>
        public DateTime ValueDate { get; }

        #endregion

    }
}
