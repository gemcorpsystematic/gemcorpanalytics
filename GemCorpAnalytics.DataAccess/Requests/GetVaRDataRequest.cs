﻿namespace GemCorpAnalytics.DataAccess.Requests
{

    using System;

    public class GetVaRDataRequest : IRequest
    {

        #region Constructor

        /// <summary>Initialize an instance of the get var data request.</summary>
        /// <param name="valueDate">The value date.</param>
        public GetVaRDataRequest(DateTime? valueDate)
        {
            ValueDate = valueDate;
        }

        #endregion

        #region Public Properties

        /// <summary>The value date.</summary>
        public DateTime? ValueDate { get; }

        #endregion

    }
}
