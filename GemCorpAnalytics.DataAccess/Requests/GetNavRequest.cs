﻿namespace GemCorpAnalytics.DataAccess.Requests
{
    using System;
    
    /// <summary>Gets the nav request.</summary>
    public class GetNavRequest : IRequest
    {

        #region Constructor

        /// <summary>Initialize an instance of the get nav request.</summary>
        /// <param name="date">The date.</param>
        /// <param name="entity">The entity.</param>
        public GetNavRequest(
            DateTime date, 
            string entity)
        {
            Date = date;
            Entity = entity;
        }

        #endregion

        #region Public Properties

        /// <summary>The date.</summary>
        public DateTime Date { get; }

        /// <summary>The entity.</summary>
        public string Entity { get; }

        #endregion

    }
}
