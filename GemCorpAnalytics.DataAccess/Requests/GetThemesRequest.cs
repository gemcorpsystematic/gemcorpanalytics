﻿namespace GemCorpAnalytics.DataAccess.Requests
{
    public class GetThemesRequest : IRequest
    {
        #region Constructor

        /// <summary>Initialize an instance of the get themes request.</summary>
        /// <param name="mappingFileConfig">The mapping file config.</param>
        public GetThemesRequest(
            string mappingFileConfig)
        {
            MappingFileConfig = mappingFileConfig;
        }

        #endregion

        #region Public Properties

        /// <summary>The mapping file config.</summary>
        public string MappingFileConfig { get; }

        #endregion

    }
}
