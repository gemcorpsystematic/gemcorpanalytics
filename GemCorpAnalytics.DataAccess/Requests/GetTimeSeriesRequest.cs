﻿namespace GemCorpAnalytics.DataAccess.Requests
{
    using System;
    using System.Collections.Generic;
    using GemCorpAnalytics.Enums;

    /// <summary>The get time series request.</summary>
    public class GetTimeSeriesRequest : IRequest
    {

        #region Constructor

        /// <summary>Initialize an instance of the get time series request.</summary>
        /// <param name="fromDate">The from date.</param>
        /// <param name="toDate">The to date.</param>
        /// <param name="timeSeriesType">The time series type.</param>
        /// <param name="timeSeriesAttributes">The time series attributes.</param>
        public GetTimeSeriesRequest(
            DateTime? fromDate=null, 
            DateTime? toDate=null,
            TimeSeriesType timeSeriesType = TimeSeriesType.Daily,
            TimeSeriesAttributes timeSeriesAttributes = TimeSeriesAttributes.OHLC)
        {
            Names = new List<KeyValuePair<string, TimeSeriesAttributes>>();
            FromDate = fromDate ?? DateTime.MinValue;
            ToDate = toDate ?? DateTime.MaxValue;
            TimeSeriesType = timeSeriesType;
        }

        #endregion

        #region Public Properties

        /// <summary>The name.</summary>
        public List<KeyValuePair<string, TimeSeriesAttributes>> Names { get; }

        /// <summary>The from date.</summary>
        public DateTime FromDate { get; }

        /// <summary>The to date.</summary>
        public DateTime ToDate { get; }

        /// <summary>The time series type.</summary>
        public TimeSeriesType TimeSeriesType { get; }


        #endregion

    }
}
