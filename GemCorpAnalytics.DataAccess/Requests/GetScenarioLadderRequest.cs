﻿namespace GemCorpAnalytics.DataAccess.Requests
{
    using System;
    
    /// <summary>Get the scenario ladder request.</summary>
    public class GetScenarioLadderRequest : IRequest
    {

        #region Constructor

        /// <summary>Initialize an instance of the get scenario ladder request.</summary>
        /// <param name="date">The date.</param>
        /// <param name="entity">The entity.</param>
        public GetScenarioLadderRequest(
            DateTime[] date,
            string[] entity)
        {
            Date = date;
            Entity = entity;
        }

        #endregion


        #region Public Properties

        /// <summary>The date.</summary>
        public DateTime[] Date { get; }

        /// <summary>The strategy.</summary>
        public string[] Entity { get; }

        #endregion

    }
}
