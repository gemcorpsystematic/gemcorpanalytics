﻿namespace GemCorpAnalytics.DataAccess.ContextBuilders
{
    using System;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.Enums;
    using Newtonsoft.Json.Linq;
    using System.IO;
    using System.Collections.Generic;


    public static class PlByThemeContextBuilder
    {

        public static PlByThemeContext Build(
            DateTime valueDate,
            string environment)
        {
            var configFile = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\PlBookMappings.json"));

            
            var fundConfig = (JObject)configFile["Fund"];
            var fundConfigMacro = CreateList((JArray)fundConfig["Macro"]);
            var fundConfigPublicCredit = CreateList((JArray)fundConfig["Public Credit"]);
            var fundConfigPrivateCredit = CreateList((JArray)fundConfig["Private Credit"]);
            var fundConfigSystematic = CreateList((JArray)fundConfig["Systematic"]);
            var multiConfig = (JObject)configFile["Multi"];
            var multiConfigMacro = CreateList((JArray)multiConfig["Macro"]);
            var multiConfigPublicCredit = CreateList((JArray)multiConfig["Public Credit"]);
            var multiConfigPrivateCredit = CreateList((JArray)multiConfig["Private Credit"]);
            var africaConfig = (JObject)configFile["Africa"];
            var africaConfigPublicCredit = CreateList((JArray)africaConfig["Public Credit"]);
            var africaConfigPrivateCredit = CreateList((JArray)africaConfig["Private Credit"]);
            var macroConfig = (JObject)configFile["Macro"];
            var macroConfigBooks = CreateList((JArray)macroConfig["Macro"]);

            var fundConfigs = new Dictionary<FundNames, Dictionary<FundStrategies, IReadOnlyList<string>>>();

            fundConfigs[FundNames.Fund] = new Dictionary<FundStrategies, IReadOnlyList<string>> {
                { FundStrategies.Macro, fundConfigMacro},
                { FundStrategies.PublicCredit, fundConfigPublicCredit},
                { FundStrategies.PrivateCredit, fundConfigPrivateCredit},
                { FundStrategies.Systematic, fundConfigSystematic},
            };

            fundConfigs[FundNames.Multi] = new Dictionary<FundStrategies, IReadOnlyList<string>> {
                { FundStrategies.Macro, multiConfigMacro},
                { FundStrategies.PublicCredit, multiConfigPublicCredit},
                { FundStrategies.PrivateCredit, multiConfigPrivateCredit}
            };

            fundConfigs[FundNames.Africa] = new Dictionary<FundStrategies, IReadOnlyList<string>> {
                { FundStrategies.PublicCredit, africaConfigPublicCredit},
                { FundStrategies.PrivateCredit, africaConfigPrivateCredit}
            };

            fundConfigs[FundNames.Macro] = new Dictionary<FundStrategies, IReadOnlyList<string>>
            {
                { FundStrategies.Macro, macroConfigBooks}
            };

            return new PlByThemeContext.Builder { 
                ValueDate = valueDate,
                Environment = environment,
                FundConfigs = fundConfigs
            }.Build();
        }

        private static List<string> CreateList(JArray values)
        {
            var output = new List<string>();

            foreach(JValue val in values)
            {
                output.Add(Convert.ToString(val));
            }

            return output;
        }

    }
}
