﻿namespace GemCorpAnalytics.DataAccess.Utils
{
    using System;
    using GemCorpAnalytics.Enums;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataAccess.Requests;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>The utils.</summary>
    public static class Utils
    {

        /// <summary>Gets the time series mapping.</summary>
        /// <param name="assetClass">The asset class.</param>
        /// <param name="name">The name.</param>
        /// <param name="timeSeriesMapping">The time series mapping.</param>
        /// <returns></returns>
        public static string TimeSeriesMapping(AssetClass assetClass, string name, string timeSeriesMapping)
        {
            switch (assetClass)
            {
                case AssetClass.Fx:
                    var suffix = timeSeriesMapping.ToLower().Contains("spot") ? "" : "_SPOT";
                    return timeSeriesMapping == "EURCZK" ? timeSeriesMapping : $"{timeSeriesMapping}{suffix}";
                case AssetClass.Equity:

                    var equityTimeSeriesMapping = timeSeriesMapping;

                    if (name.ToLower().Contains("emb"))
                        return "EMB_US";

                    if (name.ToLower().Contains("qqq"))
                        return "QQQ_US";

                    if (name.ToLower().Contains("bond etf"))
                        return "TLT";

                    if (name.ToLower().Contains("euro stoxx"))
                        return "VG1";

                    return equityTimeSeriesMapping;
                case AssetClass.Credit:
                    var mapping = timeSeriesMapping;
                    if (name.ToLower() == "colombia")
                        mapping = "CDS_COL_5Y";
                    if (name.ToLower() == "turkey")
                        mapping = "CDS_TUR_5Y";
                    if (name.ToLower().Contains("south africa"))
                        mapping = "CDS_REPSOU_5Y";
                    return mapping;
                case AssetClass.Interest:
                    var interestMapping = timeSeriesMapping;

                    if (name.ToLower().Contains("brl"))
                        interestMapping = "OD9";

                    if (name.ToLower().Contains("treasury"))
                        interestMapping = "USGG10YR";

                    if (name.ToLower().Contains("cpi"))
                        interestMapping = "USSWIT10";

                    if (name.ToLower().Contains("bund"))
                        interestMapping = "GDBR10";

                    return interestMapping;

                case AssetClass.Commodity:
                    var commodityInterestMapping = timeSeriesMapping;

                    if (name.ToLower().Contains("gold"))
                        commodityInterestMapping = "GC1";

                    return commodityInterestMapping;

                default:
                    return timeSeriesMapping;

            }
        }


        public static TimeSeriesAttributes TimeSeriesAttributeMapping(AssetClass assetClass, RiskGreekDirection riskGreekDirection)
        {
            switch(riskGreekDirection)
            {
                case RiskGreekDirection.Delta:
                    switch (assetClass) 
                    {
                        case AssetClass.BondSpread:
                            return TimeSeriesAttributes.Z_Sprd_Mid;
                        case AssetClass.Interest:
                            return TimeSeriesAttributes.Close;
                        case AssetClass.Commodity:
                            return TimeSeriesAttributes.Close;
                        case AssetClass.Equity:
                            return TimeSeriesAttributes.Close;
                        case AssetClass.Fx:
                            return TimeSeriesAttributes.Close;
                        case AssetClass.Credit:
                            return TimeSeriesAttributes.Close;
                        default:
                            throw new Exception($"Unable to hande asset class: {assetClass}");
                    }
                case RiskGreekDirection.Vega:
                    switch (assetClass)
                    {
                        case AssetClass.BondSpread:
                            return TimeSeriesAttributes.Volatility_30D;
                        case AssetClass.Interest:
                            return TimeSeriesAttributes.Volatility_30D;
                        case AssetClass.Commodity:
                            return TimeSeriesAttributes.Volatility_30D;
                        case AssetClass.Equity:
                            return TimeSeriesAttributes.Volatility_30D;
                        case AssetClass.Fx:
                            return TimeSeriesAttributes.Volatility_30D;
                        case AssetClass.Credit:
                            return TimeSeriesAttributes.Volatility_30D;
                        default:
                            throw new Exception($"Unable to hande asset class: {assetClass}");
                    }
                default:
                    throw new Exception($"Unable to handle risk direction: {riskGreekDirection}");
            }
        }

        /// <summary>The time series request builder.</summary>
        /// <param name="riskDirections">The risk directions.</param>
        /// <param name="fromDate">The from date.</param>
        /// <param name="toDate">The to date.</param>
        /// <param name="timeSeriesType">The time series type.</param>
        /// <param name="timeSeriesAttribute">The time series attribute.</param>
        /// <returns>The <see cref="GetTimeSeriesRequest"/>.</returns>
        public static GetTimeSeriesRequest TimeSeriesRequestBuilder(
            IEnumerable<RiskDirection> riskDirections, 
            DateTime fromDate, 
            DateTime toDate, 
            TimeSeriesType timeSeriesType,
            TimeSeriesAttributes timeSeriesAttribute)
        {

            var timeSeriesRequests = new List<KeyValuePair<string, TimeSeriesAttributes>>();

            foreach(var riskDirection in riskDirections)
            {
                // time series name, and attribute
                var timeSeriesMapping = TimeSeriesMapping(riskDirection.AssetClass, riskDirection.Name, riskDirection.TimeSeriesMapping);
                if (string.IsNullOrEmpty(timeSeriesMapping)) continue;
                var tsAttribute = TimeSeriesAttributeMapping(riskDirection.AssetClass, riskDirection.RiskGreekDirection);
                timeSeriesRequests.Add(new KeyValuePair<string, TimeSeriesAttributes>(timeSeriesMapping, tsAttribute));
            }

            var request = new GetTimeSeriesRequest(fromDate, toDate, timeSeriesType, timeSeriesAttribute);
            request.Names.AddRange(timeSeriesRequests);
            return request;
        }

        /// <summary>Parses asset class.</summary>
        /// <param name="assetClass">The asset class.</param>
        /// <returns>The asset class.</returns>
        public static AssetClass ParseAssetClass(string assetClass)
        {
            switch(assetClass.ToLower())
            {
                case "spread":
                case "bondspread":
                    return AssetClass.BondSpread;
                case "credit":
                    return AssetClass.Credit;
                case "commodity":
                    return AssetClass.Commodity;
                case "equity":
                    return AssetClass.Equity;
                case "fx":
                    return AssetClass.Fx;
                case "interest":
                    return AssetClass.Interest;
                default:
                    throw new Exception($"Unable to handle the following string: {assetClass}");
            }
        }

    }
}
