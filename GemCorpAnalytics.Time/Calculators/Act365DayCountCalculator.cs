﻿namespace GemCorpAnalytics.Time.Calculators
{
    using GemCorpAnalytics.Time.Calendar;
    using System;

    /// <summary>The act 365 day count calculator.</summary>
    public class Act365DayCountCalculator : IDayCountCalculator
    {

        #region Fields

        /// <summary>The days in a year.</summary>
        private const double DaysInYear = 365.0;

        #endregion

        #region Public Methods

        /// <summary>Calculates year frac.</summary>
        /// <param name="d1">The date.</param>
        /// <param name="d2">The date.</param>
        /// <param name="calendar">The calendar.</param>
        /// <returns>The year frac.</returns>
        public double CalculateYearFrac(DateTime d1, DateTime d2, ICalendar calendar = null)
        {
            if (d2 < d1)
                throw new Exception($"{d1:yyyy/MM/dd} should be before {d2:yyyy/MM/dd}");

            if (d1 == d2) return 0.0;

            return (d2 - d1).TotalDays / DaysInYear;
        }

        #endregion
    }
}
