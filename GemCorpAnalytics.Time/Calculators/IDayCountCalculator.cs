﻿namespace GemCorpAnalytics.Time.Calculators
{
    using GemCorpAnalytics.Time.Calendar;
    using System;

    /// <summary>The day count calculator.</summary>
    public interface IDayCountCalculator
    {

        /// <summary>Calculates year frac.</summary>
        /// <param name="d1">The date.</param>
        /// <param name="d2">The date.</param>
        /// <param name="calendar">The calendar.</param>
        /// <returns>The year frac.</returns>
        double CalculateYearFrac(DateTime d1, DateTime d2, ICalendar calendar=null);

    }
}
