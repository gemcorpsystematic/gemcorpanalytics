﻿namespace GemCorpAnalytics.Time.Calculators
{
    using GemCorpAnalytics.Time.Calendar;
    using System;

    /// <summary>The act act day count calculator.</summary>
    public class ActActDayCountCalculator : IDayCountCalculator
    {

        #region Public Methods

        /// <summary>Calculates year frac.</summary>
        /// <param name="d1">The date.</param>
        /// <param name="d2">The date.</param>
        /// <param name="calendar">The calendar.</param>
        /// <returns>The year frac.</returns>
        public double CalculateYearFrac(DateTime d1, DateTime d2, ICalendar calendar = null)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
