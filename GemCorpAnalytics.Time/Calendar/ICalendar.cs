﻿namespace GemCorpAnalytics.Time.Calendar
{
    using System;

    /// <summary>The calendar.</summary>
    public interface ICalendar
    {
        /// <summary>Gets holidays.</summary>
        /// <returns>The date time.</returns>
        DateTime[] GetHolidays();

        /// <summary>Is business day.</summary>
        /// <param name="date">The date.</param>
        /// <returns>A flag to indicate if date is business day.</returns>
        bool IsBusinessDay(DateTime date);

    }
}
