﻿namespace GemCorpAnalytics.Factories
{
    using System;
    using GemCorpAnalytics.Enums;
    using GemCorpAnalytics.Interpolators;
    
    /// <summary>The interpolator factory.</summary>
    public static class InterpolatorFactory
    {

        /// <summary>Get interpolator.</summary>
        /// <param name="interpolationTypes">The interpolation types.</param>
        /// <returns>The interpolator.</returns>
        public static IInterpolator GetInterpolator(InterpolationTypes interpolationTypes)
        {
            switch(interpolationTypes)
            {
                case InterpolationTypes.Linear:
                    return new LinearInterpolator();
                default:
                    throw new Exception($"Unable to handle interpolation type: {interpolationTypes}");
            }
        }

    }
}
