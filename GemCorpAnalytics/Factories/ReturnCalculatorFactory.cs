﻿namespace GemCorpAnalytics.Factories
{
    using System;
    using GemCorpAnalytics.Enums;
    
    /// <summary>The return calculator factory.</summary>
    public static class ReturnCalculatorFactory
    {

        #region Methods

        /// <summary>The commodity delta return calculator.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="lastPrice">The last price.</param>
        /// <returns>The return.</returns>
        private static double CommodityReturnCalculatorDelta(double x, double y, double? lastPrice)
        {
            if (lastPrice == null)
                throw new ArgumentNullException(nameof(lastPrice));

            var ret = y / x;
            //var ret = y - x;

            return (lastPrice.Value * ret - lastPrice.Value);
            //return ret;
        }

        /// <summary>The commodity vega return calculator.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="lastPrice">The last price.</param>
        /// <returns>The return.</returns>
        private static double CommodityReturnCalculatorVega(double x, double y, double? lastPrice)
        {
            if (lastPrice == null)
                throw new ArgumentNullException(nameof(lastPrice));

            var ret = y / x - 1.0;

            return (lastPrice.Value * (1+ret)-lastPrice.Value);
        }


        /// <summary>The interest rate return calculator.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="lastPrice">The last price.</param>
        /// <returns>The return.</returns>
        private static double InterestRateReturnCalculatorDelta(double x, double y, double? lastPrice)
        {
            return (y - x) * 100.0;
        }

        /// <summary>The interest rate return calculator.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="lastPrice">The last price.</param>
        /// <returns>The return.</returns>
        private static double InterestRateReturnCalculatorVega(double x, double y, double? lastPrice)
        {
            return (y - x) * 100.0;
        }

        /// <summary>The bond spread return calculator delta.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="lastPrice">The last price.</param>
        /// <returns>The return.</returns>
        private static double BondSpreadReturnCalculatorDelta(double x, double y, double? lastPrice)
        {
            return y - x;
        }

        /// <summary>The bond spread return calculator vega.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="lastPrice">The last price.</param>
        /// <returns>The return.</returns>
        private static double BondSpreadReturnCalculatorVega(double x, double y, double? lastPrice)
        {
            return y - x;
        }

        /// <summary>The equity delta return calculator.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="lastPrice">The last price.</param>
        /// <returns>The return.</returns>
        private static double EquityReturnCalculatorDelta(double x, double y, double? lastPrice)
        {
            return y / x - 1.0;
        }

        /// <summary>The equity vega return calculator.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="lastPrice">The last price.</param>
        /// <returns>The return.</returns>
        private static double EquityReturnCalculatorVega(double x, double y, double? lastPrice)
        {
            if (lastPrice == null)
                throw new ArgumentNullException(nameof(lastPrice));

            var ret = y / x - 1.0;

            return (lastPrice.Value * (1 + ret) - lastPrice.Value);

        }

        /// <summary>The fx return calculator.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="lastPrice">The last price.</param>
        /// <returns>The return.</returns>
        private static double FxReturnCalculatorDelta(double x, double y, double? lastPrice)
        {
            return y / x - 1.0;
        }

        /// <summary>The fx vega return calculator.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="lastPrice">The last price.</param>
        /// <returns>The return.</returns>
        private static double FxReturnCalculatorVega(double x, double y, double? lastPrice)
        {
            if (lastPrice == null)
                throw new ArgumentNullException(nameof(lastPrice));

            var ret = y / x - 1.0;

            return (lastPrice.Value * (1 + ret) - lastPrice.Value) / 100.0;
        }

        #endregion

        #region Public Methods

        /// <summary>The get return calculator.</summary>
        /// <param name="assetClass">The asset class.</param>
        /// <param name="riskGreekDirection">The risk greek direction.</param>
        /// <returns>The return.</returns>
        public static Func<double, double, double?, double> GetReturnCalculator(AssetClass assetClass, RiskGreekDirection riskGreekDirection)
        {
            switch (riskGreekDirection) 
            {
                case RiskGreekDirection.Delta:
                    switch (assetClass)
                    {
                        case AssetClass.BondSpread:
                            return BondSpreadReturnCalculatorDelta;
                        case AssetClass.Commodity:
                            return CommodityReturnCalculatorDelta;
                        case AssetClass.Equity:
                            return EquityReturnCalculatorDelta;
                        case AssetClass.Interest:
                            return InterestRateReturnCalculatorDelta;
                        case AssetClass.Credit:
                        case AssetClass.Fx:
                            return FxReturnCalculatorDelta;
                        default:
                            throw new NotImplementedException($"Unable to handle return for {assetClass}");
                    }
                case RiskGreekDirection.Vega:
                    switch (assetClass)
                    {
                        case AssetClass.BondSpread:
                            return BondSpreadReturnCalculatorVega;
                        case AssetClass.Commodity:
                            return CommodityReturnCalculatorVega;
                        case AssetClass.Equity:
                            return EquityReturnCalculatorVega;
                        case AssetClass.Interest:
                            return InterestRateReturnCalculatorVega;
                        case AssetClass.Credit:
                        case AssetClass.Fx:
                            return FxReturnCalculatorVega;
                        default:
                            throw new NotImplementedException($"Unable to handle return for {assetClass}");
                    }
                default:
                    throw new NotImplementedException("");
            }
        }

        #endregion

    }
}
