﻿namespace GemCorpAnalytics.Calculators.Interfaces
{

    using GemCorpAnalytics.Models;

    /// <summary>The data summary calculator.</summary>
    public interface IDataSummaryCalculator
    {
        /// <summary>The price scenario.</summary>
        IPriceScenario PriceScenario { get; }

        /// <summary>The average value.</summary>
        double AverageValue { get; }

        /// <summary>The bucket size.</summary>
        double BucketSize { get; }

    }
}
