﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using System.Collections.Generic;

    /// <summary>The risk allocation calculator result.</summary>
    public interface IRiskAllocationCalculatorResult
    {

        /// <summary>The risk allocation.</summary>
        Dictionary<string, double> RiskAllocation { get; }
    }
}
