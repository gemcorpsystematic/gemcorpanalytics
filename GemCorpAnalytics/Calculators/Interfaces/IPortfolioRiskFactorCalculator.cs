﻿namespace GemCorpAnalytics.Calculators.Interfaces
{   
    /// <summary>The portfolio risk calculator.</summary>
    public interface IPortfolioRiskFactorCalculator
    {
        /// <summary>Calculates.</summary>
        /// <returns>The <see cref="IPortfolioRiskCalculatorResult"/>.</returns>
        IPortfolioRiskCalculatorResult Calculate();

    }
}
