﻿namespace GemCorpAnalytics.Calculators.Interfaces
{

    using GemCorpAnalytics.Data;

    public interface IRiskSensitivityTimeSeriesMapper
    {
        string MapTimeSeries(RiskDirection riskDirection);
    }
}
