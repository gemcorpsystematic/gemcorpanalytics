﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using GemCorpAnalytics.Data;
    using System;
    using System.Collections.Generic;
    
    /// <summary>The proxy time series calculator.</summary>
    public interface IProxyTimeSeriesCalculator
    {
        /// <summary>Calculates a proxy value, given a date.</summary>
        /// <param name="proxyDates">The proxy dates.</param>
        /// <param name="timeSeries">The time series.</param>
        /// <returns>The proxy date and proxy value.</returns>
        TimeSeries Calculate(IEnumerable<DateTime> proxyDates, TimeSeries timeSeries);

    }
}
