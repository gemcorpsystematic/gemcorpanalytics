﻿namespace GemCorpAnalytics.Calculators.Interfaces
{

    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.DataCollections;

    /// <summary>The stress test calculator.</summary>
    public interface IStressTestCalculator
    {

        /// <summary>Calculates the stress tests.</summary>
        /// <returns>The <see cref="IStressTestCalculatorResult"/>.</returns>
        StressTestCalculatorResultCollection Calculate(StressTestCalculatorContext context);

    }
}
