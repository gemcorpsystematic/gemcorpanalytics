﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    /// <summary>The curve interpolator.</summary>
    public interface ICurveInterpolator
    {

        /// <summary>Gets value.</summary>
        /// <param name="x">The x.</param>
        /// <returns>The interpolated value</returns>
        double GetValue(double x);

        /// <summary>Gets value.</summary>
        /// <param name="x">The x.</param>
        /// <returns>The interpolated value.</returns>
        double[] GetValue(double[] x);

    }
}
