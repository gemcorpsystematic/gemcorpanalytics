﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using GemCorpAnalytics.Data;
    
    /// <summary>The projected risk theme mapper.</summary>
    public interface IProjectedRiskThemeMapper
    {
        /// <summary>Gets theme.</summary>
        /// <param name="riskDirection">The risk direction.</param>
        /// <returns>The theme.</returns>
        string GetTheme(RiskDirection riskDirection);

    }
}
