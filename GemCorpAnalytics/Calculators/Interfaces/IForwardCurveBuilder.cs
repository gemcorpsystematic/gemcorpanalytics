﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using GemCorpAnalytics.Data.Interfaces;

    /// <summary>The forward curve builder interface.</summary>
    public interface IForwardCurveBuilder
    {
        /// <summary>Build the forward curve, as per Orchestrade.</summary>
        /// <param name="curve">The curve.</param>
        /// <param name="fwdRateTenor">The fwd rate tenor.</param>
        /// <param name="tenorCutOff">The tenor cut off.</param>
        /// <returns></returns>
        ICurve BuildForwardCurve(ICurve curve, double fwdRateTenor, double tenorCutOff);

        /// <summary>Removes outliers from curve.</summary>
        /// <param name="curve">The curve.</param>
        /// <param name="zScore">The zscore used for filtering out points.</param>
        /// <returns>The filtered out curve.</returns>
        ICurve RemoveOutliersFromCurve(ICurve curve, double zScore);

    }
}
