﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using System;
    using System.Collections.Generic;
    using GemCorpAnalytics.Data.Interfaces;

    /// <summary>The conso risk report summary calculator.</summary>
    public interface IConsoRiskReportSummaryCalculator
    {
        /// <summary>Calculate conso risk report summary.</summary>
        /// <param name="consoRiskReportResult">The conso risk report result.</param>
        /// <returns>The <see cref="IConsoRiskReportSummary"/>.</returns>
        IConsoRiskReportSummary Calculate(IConsoRiskReportResult consoRiskReportResult);

        /// <summary>Calculate the conso risk report summary using a filter.</summary>
        /// <param name="consoRiskReportResult"></param>
        /// <param name="books"></param>
        /// <param name="tradeFilter"></param>
        /// <returns></returns>
        IConsoRiskReportSummary CalculateUsingFilter(IConsoRiskReportResult consoRiskReportResult,
            List<string> books,
            Func<ITrade, bool> tradeFilter);

    }
}
