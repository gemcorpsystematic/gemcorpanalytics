﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using GemCorpAnalytics.Data.Interfaces;
    using System.Collections.Generic;
    using System.Data;
    
    /// <summary>The pl by theme calculator result.</summary>
    public interface IPlByThemeCalculatorResult
    {
        /// <summary>The results table.</summary>
        IList<DataTable> ResultsTable { get; }

        /// <summary>The themes collection.</summary>
        List<IThemes> ThemesCollection { get; }

        /// <summary>Trades with missing themes.</summary>
        List<IThemes> MissingThemes { get; }

    }
}
