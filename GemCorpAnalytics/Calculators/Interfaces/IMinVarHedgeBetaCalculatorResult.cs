﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using System;
    using System.Collections.Generic;
    using GemCorpAnalytics.Data;

    /// <summary>The minimum variance hedge result.</summary>
    public interface IMinVarHedgeBetaCalculatorResult
    {
        /// <summary>The value date.</summary>
        DateTime ValueDate { get; }

        /// <summary>The beta.</summary>
        Dictionary<string, double> Beta { get; }

        /// <summary>The r squared.</summary>
        Dictionary<string, double> RSquared { get; }

        /// <summary>The pl breakdown.</summary>
        Dictionary<RiskDirection, List<KeyValuePair<DateTime, double>>> PlBreakDown { get; }

        /// <summary>The pl series.</summary>
        Dictionary<DateTime, double> PlSeries { get; }

        /// <summary>The risk direction.</summary>
        IEnumerable<RiskDirection> RiskDirection { get; }

        /// <summary>The risk allocations.</summary>
        Dictionary<string, double> RiskAllocations { get; }

        /// <summary>The risk allocations r squared.</summary>
        Dictionary<string, double> RiskAllocationsRSquared { get; }

    }
}
