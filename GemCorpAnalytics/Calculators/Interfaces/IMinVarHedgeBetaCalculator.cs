﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using System;

    /// <summary>The minimum variance hedge beta calculator.</summary>
    /// <typeparam name="T">The result type.</typeparam>
    public interface IMinVarHedgeBetaCalculator<T> where T:IMinVarHedgeBetaCalculatorResult
    {
        /// <summary>Calcualate the minimum variance hedge.</summary>
        /// <param name="date">The date.</param>
        /// <param name="startDate">The start date.</param>
        /// <returns>The calculator result.</returns>
        T Calculate(DateTime date, DateTime startDate, double? lowerBound, double? upperBound);
    }
}
