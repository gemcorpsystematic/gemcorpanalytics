﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using System;
    
    /// <summary>The stress test calculator result.</summary>
    public interface IStressTestCalculatorResult
    {
        /// <summary>The value date.</summary>
        DateTime ValueDate { get; }

        /// <summary>The scenario name.</summary>
        string ScenarioName { get; }

        /// <summary>The underlier name.</summary>
        string UnderlierName { get; }

        /// <summary>The start date.</summary>
        DateTime StartDate { get; }

        /// <summary>The end date.</summary>
        DateTime EndDate { get; }

        /// <summary>The start value.</summary>
        double StartValue { get; }

        /// <summary>The end value.</summary>
        double EndValue { get; }

        /// <summary>The shock.</summary>
        double Shock { get; }

        /// <summary>The pl amount.</summary>
        double PlAmount { get; }

        /// <summary>The risk direction amount.</summary>
        double RiskDirectionAmount { get; }

        /// <summary>The asset class.</summary>
        string AssetClass { get; }

    }
}
