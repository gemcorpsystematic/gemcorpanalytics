﻿namespace GemCorpAnalytics.Calculators.Interfaces
{

    /// <summary>The volatility calculator.</summary>
    /// <typeparam name="T">The volatility calculator result.</typeparam>
    public interface IVolatilityCalculator<T> where T : IVolatilityCalculatorResult
    {
        /// <summary>Calculates the volatility.</summary>
        /// <returns>The volatility calculator result.</returns>
        T Calculate();
    }
}
