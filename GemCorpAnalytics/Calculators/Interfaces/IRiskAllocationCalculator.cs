﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    /// <summary>The risk allocation calculator.</summary>
    public interface IRiskAllocationCalculator
    {
        /// <summary>Calculates risk allocation.</summary>
        /// <param name="minVarHedgeBetaCalculatorResult">The min var hedge beta calculator result.</param>
        /// <returns>The <see cref="IRiskAllocationCalculatorResult"/>.</returns>
        IRiskAllocationCalculatorResult Calculate(IMinVarHedgeBetaCalculatorResult minVarHedgeBetaCalculatorResult);

    }
}
