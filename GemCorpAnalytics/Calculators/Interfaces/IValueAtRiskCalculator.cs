﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using System;

    /// <summary>The value at risk calculator.</summary>
    /// <typeparam name="T">The value at risk calculator result.</typeparam>
    public interface IValueAtRiskCalculator<T> where T:IValueAtRiskCalculatorResult
    {
        /// <summary>Calculate</summary>
        /// <param name="valueDate"></param>
        /// <returns></returns>
        T Calculate(DateTime valueDate);
    }
}
