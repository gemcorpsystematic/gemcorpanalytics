﻿namespace GemCorpAnalytics.Calculators.Interfaces
{

    /// <summary>The correlation calculator result.</summary>
    public interface ICorrelationCalculatorResult
    {
        /// <summary>The correlation.</summary>
        double Correlation { get; }
    }
}
