﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using GemCorpAnalytics.Enums;

    /// <summary>The pl by theme calculator.</summary>
    public interface IPlByThemeCalculator
    {
        /// <summary>Calcuates pl by theme</summary>
        /// <param name="fundName">The fund name.</param>
        /// <returns></returns>
        IPlByThemeCalculatorResult Calculate(FundNames fundName);        

    }
}
