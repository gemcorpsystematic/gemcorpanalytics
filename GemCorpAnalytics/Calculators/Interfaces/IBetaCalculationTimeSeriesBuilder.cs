﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using System;
    using System.Collections.Generic;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Context;

    /// <summary>The beta calculation time series builder.</summary>
    public interface IBetaCalculationTimeSeriesBuilder
    {
        /// <summary>Manipulates time series.</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="seriesToExclude">The series to exclude.</param>
        /// <param name="spreadSeriesMapping">The spread series mapping.</param>
        /// <returns>The <see cref="IReadOnlyDictionary{TKey, TValue}"/>.</returns>
        IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> GetTimeSeries(
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> timeSeries,
            DateTime startDate,
            DateTime endDate,
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, double>> seriesToExclude,
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, ITimeSeriesRepositoryEntry>> spreadSeriesMapping);

    }
}
