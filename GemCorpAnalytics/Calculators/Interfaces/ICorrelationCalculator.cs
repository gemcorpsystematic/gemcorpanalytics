﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    /// <summary>The correlation calculator result.</summary>
    /// <typeparam name="T">The correlation calculator result.</typeparam>
    public interface ICorrelationCalculator<T> where T:ICorrelationCalculatorResult
    {
        T Calculate();
    }
}
