﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using System;

    /// <summary>The value at risk calculator result.</summary>
    public interface IValueAtRiskCalculatorResult
    {
        /// <summary>The value date.</summary>
        DateTime ValueDate { get; }

        /// <summary>The value at risk.</summary>
        double ValueAtRisk { get; }

        /// <summary>The confidence level.</summary>
        double ConfidenceLevel { get; }

    }
}
