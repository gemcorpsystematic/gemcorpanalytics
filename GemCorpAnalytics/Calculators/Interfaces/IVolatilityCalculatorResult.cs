﻿namespace GemCorpAnalytics.Calculators.Interfaces
{

    /// <summary>The volatility calculator result.</summary>
    public interface IVolatilityCalculatorResult
    {
        /// <summary>The volatility.</summary>
        double Volatility { get; }
    }
}
