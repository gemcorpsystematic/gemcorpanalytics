﻿namespace GemCorpAnalytics.Calculators.Interfaces
{
    using System.Collections.Generic;
    using GemCorpAnalytics.Data;


    /// <summary>The portfolio risk calculator result.</summary>
    public interface IPortfolioRiskCalculatorResult
    {
        /// <summary>The fx delta.</summary>
        Dictionary<string, double> FxDelta { get; }

        /// <summary>The credit delta.</summary>
        Dictionary<string, double> CreditDelta { get; }

        /// <summary>The interest delta.</summary>
        Dictionary<string, double> InterestDelta { get; }

        /// <summary>The equity delta.</summary>
        Dictionary<string, double> EquityDelta { get; }

        /// <summary>The commodity delta.</summary>
        Dictionary<string, double> CommodityDelta { get; }

        /// <summary>The commodity vega.</summary>
        Dictionary<string, double> CommodityVega { get; }

        /// <summary>The risk direction collection.</summary>
        IEnumerable<RiskDirection> RiskDirectionCollection { get; }

    }
}
