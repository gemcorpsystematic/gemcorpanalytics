﻿namespace GemCorpAnalytics.Calculators
{
    using GemCorpAnalytics.Enums;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Calculators.Interfaces;
    using System.Data;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    

    /// <summary>The portfolio risk calculator.</summary>
    public class PortfolioRiskFactorCalculator : IPortfolioRiskFactorCalculator
    {

        #region Fields

        /// <summary>The risk sensitivity table.</summary>
        private readonly PortfolioRiskSensitivityCollection _riskSensitivityCollection;

        /// <summary>The risk time series mapper.</summary>
        private readonly IRiskSensitivityTimeSeriesMapper _riskTimeSeriesMapper;

        /// <summary>The risk type forex.</summary>
        private const string Forex = "forex";

        /// <summary>The risk type credit.</summary>
        private const string Credit = "credit";

        /// <summary>The risk type equity.</summary>
        private const string Equity = "equity";

        /// <summary>The equity volatility.</summary>
        private const string EquityVol = "equity volatility";

        /// <summary>The risk type interest.</summary>
        private const string Interest = "interest";

        /// <summary>The risk type commodity.</summary>
        private const string Commodity = "commodity";
        
        /// <summary>The risk type commodity volatility.</summary>
        private const string CommodityVol = "commodity volatility";

        /// <summary>The risk type bond spread</summary>
        private const string BondSpread = "bondspread";

        #endregion

        #region Constructor

        /// <summary>Initialise an instance of the risk sensitivity table.</summary>
        /// <param name="riskSensitivityCollection">The risk sensitivity collection.</param>
        /// <param name="riskTimeSeriesMapper">The risk time series mapper.</param>
        public PortfolioRiskFactorCalculator(
            PortfolioRiskSensitivityCollection riskSensitivityCollection,
            IRiskSensitivityTimeSeriesMapper riskTimeSeriesMapper)
        {
            _riskSensitivityCollection = riskSensitivityCollection;
            _riskTimeSeriesMapper = riskTimeSeriesMapper;
        }

        #endregion



        #region Public Methods

        /// <summary>Calculate.</summary>
        /// <returns>The <see cref="IPortfolioRiskCalculatorResult"/>.</returns>
        public IPortfolioRiskCalculatorResult Calculate()
        {
            
            Func<IEnumerable<PortfolioRiskSensitivity>, double> sumRiskDelta = r => r.Sum(s => s.Delta);

            Func<IEnumerable<PortfolioRiskSensitivity>, double> sumRiskDeltaBase = r => r.Sum(s => s.DeltaBase);

            Func<IEnumerable<PortfolioRiskSensitivity>, double> sumVol = r => r.Sum(s => s.Vega);
            
            Func<IEnumerable<PortfolioRiskSensitivity>, double> sumPlBase = r => r.Sum(s => s.PlBase);

            var riskSensitivityTypes = new[] { Forex, Credit, Equity, EquityVol, Interest, Commodity, CommodityVol, BondSpread};

            var output = new Dictionary<string, Dictionary<string, double>>();

            var outputRiskDirection = new List<RiskDirection>();

            var sensitivitiesByBook = new Dictionary<string, Dictionary<string, double>>();

            var distinctProductType = _riskSensitivityCollection.Select(r => r.ProductType).Distinct();

            foreach(var riskSensitivityType in riskSensitivityTypes)
            {
                var riskSensitivitiesCollections = _riskSensitivityCollection.Where(r => r.RiskType.ToLower() == riskSensitivityType);

                // take distinct values and sum deltas
                var riskSensitivities = riskSensitivitiesCollections.Select(r => r.Underlier).Distinct();

                var riskSensitivitiesByBook = riskSensitivitiesCollections.Select(r => r.Book).Distinct();

                var sumRisk = sumRiskDeltaBase;

                if (riskSensitivityType == CommodityVol || riskSensitivityType == EquityVol)
                    sumRisk = sumVol;

                if (riskSensitivityType == Interest || riskSensitivityType == Credit)
                    sumRisk = sumRiskDelta;

                if (riskSensitivityType == BondSpread)
                    sumRisk = sumPlBase;

                var riskDeltas = riskSensitivities.Select(r =>
                {
                    var sensitivityCollection = riskSensitivitiesCollections.Where(s => r == s.Underlier);
                    var delta = sumRisk(sensitivityCollection);
                    return new KeyValuePair<string, double>(r, delta);
                }).ToDictionary(x => x.Key, y => y.Value);

                var riskDeltasByBook = riskSensitivitiesByBook.Select(r =>
                {
                    var delta = sumRisk(riskSensitivitiesCollections.Where(s => r == s.Book));
                    return new KeyValuePair<string, double>(r, delta);
                }).ToDictionary(x => x.Key, y => y.Value);

                if(riskSensitivityType == Interest)
                {
                    // here we need to group by curve currency

                    var mappedCurrencies = riskSensitivitiesCollections.Select(r => r.CurveCurrency).Distinct();

                    var tmpDict = new Dictionary<string, double>();

                    foreach(var mappedUnderlier in mappedCurrencies)
                    {
                        tmpDict[mappedUnderlier] = riskSensitivitiesCollections.Where(c => c.CurveCurrency == mappedUnderlier).Sum(x => x.Delta);
                    }
                    output[riskSensitivityType] = tmpDict;

                    outputRiskDirection.AddRange(CreateRiskDirectionCollection(tmpDict, AssetClass.Interest, RiskGreekDirection.Delta));

                    continue;
                }

                if (riskSensitivityType == Equity || riskSensitivityType == EquityVol){

                    var filteredValues = FilterRiskType(riskDeltas, riskDeltasByBook, riskSensitivitiesCollections);
                    var equityRiskDict = MergeSensitivityValues(filteredValues, "EquityListedOption", "Equity");
                    var riskGreekDirection = riskSensitivityType == EquityVol ? RiskGreekDirection.Vega : RiskGreekDirection.Delta;
                    output[riskSensitivityType] = equityRiskDict;
                    outputRiskDirection.AddRange(CreateRiskDirectionCollection(equityRiskDict, AssetClass.Equity, riskGreekDirection));
                }
                else if(riskSensitivityType == Commodity || riskSensitivityType == CommodityVol)
                {
                    var commodityRiskDict = MergeSensitivityValues(riskDeltas, "CommodityFutureListedOption", "CommodityFuture");
                    var riskGreekDirection = riskSensitivityType == CommodityVol ? RiskGreekDirection.Vega : RiskGreekDirection.Delta;
                    outputRiskDirection.AddRange(CreateRiskDirectionCollection(commodityRiskDict, AssetClass.Commodity, riskGreekDirection));
                    output[riskSensitivityType] = commodityRiskDict;
                }
                else {

                    var assetClass = riskSensitivityType == Forex ? AssetClass.Fx : AssetClass.Credit;
                    outputRiskDirection.AddRange(CreateRiskDirectionCollection(riskDeltas, assetClass, RiskGreekDirection.Delta));
                    output[riskSensitivityType] = riskDeltas;
                }

                // sensitivities by book is not currently used anywhere
                sensitivitiesByBook[riskSensitivityType] = riskDeltasByBook;
            }

            //return new PortfolioRiskCalculatorResult(output[Credit], output[Forex], output[Equity], output[Interest], output[Commodity], output[CommodityVol]);
            return new PortfolioRiskCalculatorResult(outputRiskDirection);
        }

        #endregion

        #region Methods

        /// <summary>Filters the sensitivities by book or underlier depending on size.</summary>
        /// <param name="riskSensitivityByUnderlier">The risk sensitivity by underlier.</param>
        /// <param name="riskSensitivityByBook">The risk sensitivity by book.</param>
        /// <param name="riskSensitivityCollection">The risk sensitivity collection by risk type.</param>
        /// <returns>The filtered risk type.</returns>
        private Dictionary<string, double> FilterRiskType(
            Dictionary<string, double> riskSensitivityByUnderlier,
            Dictionary<string, double> riskSensitivityByBook,
            IEnumerable<PortfolioRiskSensitivity> riskSensitivityCollection)
        {
            var output = new Dictionary<string, double>();

            var totalByBook = 0.0;

            foreach(var val in riskSensitivityByBook.Values)
            {
                totalByBook += Math.Abs(val);
            }

            foreach(var book in riskSensitivityByBook.Keys)
            {
                if(Math.Abs(riskSensitivityByBook[book])/totalByBook > 0.25 && book != "Macro RV")
                {
                    output[book] = riskSensitivityByBook[book];
                }
            }

            if (output.Keys.Count() == 0) return riskSensitivityByUnderlier;

            var residualUnderliers = riskSensitivityCollection
                .Where(r => riskSensitivityByUnderlier.Keys.Contains(r.Underlier) && !output.Keys.Contains(r.Book))
                .Select(u => u.Underlier);

            
            foreach(var underlier in residualUnderliers)
            {
                output[underlier] = riskSensitivityByUnderlier[underlier];
            }

            return output;
        }


        private Dictionary<string, double> MergeSensitivityValues(Dictionary<string, double> riskSensitivity, string optionString, string nonOptionString)
        {
            var filtValueMapping = new List<Tuple<string, string>>();

            foreach (var filtValue in riskSensitivity.Keys)
            {
                var productType = filtValue.Contains("Call") || filtValue.Contains("Put") ? optionString : nonOptionString;
                var mapping = InstrumentParser.GetInstrumentString(filtValue, productType);
                filtValueMapping.Add(new Tuple<string, string>(filtValue, mapping));
            }
            var distinctMappings = filtValueMapping.Select(m => m.Item2).Distinct();

            var tmpDict = distinctMappings.Select(m => {
                var keys = filtValueMapping.Where(k => k.Item2 == m).Select(k => k.Item1);

                var sensitivity = 0.0;
                foreach (var key in keys)
                {
                    sensitivity += riskSensitivity[key];
                }

                return new KeyValuePair<string, double>(m, sensitivity);
            }).ToDictionary(x => x.Key, y => y.Value);

            return tmpDict;
        }


        /// <summary>Creates risk direction collection.</summary>
        /// <param name="riskValues">The risk values.</param>
        /// <param name="assetClass">The asset class.</param>
        /// <param name="riskGreekDirection">The risk greek direction.</param>
        /// <returns>The <see cref="IEnumerable{T}"/>.</returns>
        private IEnumerable<RiskDirection> CreateRiskDirectionCollection(Dictionary<string, double> riskValues, AssetClass assetClass, RiskGreekDirection riskGreekDirection)
        {
            var output = new List<RiskDirection>();

            foreach (var riskValue in riskValues)
            {
                var timeSeriesMapping = string.Empty;

                if (_riskTimeSeriesMapper != null)
                {
                    timeSeriesMapping = _riskTimeSeriesMapper.MapTimeSeries(new RiskDirection.Builder
                    {
                        AssetClass = assetClass,
                        RiskGreekDirection = riskGreekDirection,
                        Name = riskValue.Key
                    }.Build());
                }
                output.Add(
                    new RiskDirection.Builder
                    {
                        AssetClass = assetClass,
                        RiskGreekDirection = riskGreekDirection,
                        RiskAmount = riskValue.Value,
                        Name = riskValue.Key,
                        TimeSeriesMapping = timeSeriesMapping
                    }.Build());
            }

            return output;
        }

        #endregion


    }
}
