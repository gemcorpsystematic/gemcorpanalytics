﻿namespace GemCorpAnalytics.Calculators
{
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.Models;
    using GemCorpAnalytics.Interpolators;
    using GemCorpAnalytics.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>The beta calculation time series builder.</summary>
    public class BetaCalculationTimeSeriesBuilder : IBetaCalculationTimeSeriesBuilder
    {
        #region Fields

        /// <summary>The proxy time series generator.</summary>
        private readonly IProxyTimeSeriesGenerator _proxyTimeSeriesGenerator;

        /// <summary>The interpolator.</summary>
        private readonly IInterpolator _interpolator;

        #endregion

        #region Constructor

        /// <summary>Initialise an instance of the beta calculation time series builder.</summary>
        /// <param name="proxyTimeSeriesGenerator">The proxy time series generator.</param>
        /// <param name="interpolator">The interpolator.</param>
        public BetaCalculationTimeSeriesBuilder(
            IProxyTimeSeriesGenerator proxyTimeSeriesGenerator,
            IInterpolator interpolator)
        {

            _proxyTimeSeriesGenerator = proxyTimeSeriesGenerator==null ? throw new ArgumentNullException("Proxy time series generator cannot be null") : proxyTimeSeriesGenerator;
            _interpolator = interpolator==null ? throw new ArgumentNullException("The interpolator cannot be null") : interpolator;
        }

        #endregion


        #region Public Methods

        /// <summary>Manipulates the time series required for beta calculation.</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="seriesToProxyMapping">The series to exclude.</param>
        /// <param name="spreadSeriesMapping">The spread series mapping.</param>
        /// <returns>The <see cref="IReadOnlyDictionary{TKey, TValue}"/>.</returns>
        public IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> GetTimeSeries(
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> timeSeries,
            DateTime startDate,
            DateTime endDate,
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, double>> seriesToProxyMappings,
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, ITimeSeriesRepositoryEntry>> spreadSeriesMappings)
        {
            var truncatedTimeSeries = TruncateTimeSeries(startDate, timeSeries);
            var longestTimeSeriesKey = GetLongestTimeSeries(timeSeries, seriesToProxyMappings.Keys);
            var longestSeries = timeSeries[longestTimeSeriesKey];
            var paddedSeries = new Dictionary<ITimeSeriesRepositoryEntry, TimeSeries>();

            // I need to calculate the proxy time series
            // The series to need padding

            paddedSeries[longestTimeSeriesKey] = longestSeries;
            var longestTimeSeriesDates = longestSeries.Where(d => d.Date >= startDate && d.Date <= endDate).Select(p=>p.Date);

            foreach(var proxySeriesConfig in seriesToProxyMappings)
            {
                var proxySeries = timeSeries[proxySeriesConfig.Value.Item1];
                var beta = proxySeriesConfig.Value.Item2;
                paddedSeries[proxySeriesConfig.Key] = _proxyTimeSeriesGenerator.GenerateProxySeries(proxySeries, timeSeries[proxySeriesConfig.Key], 
                    longestTimeSeriesDates, beta);
            }

            var seriesToExclude = new List<ITimeSeriesRepositoryEntry>();
            seriesToExclude.AddRange(seriesToProxyMappings.Keys);
            seriesToExclude.Add(longestTimeSeriesKey);

            foreach (var ts in truncatedTimeSeries)
            {

                if (seriesToExclude.Contains(ts.Key))
                    continue;

                var tempTimeSeriesDates = ts.Value.Select(x => x.Date);
                var tempTimeSeriesPoints = new List<ITimeSeriesPoint>();

                foreach (var p in longestTimeSeriesDates)
                {
                    if (!tempTimeSeriesDates.Contains(p))
                    {

                        ITimeSeriesPoint longestTimeSeriesPoint;

                        if (ts.Value.Count(pt => pt.Date < p) < 1) {
                            longestTimeSeriesPoint = new TimeSeriesPoint.Builder { 
                                Date = p.Date,
                                Name = ts.Value[0].Name,
                                Currency = ts.Value[0].Currency,
                                Ticker = ts.Value[0].Ticker,
                                Contributor = ts.Value[0].Contributor,
                                Value = _interpolator.GetValue(ts.Value, p),
                                Unit= ts.Value[0].Unit,
                                InstrumentId = ts.Value[0].InstrumentId,
                                TimeSeriesAttribute =ts.Value[0].TimeSeriesAttribute
                            }.Build(); _interpolator.GetValue(ts.Value, p.Date);
                        }
                        else {
                            longestTimeSeriesPoint = ts.Value.Last(pt => pt.Date < p);
                        }
                        var upperBoundValues = ts.Value.Count(pt => pt.Date > p);
                        var interpValue = upperBoundValues > 0 ? _interpolator.GetValue(longestTimeSeriesPoint, ts.Value.First(pt => pt.Date > p), p)
                            : longestTimeSeriesPoint.Value;
                        tempTimeSeriesPoints.Add(new TimeSeriesPoint.Builder
                        {
                            Date = p,
                            Name = longestTimeSeriesPoint.Name,
                            Ticker = longestTimeSeriesPoint.Ticker,
                            Value = interpValue,
                            Currency = longestTimeSeriesPoint.Currency,
                            Contributor = longestTimeSeriesPoint.Contributor,
                            Unit = longestTimeSeriesPoint.Unit,
                            InstrumentId = longestTimeSeriesPoint.InstrumentId,
                            TimeSeriesAttribute = longestTimeSeriesPoint.TimeSeriesAttribute
                        }.Build());
                        continue;
                    }
                    tempTimeSeriesPoints.Add(ts.Value.First(pt => pt.Date == p));
                }
                paddedSeries[ts.Key] = new TimeSeries(tempTimeSeriesPoints);
            }

            foreach(var spreadSeriesMapping in spreadSeriesMappings)
            {
                var ticker = spreadSeriesMapping.Key;
                var a = paddedSeries[spreadSeriesMapping.Value.Item1];
                var b = paddedSeries[spreadSeriesMapping.Value.Item1];
                var spreadSeriesPoints = new List<ITimeSeriesPoint>();

                foreach (var tsPoint in a)
                {
                    spreadSeriesPoints.Add(new TimeSeriesPoint.Builder
                    {
                        Date = tsPoint.Date,
                        Ticker = ticker.Name,
                        Value = tsPoint.Value - b.First(p => p.Date == tsPoint.Date).Value,
                        Unit = tsPoint.Unit,
                        Name = ticker.Name,
                        TimeSeriesAttribute = TimeSeriesAttributes.Close
                    }.Build());
                }
                paddedSeries[ticker] = new TimeSeries(spreadSeriesPoints);
            }

            return paddedSeries;
        }

        #endregion

        #region Methods

        /// <summary>Truncates the time series from the front.</summary>
        /// <param name="startDate">The start date.</param>
        /// <param name="timeSeries">The time series dictionary.</param>
        /// <returns>The <see cref="IReadOnlyDictionary{TKey, TValue}"/>.</returns>
        private IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> TruncateTimeSeries(
            DateTime startDate, 
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> timeSeries)
        {
            // check time series have more than 0 points.
            var output = timeSeries.Select(ts => new KeyValuePair<ITimeSeriesRepositoryEntry, TimeSeries>(ts.Key,
                new TimeSeries(ts.Value.Where(p => p.Date >= startDate))))
                .ToDictionary(x => x.Key, y => y.Value);
            return output;
        }

        /// <summary>Gets the longest time series.</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <param name="seriesToExclude">The series to exclude.</param>
        /// <returns>The key.</returns>
        private ITimeSeriesRepositoryEntry GetLongestTimeSeries(
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> timeSeries, 
            IEnumerable<ITimeSeriesRepositoryEntry> seriesToExclude)
        {
            ITimeSeriesRepositoryEntry maxPointSeries = null;
            var maxPoints = int.MinValue;

            foreach (var ts in timeSeries)
            {
                if (seriesToExclude.Contains(ts.Key)) continue;
                var points = ts.Value.Count;
                if (points > maxPoints)
                {
                    maxPoints = points;
                    maxPointSeries = ts.Key;
                }
            }

            return maxPointSeries;
        }

        #endregion

    }
}
