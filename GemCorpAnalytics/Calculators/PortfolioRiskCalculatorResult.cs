﻿namespace GemCorpAnalytics.Calculators
{
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Data;
    using System.Collections.Generic;

    /// <summary>The portfolio risk calculator result.</summary>
    public class PortfolioRiskCalculatorResult : IPortfolioRiskCalculatorResult
    {

        #region Constructor


        /// <summary>Initialise an instance of the portfolio risk calculator result.</summary>
        /// <param name="creditDelta">The credit delta.</param>
        /// <param name="fxDelta">The fx delta.</param>
        /// <param name="equityDelta">The equity delta.</param>
        /// <param name="interestDelta">The interest delta.</param>
        /// <param name="commodityDelta">The commodity delta.</param>
        /// <param name="commodityVega">The commodity vega.</param>
        public PortfolioRiskCalculatorResult(
            Dictionary<string, double> creditDelta,
            Dictionary<string, double> fxDelta,
            Dictionary<string, double> equityDelta,
            Dictionary<string, double> interestDelta,
            Dictionary<string, double> commodityDelta,
            Dictionary<string, double> commodityVega
            )
        {
            CreditDelta = creditDelta;
            FxDelta = fxDelta;
            EquityDelta = equityDelta;
            InterestDelta = interestDelta;
            CommodityDelta = commodityDelta;
            CommodityVega = commodityVega;
        }

        public PortfolioRiskCalculatorResult(
            IEnumerable<RiskDirection> riskDirection)
        {
            RiskDirectionCollection = riskDirection;
        }

        #endregion

        #region Properties

        /// <summary>The credit delta.</summary>
        public Dictionary<string, double> CreditDelta { get; }

        /// <summary>The fx delta.</summary>
        public Dictionary<string, double> FxDelta { get; }

        /// <summary>The equity delta.</summary>
        public Dictionary<string, double> EquityDelta { get; }

        /// <summary>The interest delta.</summary>
        public Dictionary<string, double> InterestDelta { get; }

        /// <summary>The commodity delta.</summary>
        public Dictionary<string, double> CommodityDelta { get; }

        /// <summary>The commodity vega.</summary>
        public Dictionary<string, double> CommodityVega { get; }
        
        /// <summary>The risk direction collection.</summary>
        public IEnumerable<RiskDirection> RiskDirectionCollection { get; }

        #endregion

    }
}
