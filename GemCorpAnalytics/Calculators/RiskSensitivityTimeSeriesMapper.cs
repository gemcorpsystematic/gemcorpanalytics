﻿namespace GemCorpAnalytics.Calculators
{
    using System;
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Enums;
    using GemCorpAnalytics.Data;

    /// <summary>The risk sensitivity time series mapper.</summary>
    public class RiskSensitivityTimeSeriesMapper : IRiskSensitivityTimeSeriesMapper
    {

        #region Public Methods
        
        /// <summary>Maps time series to risk direction.</summary>
        /// <param name="riskDirection">The risk direction.</param>
        /// <returns>The time series.</returns>
        public string MapTimeSeries(RiskDirection riskDirection)
        {

            switch (riskDirection.AssetClass)
            {
                case AssetClass.Fx:
                    var nameBreakdown = riskDirection.Name.Split(':');
                    return $"{nameBreakdown[1]}{nameBreakdown[2]}";
                case AssetClass.Commodity:
                    return "CO1";
                case AssetClass.Credit:
                    var name = riskDirection.Name.Trim().ToLower();

                    if (name.Contains("south africa"))
                        return "CSOAF1U5";

                    if (name.Contains("colombia"))
                        return "CCOL1U5";

                    if (name.Contains("turkey"))
                        return "CTURK1U5";

                    return string.Empty;
                case AssetClass.Equity:
                    // contains TIP -> TIP US
                    // Emerging market -> EMB US
                    // SPAC -> SPAC
                    var equityTimeSeriesMapping = string.Empty;
                    var equityLoweredName = riskDirection.Name.ToLower();

                    if(equityLoweredName.Contains("arrival"))
                    {
                        equityTimeSeriesMapping = "ARVL US";
                    }

                    if (equityLoweredName.Contains("tip")) 
                    {
                        equityTimeSeriesMapping = "TIP_US";
                    }

                    if (equityLoweredName.Contains("tlt"))
                    {
                        equityTimeSeriesMapping = "TLT_US";
                    }

                    if (equityLoweredName.Contains("usd emerging markets bond"))
                    {
                        equityTimeSeriesMapping = "EMB US";
                    }

                    if(equityLoweredName.Contains("spacs"))
                    {
                        equityTimeSeriesMapping = "SPAC";
                    }

                    if (equityLoweredName.Contains("spx")) 
                    {
                        equityTimeSeriesMapping = "SPX";
                    }

                    return equityTimeSeriesMapping;
                case AssetClass.Interest:
                    var timeSeriesMapping = string.Empty;
                    var loweredName = riskDirection.Name.ToLower();
                    if (loweredName.Contains("lauca") || loweredName.Contains("angol") || loweredName.Contains("rcf"))
                    {
                        timeSeriesMapping = "BBG00BD31G34";
                    }

                    if(loweredName.Contains("repo"))
                    {
                        timeSeriesMapping = "US3Y2Y";
                    }

                    if (loweredName.Contains("swap") || loweredName.Contains("usd01") || loweredName.Contains("u.s."))
                    {
                        timeSeriesMapping = "USGG5YR";
                    }

                    if(loweredName.Contains("treasurynote"))
                    {
                        timeSeriesMapping = "USGG2YR";
                    }

                    if(loweredName.Contains("telconet"))
                    {
                        timeSeriesMapping = "BBG0000GXSD1";
                    }

                    if(loweredName.Contains("oman"))
                    {
                        timeSeriesMapping = "BBG00G41HPF7";
                    }

                    if (loweredName.Contains("brazil") || loweredName.Contains("brl"))
                    {
                        timeSeriesMapping = "OD9";
                    }

                    return timeSeriesMapping;
                default:
                    throw new NotImplementedException($"Unable to handle asset class:{riskDirection.AssetClass}");
            }
            
        }

        #endregion
    }
}
