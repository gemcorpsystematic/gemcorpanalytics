﻿namespace GemCorpAnalytics.Calculators
{
    using System;
    using System.Linq;
    using GemCorpAnalytics.Interpolators;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.Factories;
    using GemCorpAnalytics.Enums;
    using GemCorpAnalytics.Utils;
    using System.Collections.Generic;

    /// <summary>The stress test calculator.</summary>
    public class StressTestCalculator : IStressTestCalculator
    {
        #region Fields

        /// <summary>The interpolator.</summary>
        private readonly IInterpolator _interpolator;

        #endregion


        #region Constructor

        /// <summary>Initialize an instance of the stress test calculator.</summary>
        /// <param name="interpolator">The interpolator.</param>
        public StressTestCalculator(
            IInterpolator interpolator)
        {
            _interpolator = interpolator ?? throw new ArgumentNullException(nameof(interpolator));
        }

        #endregion


        #region Public Methods

        /// <summary>Calculates the stress tests.</summary>
        /// <param name="context">The context.</param>
        /// <returns>The <see cref="StressTestCalculatorResultCollection"/>.</returns>
        public StressTestCalculatorResultCollection Calculate(StressTestCalculatorContext context)
        {
            var output = new StressTestCalculatorResultCollection();

            foreach(var stressTest in context.StressTestCollection)
            {
                var scenarioStartDate = stressTest.StartDate;
                var scenarioEndDate = stressTest.EndDate;
                foreach (var riskDirection in context.RiskDirectionCollection) 
                {
                    if (Math.Abs(riskDirection.RiskAmount) < context.RiskToleranceAmount)
                        continue;

                    if (riskDirection.TimeSeriesMapping =="")
                        continue;

                    var timeSeriesAttribute = riskDirection.RiskGreekDirection == RiskGreekDirection.Vega ? TimeSeriesAttributes.Volatility_30D : TimeSeriesAttributes.Close;
                    
                    var timeSeriesRepositoryEntry = new TimeSeriesRepositoryEntry(riskDirection.TimeSeriesMapping, timeSeriesAttribute);

                    var timeSeries = context.TimeSeriesRepository.GetTimeSeries(timeSeriesRepositoryEntry);

                    if (timeSeries == null)
                        continue;

                    //if (timeSeries.First().Date > scenarioStartDate)
                    //    continue;

                    //if (!timeSeries.ContainsDate(scenarioStartDate) || !timeSeries.ContainsDate(scenarioEndDate))
                    //    continue;

                    // this needs to be approved and logic is needed for when values do not exist

                    var scenario = stressTest.Scenarios.First(s => s.Underlier == riskDirection.TimeSeriesMapping);

                    //var startPoint = timeSeries.First(p => p.Date == scenarioStartDate);
                    var startPoint = scenario.StartValue;
                    //var endPoint = timeSeries.First(p => p.Date == scenarioEndDate);
                    var endPoint = scenario.EndValue;

                    var assetReturn = ReturnCalculatorFactory.GetReturnCalculator(riskDirection.AssetClass, riskDirection.RiskGreekDirection);
                    var lastPoint = timeSeries.First(ts => ts.Date == context.ValueDate).Value;
                    var scenarioReturn = assetReturn(startPoint, endPoint, lastPoint);

                    var scenarioLoss = 0.0;

                    var riskDirectionName = riskDirection.Name.Contains("Option") ? riskDirection.Name.Replace("Option","").TrimEnd(' ') : riskDirection.Name;

                    var ladderDirectionName = riskDirectionName == "SPXW 4" ? "S&P 500 INDEX" : riskDirectionName;

                    ladderDirectionName = riskDirectionName == "Bund" ? "Euro Bund" : ladderDirectionName;

                    ladderDirectionName = riskDirectionName == "Treasury" ? "U.S. Treasury Bond" : ladderDirectionName;

                    if ((riskDirection.AssetClass == AssetClass.Equity && riskDirection.RiskGreekDirection == RiskGreekDirection.Vega))
                    {
                        scenarioReturn /= 100.0;
                    }

                    var ladderAssetClass = riskDirection.RiskGreekDirection == RiskGreekDirection.Vega ? $"{riskDirection.AssetClass.ToString().ToLower().Trim()} vol" :
                            riskDirection.AssetClass.ToString().ToLower().Trim();

                    if (ladderAssetClass == "interest")
                        ladderAssetClass = $"{ladderAssetClass} rate";

                    var scenarioLadder = CreateLadder(context.ScenarioLadderCollection, ladderDirectionName, ladderAssetClass);

                    if (scenarioLadder == null)
                    {
                        scenarioLoss = riskDirection.RiskAmount * scenarioReturn;
                    }
                    else
                    {

                        //var ladderAssetClass = riskDirection.RiskGreekDirection==RiskGreekDirection.Vega ? $"{riskDirection.AssetClass.ToString().ToLower().Trim()} vol" :
                        //    riskDirection.AssetClass.ToString().ToLower().Trim();
                        //var scenarioLadder = context.ScenarioLadderCollection.First(s => s.Name == ladderDirectionName && s.RiskType.ToLower().Trim().Contains(ladderAssetClass)).Ladder;
                        
                        if (scenarioLadder.Sum(l => Math.Abs(l.Value)) < 1.0)
                        {
                            if ((riskDirection.AssetClass == AssetClass.Equity || riskDirection.AssetClass == AssetClass.Fx) && riskDirection.RiskGreekDirection == RiskGreekDirection.Vega)
                            {
                                scenarioLoss = scenarioLoss * (1.0 + scenarioReturn);
                            }
                            else
                            {
                                scenarioLoss = riskDirection.RiskAmount * scenarioReturn;
                            }
                        }
                        else {

                            var scenarioShocks = scenarioLadder.Keys.ToArray();

                            var assetClasses = new List<AssetClass> { AssetClass.Commodity, AssetClass.Interest, AssetClass.BondSpread };

                            if (assetClasses.Contains(riskDirection.AssetClass))
                                scenarioShocks = scenarioShocks.Select(x => x * 100.0).ToArray();

                            scenarioLoss = _interpolator.GetValue(scenarioShocks, scenarioLadder.Values.ToArray(), scenarioReturn, true);
                        }

                    }

                    var stressTestCalculatorResult = new StressTestCalculatorResult(
                        context.ValueDate, 
                        stressTest.Name, 
                        riskDirection.Name, 
                        scenarioStartDate, 
                        scenarioEndDate,
                        startPoint,
                        endPoint,
                        scenarioReturn, 
                        scenarioLoss, 
                        riskDirection.RiskAmount,
                        riskDirection.AssetClass.ToString());

                    output.Add(stressTestCalculatorResult);
                }
            
            }

            return output;
        }

        #endregion


        #region Methods

        /// <summary>Creates the scenario ladder</summary>
        /// <param name="scenarioLadderCollection">The scenario ladder collection.</param>
        /// <param name="ladderName">The ladder name.</param>
        /// <param name="riskType">The risk type.</param>
        /// <returns>The scnenario ladder.</returns>
        private Dictionary<double, double> CreateLadder(
            ScenarioLadderCollection scenarioLadderCollection, 
            string ladderName, 
            string riskType)
        {

            // filter the ladder collection by name and risk type, then parse the scenario
            var collection = scenarioLadderCollection.Where(c => c.Name == ladderName && c.RiskType.ToLower()==$"{riskType} ladder");

            // unable to find a scenario ladder so return null
            if (collection.ToList().Count < 1)
                return null;

            var output = collection
                .Select(p => new KeyValuePair<double, double>(Utils.ParseScenarioLadder(p.ScenarioName, p.RiskType), p.ScenarioValue))
                .ToDictionary(x => x.Key, y => y.Value);

            return output;
        }

        #endregion

    }
}
