﻿namespace GemCorpAnalytics.Calculators
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Utils;

    /// <summary>The risk allocation calculator.</summary>
    public class RiskAllocationCalculator : IRiskAllocationCalculator
    {
        #region Fields

        /// <summary>The projected risk theme mapper.</summary>
        private readonly IProjectedRiskThemeMapper _projectedRiskThemeMapper;

        #endregion


        #region Constructor

        /// <summary>Initialize an instance of the projected risk theme mapper.</summary>
        /// <param name="projectedRiskThemeMapper">The projected risk theme mapper.</param>
        public RiskAllocationCalculator(
            IProjectedRiskThemeMapper projectedRiskThemeMapper)
        {

            _projectedRiskThemeMapper = projectedRiskThemeMapper ?? throw new ArgumentNullException(nameof(projectedRiskThemeMapper));
        }

        #endregion

        #region Public Methods

        /// <summary>Calculate risk allocation.</summary>
        /// <param name="minVarHedgeBetaCalculatorResult">The min var hedge beta calculator.</param>
        /// <returns>The <see cref="IRiskAllocationCalculatorResult"/>.</returns>
        public IRiskAllocationCalculatorResult Calculate(IMinVarHedgeBetaCalculatorResult minVarHedgeBetaCalculatorResult)
        {
            var output = new Dictionary<string, double>();

            // get theme for each risk direction.
            var themeMapping = new Dictionary<string, List<KeyValuePair<DateTime, double>>>();

            foreach (var riskDirection in minVarHedgeBetaCalculatorResult.PlBreakDown)
            {
                var theme = _projectedRiskThemeMapper.GetTheme(riskDirection.Key);

                var themePl = new List<KeyValuePair<DateTime, double>>();
                if (themeMapping.TryGetValue(theme, out themePl))
                {
                    var conflatedPlSeries = ConflatePlSeries(themePl, riskDirection.Value);
                    themeMapping.Remove(theme);
                    themeMapping[theme] = conflatedPlSeries;
                    continue;
                }
                themeMapping[theme] = minVarHedgeBetaCalculatorResult.PlBreakDown[riskDirection.Key];
            }

            // get covariance between theme mapping pl and risk pl.

            var plSeries = minVarHedgeBetaCalculatorResult.PlSeries.Values.ToArray();

            foreach (var theme in themeMapping) 
            {
                if (theme.Value.Count == 0)
                    continue;
                var themeSeries = theme.Value.Select(x => x.Value).ToArray();
                output[theme.Key] = Utils.Covariance(themeSeries, plSeries)/Utils.Variance(plSeries);   
            }

            return new RiskAllocationCalculatorResult(output);
        }

        #endregion

        #region Methods

        /// <summary>Conflates the pl series.</summary>
        /// <param name="seriesA">The series A.</param>
        /// <param name="seriesB">The series B.</param>
        /// <returns>The conflated series.</returns>
        private List<KeyValuePair<DateTime, double>> ConflatePlSeries(
            List<KeyValuePair<DateTime, double>> seriesA, 
            List<KeyValuePair<DateTime, double>> seriesB)
        {

            if (seriesA.Count != seriesB.Count)
            {
                if (seriesA.Count == 0 || seriesB.Count == 0)
                    return seriesA.Count == 0 ? seriesA : seriesB;
                throw new Exception("The two series must be of equal length.");
            }

            var tmp = seriesA.Select(x => {
                var y = seriesB.First(t => t.Key == x.Key).Value;
                return new KeyValuePair<DateTime, double>(x.Key, x.Value + y);
            }).ToList();

            return tmp;
        }

        #endregion

    }
}
