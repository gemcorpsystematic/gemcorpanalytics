﻿namespace GemCorpAnalytics.Calculators
{
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.Enums;
    using GemCorpAnalytics.Interpolators;
    using GemCorpAnalytics.Models;
    using GemCorpAnalytics.Utils;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>The proxy time series calculator.</summary>
    public class ProxyTimeSeriesCalculator : IProxyTimeSeriesCalculator
    {
        #region Fields

        /// <summary>The proxy mappings.</summary>
        private readonly IEnumerable<IProxyMapping> _proxyMappings;

        /// <summary>The time series.</summary>
        private readonly TimeSeries _timeSeries;

        /// <summary>The interpolator.</summary>
        private readonly IInterpolator _interpolator;

        #endregion


        #region Constructor

        /// <summary>Initialize an instance of the proxy time series calculator.</summary>
        /// <param name="proxyMappings">The proxy mappings.</param>
        /// <param name="timeSeries">The time series.</param>
        public ProxyTimeSeriesCalculator(
            IEnumerable<IProxyMapping> proxyMappings,
            TimeSeries timeSeries,
            IInterpolator interpolator
            )
        {
            _proxyMappings = proxyMappings;
            _timeSeries = timeSeries;
            _interpolator = interpolator;
        }

        #endregion

        #region Methods

        /// <summary>Calculate proxy values.</summary>
        /// <param name="proxyDates">The proxy dates.</param>
        /// <param name="timeSeriesKey">The time series key.</param>
        /// <returns>The <see cref="TimeSeries"/>.</returns>
        public TimeSeries Calculate(IEnumerable<DateTime> proxyDates, TimeSeries proxyTimeSeries)
        {
            var timeSeriesName = _timeSeries.First().Name;
            var timeSeriesAttribute = _timeSeries.First().TimeSeriesAttribute;

            var mappings = _proxyMappings
                .Where(m => m.TimeSeriesKey.Name == timeSeriesName && m.TimeSeriesKey.TimeSeriesAttribute == timeSeriesAttribute)
                .OrderByDescending(k => k.LastDate);

            //var timeSeries = _timeSeries[timeSeriesKey];

            var firstTimeSeriesPoint = _timeSeries.First();
            var firstDateTimeSeries = firstTimeSeriesPoint.Date;
            var lastDateTimeSeries = _timeSeries.Last().Date;

            var output = new List<TimeSeriesPoint>();

            foreach(var proxyDate in proxyDates)
            {
                if (proxyDate >= firstDateTimeSeries && proxyDate <= lastDateTimeSeries)
                {
                    var interpValue = _interpolator.GetValue(_timeSeries, proxyDate);

                    output.Add(new TimeSeriesPoint.Builder{
                        Date = proxyDate,
                        Name = firstTimeSeriesPoint.Name,
                        Ticker = firstTimeSeriesPoint.Ticker,
                        Value = interpValue,
                        TimeSeriesAttribute = firstTimeSeriesPoint.TimeSeriesAttribute
                    }.Build());
                    continue;
                }

                // find a proxy, handle if there are multiple proxies
                // if first date of time series is: first proxy date time series data, proxy Date, and first time series date
                // sort mappings reverse chronological order
                var ret = 1.0;

                foreach(var mapping in mappings)
                {
                    var proxyTimeSeriesFirstDate = proxyTimeSeries.First().Date;
                    var proxyTimeSeriesLastDate = proxyTimeSeries.Last().Date;

                    if(proxyDate >= proxyTimeSeriesFirstDate && proxyDate <= firstDateTimeSeries)
                    {
                        // get return proxy return from proxy date to firstDateTimeSeries
                        var interpValue = _interpolator.GetValue(proxyTimeSeries, proxyDate);
                        var interpTimeSeriesPoint = new TimeSeriesPoint.Builder {
                            Date = proxyDate,
                            Name = proxyTimeSeries.First().Name,
                            Value =interpValue
                        }.Build();

                        var endValue = _interpolator.GetValue(proxyTimeSeries, mapping.LastDate);
                        var endTimeSeriesPoint = new TimeSeriesPoint.Builder
                        {
                            Date = mapping.LastDate,
                            Name = proxyTimeSeries.First().Name,
                            Value = endValue
                        }.Build();

                        var tmpTimeSeriesPoints = new[] {interpTimeSeriesPoint, endTimeSeriesPoint };
                        var tmpTimeSeries = new TimeSeries(tmpTimeSeriesPoints);
                        var retType = Utils.GetTimeSeriesReturnType(AssetClass.Equity);
                        var tmpRet = tmpTimeSeries.CalculateDailyReturn(retType).First().Value;
                        ret *= 1.0 / Math.Pow(1.0 + tmpRet, mapping.Beta);
                    }

                    if(proxyDate < proxyTimeSeriesFirstDate)
                    {
                        // still carry the return onto the second mapping
                        var tmpTimeSeriesPoints = new []{ proxyTimeSeries.First(), proxyTimeSeries.Last(p=>p.Date<=mapping.LastDate)};
                        var tmpTimeSeries = new TimeSeries(tmpTimeSeriesPoints);
                        var retType = Utils.GetTimeSeriesReturnType(AssetClass.Equity); // need to get asset class from somewhere
                        var tmpRet = tmpTimeSeries.CalculateDailyReturn(retType).First().Value;
                        ret *= 1.0 / Math.Pow(1.0 + tmpRet, mapping.Beta);
                    }
                }

                output.Add(new TimeSeriesPoint.Builder
                {
                    Date = proxyDate,
                    Name = firstTimeSeriesPoint.Name,
                    Ticker = firstTimeSeriesPoint.Ticker,
                    Value = _timeSeries.First().Value * ret,
                    TimeSeriesAttribute = firstTimeSeriesPoint.TimeSeriesAttribute
                }.Build());

            }

            return new TimeSeries(output);
        }

        #endregion 
    }
}
