﻿namespace GemCorpAnalytics.Calculators
{
    using System;
    using System.Collections.Generic;
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Data;

    /// <summary>The minimum var hedge beta calculator result.</summary>
    public class MinVarHedgeBetaCalculatorResult : IMinVarHedgeBetaCalculatorResult
    {
        #region Constructor

        /// <summary>Initialize an instance of the min var hedge beta calculator result.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="beta">The beta.</param>
        /// <param name="rSquared">The r squared.</param>
        /// <param name="plBreakdown">The pl breakdown.</param>
        /// <param name="plSeries">The pl series.</param>
        /// <param name="riskDirection">The risk direction.</param>
        /// <param name="riskAllocations">The risk direction.</param>
        /// <param name="riskAllocationsRSquared">The risk direction.</param>
        public MinVarHedgeBetaCalculatorResult(
            DateTime valueDate,
            Dictionary<string, double> beta,
            Dictionary<string, double> rSquared,
            Dictionary<RiskDirection, List<KeyValuePair<DateTime, double>>> plBreakdown,
            Dictionary<DateTime,double> plSeries,
            IEnumerable<RiskDirection> riskDirection,
            Dictionary<string, double> riskAllocations,
            Dictionary<string, double> riskAllocationsRSquared)
        {
            ValueDate = valueDate;
            Beta = beta;
            RSquared = rSquared;
            PlBreakDown = plBreakdown;
            PlSeries = plSeries;
            RiskDirection = riskDirection;
            RiskAllocations = riskAllocations;
            RiskAllocationsRSquared = riskAllocationsRSquared;
        }

        #endregion

        #region Public Fields

        /// <summary>The value date.</summary>
        public DateTime ValueDate { get; }

        /// <summary>The beta.</summary>
        public Dictionary<string, double> Beta { get; }

        /// <summary>The Rsquared.</summary>
        public Dictionary<string, double> RSquared { get; }

        /// <summary>The pl breakdown.</summary>
        public Dictionary<RiskDirection, List<KeyValuePair<DateTime, double>>> PlBreakDown { get; }

        /// <summary>The pl series.</summary>
        public Dictionary<DateTime, double> PlSeries { get; }

        /// <summary>The risk direction.</summary>
        public IEnumerable<RiskDirection> RiskDirection { get; }

        /// <summary>The risk allocations.</summary>
        public Dictionary<string, double> RiskAllocations { get; }

        /// <summary>The risk allocations r squared.</summary>
        public Dictionary<string, double> RiskAllocationsRSquared { get; }

        #endregion
    }
}
