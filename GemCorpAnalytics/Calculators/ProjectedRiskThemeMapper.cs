﻿namespace GemCorpAnalytics.Calculators
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Data;
    using Newtonsoft.Json.Linq;

    /// <summary>The projected risk theme mapper.</summary>
    public class ProjectedRiskThemeMapper : IProjectedRiskThemeMapper
    {

        #region Fields

        /// <summary>The projected risk theme mappings file name.</summary>
        private const string ProjectedRiskThemeMappingsFileName = "ProjectedRiskThemeMappings.json";

        /// <summary>The projected risk theme mapppings.</summary>
        private Dictionary<string, string> projectedRiskThemeMappings;

        #endregion


        #region Constructor
        /// <summary>Initialize an instance of the projected risk theme mapper.</summary>
        public ProjectedRiskThemeMapper()
        {
            projectedRiskThemeMappings = JObject.Parse(File.ReadAllText($"{AppDomain.CurrentDomain.BaseDirectory}\\{ProjectedRiskThemeMappingsFileName}"))
                .ToObject<Dictionary<string, string>>();
        }

        #endregion


        #region Public Methods

        /// <summary>Gets the theme.</summary>
        /// <param name="riskDirection">The risk direction.</param>
        /// <returns>The theme.</returns>
        public string GetTheme(RiskDirection riskDirection)
        {
            var theme = string.Empty;
            if (!projectedRiskThemeMappings.TryGetValue(riskDirection.Name, out theme))
                return string.Empty;

            return theme;
        }

        #endregion
    }
}
