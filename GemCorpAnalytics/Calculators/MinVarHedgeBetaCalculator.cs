﻿namespace GemCorpAnalytics.Calculators
{
    using System;
    using System.Linq;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Utils;
    using GemCorpAnalytics.Models;
    using GemCorpAnalytics.Factories;
    using GemCorpAnalytics.Enums;
    using System.Collections.Generic;
    
    /// <summary>The minimum variance hedge beta calculator.</summary>
    public class MinVarHedgeBetaCalculator : IMinVarHedgeBetaCalculator<IMinVarHedgeBetaCalculatorResult>
    {
        #region Fields

        /// <summary>The projected risk context</summary>
        private readonly ProjectedRiskContext _projectedRiskContext;

        /// <summary>The correlation calculator.</summary>
        private readonly ICorrelationCalculator<ICorrelationCalculatorResult> _correlationCalculator;

        /// <summary>The volatility calculator.</summary>
        private readonly IVolatilityCalculator<IVolatilityCalculatorResult> _volatilityCalculator;

        #endregion
        #region Constructor

        /// <summary>Initialize an instance of the minimum variance hedge beta calculator.</summary>
        /// <param name="projectedRiskContext">The projected risk context.</param>
        /// <param name="correlationCalculator">The correlation calculator.</param>
        /// <param name="volatilityCalcultor">The volatility calculator.</param>
        public MinVarHedgeBetaCalculator(
            ProjectedRiskContext projectedRiskContext,
            ICorrelationCalculator<ICorrelationCalculatorResult> correlationCalculator,
            IVolatilityCalculator<IVolatilityCalculatorResult> volatilityCalcultor)
        {
            _projectedRiskContext = projectedRiskContext ?? throw new ArgumentNullException(nameof(projectedRiskContext));
            _correlationCalculator = correlationCalculator;
            _volatilityCalculator = volatilityCalcultor;
        }

        #endregion

        #region Public Methods

        /// <summary>Calculates the minimum variance hedge.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="lowerBound">The lower bound.</param>
        /// <param name="upperBound">The upper bound.</param>
        /// <returns>The minimum variance hedge beta calculator result.</returns>
        public IMinVarHedgeBetaCalculatorResult Calculate(DateTime valueDate, DateTime startDate, double? lowerBound, double? upperBound)
        {
            var environment = _projectedRiskContext.Environment;
            var cutOffDate = _projectedRiskContext.ValueDate;

            var minTimeSeriesDate = startDate;
            var timeSeries = _projectedRiskContext.TimeSeries;

            var seriesToProxy = _projectedRiskContext.SeriesToProxy;

            // these should be moved into the contructor.
            var interpolator = InterpolatorFactory.GetInterpolator(InterpolationTypes.Linear);
            var proxyTimeSeriesGenerator = new ProxyTimeSeriesGenerator(interpolator);
            var betaCalcTimeSeriesBuilder = new BetaCalculationTimeSeriesBuilder(proxyTimeSeriesGenerator, interpolator);
            
            //  Risk Directions to Project
            var riskDirectionsToProject = _projectedRiskContext.RiskDirection.Where(r => Math.Abs(r.RiskAmount) > 0.0);

            // once we have the risk directions, we get the time series.

            foreach (var ts in timeSeries)
            {
                var ticker = ts.Key;

                if (seriesToProxy.Contains(ticker)) continue;

                var values = ts.Value;
                var firstDate = values.First().Date; // thiis is assuming that the time series are ordered
                if (firstDate > minTimeSeriesDate)
                    minTimeSeriesDate = firstDate;
            }

            // Go through all relevant time series

            var paddedSeries = betaCalcTimeSeriesBuilder.GetTimeSeries(timeSeries, minTimeSeriesDate, cutOffDate,
                _projectedRiskContext.SeriesMapping,
                _projectedRiskContext.SeriesComposition);

            // padded Series contains risk direction series and projection serie
            
            // If I set a lower bound and upper bound on a time series, I need to filter for dates where CO1 was in those values.

            var riskReturnSeries = riskDirectionsToProject.Where(d=>!string.IsNullOrEmpty(d.TimeSeriesMapping)).Select(direction =>
            {
                var timeSeriesReturnType = Utils.GetTimeSeriesReturnType(direction.AssetClass);
                var timeSeriesAttribute = direction.RiskGreekDirection == RiskGreekDirection.Delta ? TimeSeriesAttributes.Close : TimeSeriesAttributes.Volatility_30D;
                var timeSeriesRepositoryEntry = new TimeSeriesRepositoryEntry(direction.TimeSeriesMapping, timeSeriesAttribute);
                var paddedSeriesKey = paddedSeries.Keys.First(k => k.Name.ToLower() == direction.TimeSeriesMapping.ToLower() && k.TimeSeriesAttribute == timeSeriesAttribute);
                var riskTimeSeries = paddedSeries[paddedSeriesKey];
                //var t = riskTimeSeries.FilterByWeekDay(DayOfWeek.Friday) // it's not always friday so this is dangerous 
                //.CalculateDailyReturn(timeSeriesReturnType)
                //.ScaleTimeSeries(GetTimeSeriesMultiplier(direction.AssetClass));

                var t = riskTimeSeries // it's not always friday so this is dangerous 
                .CalculateDailyReturn(timeSeriesReturnType)
                .ScaleTimeSeries(GetTimeSeriesMultiplier(direction.AssetClass));
                return new KeyValuePair<ITimeSeriesRepositoryEntry, TimeSeries>(timeSeriesRepositoryEntry, t);
            }).ToDictionary(x => x.Key, x => x.Value);

            var riskDataesCount = riskReturnSeries.Last().Value.Where(p => p.Date <= cutOffDate).Select(p => p.Date).Count();

            var riskDates = riskReturnSeries.Last().Value.Where(p => p.Date <= cutOffDate).Select(p => p.Date).Distinct().ToList();
            var brentTimeSeries = timeSeries[new TimeSeriesRepositoryEntry("CO1", TimeSeriesAttributes.Close)];

            if (lowerBound != null && upperBound != null)
            {
                var filteredTimeSeriesDate = brentTimeSeries.Where(p => p.Value >= lowerBound && p.Value <= upperBound).Select(p => p.Date).ToList();
                riskDates = riskDates.Where(rd => filteredTimeSeriesDate.Contains(rd)).ToList();
            }

            var riskDirectionPl = GenerateRiskDirectionPl(riskDirectionsToProject, riskDates, riskReturnSeries);

            var riskBreakdown = riskDates.Select(riskDate =>
            {
                var riskValueByDate = 0.0;

                foreach (var rd in riskDirectionPl)
                {
                    if (rd.Value.Count < 1)
                        continue;
                    riskValueByDate += rd.Value.First(d => d.Key == riskDate).Value;
                }

                return new KeyValuePair<DateTime, double>(riskDate, riskValueByDate);
            }).ToDictionary(x => x.Key, y => y.Value);

            // Currently t_y is the portfolio pnl, this is incorrect as it will distort the relation ship if computed against an unscaled time series
            var t_y = riskBreakdown.Select(v => v.Value).ToArray();
            var var_y = Utils.Variance(t_y);
            var nav = _projectedRiskContext.Nav.Last(n => n.Date <= valueDate && n.Entity.ToLower() == _projectedRiskContext.Entity.ToLower()).Amount;
            

            var deltas = new Dictionary<string, double>();
            var r_squared = new Dictionary<string, double>();
            var betaBreakdowns = new Dictionary<string, Dictionary<string,double>>();
            var rSquaredBreakdowns = new Dictionary<string, Dictionary<string,double>>();

            foreach (var hedgingVals in _projectedRiskContext.ProjectionEntries)
            {
                var betaOutput = CalculateBeta(hedgingVals.Key, hedgingVals.Value, paddedSeries, riskDates.ToList(), t_y, var_y, nav, brentTimeSeries.Last().Value, riskDirectionPl, riskBreakdown);
                deltas[hedgingVals.Key.Name] = betaOutput.Item1;
                r_squared[hedgingVals.Key.Name] = betaOutput.Item2;
                betaBreakdowns[hedgingVals.Key.Name] = betaOutput.Item3;
                rSquaredBreakdowns[hedgingVals.Key.Name] = betaOutput.Item4;
            }

            // risk breakdown is the portfolio pnl
            var portfolioPl = riskBreakdown.Select(x => x.Value).ToArray();
            var riskAllocations = new Dictionary<string, double>();
            var riskAllocationsRSquared = new Dictionary<string, double>();
            var deltaType = "RiskAlloc"; // This is an arbitrary deltatype for now, will ensure we get a beta calc which is not manipulated by anything

            foreach(var riskDirection in riskDirectionPl.Keys)
            {
                var riskPl = riskDirectionPl[riskDirection];
                // get beta of one time series against another
                var riskTimeSeries = riskPl.Where(x=>riskDates.Contains(x.Key)).Select(x=>x.Value).ToArray();
                var riskAlloc = CalculateStats(portfolioPl, riskTimeSeries, deltaType, 1.0, 1.0);
                riskAllocations[riskDirection.Name] = riskAlloc.Item1;
                riskAllocationsRSquared[riskDirection.Name] = riskAlloc.Item2;
            }

            return new MinVarHedgeBetaCalculatorResult(valueDate, deltas, r_squared, riskDirectionPl, riskBreakdown, riskDirectionsToProject, riskAllocations, riskAllocationsRSquared);
            
        }

        #endregion

        #region Methods

        
        /// <summary>Gets the time series multiplier.</summary>
        /// <param name="riskType">The risk type.</param>
        /// <returns></returns>
        private double GetTimeSeriesMultiplier(AssetClass assetClass)
        {
            switch (assetClass)
            {
                case AssetClass.Interest:
                    return 100.0;
                default:
                    return 1.0;
            }
        }

        /// <summary>Gets the delta.</summary>
        /// <param name="delta">The delta.</param>
        /// <param name="riskType">The risk type.</param>
        /// <param name="nav">The amount.</param>
        /// <param name="brent">The brent value.</param>
        /// <param name="cdxEm">The cdx em value.</param>
        /// <returns></returns>
        private double GetDelta(double delta, string riskType, double nav,double? brent, double? cdxEm) 
        {
            /*
             * CO1 -> Delta * CO1 / NAV
             * CDX EM -> Delta / CDXEMcs01_10m * 10 * 1000000 / NAV
             * Else -> Delta / NAV
             * CDX EM CDSI GEN 5Y SPRD Corp
             */
            
            switch(riskType.ToLower())
            {
                case "co1":
                    if (brent == null) throw new ArgumentNullException(nameof(brent));
                    return delta * brent.Value / nav;
                case "cdxem":
                    throw new NotImplementedException();
                default:
                    return delta / nav;
            }

        }

        /// <summary>Generates the risk direction pl.</summary>
        /// <param name="riskDirections">The risk directions.</param>
        /// <param name="riskDates">The risk dates.</param>
        /// <param name="riskReturnSeries">The risk return series.</param>
        /// <returns>The risk direction and the pl series.</returns>
        private Dictionary<RiskDirection, List<KeyValuePair<DateTime, double>>> GenerateRiskDirectionPl(
            IEnumerable<RiskDirection> riskDirections,
            IEnumerable<DateTime> riskDates,
            Dictionary<ITimeSeriesRepositoryEntry, TimeSeries> riskReturnSeries)
        {
            var riskDirectionPl = riskDirections.Select(riskDirection =>
            {
                var pl = new List<KeyValuePair<DateTime, double>>();
                foreach (var riskDate in riskDates)
                {
                    if (riskDirection.RiskGreekDirection == RiskGreekDirection.Vega && (riskDirection.TimeSeriesMapping == "VG1" || riskDirection.TimeSeriesMapping == "TLT"))
                        continue;

                    var timeSeriesAtt = riskDirection.RiskGreekDirection == RiskGreekDirection.Vega ? TimeSeriesAttributes.Volatility_30D : TimeSeriesAttributes.Close;
                    //timeSeriesAtt = riskDirection.AssetClass == AssetClass.Interest && riskDirection.TimeSeriesMapping != "USGG5YR" ? TimeSeriesAttributes.Z_Sprd_Mid : timeSeriesAtt;
                    timeSeriesAtt = riskDirection.TimeSeriesMapping == "OD9" ? TimeSeriesAttributes.Low : timeSeriesAtt;

                    if (riskDirection.RiskGreekDirection == RiskGreekDirection.Vega && riskDirection.TimeSeriesMapping == "SPX")
                        timeSeriesAtt = TimeSeriesAttributes.Volatility_30D_50Delta;

                    if (string.IsNullOrEmpty(riskDirection.TimeSeriesMapping) || Math.Abs(riskDirection.RiskAmount) < 10.0
                    || !riskReturnSeries.ContainsKey(new TimeSeriesRepositoryEntry(riskDirection.TimeSeriesMapping,timeSeriesAtt)))
                        continue;
                    var riskSeries = riskReturnSeries.First(k => k.Key.Name == riskDirection.TimeSeriesMapping && k.Key.TimeSeriesAttribute == timeSeriesAtt);
                    var riskProjectionValue = riskSeries.Value.First(p => p.Date == riskDate).Value;

                    var riskDirectionAmount = riskDirection.RiskAmount;

                    // move this logic into the risk direction
                    //if (riskDirection.TimeSeriesMapping == "GBPUSD_SPOT" || riskDirection.TimeSeriesMapping == "EURUSD_SPOT")
                    //    riskDirectionAmount = riskDirectionAmount * 1.0;

                    pl.Add(new KeyValuePair<DateTime, double>(riskDate, riskProjectionValue * riskDirectionAmount));
                }
                return new KeyValuePair<RiskDirection, List<KeyValuePair<DateTime, double>>>(riskDirection, pl);
            }).ToDictionary(x => x.Key, y => y.Value);

            return riskDirectionPl;
        }

        private Tuple<double, double, Dictionary<string, double>, Dictionary<string, double>> CalculateBeta(
            ITimeSeriesRepositoryEntry hedgeSeries,
            AssetClass hedgeSeriesAssetClass, 
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> paddedSeries, 
            List<DateTime> riskDates,
            double[] t_y,
            double var_y,
            double nav, 
            double brent,
            Dictionary<RiskDirection, List<KeyValuePair<DateTime, double>>> riskDirectionPl,
            Dictionary<DateTime, double> riskBreakdown)
        {
            

            // CO1, SPGSCI, EMB US, JPEIGLBL, CDX EM CDSI GEN 5Y SPRD, LBUTTRUU, EMLCTRUU, SPBDU10T, JPEIGLBL
            var hedgeSeriesReturnType = Utils.GetTimeSeriesReturnType(hedgeSeriesAssetClass);

            var regressorReturnSeries = paddedSeries[hedgeSeries]
                .CalculateDailyReturn(hedgeSeriesReturnType);

            var regressorSeries = riskDates.Select(riskDate =>
            {
                return new KeyValuePair<DateTime, double>(riskDate, regressorReturnSeries.First(p => p.Date == riskDate).Value);
            }).ToDictionary(x => x.Key, y => y.Value);

            //filter the series for every friday
            var t_x = regressorSeries.Select(v => v.Value).ToArray();

            var deltaType = hedgeSeries.Name.ToLower().Contains("co1") ? "co1" : "other";

            var betaBreakDown = new Dictionary<string, double>();
            var correlBreakDown = new Dictionary<string, double>();

            var stats = CalculateStats(t_x, t_y, deltaType, nav, brent);

            foreach (var riskDirection in riskDirectionPl)
            {
                var risk = riskDirection.Key;
                var riskSeries = riskDirection.Value;

                var projectionSeriesReturnType = Utils.GetTimeSeriesReturnType(riskDirection.Key.AssetClass);
                var projectionReturnSeries = paddedSeries[new TimeSeriesRepositoryEntry(risk.TimeSeriesMapping, TimeSeriesAttributes.Close)]
                    .CalculateDailyReturn(projectionSeriesReturnType);
                var projectionSeries = riskDates.Select(riskDate =>
                {
                    return new KeyValuePair<DateTime, double>(riskDate, projectionReturnSeries.First(p => p.Date == riskDate).Value);
                }).ToDictionary(x => x.Key, y => y.Value);

                //var tmp_y_series = new List<double>();

                //foreach(var riskVal in riskBreakdown) 
                //{
                //    var adjustment = 0.0;
                //    if(riskSeries.Select(s=>s.Key).Contains(riskVal.Key))
                //        adjustment = riskSeries.First(p => p.Key == riskVal.Key).Value;
                //    tmp_y_series.Add(adjustment/risk.RiskAmount);
                //}
                var tmpStats = CalculateStats(t_x, projectionSeries.Values.ToArray(), deltaType, nav, brent);
                betaBreakDown[$"{risk.Name},{risk.RiskGreekDirection}"] = tmpStats.Item1*risk.RiskAmount;
                correlBreakDown[$"{risk.Name},{risk.RiskGreekDirection}"] = tmpStats.Item2;
            }

            var overallBeta = betaBreakDown.Values.Sum();

            var output = new Tuple<double, double, Dictionary<string, double>, Dictionary<string, double>>(overallBeta, stats.Item2, betaBreakDown, correlBreakDown);

            return output;
        }

        private Tuple<double, double> CalculateStats(double[] t_x, double[] t_y, string deltaType, double nav, double brent)
        {
            var var_x = Utils.Variance(t_x);
            var var_y = Utils.Variance(t_y);
            var cov_xy = Utils.Covariance(t_x, t_y);

            var corr_xy = cov_xy / Math.Sqrt(var_x * var_y);

            var beta = cov_xy / var_x;

            var delta = GetDelta(beta, deltaType, nav, brent, null);

            var output = new Tuple<double, double>(delta, corr_xy * corr_xy);

            return output;
        }

        #endregion

    }
}
