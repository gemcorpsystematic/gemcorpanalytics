﻿namespace GemCorpAnalytics.Calculators
{
    using System;
    using System.Runtime.Serialization;
    using GemCorpAnalytics.Calculators.Interfaces;

    [DataContract]
    public class StressTestCalculatorResult : IStressTestCalculatorResult
    {

        #region Constructor

        /// <summary>Initialize an instance of the stress test calculator result.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="scenarioName">The scenario name.</param>
        /// <param name="underlierName">The underlier name.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="startValue">The start value.</param>
        /// <param name="endValue">The end value.</param>
        /// <param name="shock">The shock.</param>
        /// <param name="plAmount">The pl amount.</param>
        /// <param name="riskDirectionAmount">The risk direction amount.</param>
        /// <param name="assetClass">The asset class.</param>
        public StressTestCalculatorResult(
            DateTime valueDate,
            string scenarioName,
            string underlierName,
            DateTime startDate,
            DateTime endDate,
            double startValue,
            double endValue,
            double shock,
            double plAmount,
            double riskDirectionAmount,
            string assetClass
            )
        {
            ValueDate = valueDate;
            ScenarioName = scenarioName;
            UnderlierName = underlierName;
            StartDate = startDate;
            EndDate = endDate;
            StartValue = startValue;
            EndValue = endValue;
            Shock = shock;
            PlAmount = plAmount;
            RiskDirectionAmount = riskDirectionAmount;
            AssetClass = assetClass;
        }

        #endregion

        #region Public Properties

        /// <summary>The value date.</summary>
        [DataMember]
        public DateTime ValueDate { get; }

        /// <summary>The scenario name.</summary>
        [DataMember]
        public string ScenarioName { get; }

        /// <summary>The underlier name.</summary>
        [DataMember]
        public string UnderlierName { get; }

        /// <summary>The start date.</summary>
        [DataMember]
        public DateTime StartDate { get; }

        /// <summary>The end date.</summary>
        [DataMember]
        public DateTime EndDate { get; }

        /// <summary>The start value.</summary>
        [DataMember]
        public double StartValue {get;}

        /// <summary>The end value.</summary>
        [DataMember]
        public double EndValue { get; }

        /// <summary>The shock.</summary>
        [DataMember]
        public double Shock { get; }

        /// <summary>The pl amount.</summary>
        [DataMember]
        public double PlAmount { get; }

        /// <summary>The risk direction amount.</summary>
        [DataMember]
        public double RiskDirectionAmount { get; }

        /// <summary>The asset class.</summary>
        [DataMember]
        public string AssetClass { get; }

        #endregion

    }
}
