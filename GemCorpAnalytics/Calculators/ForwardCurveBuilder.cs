﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GemCorpAnalytics.Calculators
{
    using System;
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Interpolators;
    using GemCorpAnalytics.Data.Interfaces;
    using MathNet.Numerics.Statistics;

    /// <summary>The forward curve builder.</summary>
    public class ForwardCurveBuilder : IForwardCurveBuilder
    {

        #region Public Methods

        /// <summary>Builds the forward cure as per Orchestrade.</summary>
        /// <param name="curve">The curve.</param>
        /// <param name="fwdRateTenor">The forward rate tenor.</param>
        /// <param name="tenorCutOff">The tenor cut off.</param>
        /// <returns></returns>
        public ICurve BuildForwardCurve(ICurve curve, double fwdRateTenor, double tenorCutOff)
        {

            // log the discount factors
            // interpolate the log discount factors out to tenor cut off
            // calculate fwd rates.

            var logY = curve.Y.Select(y => Math.Log(y)).ToArray();

            var tenors = new List<double>();
            tenors.Add(fwdRateTenor);
            var tmpTenor = fwdRateTenor;

            while(tmpTenor < tenorCutOff)
            {
                tmpTenor += fwdRateTenor;
                tenors.Add(tmpTenor);
            }

            var logCurve = new Curve(curve.Name, curve.DayCountConvention, curve.Currency, curve.CurveDate, curve.X, logY);

            var curveInterpolator = new CurveInterpolator(logCurve, new LinearInterpolator());
            var interpPoints = curveInterpolator.GetValue(tenors.ToArray());

            var expInterpPoints = new List<double>();
            expInterpPoints.Add(1.0);

            foreach(var interpPoint in interpPoints)
            {
                expInterpPoints.Add(Math.Exp(interpPoint));
            }


            var fwdRates = new List<double>();

            for (var i=1;i<expInterpPoints.Count;i++) 
            {
                fwdRates.Add((expInterpPoints[i - 1] / expInterpPoints[i] - 1) / fwdRateTenor);
            }

            var outputFwdCurve = new Curve(curve.Name, curve.DayCountConvention, curve.Currency, curve.CurveDate, tenors.ToArray(), fwdRates.ToArray());

            return outputFwdCurve;
        }


        public ICurve RemoveOutliersFromCurve(ICurve curve, double zScore)
        {
            // the curve will come in with discount factors, compute rates and then convexity, then take average and std dev of convexity and exclude anything outside of zscore
            // only remove one point per iteration
            // for now we only want to remove one point

            var fwdRates = new List<Tuple<double,double,double>>();
            var convexity = new List<Tuple<double,double>>();

            var cumulativeTenor = 0.0;

            for(var i=1;i<curve.X.Length;i++)
            {
                var tenor = curve.X[i] - curve.X[i - 1];
                cumulativeTenor += tenor;
                var fwdRate = (curve.Y[i - 1] / curve.Y[i] - 1.0) / tenor;
                fwdRates.Add(new Tuple<double, double, double>(tenor, fwdRate, cumulativeTenor));
            }

            // convexity and then z scores
            for (var i=1;i<fwdRates.Count-1;i++) 
            {
                var t1 = (fwdRates[i].Item2 - fwdRates[i - 1].Item2) / fwdRates[i].Item1;
                var t2 = (fwdRates[i+1].Item2 - fwdRates[i].Item2) / fwdRates[i+1].Item1;
                convexity.Add(new Tuple<double, double>(fwdRates[i].Item3, t1 - t2));
            }

            var convexityAverage = convexity.Select(c => c.Item2).Average();
            var convexityStdDev = convexity.Select(c => c.Item2).StandardDeviation();
            var zScores = convexity.Select(c => Math.Abs((c.Item2 - convexityAverage) / convexityStdDev));

            var zScoresWithTenors = convexity.Select(c => c.Item1).ToList().Zip(zScores, (a, b) => new Tuple<double, double>(a, b));

            var filteredZScore = zScoresWithTenors.Where(s => s.Item2 >= zScore).ToList();

            if (filteredZScore.Count < 1)
                return curve;

            var filteredX = new List<double>();
            var filteredY = new List<double>();

            for(var i=0;i<curve.X.Length; i++)
            {
                if (Math.Abs(curve.X[i] - filteredZScore.First().Item1) < 1e-6)
                    continue;
                filteredX.Add(curve.X[i]);
                filteredY.Add(curve.Y[i]);
            }

            return new Curve(curve.Name, curve.DayCountConvention, curve.Currency, curve.CurveDate, filteredX.ToArray(), filteredY.ToArray());
        }

        #endregion

    }
}
