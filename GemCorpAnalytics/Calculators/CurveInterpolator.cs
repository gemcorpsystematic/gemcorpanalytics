﻿namespace GemCorpAnalytics.Calculators
{
    using System;
    using System.Linq;
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.Interpolators;

    /// <summary>The curve interpolator.</summary>
    public class CurveInterpolator : ICurveInterpolator
    {

        #region Fields

        /// <summary>The curve.</summary>
        private readonly ICurve _curve;

        /// <summary>The interpolator.</summary>
        private readonly IInterpolator _interpolator;

        #endregion

        #region Constructor

        /// <summary>Initialise an instance of the curve interpolator.</summary>
        /// <param name="curve">The curve.</param>
        /// <param name="interpolator">The interpolator.</param>
        public CurveInterpolator(
            ICurve curve,
            IInterpolator interpolator)
        {
            _curve = curve ?? throw new ArgumentNullException(nameof(curve));
            _interpolator = interpolator ?? throw new ArgumentNullException(nameof(interpolator));
        }

        #endregion

        #region Public Methods
        
        /// <summary>Gets value for single tenor point.</summary>
        /// <param name="x">The tenor point.</param>
        /// <returns>The interpolated values.</returns>
        public double GetValue(double x)
        {
            var interpValue = _interpolator.GetValue(_curve.X, _curve.Y, x);
            return interpValue;
        }

        /// <summary>Gets value for an array of tenors.</summary>
        /// <param name="x">The tenors.</param>
        /// <returns>The interpolated values.</returns>
        public double[] GetValue(double[] x)
        {
            var interpVals = x.Select(p => _interpolator.GetValue(_curve.X, _curve.Y, p)).ToArray();
            return interpVals;
        }

        #endregion
    }
}
