﻿namespace GemCorpAnalytics.Calculators
{
    using System;
    using System.Globalization;

    /// <summary>The instrument parser.</summary>
    public static class InstrumentParser
    {

        public static string GetInstrumentString(string identifier, string instrumentType)
        {

            switch(instrumentType.ToLower())
            {
                case "tbillbond":
                case "bond":
                case "perpetualbond":
                case "callablebond":
                    return ParseBond(identifier);
                case "bondfuture":
                    return ParseBondFuture(identifier);
                case "bondfuturelistedoption":
                    return ParseBondFutureListedOption(identifier);
                case "cds":
                    return ParseCds(identifier);
                case "commodityfuture":
                    return identifier;
                case "commodityfuturelistedoption":
                    return ParseCommodityFutureListedOption(identifier);
                case "els":
                case "equity":
                    return ParseEquity(identifier);
                case "equityoption":
                case "equitylistedoption":
                    return ParseEquityListedOption(identifier);
                case "equityindexlistedoption":
                    return ParseEquityIndexListedOption(identifier);
                case "equityindexfuturelistedoption":
                    return ParseEquityIndexFutureListedOption(identifier);
                case "fxoption":
                    return ParseFxOption(identifier);
                case "inflationswap":
                    return ParseInflationSwap(identifier);
                case "interestfuture":
                    return "";
                case "interestfuturelistedoption":
                    return ParseInterestFutureListedOption(identifier);
                case "loan":
                    return ParseLoan(identifier);
                case "loandeposit":
                    return ParseLoanDeposit(identifier);
                case "fx":
                case "fxswap":
                    return ParseFxSwap(identifier);
                case "swap":
                    return ParseSwap(identifier);
                case "nds":
                    return ParseNonDeliverableSwap(identifier);
                case "trs":
                    return "trs";
                case "repo":
                    return ParseRepo(identifier);
                case "none":
                    return string.Empty;
                default:
                    throw new Exception($"Unable to handle instrument type: {instrumentType},{identifier}");
            }

        }

        /// <summary>Parses a bond string.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The parsed bond representation.</returns>
        public static string ParseBond(string identifier) 
        {
            var stringTokens = identifier.Split(' ');

            // Issuer coupon maturity REGS
            var issuer = "";
            var coupon = 0.0;
            DateTime maturity = DateTime.MinValue;

            foreach(var token in stringTokens)
            {
                if(string.IsNullOrEmpty(issuer))
                    issuer = token;
                
                if(coupon==0.0)
                {
                    try
                    {
                        coupon = Convert.ToDouble(token);
                    }catch(Exception e)
                    {
                        continue;
                    }
                }

                // ignore maturity condition for perpetual bond
                if(maturity==DateTime.MinValue && !identifier.ToLower().Contains("perp"))
                {
                    try 
                    {
                        maturity = DateTime.ParseExact(token, "MM/dd/yy",CultureInfo.InvariantCulture);
                    }catch(Exception e)
                    { continue; 
                    }
                }

            }

            if (issuer == "T" || issuer == "B")
                issuer = "USD";

            return $"{issuer}{maturity:yy}";
        }

        
        /// <summary>Parses a bond future.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The bond future.</returns>
        public static string ParseBondFuture(string identifier) 
        {
            var identifierTokens = identifier.Split(' ');

            var tenor = identifierTokens[0];
            var maturity = identifierTokens[identifierTokens.Length - 1];

            var underlier = string.Empty;

            for(var i=1;i<(identifierTokens.Length - 1);i++)
            {
                underlier += identifierTokens[i];
            }

            return $"{underlier},{tenor}";
        }

        /// <summary>Parses bond future listed option.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The identifier.</returns>
        public static string ParseBondFutureListedOption(string identifier)
        {
            var identifierTokens = identifier.Split(' ');
            var issuer = identifierTokens[2];

            return $"{issuer}";
        }

        /// <summary>Parses a cds.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The cds string.</returns>
        public static string ParseCds(string identifier) 
        {
            var identifierTokens = identifier.Split(' ');
            var direction = identifierTokens[0];
            var maturityYear = Convert.ToInt32(identifierTokens[identifierTokens.Length - 1]);
            var maturityMonth = identifierTokens[identifierTokens.Length - 2];
            var parAmount = Convert.ToDouble(identifierTokens[identifierTokens.Length - 3]);
            var currency = identifierTokens[identifierTokens.Length - 4];

            var underlier = string.Empty;

            for(var i=1;i< identifierTokens.Length - 4;i++)
            {
                underlier += identifierTokens[i];
            }

            return $"{direction},{underlier},{maturityYear}";
        }

        /// <summary>The commoddity future option listed.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The commodity future listed option string.</returns>
        public static string ParseCommodityFutureListedOption(string identifier)
        {
            var identifierTokens = identifier.Split(' ');

            var optionType = identifierTokens[0];
            var strike = Convert.ToDouble(identifierTokens[identifierTokens.Length-1]);
            var maturity = identifierTokens[identifierTokens.Length - 2];

            var underlier = string.Empty;

            for(var i=1;i< identifierTokens.Length - 2;i++)
            {
                underlier += identifierTokens[i] + " ";
            }

            underlier = underlier.TrimEnd(' ');

            return underlier;
        }

        /// <summary>The equity identifier.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The equity string.</returns>
        public static string ParseEquity(string identifier)
        {
            return identifier;
        }

        /// <summary>Parse an equity listed option.</summary>
        /// <param name="identifier">The identifier</param>
        /// <returns>The equity listed option string.</returns>
        public static string ParseEquityListedOption(string identifier)
        {
            var identifierTokens = identifier.Split(' ');
            var optionType = identifierTokens[0];
            var strike = identifierTokens[identifierTokens.Length - 1];
            var maturity = identifierTokens[identifierTokens.Length - 2];

            var underlier = string.Empty;

            for (var i = 1; i < identifierTokens.Length - 2; i++)
            {
                underlier += identifierTokens[i] + " ";
            }

            underlier = underlier.TrimEnd(' ');

            if (underlier == "ARRIVAL")
                underlier = "ARRIVAL GROUP US";

            if (underlier == "TIP")
                underlier = "ISHARES TIPS BON US";

            return $"{underlier}";
        }

        /// <summary>Parses the equity index listed option.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The identifier.</returns>
        public static string ParseEquityIndexListedOption(string identifier)
        {
            var tokens = identifier.Split(' ');

            var issuer = tokens[1] == "SPX" || tokens[1] == "SPXW" ? "SPX" : "EURO STOXX 50";

            return $"{issuer}";
        }

        /// <summary>Parses the equity index future listed option.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The identifier.</returns>
        public static string ParseEquityIndexFutureListedOption(string identifier)
        {
            var tokens = identifier.Split(' ');
            var issuer = tokens[1] == "S&P" ? "SPX" : tokens[1];
            return $"{issuer}";
        }

        /// <summary>Parses the fx option.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The fx option string.</returns>
        public static string ParseFxOption(string identifier)
        {
            var identifierTokens = identifier.Split(' ');

            var optionType = identifierTokens[0];
            var exerciseType = identifierTokens[1];
            var strike = Convert.ToDouble(identifierTokens[3]);
            var underlierMatStub = identifierTokens[identifierTokens.Length - 1];
            var underlier = underlierMatStub.Split('|')[0];
            var maturity = DateTime.ParseExact(underlierMatStub.Split('|')[1], "d/MMM/yy", CultureInfo.InvariantCulture);

            return $"{underlier},{optionType}";
        }

        /// <summary>Parses the fx swap.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The fx swap string.</returns>
        public static string ParseFxSwap(string identifier)
        {
            var identifierTokens = identifier.Split(' ');
            var tokenStub = identifierTokens[1].Split('|');

            var underlier = string.Empty;
            var startDate = DateTime.MinValue;
            var endDate = DateTime.MinValue;

            if (tokenStub.Length < 3)
            {
                underlier = tokenStub[0];
                startDate = DateTime.ParseExact(tokenStub[1], "d/MMM/yy", CultureInfo.InvariantCulture);
            }
            else {
                underlier = tokenStub[0];
                startDate = DateTime.ParseExact(tokenStub[1], "d/MMM/yy", CultureInfo.InvariantCulture);
                endDate = DateTime.ParseExact(tokenStub[2], "d/MMM/yy", CultureInfo.InvariantCulture);
            }

            return $"{underlier}";
        }

        /// <summary>Parses inflation swap.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The identifier.</returns>
        public static string ParseInflationSwap(string identifier)
        {
            var tokens = identifier.Split('|');
            return $"{tokens[4]},InflationSwap";
        }

        /// <summary>Parses interest future.</summary>
        /// <param name="identifier">The identifiers.</param>
        /// <returns>The identifier.</returns>
        public static string ParseInterestFuture(string identifier)
        {
            var tokens = identifier.Split(' ');

            return tokens[tokens.Length-2];
        }

        /// <summary>Parses the interest future listed option.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The identifier.</returns>
        public static string ParseInterestFutureListedOption(string identifier)
        {
            var tokens = identifier.Split(' ');
            var issuer = tokens[1];
            return $"{issuer}";
        }

        /// <summary>Parses the loan identifier.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The identifier string.</returns>
        public static string ParseLoan(string identifier) 
        {
            if (identifier.Contains("RCF"))
                return "RCF";
            return identifier.Split(' ')[0];
        }


        /// <summary>Parses the loan deposit identifier.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The loan identifier string.</returns>
        public static string ParseLoanDeposit(string identifier)
        {
            var identifierTokens = identifier.Split(' ');

            var type = identifierTokens[0];
            var tenor = identifierTokens[1];
            var currency = identifierTokens[2];

            return $"{type},{currency}";
        }

        /// <summary>Parses a swap.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The swap string.</returns>
        public static string ParseNonDeliverableSwap(string identifier)
        {
            var identifierToken = identifier.Split(new String[] {"/R"}, StringSplitOptions.None);

            string payLeg;
            string receiveLeg;

            if (identifierToken[0].Contains(" P ")){
                payLeg = identifierToken[0];
                receiveLeg = identifierToken[1];
            }
            else {
                payLeg = identifierToken[1];
                receiveLeg = identifierToken[0];
            }

            var payLegCurrency = payLeg.Split(' ')[0].Trim();

            return $"{payLegCurrency}";
        }

        /// <summary>Parses the repo.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The repo string.</returns>
        public static string ParseRepo(string identifier)
        {
            var identifierToken = identifier.Split(' ');
            var repoType = identifierToken[0];
            var underlier = identifierToken[2];
            //var bondCoupon = Convert.ToDouble(identifierToken[3]);
            //var maturity = DateTime.ParseExact(identifierToken[4], "MM/dd/yy", CultureInfo.InvariantCulture);

            //return $"{repoType},{underlier}";
            return $"Repo,{underlier}";
        }


        /// <summary>Parses a swap.</summary>
        /// <param name="identifier">The identifier.</param>
        /// <returns>The swap string.</returns>
        public static string ParseSwap(string identifier)
        {
            var identifierToken = identifier.Split(new String[] { "/R" }, StringSplitOptions.None);
            var payLeg = identifierToken[0];
            var receiveLeg = identifierToken[1];
            var currency = payLeg.Split(' ')[1];
            return $"{currency},swap";
        }


    }
}
