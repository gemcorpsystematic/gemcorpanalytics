﻿namespace GemCorpAnalytics.Calculators
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using GemCorpAnalytics.Calculators.Interfaces;
    
    /// <summary>The risk allocation calculator result.</summary>
    [DataContract]
    public class RiskAllocationCalculatorResult : IRiskAllocationCalculatorResult
    {
        #region Constructor

        /// <summary>The risk allocation calculator result.</summary>
        /// <param name="riskAllocation">The risk allocation.</param>
        public RiskAllocationCalculatorResult(
            Dictionary<string, double> riskAllocation)
        {
            RiskAllocation = riskAllocation;
        }

        #endregion

        #region Public Properties

        /// <summary>The risk allocation.</summary>
        [DataMember]
        public Dictionary<string, double> RiskAllocation { get; }

        #endregion
    }
}
