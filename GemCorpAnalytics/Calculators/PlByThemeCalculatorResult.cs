﻿namespace GemCorpAnalytics.Calculators
{
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.Calculators.Interfaces;
    using System.Data;
    using System.Runtime.Serialization;
    using System.Collections.Generic;

    /// <summary>The pl by theme calculator result.</summary>
    [DataContract]
    public class PlByThemeCalculatorResult : IPlByThemeCalculatorResult
    {

        #region Constructor
        
        /// <summary>Initialize an instance of the pl by theme calculator.</summary>
        /// <param name="resultsTable">The results table.</param>
        /// <param name="themesCollection">The themes collection.</param>
        /// <param name="missingThemes">The missing themes.</param>
        public PlByThemeCalculatorResult(
            IList<DataTable> resultsTable,
            List<IThemes> themesCollection,
            List<IThemes> missingThemes)
        {
            ResultsTable = resultsTable;
            ThemesCollection = themesCollection;
            MissingThemes = missingThemes;
        }

        #endregion


        #region Public Properties

        /// <summary>The results table.</summary>
        [DataMember]
        public IList<DataTable> ResultsTable { get; protected set; }

        /// <summary>The themese collection.</summary>
        [DataMember]
        public List<IThemes> ThemesCollection { get; protected set; }

        /// <summary>The trades with missing themes.</summary>
        [DataMember]
        public List<IThemes> MissingThemes { get; protected set; }

        #endregion

    }
}
