﻿namespace GemCorpAnalytics.Calculators
{
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>The conso risk report summary calculator.</summary>
    public class ConsoRiskReportSummaryCalculator : IConsoRiskReportSummaryCalculator
    {
        #region Fields

        /// <summary>The tolerance.</summary>
        private const double Tolerance = 1e-6;

        #endregion


        #region Methods

        /// <summary>Calculates the conso risk report summary.</summary>
        /// <param name="consoRiskReportResult">The conso risk report result.</param>
        /// <returns>The <see cref="IConsoRiskReportSummary"/>.</returns>
        public IConsoRiskReportSummary Calculate(IConsoRiskReportResult consoRiskReportResult)
        {
            // Equity, EquityVolatility, Interest, Forex, Commodity, CommodityVolatility, BondSpread, FXBase
            // EquityLadder, EquityVolLadder, InterestRateLadder, FXLadder, FXVolLadder, CommodityLadder, CommodityVolLadder, BondSpreadLadder

            // The more I think about this method, the more I feel this is redundant....

            // Equity -> I would like a breakdown by underlier and delta. in the object it will be trade values

            


            var equityBreakDown = consoRiskReportResult.Perturbations.Where(p=>string.Equals(p.Name,"Equity"));

            var equityDeltaBreakdown = equityBreakDown.Select(b =>
            {
                var delta = b.TradeDeltaBase.Values.Sum();
                return new KeyValuePair<string, double>(b.Description, delta);
            }).ToDictionary(x => x.Key, y => y.Value);

            var interestBreakdown = consoRiskReportResult.Perturbations.Where(p=>string.Equals(p.Name,"Interest"));

            var interestDeltaBreakdown = interestBreakdown.Select(b =>
            {
                var delta = b.TradeBaseResults.Values.Sum();
                return new KeyValuePair<string, double>(b.Description, delta);
            }).ToDictionary(x => x.Key, y => y.Value);

            // InterestPerturbations, Creditperturbations, Equity, EquityVol, Forex, Commodity, CommodityVol

            // Don't hardcode BondSpread.CS01#1, make it dynamic choices include CS01#10
            // It might help to store trades by entity, strategy and book
            // keep them as they are, then group by entity and book as required. No need to group as per below.
            var equityPerturbations = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "Equity"));
            var equityVolPerturbations = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "Equity Volatility"));
            var interestPerturbations = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "Interest"));
            var forexPerturbations = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "Forex"));
            var bondPerturbations = consoRiskReportResult.Perturbations.FirstOrDefault(p => string.Equals(p.Description, "BondSpread.CS01#1")); 
            var commodityPerturbations = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "Commodity"));
            var commodityVolPerturbations = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "Commodity Volatility"));
            var fxBasePerturbations = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "FX Base"));
            var creditPerturbations = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "CreditSpread"));

            var curveCurrency = interestPerturbations.Select(p=>p.CurveDescription).Distinct().ToList();

            var interestOutput = curveCurrency.Select(c=> {
                var perturbSubset = interestPerturbations.Where(p => string.Equals(p.CurveDescription, c));
                var delta = 0.0;
                foreach (var perturb in perturbSubset)
                {
                    foreach (var k in perturb.TradeBaseResults.Keys)
                    {
                        delta += perturb.TradeBaseResults[k];
                    }
                }
                return new KeyValuePair<string, double>(c, delta);
            }).ToDictionary(x=>x.Key, y=>y.Value);


            var bondSpread = new Dictionary<ITrade, double>();
            var commodity = CreateGroupSensitivitiesByTrade(consoRiskReportResult.Trades, commodityPerturbations, "commodity");
            var commodityVol = CreateGroupSensitivitiesByTrade(consoRiskReportResult.Trades, commodityVolPerturbations, "commodity vol");
            var equity = CreateGroupSensitivitiesByTrade(consoRiskReportResult.Trades, equityPerturbations, "equity");
            var equityVol = CreateGroupSensitivitiesByTrade(consoRiskReportResult.Trades, equityVolPerturbations, "equity vol");
            var interest = CreateGroupSensitivitiesByTrade(consoRiskReportResult.Trades, interestPerturbations, "interest");
            var forex = CreateGroupSensitivitiesByTrade(consoRiskReportResult.Trades, forexPerturbations, "forex"); // this needs to be underlier info

            //var countryOfRisk = relevantTrades.Select(t => t.CountryOfRisk).Distinct();
            // possibly need to add a country of risk.

            var trades = consoRiskReportResult.Trades;

            foreach (var trade in trades)
            {
                var delta = 0.0;
                var deltaFlag = bondPerturbations.TradeBaseResults.TryGetValue(trade.TradeId, out var tmpDelta);
                delta += tmpDelta;
                
                if (Math.Abs(delta) > Tolerance)
                {
                    bondSpread[trade] = delta;
                }   
            }

            // group the perturbations by Trade
            
            return new ConsoRiskReportSummary(consoRiskReportResult.ValueDate, bondSpread, commodity, commodityVol, equity, equityVol, forex, null, interest);
            // there will be many commodity shocks
        }

        /// <summary>The conso risk report result.</summary>
        /// <param name="consoRiskReportResult">The conso risk report result.</param>
        /// <param name="books">The books</param>
        /// <param name="tradeFilter">The trade filter.</param>
        /// <returns></returns>
        public IConsoRiskReportSummary CalculateUsingFilter(
            IConsoRiskReportResult consoRiskReportResult, 
            List<string> books, 
            Func<ITrade, bool> tradeFilter) 
        {
            var equityBreakDown = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "Equity"));

            var equityDeltaBreakdown = equityBreakDown.Select(b =>
            {
                var delta = b.TradeDeltaBase.Values.Sum(); // this is sum across all the trades
                return new KeyValuePair<string, double>(b.Description, delta);
            }).ToDictionary(x => x.Key, y => y.Value);

            var interestBreakdown = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "Interest"));

            var interestDeltaBreakdown = interestBreakdown.Select(b =>
            {
                var delta = b.TradeBaseResults.Values.Sum();
                return new KeyValuePair<string, double>(b.Description, delta);
            }).ToDictionary(x => x.Key, y => y.Value);

            var bondPerturbations = consoRiskReportResult.Perturbations.FirstOrDefault(p => string.Equals(p.Description, "BondSpread.CS01#1"));
            var commodityPerturbations = consoRiskReportResult.Perturbations.Where(p => string.Equals(p.Name, "Commodity"));

            var bondSpread = new Dictionary<ITrade, double>();
            var commodity = new Dictionary<ITrade, double>();

            foreach (var book in books)
            {
                var relevantTrades = consoRiskReportResult.Trades.Where(t => string.Equals(t.Book, book));
                var relevantTradeIds = relevantTrades.Where(t => tradeFilter(t)).Select(t => t.TradeId).Distinct();
                //var countryOfRisk = relevantTrades.Select(t => t.CountryOfRisk).Distinct();
                // possibly need to add a country of risk.

                foreach (var tradeId in relevantTradeIds)
                {
                    var delta = 0.0;
                    var deltaFlag = bondPerturbations.TradeBaseResults.TryGetValue(tradeId, out var tmpDelta);
                    delta += tmpDelta;

                    var trade = relevantTrades.First(rv => rv.TradeId == tradeId);

                    if (Math.Abs(delta) > Tolerance)
                    {
                        bondSpread[trade] = delta;
                    }

                }

                foreach (var tradeId in relevantTradeIds)
                {
                    var commodDelta = 0.0;
                    foreach (var commodPerturbation in commodityPerturbations)
                    {
                        var tmpDeltaFlag = commodPerturbation.TradeBaseResults.TryGetValue(tradeId, out var commodTmpDelta);
                        commodDelta += commodTmpDelta;
                    }

                    var trade = relevantTrades.First(rv => rv.TradeId == tradeId);

                    if (Math.Abs(commodDelta) > Tolerance)
                    {
                        commodity[trade] = commodDelta;
                    }
                }

            }

            return new ConsoRiskReportSummary(consoRiskReportResult.ValueDate, bondSpread, commodity, null, null, null, null, null, null);
        }

        #endregion

        #region

        private IDictionary<ITrade, double> CreateGroupSensitivitiesByTrade(IList<Trade> trades, IEnumerable<Perturbation> perturbations, string assetClass)
        {
            var output = new Dictionary<ITrade, double>();

            switch (assetClass.ToLower()) {

                case "commodity":
                case "interest":
                    foreach (var trade in trades)
                    {
                        var delta = 0.0;
                        foreach (var perturbation in perturbations)
                        {
                            double commodTmpDelta = 0.0;
                            var tmpDeltaFlag = perturbation.TradeBaseResults.TryGetValue(trade.TradeId, out commodTmpDelta);
                            delta += commodTmpDelta;
                        }

                        if (Math.Abs(delta) > Tolerance)
                        {
                            output[trade] = delta;
                        }
                    }
                    return output;

                case "equity":
                case "forex":

                    foreach (var trade in trades)
                    {
                        var delta = 0.0;
                        foreach (var perturbation in perturbations)
                        {
                            double commodTmpDelta = 0.0;
                            var tmpDeltaFlag = perturbation.TradeDeltaBase.TryGetValue(trade.TradeId, out commodTmpDelta);
                            delta += commodTmpDelta;
                        }

                        if (Math.Abs(delta) > Tolerance)
                        {
                            output[trade] = delta;
                        }
                    }
                    return output;

                case "equity vol":
                case "commodity vol":

                    foreach (var trade in trades)
                    {
                        var delta = 0.0;
                        foreach (var perturbation in perturbations)
                        {
                            double commodTmpDelta = 0.0;
                            var tmpDeltaFlag = perturbation.VegaByTrade.TryGetValue(trade.TradeId, out commodTmpDelta);
                            delta += commodTmpDelta;
                        }

                        if (Math.Abs(delta) > Tolerance)
                        {
                            output[trade] = delta;
                        }
                    }
                    return output;

                default:
                    throw new NotImplementedException();

            }
            return output;
        }

        #endregion

    }
}
