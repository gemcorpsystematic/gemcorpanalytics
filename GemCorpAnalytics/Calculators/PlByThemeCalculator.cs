﻿namespace GemCorpAnalytics.Calculators
{
    using GemCorpAnalytics.Enums;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.Calculators.Interfaces;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.DataCollections;
    using System;
    using System.Data;
    using System.Linq;
    using System.Collections.Generic;
    using MathNet.Numerics.Statistics;

    /// <summary>The pl by theme calculator.</summary>
    public class PlByThemeCalculator : IPlByThemeCalculator
    {
        #region Fields

        /// <summary>The pl by theme.</summary>
        private readonly IList<IPlEntry> _plByTheme;

        /// <summary>The var data table.</summary>
        private readonly IList<IValueAtRiskEntry> _varDataTable;

        /// <summary>The pl by theme context.</summary>
        private readonly PlByThemeContext _plByThemeContext;

        /// <summary>The daily pl calculator.</summary>
        private readonly Func<IEnumerable<IPlEntry>, double> _dailyPlCalculator = r=>r.Sum(f=>f.PlDaily);

        /// <summary>The weekly pl calculator.</summary>
        private readonly Func<IEnumerable<IPlEntry>, double> _weeklyPlCalculator = r=>r.Sum(f=>f.PlWeek);
        
        /// <summary>The monthly pl calculator.</summary>
        private readonly Func<IEnumerable<IPlEntry>, double> _monthlyPlCalculator = r=>r.Sum(f=>f.PlMonth);
        
        /// <summary>The ytd to calculator.</summary>
        private readonly Func<IEnumerable<IPlEntry>, double> _ytdPlCalculator = r=>r.Sum(f=>f.PlYtd);

        /// <summary>The value at risk 95% aggregator.</summary>
        private readonly Func<IEnumerable<IValueAtRiskEntry>, double> _var95AggCalculator = r => CalculateVaR(r, 5);
        
        /// <summary>The value at risk 99% aggregator.</summary>
        private readonly Func<IEnumerable<IValueAtRiskEntry>, double> _var99AggCalculator = r => CalculateVaR(r, 1);

        #endregion


        #region Constructor

        /// <summary>Initialize an instance of the pl by theme calculator.</summary>
        /// <param name="plByTheme">The pl by theme.</param>
        public PlByThemeCalculator(
            IList<IPlEntry> plByTheme,
            IList<IValueAtRiskEntry> varDataTable,
            PlByThemeContext plByThemeContext)
        {
            _plByTheme = plByTheme;
            _varDataTable = varDataTable;
            _plByThemeContext = plByThemeContext;
        }

        #endregion

        #region Public Methods

        /// <summary>Aggregates the pl by themes and frequencies.</summary>
        /// <returns>The <see cref="Dictionary{TKey, TValue}"/>.</returns>
        public IPlByThemeCalculatorResult Calculate(FundNames fundName)
        {
            try
            {
                // still need overall VaR, just pass in all rows into var aggregate function
                var output = new List<DataTable>();
                var distinctThemes = _plByTheme.Select(r => r.Theme).Distinct();
                var missingThemes = _plByTheme.Where(r => r.Theme.ToLower() == "missing theme").ToList();
                var vaR95Dictionaries = CalculateVaRDictionaries(distinctThemes, _varDataTable, _plByThemeContext.FundConfigs[fundName], _var95AggCalculator);
                var vaR99Dictionaries = CalculateVaRDictionaries(distinctThemes, _varDataTable, _plByThemeContext.FundConfigs[fundName], _var99AggCalculator);
                var dailyPlDictionaries = CalculateDailyPlDictionaries(distinctThemes, _plByTheme, _plByThemeContext.FundConfigs[fundName],_dailyPlCalculator);
                var weeklyPlDictionaries = CalculateDailyPlDictionaries(distinctThemes, _plByTheme, _plByThemeContext.FundConfigs[fundName], _weeklyPlCalculator);
                var monthlyPlDictionaries = CalculateDailyPlDictionaries(distinctThemes, _plByTheme, _plByThemeContext.FundConfigs[fundName], _monthlyPlCalculator);
                var yearlyPlDictionaries = CalculateDailyPlDictionaries(distinctThemes, _plByTheme, _plByThemeContext.FundConfigs[fundName], _ytdPlCalculator);
                

                
                output = new List<DataTable>{
                    AggregateStrategyTable(dailyPlDictionaries.Item1,
                    weeklyPlDictionaries.Item1, monthlyPlDictionaries.Item1, yearlyPlDictionaries.Item1,
                    vaR95Dictionaries.Item1, vaR99Dictionaries.Item1, _varDataTable, fundName)
                    ,AggregateDataTable(dailyPlDictionaries.Item2,
                    weeklyPlDictionaries.Item2, monthlyPlDictionaries.Item2, yearlyPlDictionaries.Item2,
                    vaR95Dictionaries.Item2, vaR99Dictionaries.Item2, fundName.ToString()) };

                var tradesMissingThemes = missingThemes.Count > 0 ? missingThemes.Select(t => {
                    return new Themes.Builder
                    {
                        CountryOfRisk = $"{t.UnderlierInfo}, {t.Book}",
                        Theme = t.Theme,
                        Region = string.Empty
                    }.Build();
                }).ToList() : new ThemesCollection();

                var themesCollection = _plByTheme.Where(p => p.Theme.ToLower() != "missing theme").Select(p =>
                {
                    return new Themes.Builder {
                        CountryOfRisk = $"{p.UnderlierInfo},{p.Book}",
                        Theme = p.Theme,
                        Region = string.Empty
                    }.Build();
                }).ToList();

                return new PlByThemeCalculatorResult(output, themesCollection, tradesMissingThemes);
            }catch(Exception e)
            {
                var errMsg = e.InnerException != null ? e.InnerException.Message : e.Message;
                throw new Exception(errMsg);
            }
        }

        #endregion

        #region Methods

        private Tuple<Dictionary<FundStrategies, double>, Dictionary<string, double>> CalculateDailyPlDictionaries(
            IEnumerable<string> distinctThemes,
            IList<IPlEntry> dailyPnlTable,
            Dictionary<FundStrategies, IReadOnlyList<string>> stratDict,
            Func<IEnumerable<IPlEntry>, double> aggregateFunc)
        {

            var fundBooks = new List<string>();

            var fundStratDict = new Dictionary<FundStrategies, double>();
            foreach(var fundStrat in stratDict)
            {
                fundBooks.AddRange(fundStrat.Value);
                var fundStratEntries = dailyPnlTable
                    .Where(r => fundStrat.Value.Contains(r.Book));
                fundStratDict[fundStrat.Key] = aggregateFunc(fundStratEntries);
            }

            var dailyPlByThemeFund = distinctThemes.Select(t =>
            {
                var pnlEntries = dailyPnlTable.AsEnumerable()
                .Where(r => r.Theme == t && fundBooks.Contains(r.Book));
                var sumByTheme = aggregateFunc(pnlEntries);
                return new KeyValuePair<string, double>(t, sumByTheme);
            }).ToDictionary(x => x.Key, y => y.Value);

            var output = new Tuple<Dictionary<FundStrategies, double>, Dictionary<string, double>>(
                fundStratDict,
                dailyPlByThemeFund);

            return output;
        }

        private Tuple<Dictionary<FundStrategies, double>, Dictionary<string, double>> CalculateVaRDictionaries(
            IEnumerable<string> distinctThemes,
            IList<IValueAtRiskEntry> dailyPnlTable,
            Dictionary<FundStrategies, IReadOnlyList<string>> stratDict,
            Func<IEnumerable<IValueAtRiskEntry>, double> aggregateFunc)
        {

            var fundBooks = new List<string>();

            var fundStratDict = new Dictionary<FundStrategies, double>();
            foreach (var fundStrat in stratDict)
            {
                fundBooks.AddRange(fundStrat.Value);
                var fundStratEntries = dailyPnlTable
                    .Where(r => fundStrat.Value.Contains(r.Book));
                fundStratDict[fundStrat.Key] = aggregateFunc(fundStratEntries);
            }

            var dailyPlByThemeFund = distinctThemes.Select(t =>
            {
                var pnlEntries = dailyPnlTable.AsEnumerable()
                .Where(r => r.Theme == t && fundBooks.Contains(r.Book));
                var sumByTheme = aggregateFunc(pnlEntries);
                return new KeyValuePair<string, double>(t, sumByTheme);
            }).ToDictionary(x => x.Key, y => y.Value);

            var output = new Tuple<Dictionary<FundStrategies, double>, Dictionary<string, double>>(
                fundStratDict,
                dailyPlByThemeFund);

            return output;
        }

        private static double CalculateVaR(IEnumerable<IValueAtRiskEntry> dataRows, int percentile)
        {
            if (dataRows.Count() < 1) return 0.0;

            var results = new List<double[]>();

            foreach (var dataRow in dataRows)
            {
                var result = dataRow.Result;
                var splitResult = result.Split('|');
                var splitResultDouble = splitResult.Select(r => Convert.ToDouble(r)).ToArray();
                results.Add(splitResultDouble);
            }

            var lengthOfResults = results.First().Length;

            var finalTimeSeries = new double[lengthOfResults];

            for (var i = 0; i < lengthOfResults; i++)
            {
                var tmp = 0.0;
                foreach (var result in results)
                {
                    tmp += result[i];
                }
                finalTimeSeries[i] = tmp;
            }

            var pctile = Statistics.QuantileCustom(finalTimeSeries, percentile / 100d, QuantileDefinition.R4);

            return  Math.Min(0.0, pctile);

        }

        private DataTable AggregateStrategyTable(
            Dictionary<FundStrategies, double> dailyPl,
            Dictionary<FundStrategies, double> weeklyPl,
            Dictionary<FundStrategies, double> monthlyPl,
            Dictionary<FundStrategies, double> ytdPl,
            Dictionary<FundStrategies, double> vaR95,
            Dictionary<FundStrategies, double> vaR99,
            IList<IValueAtRiskEntry> valueAtRiskEntryCollection,
            FundNames fundName)
        {
            var output = new DataTable($"{fundName} Summary");
            var cols = new[] { 
                CreateColumn("Strategy","System.String"),
                CreateColumn("DailyPl","System.Double"),
                CreateColumn("WeeklyPl","System.Double"),
                CreateColumn("MonthlyPl","System.Double"),
                CreateColumn("YtdPl","System.Double"),
                CreateColumn("VaR95","System.Double"),
                CreateColumn("VaR99","System.Double"),
            };

            foreach(var col in cols) { output.Columns.Add(col); }

            var strategies = dailyPl.Keys;
            
            foreach(var strat in strategies)
            {
                var row = output.NewRow();
                row["Strategy"] = strat;
                row["DailyPl"] = dailyPl.TryGetValue(strat, out var dailyPlVal) ? dailyPlVal : 0.0;
                row["WeeklyPl"] = weeklyPl.TryGetValue(strat, out var weeklyPlVal) ? weeklyPlVal : 0.0;
                row["MonthlyPl"] = monthlyPl.TryGetValue(strat, out var monthlyPlVal) ? monthlyPlVal : 0.0;
                row["YtdPl"] = ytdPl.TryGetValue(strat, out var ytdPlVal) ? ytdPlVal : 0.0;
                row["VaR95"] = vaR95.TryGetValue(strat, out var vaR95Val) ? vaR95Val : 0.0;
                row["VaR99"] = vaR99.TryGetValue(strat, out var vaR99Val) ? vaR99Val : 0.0;
                output.Rows.Add(row);
            }

            var dailyPlTotal = 0.0;
            var weeklyPlTotal = 0.0;
            var monthlyPlTotal = 0.0;
            var ytdPlTotal = 0.0;

            foreach(var r in output.AsEnumerable())
            {
                dailyPlTotal += r.Field<double>("DailyPl");
                weeklyPlTotal += r.Field<double>("WeeklyPl");
                monthlyPlTotal += r.Field<double>("MonthlyPl");
                ytdPlTotal += r.Field<double>("YtdPl");
            }


            var fundBooksByFund = _plByThemeContext.FundConfigs[fundName];
            var fundBooks = new List<string>();

            foreach(var fundBook in fundBooksByFund)
            {
                fundBooks.AddRange(fundBook.Value);
            }

            var dataTableRows = valueAtRiskEntryCollection.Where(r => fundBooks.Contains(r.Book));

            var totalRow = output.NewRow();
            totalRow["Strategy"] = "Grand Total";
            totalRow["DailyPl"] = dailyPlTotal;
            totalRow["WeeklyPl"] = weeklyPlTotal;
            totalRow["MonthlyPl"] = monthlyPlTotal;
            totalRow["YtdPl"] = ytdPlTotal;
            totalRow["VaR95"] = _var95AggCalculator(dataTableRows);
            totalRow["VaR99"] = _var99AggCalculator(dataTableRows);
            output.Rows.Add(totalRow);

            return output;
        }

        private DataTable AggregateDataTable(
            Dictionary<string, double> dailyPl,
            Dictionary<string, double> weeklyPl,
            Dictionary<string, double> monthlyPl,
            Dictionary<string, double> ytdPl,
            Dictionary<string, double> vaR95,
            Dictionary<string, double> vaR99,
            string fundName)
        {
            var output = new DataTable(fundName);
            var cols = new[] {
            CreateColumn("Themes", "System.String"),
            CreateColumn("DailyPl", "System.Double"),
            CreateColumn("WeeklyPl", "System.Double"),
            CreateColumn("MonthlyPl", "System.Double"),
            CreateColumn("YtdPl", "System.Double"),
            CreateColumn("VaR95", "System.Double"),
            CreateColumn("VaR99", "System.Double")
            };

            foreach(var col in cols) { output.Columns.Add(col); };

            var themes = dailyPl.Keys.ToList();
            themes.AddRange(weeklyPl.Keys.ToList());
            themes.AddRange(monthlyPl.Keys.ToList());
            themes.AddRange(ytdPl.Keys.ToList());
            themes.AddRange(vaR95.Keys.ToList());
            themes.AddRange(vaR99.Keys.ToList());
            var distinctThemes = themes.Distinct();

            foreach(var theme in distinctThemes)
            {
                var row = output.NewRow();
                row["Themes"] = theme;
                row["DailyPl"] = dailyPl.TryGetValue(theme, out var dailyPlVal) ? dailyPlVal : 0.0;
                row["WeeklyPl"] = weeklyPl.TryGetValue(theme, out var weeklyPlVal) ? weeklyPlVal : 0.0;
                row["MonthlyPl"] = monthlyPl.TryGetValue(theme, out var monthlyPlVal) ? monthlyPlVal : 0.0;
                row["YtdPl"] = ytdPl.TryGetValue(theme, out var ytdPlVal) ? ytdPlVal : 0.0;
                row["VaR95"] = vaR95.TryGetValue(theme, out var vaR95Val) ? vaR95Val : 0.0;
                row["VaR99"] = vaR99.TryGetValue(theme, out var vaR99Val) ? vaR99Val : 0.0;
                output.Rows.Add(row);
            }

            var sortedTable = output.AsEnumerable()
                .OrderBy(r => r.Field<double>("VaR95"))
                .ThenBy(r => r.Field<double>("YtdPl"))
                .CopyToDataTable();

            var rowsToRemove = new List<DataRow>();

            foreach(DataRow row in sortedTable.Rows)
            {
                var sum = row.ItemArray.Sum(r => {
                    return r.GetType() == Type.GetType("System.Double") ? Convert.ToDouble(r) : 0.0;
                });
                if (Math.Abs(sum) < 1e-3)
                    rowsToRemove.Add(row);
            }

            foreach(var row in rowsToRemove) { sortedTable.Rows.Remove(row); }

            sortedTable.TableName = fundName;

            return sortedTable;
        }

        private DataColumn CreateColumn(string name, string dataType)
        {
            return new DataColumn
            {
                ColumnName = name,
                DataType = Type.GetType(dataType)
            };
        }


        #endregion

    }
}
