﻿namespace GemCorpAnalytics.Utils
{
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Enums;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Text.RegularExpressions;


    /// <summary>The utils.</summary>
    public static class Utils
    {
        /// <summary>Gets the time series return type.</summary>
        /// <param name="riskType">The risk type.</param>
        /// <returns>The <see cref="TimeSeriesReturnType"/>.</returns>
        public static TimeSeriesReturnType GetTimeSeriesReturnType(AssetClass assetClass)
        {
            switch (assetClass)
            {
                case AssetClass.Equity:
                case AssetClass.Fx:
                    return TimeSeriesReturnType.Relative;
                default:
                    return TimeSeriesReturnType.Absolute;

            }

        }

        /// <summary>The covariance function.</summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns>The covariance.</returns>
        public static double Covariance(double[] x, double[] y)
        {
            // E(xy) - E(x)E(y)

            var tmp = x.Zip(y, (a, b) => a * b).Sum() / x.Length;

            return tmp - x.Average() * y.Average();
        }

        /// <summary>Calculates variance</summary>
        /// <param name="x">The time series.</param>
        /// <returns>The variance.</returns>
        public static double Variance(double[] x)
        {

            var mu = x.Average();
            var tmp = 0.0;

            for (var i = 0; i < x.Length; i++)
            {
                tmp += Math.Pow(x[i] - mu, 2);
            }

            return tmp / (x.Length - 1);
        }

        /// <summary>Parses scenario ladder.</summary>
        /// <param name="scenario">The scenario.</param>
        /// <param name="riskType">The risk type.</param>
        /// <returns>The scenario.</returns>
        public static double ParseScenarioLadder(string scenario, string riskType)
        {
            var tokens = scenario.Split(' ');

            int numIdx = 0;
            foreach (var token in tokens)
            {
                if (token.Any(Char.IsDigit))
                    break;
                numIdx += 1;
            }

            // the rate ladders have a tag dv250 (+)
            var number = tokens[numIdx];
            number = number.TrimEnd(new[] { '%', '$', '+' });
            number = number.TrimStart(new[] { '%', '$', '+' });
            var numString = Regex.Match(number, @"[+-]?\d+?\d*").Value;
            var num = Int32.Parse(numString);
            var numAdj = Convert.ToDouble(num) / 100.0;

            if (riskType.ToLower().Contains("interest rate"))
            {
                var sign = tokens.Last();
                return sign.Contains('-') ? -numAdj : numAdj;
            }

            return numAdj;
        }

        public static Dictionary<string, Tuple<double,double>> CalculateFundLevelValueAtRisk(IEnumerable<ValueAtRiskEntry> valueAtRiskData, int percentile) 
        {
            var fundNames = valueAtRiskData.Select(d=>d.Entity).Distinct().ToList();
            var output = new Dictionary<string, Tuple<double, double>>();

            // need to combine Fund I LP and Fund I Limited

            foreach (var fundName in fundNames)
            {
                var fundData = valueAtRiskData.Where(d => string.Equals(fundName, d.Entity));
                
                // ned to sum up all the var data
                var fundVaRData = new double[256]; // might have to create a 0 array
                foreach (var tmpData in fundData)
                {
                    fundVaRData = fundVaRData.Zip(tmpData.VaRResults, (a, b) => a + b).ToArray();
                }
                // Now calculate percentile
                var fundVaR = MathNet.Numerics.Statistics.Statistics.QuantileCustom(fundVaRData, percentile / 100d, MathNet.Numerics.Statistics.QuantileDefinition.R4);
                var cvar = fundVaRData.Where(d => d <= fundVaR).Average();
                output[fundName] = new Tuple<double, double>(fundVaR, cvar);
            }

            var combinedFundData = valueAtRiskData.Where(d => d.Entity.ToLower().Contains("gemcorp fund i"));

            var combinedFundVaRData = new double[256]; // might have to create a 0 array
            foreach (var tmpData in combinedFundData)
            {
                combinedFundVaRData = combinedFundVaRData.Zip(tmpData.VaRResults, (a, b) => a + b).ToArray();
            }
            // Now calculate percentile
            var combinedFundVaR = MathNet.Numerics.Statistics.Statistics.QuantileCustom(combinedFundVaRData, percentile / 100d, MathNet.Numerics.Statistics.QuantileDefinition.R4);
            var combinedCVaR = combinedFundVaRData.Where(d => d <= combinedFundVaR).Average();
            output["Fund I LTD LP"] = new Tuple<double, double>(combinedFundVaR, combinedCVaR);

            return output;
        }


    }
}
