﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Runtime.Serialization;
    using Data.Interfaces;

    [DataContract]
    public class ValueAtRiskEntry : IValueAtRiskEntry
    {

        #region Constructor

        /// <summary>Initialise an instance of the value at risk entry.</summary>
        /// <param name="underlierInfo">The underlier info.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="description">The description.</param>
        /// <param name="book">The book.</param>
        /// <param name="currency">The currency.</param>
        /// <param name="tradeId">The trade id.</param>
        /// <param name="tradeDate">The trade date.</param>
        /// <param name="pv">The pv.</param>
        /// <param name="pvLocal">The pv local.</param>
        /// <param name="cvar">The cvar.</param>
        /// <param name="var">The var.</param>
        /// <param name="mvar">The mvar.</param>
        /// <param name="result">The result.</param>
        /// <param name="theme">The theme.</param>
        /// <param name="countryOfRisk">The country of risk.</param>
        /// <param name="varResults">The var results.</param>
        /// <param name="strategy">The strategy.</param>
        /// <param name="assetClass">The asset class.</param>
        /// <param name="parentFund">The parent fund.</param>
        /// <param name="productType">The product type.</param>
        private ValueAtRiskEntry(
            string underlierInfo,
            string entity,
            string description,
            string book,
            string currency,
            int tradeId,
            DateTime tradeDate,
            double pv,
            double pvLocal,
            double cvar,
            double var,
            double mvar,
            string result,
            string theme,
            string countryOfRisk,
            double[] varResults,
            string strategy,
            string assetClass,
            string parentFund,
            string productType)
        {
            UnderlierInfo = underlierInfo;
            Entity = entity;
            Description = description;
            Book = book;
            Currency = currency;
            TradeId = tradeId;
            TradeDate = tradeDate;
            Pv = pv;
            PvLocal = pvLocal;
            CVaR = cvar;
            VaR = var;
            MVaR = mvar;
            Result = result;
            Theme = theme;
            CountryOfRisk = countryOfRisk;
            VaRResults = varResults;
            Strategy = strategy;
            AssetClass = assetClass;
            ParentFund = parentFund;
            ProductType = productType;
        }

        #endregion

        #region Public Properties

        /// <summary>The underlier info.</summary>
        [DataMember]
        public string UnderlierInfo { get; protected set; }

        /// <summary>The entity.</summary>
        [DataMember]
        public string Entity { get; protected set; }

        /// <summary>The description.</summary>
        [DataMember]
        public string Description { get; protected set; }

        /// <summary>The book.</summary>
        [DataMember]
        public string Book { get; protected set; }

        /// <summary>The currency.</summary>
        [DataMember]
        public string Currency { get; protected set; }

        /// <summary>The trade id.</summary>
        [DataMember]
        public int TradeId { get; protected set; }

        /// <summary>The trade date.</summary>
        [DataMember]
        public DateTime TradeDate { get; protected set; }

        /// <summary>The pv.</summary>
        [DataMember]
        public double Pv { get; protected set; }

        /// <summary>The pv local.</summary>
        [DataMember]
        public double PvLocal { get; protected set; }

        /// <summary>The cvar.</summary>
        [DataMember]
        public double CVaR { get; protected set; }

        /// <summary>The value at risk.</summary>
        [DataMember]
        public double VaR { get; protected set; }

        /// <summary>The mvar.</summary>
        [DataMember]
        public double MVaR { get; protected set; }

        /// <summary>The result.</summary>
        [DataMember]
        public string Result { get; protected set; }

        /// <summary>The theme.</summary>
        [DataMember]
        public string Theme { get; protected set; }

        /// <summary>The country of risk.</summary>
        [DataMember]
        public string CountryOfRisk { get; protected set; }

        /// <summary>The VaR results.</summary>
        [DataMember]
        public double[] VaRResults { get; protected set; }

        /// <summary>The strategy.</summary>
        [DataMember]
        public string Strategy { get; protected set; }

        /// <summary>The asset class.</summary>
        [DataMember]
        public string AssetClass { get; protected set; }

        /// <summary>The parent fund.</summary>
        [DataMember]
        public string ParentFund { get; protected set; }

        /// <summary>The product type.</summary>
        [DataMember]
        public string ProductType { get; protected set; }

        #endregion

        #region Public Methods

        /// <summary>Translates object to string.</summary>
        /// <returns>The string.</returns>
        public override string ToString()
        {
            var output = $"{UnderlierInfo}<{Entity}<{Description}<{Book}<{Currency}<{TradeId}<{TradeDate:dd/MM/yyyy}<{Pv}<{PvLocal}<{CVaR}<{VaR}<{MVaR}<{Result}<{Theme}";
            return output;
        }

        /// <summary>Creates the builder.</summary>
        /// <returns>The <see cref="Builder"/>.</returns>
        public Builder ToBuilder() 
        {
            return new Builder{
                UnderlierInfo = UnderlierInfo,
                Entity = Entity,
                Description = Description,
                Book = Book,
                Currency = Currency,
                TradeId = TradeId,
                TradeDate = TradeDate,
                Pv = Pv,
                PvLocal = PvLocal,
                CVaR = CVaR,
                VaR = VaR,
                MVaR = MVaR,
                Result = Result,
                Theme = Theme,
                CountryOfRisk = CountryOfRisk,
                VaRResults = VaRResults,
                Strategy = Strategy,
                AssetClass = AssetClass,
                ParentFund = ParentFund,
                ProductType = ProductType
            };
        }

        #endregion

        #region Builder

        /// <summary>The builder.</summary>
        public class Builder
        {

            #region Public Properties

            /// <summary>The country of risk.</summary>
            public string CountryOfRisk { get; set; }

            /// <summary>The underlier info.</summary>
            public string UnderlierInfo { get; set; }

            /// <summary>The entity.</summary>
            public string Entity { get; set; }

            /// <summary>The description.</summary>
            public string Description { get; set; }

            /// <summary>The book.</summary>
            public string Book { get; set; }

            /// <summary>The currency.</summary>
            public string Currency { get; set; }

            /// <summary>The trade id.</summary>
            public int TradeId { get; set; }

            /// <summary>The trade date.</summary>
            public DateTime TradeDate { get; set; }

            /// <summary>The pv.</summary>
            public double Pv { get; set; }

            /// <summary>The pv local.</summary>
            public double PvLocal { get; set; }

            /// <summary>The cvar.</summary>
            public double CVaR { get; set; }

            /// <summary>The value at risk.</summary>
            public double VaR { get; set; }

            /// <summary>The mvar.</summary>
            public double MVaR { get; set; }

            /// <summary>The result.</summary>
            public string Result { get; set; }

            /// <summary>The theme.</summary>
            public string Theme { get; set; }

            /// <summary>The var results.</summary>
            public double[] VaRResults { get; set; }

            /// <summary>The strategy.</summary>
            public string Strategy { get; set; }

            /// <summary>The asset class.</summary>
            public string AssetClass { get; set; }

            /// <summary>The parent fund.</summary>
            public string ParentFund { get; set; }

            /// <summary>The product type.</summary>
            public string ProductType { get; set; }

            #endregion

            #region Public Methods

            /// <summary>Builds the value at risk entry object.</summary>
            /// <returns>The <see cref="ValueAtRiskEntry"/>.</returns>
            public IValueAtRiskEntry Build()
            {
                return new ValueAtRiskEntry(
                    UnderlierInfo, Entity,
                    Description, Book, 
                    Currency, TradeId, 
                    TradeDate, Pv, 
                    PvLocal, CVaR, 
                    VaR, MVaR, 
                    Result, Theme, CountryOfRisk, VaRResults,
                    Strategy, AssetClass, ParentFund, ProductType);
            }

            #endregion

        }

        #endregion
    }
}
