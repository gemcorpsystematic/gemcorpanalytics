﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System;
    using System.Collections.Generic;
    
    /// <summary>The stress test.</summary>
    public interface IStressTest
    {

        /// <summary>The name.</summary>
        string Name { get; }

        /// <summary>The value date.</summary>
        DateTime ValueDate { get; }

        /// <summary>The start date.</summary>
        DateTime StartDate { get; }

        /// <summary>The end date.</summary>
        DateTime EndDate { get; }

        /// <summary>The scenarios.</summary>
        IEnumerable<IScenario> Scenarios { get; }
    }
}
