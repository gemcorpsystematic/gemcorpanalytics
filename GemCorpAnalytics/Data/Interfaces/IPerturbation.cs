﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System.Collections.Generic;

    /// <summary>The perturbation class interface.</summary>
    public interface IPerturbation
    {
        /// <summary>The description.</summary>
        string Description { get; }
     
        /// <summary>The name.</summary>
        string Name { get; }

        /// <summary>The bump type.</summary>
        string BumpType { get; }

        /// <summary>The perturbation type.</summary>
        string PerturbationType { get; }

        /// <summary>The quote name.</summary>
        string QuoteName { get; }

        /// <summary>The quote value.</summary>
        double QuoteValue { get; }

        /// <summary>The shift value.</summary>
        double ShiftValue { get; }

        /// <summary>The products.</summary>
        IList<IProduct> Products { get; }

        /// <summary>The trade values.</summary>
        IDictionary<long, double> TradeValue { get; }

        /// <summary>The trade delta base.</summary>
        IDictionary<long, double> TradeDeltaBase { get; }

        /// <summary>The trade gamma base.</summary>
        IDictionary<long, double> TradeGammaBase { get; }

        /// <summary>The vega by trade.</summary>
        IDictionary<long, double> VegaByTrade { get; }

        /// <summary>The trade base results.</summary>
        IDictionary<long, double> TradeBaseResults { get; }

    }

}
