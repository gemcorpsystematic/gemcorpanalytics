﻿namespace GemCorpAnalytics.Data.Interfaces
{
    
    /// <summary>The product interface.</summary>
    public interface IProduct
    {
        /// <summary>The product Id</summary>
        int ProductId { get; }

        /// <summary>The product type.</summary>
        string ProductType { get; }

        /// <summary>The name.</summary>
        string Name { get; }

        /// <summary>The description.</summary>
        string Description { get; }

        /// <summary>The asset class.</summary>
        string AssetClass { get; }

        /// <summary>The option type.</summary>
        string OptionType { get; }

        /// <summary>The currency.</summary>
        string Currency { get; }

        /// <summary>The clone method.</summary>
        /// <returns>The <see cref="IProduct"/>.</returns>
        IProduct Clone();


    }
}
