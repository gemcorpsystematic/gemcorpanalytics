﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System;
    using System.Collections.Generic;


    /// <summary>The conso risk result.</summary>
    public interface IConsoRiskReportResult
    {
        /// <summary>The value date.</summary>
        DateTime ValueDate { get; }

        /// <summary>The bond z spreads.</summary>
        IDictionary<long, double> BondZSpreads { get; }

        /// <summary>The z spreads by country of risk.</summary>
        IDictionary<string, double> ZSpreadsByCountry { get; }

        /// <summary>The perturbations.</summary>
        IList<Perturbation> Perturbations { get; }

        /// <summary>The trades.</summary>
        IList<Trade> Trades { get; }

        /// <summary>The products.</summary>
        IList<Product> Products { get; }

    }
}
