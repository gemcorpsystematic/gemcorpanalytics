﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System;
    
    /// <summary>The scenario.</summary>
    public interface IScenario
    {
        /// <summary>The name.</summary>
        string Underlier { get; }

        /// <summary>The start date.</summary>
        DateTime StartDate { get; }

        /// <summary>The end date.</summary>
        DateTime EndDate { get; }

        /// <summary>The current date.</summary>
        DateTime CurrentDate { get; }

        /// <summary>The start value.</summary>
        double StartValue { get; }

        /// <summary>The end value.</summary>
        double EndValue { get; }

        /// <summary>The current value.</summary>
        double CurrentValue { get; }
    }
}
