﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System;
    
    /// <summary>The value at risk entry interface.</summary>
    public interface IValueAtRiskEntry
    {
        /// <summary>The underlier info.</summary>
        string UnderlierInfo { get; }

        /// <summary>The entity.</summary>
        string Entity { get;}

        /// <summary>The decription.</summary>
        string Description { get; }

        /// <summary>The book.</summary>
        string Book { get; }

        /// <summary>The currency.</summary>
        string Currency { get; }

        /// <summary>The trade id.</summary>
        int TradeId { get; }

        /// <summary>The trade date.</summary>
        DateTime TradeDate { get; }

        /// <summary>The pv.</summary>
        double Pv { get; }

        /// <summary>The pv local.</summary>
        double PvLocal { get; }

        /// <summary>The cvar.</summary>
        double CVaR { get; }

        /// <summary>The value at risk.</summary>
        double VaR { get; }

        /// <summary>The mvar.</summary>
        double MVaR { get; }

        /// <summary>The result.</summary>
        string Result { get; }

        /// <summary>The theme.</summary>
        string Theme { get; }
    }
}
