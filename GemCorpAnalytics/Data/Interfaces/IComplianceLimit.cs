﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System.Collections.Generic;
    
    /// <summary>The compliance limit interface.</summary>
    public interface IComplianceLimit
    {
        /// <summaryThe country of risk.</summary>
        string CountryOfRisk { get; }

        /// <summary>The output.</summary>
        double Output { get; }

        /// <summary>The limit.</summary>
        double Limit { get; }

        /// <summary>The remaining.</summary>
        double Remaining { get; }

        /// <summary>The trade ids.</summary>
        List<int> TradeIds { get; }
    }

}
