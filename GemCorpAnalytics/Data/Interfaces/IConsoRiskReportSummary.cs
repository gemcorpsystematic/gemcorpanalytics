﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System;
    using System.Collections.Generic;

    
    /// <summary>The conso risk report summary.</summary>
    public interface IConsoRiskReportSummary
    {
        /// <summary>The value date.</summary>
        DateTime ValueDate { get; }

        /// <summary>The bond spread.</summary>
        IDictionary<ITrade, double> BondSpread { get; }

        /// <summary>The commodity delta.</summary>
        IDictionary<ITrade, double> Commodity { get; }

        /// <summary>The commodity volatility.</summary>
        IDictionary<ITrade, double> CommodityVolatility { get; }

        /// <summary>The Equity delta.</summary>
        IDictionary<ITrade, double> Equity { get; }

        /// <summary>The Equity volatility delta.</summary>
        IDictionary<ITrade, double> EquityVolatility { get; }

        /// <summary>The Forex.</summary>
        IDictionary<ITrade, double> Forex { get; }

        /// <summary>The Fx base.</summary>
        IDictionary<ITrade, double> FxBase { get; }

        /// <summary>The Interest.</summary>
        IDictionary<ITrade, double> Interest { get; }


        /// <summary>A list of strings</summary>
        /// <returns></returns>
        List<string> ToString();

    }
}
