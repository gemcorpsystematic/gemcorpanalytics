﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using GemCorpAnalytics.Context;
    using System;
        
    /// <summary>The proxy mapping interface.</summary>
    public interface IProxyMapping
    {
        /// <summary>The time series repository entry.</summary>
        ITimeSeriesRepositoryEntry TimeSeriesKey { get; }

        /// <summary>The proxy time series repository entry.</summary>
        ITimeSeriesRepositoryEntry ProxyTimeSeriesKey { get; }

        /// <summary>The beta.</summary>
        double Beta { get; }
        
        /// <summary>The first date.</summary>
        DateTime FirstDate { get; }

        /// <summary>The last date.</summary>
        DateTime LastDate { get; }

    }

}
