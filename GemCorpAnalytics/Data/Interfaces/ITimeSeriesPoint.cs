﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System;
    using GemCorpAnalytics.Enums;

    /// <summary>The time series point interface.</summary>
    public interface ITimeSeriesPoint
    {
        /// <summary>The date.</summary>
        DateTime Date { get; }

        /// <summary>The name.</summary>
        string Name { get; }

        /// <summary>The ticker.</summary>
        string Ticker { get; }

        /// <summary>The value.</summary>
        double Value { get; }

        /// <summary>The currency.</summary>
        string Currency { get; }

        /// <summary>The contributor.</summary>
        string Contributor { get; }

        /// <summary>The unit.</summary>
        string Unit { get; }

        /// <summary>The instrument id.</summary>
        int? InstrumentId { get; }

        /// <summary>The time series attribute.</summary>
        TimeSeriesAttributes? TimeSeriesAttribute { get; }

    }
}
