﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System.Collections.Generic;

    public interface IMatrix<T>
    {

        /// <summary>Gets columns names.</summary>
        IEnumerable<string> ColumnNames { get; }

        /// <summary>The row ids.</summary>
        IEnumerable<string> RowIds { get; }

        /// <summary>The number of rows.</summary>
        int NumberOfRows { get; }

        /// <summary>The number of columns.</summary>
        int NumberOfColumns { get; }

        /// <summary>Array indexers.</summary>
        /// <param name="rowNumber">The row number.</param>
        /// <param name="columnNumber">The column number.</param>
        /// <returns>The value.</returns>
        T this[int rowNumber, int columnNumber] { get; set; }


        /// <summary>Gets the underlying data.</summary>
        /// <returns>A 2-d array of values.</returns>
        T[,] GetUnderlyingData();

        /// <summary>Adds column names.</summary>
        /// <param name="columnName">The column name.</param>
        void AddColumnNames(string columnName);

        /// <summary>Add a list of column names.</summary>
        /// <param name="columnNames">The column names.</param>
        void AddColumnNames(IEnumerable<string> columnNames);

        /// <summary>Add row ids.</summary>
        /// <param name="rowId">The row id.</param>
        void AddRowIds(string rowId);

        /// <summary>Adds a list of row ids.</summary>
        /// <param name="rowIds">The list of row ids.</param>
        void AddRowIds(IEnumerable<string> rowIds);
        
    }
}
