﻿namespace GemCorpAnalytics.Data.Interfaces
{
    /// <summary>The themes interface.</summary>
    public interface IThemes
    {
        /// <summary>The country of risk.</summary>
        string CountryOfRisk { get; }

        /// <summary>The theme.</summary>
        string Theme { get; }

        /// <summary>The region.</summary>
        string Region { get; }
    }
}
