﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System;
    using GemCorpAnalytics.Enums;

    /// <summary>The curve interface.</summary>
    public interface ICurve
    {
        /// <summary>The name.</summary>
        string Name { get; }

        /// <summary>The day count convention.</summary>
        DayCountConvention DayCountConvention { get; }

        /// <summary>The currency string.</summary>
        string Currency { get; }

        /// <summary>The curve date.</summary>
        DateTime CurveDate { get; }

        /// <summary>The tenor collection.</summary>
        double[] X { get; }

        /// <summary>The rate collection.</summary>
        double[] Y { get; }

    }
}
