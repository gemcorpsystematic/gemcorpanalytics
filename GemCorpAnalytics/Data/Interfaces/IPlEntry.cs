﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System;

    /// <summary>The pl entry interface.</summary>
    public interface IPlEntry
    {
        /// <summary>The country of risk.</summary>
        string CountryOfRisk { get; }

        /// <summary>The description.</summary>
        string Description { get; }

        /// <summary>The underlier info.</summary>
        string UnderlierInfo { get; }

        /// <summary>The book.</summary>
        string Book { get; }

        /// <summary>The currency.</summary>
        string Currency { get; }

        /// <summary>The product id.</summary>
        int ProductId { get; }

        /// <summary>The trade id.</summary>
        int TradeId { get; }

        /// <summary>The asset class.</summary>
        string AssetClass { get; }

        /// <summary>The parent fund.</summary>
        string ParentFund { get; }

        /// <summary>The trade date.</summary>
        DateTime TradeDate { get; }

        /// <summary>The pl inception.</summary>
        double PlInception { get; }

        /// <summary>The pl daily.</summary>
        double PlDaily { get; }
        
        /// <summary>The pl week.</summary>
        double PlWeek { get; }

        /// <summary>The pl month.</summary>
        double PlMonth { get; }

        /// <summary>The pl ytd.</summary>
        double PlYtd { get; }

        /// <summary>The theme.</summary>
        string Theme { get; }

    }
}
