﻿namespace GemCorpAnalytics.Data.Interfaces
{
    using System;
    
    /// <summary>The trade interface</summary>
    public interface ITrade
    {

        /// <summary>The trade date.</summary>
        DateTime TradeDate { get; }

        /// <summary>The product.</summary>
        IProduct Product { get; }

        /// <summary>The name.</summary>
        string Name { get; }

        /// <summary>The description.</summary>
        string Description { get; }

        /// <summary>The trade id.</summary>
        int TradeId { get; }

        /// <summary>The book.</summary>
        string Book { get; }

        /// <summary>The book id.</summary>
        int BookId { get; }

        /// <summary>The strategy.</summary>
        string Strategy { get; }

        /// <summary>The entity.</summary>
        string Entity { get; }

        /// <summary>The parent fund.</summary>
        string ParentFund { get; }

        /// <summary>The buy sell indicator.</summary>
        string BuySell { get; }

        /// <summary>The country of risk.</summary>
        string CountryOfRisk { get; }

        /// <summary>The quantity.</summary>
        double Quantity { get; }

        /// <summary>The trade settle currency.</summary>
        string TradeSettleCurrency { get; }


        /// <summary>The notional.</summary>
        double Notional { get;}

        /// <summary>The is position.</summary>
        bool IsPosition { get;}

        /// <summary>Clones a trade object.</summary>
        /// <returns>The <see cref="ITrade"/>.</returns>
        ITrade Clone();

    }
}
