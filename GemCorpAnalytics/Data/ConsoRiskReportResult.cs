﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Collections.Generic;
    using Data.Interfaces;

    /// <summary>The conso risk report result.</summary>
    public class ConsoRiskReportResult : IConsoRiskReportResult
    {

        #region Constructor

        /// <summary>Initialize an instance of the conso risk report result.</summary>
        /// <param name="bondZSpreads">The bond z spreads.</param>
        /// <param name="perturbations">The perturbations.</param>
        /// <param name="trades">The trades.</param>
        /// <param name="products">The products.</param>
        /// <param name="ZSpreadsByCountry">The z spreads by country.</param>
        private ConsoRiskReportResult(
            DateTime valueDate,
            IDictionary<long, double> bondZSpreads,
            IList<Perturbation> perturbations,
            IList<Trade> trades,
            IList<Product> products,
            IDictionary<string, double> zSpreadsByCountry)
        {
            ValueDate = valueDate;
            BondZSpreads = bondZSpreads;
            Perturbations = perturbations;
            Trades = trades;
            Products = products;
            ZSpreadsByCountry = zSpreadsByCountry;
        }

        #endregion

        #region Public Properties

        /// <summary>The value date.</summary>
        public DateTime ValueDate { get; }

        /// <summary>The bond z spreads.</summary>
        public IDictionary<long, double> BondZSpreads { get; }

        /// <summary>The perturbations.</summary>
        public IList<Perturbation> Perturbations { get; }

        /// <summary>The trades.</summary>
        public IList<Trade> Trades { get; }

        /// <summary>The products.</summary>
        public IList<Product> Products { get; }

        /// <summary>The z spreads by country.</summary>
        public IDictionary<string, double> ZSpreadsByCountry { get; }

        #endregion

        #region Public Methods

        /// <summary>Creates a builder.</summary>
        /// <returns>The <see cref="Builder"/>.</returns>
        public Builder ToBuilder()
        {
            return new Builder {
                ValueDate = ValueDate,
                BondZSpreads = BondZSpreads,
                Perturbations = Perturbations,
                Trades = Trades,
                Products = Products,
                ZSpreadsByCountry = ZSpreadsByCountry
            };        
        }

        /// <summary>Clones a conso risk report result.</summary>
        /// <returns>The <see cref="ConsoRiskReportResult"/>.</returns>
        public ConsoRiskReportResult Clone()
        {
            return new ConsoRiskReportResult(ValueDate, BondZSpreads, Perturbations, Trades, Products, ZSpreadsByCountry);
        }

        #endregion

        #region Builder

        public class Builder 
        {
            #region Public Properties

            /// <summary>The value date.</summary>
            public DateTime ValueDate { get; set; }

            /// <summary>The bond z spreads.</summary>
            public IDictionary<long, double> BondZSpreads { get; set; }

            /// <summary>The z spreads by country.</summary>
            public IDictionary<string, double> ZSpreadsByCountry { get; set; }

            /// <summary>The perturbations.</summary>
            public IList<Perturbation> Perturbations { get; set; }

            /// <summary>The trades.</summary>
            public IList<Trade> Trades { get; set; }

            /// <summary>The products.</summary>
            public IList<Product> Products { get; set; }

            #endregion

            #region Public Methods

            /// <summary>Builds a conso risk report object.</summary>
            /// <returns>The <see cref="ConsoRiskReportResult"/>.</returns>
            public ConsoRiskReportResult Build()
            {
                return new ConsoRiskReportResult(ValueDate, BondZSpreads, Perturbations, Trades, Products, ZSpreadsByCountry);
            }

            #endregion

        }

        #endregion

    }
}
