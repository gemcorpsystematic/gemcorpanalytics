﻿namespace GemCorpAnalytics.Data
{
    using System;
    using Data.Interfaces;

    /// <summary>The nav.</summary>
    public class Nav : IData
    {
        #region Constructor

        /// <summary>Initialize an instance of the nav.</summary>
        /// <param name="date">The date.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="amount">The amount.</param>
        private Nav(
            DateTime date,
            string entity,
            double amount)
        {
            Date = date;
            Entity = entity;
            Amount = amount;
        }

        #endregion

        #region Public Properties

        /// <summary>The date.</summary>
        public DateTime Date { get; protected set; }

        /// <summary>The entity.</summary>
        public string Entity { get; protected set; }

        /// <summary>The amount.</summary>
        public double Amount { get; protected set; }

        #endregion


        #region Builder

        /// <summary>The builder.</summary>
        public class Builder {

            #region Public Properties

            /// <summary>The date.</summary>
            public DateTime Date { get; set; }

            /// <summary>The entity.</summary>
            public string Entity { get; set; }

            /// <summary>The amount.</summary>
            public double Amount { get; set; }

            #endregion

            #region Public Methods
            
            /// <summary>Build the nav.</summary>
            /// <returns>The <see cref="Nav"/>.</returns>
            public Nav Build()
            {
                return new Nav(Date, Entity, Amount);
            }

            #endregion

        }

        #endregion

    }
}
