﻿namespace GemCorpAnalytics.Data
{
    using GemCorpAnalytics.Context;
    using System;
    using Data.Interfaces;

    /// <summary>The proxy mapping.</summary>
    public class ProxyMapping : IProxyMapping
    {

        #region Constructor

        /// <summary>Initialize an instance of the proxy mapping.</summary>
        /// <param name="timeSeriesKey">The time series key.</param>
        /// <param name="proxyTimeSeriesKey">The proxy time series key.</param>
        /// <param name="beta">The beta.</param>
        /// <param name="firstDate">The first date.</param>
        /// <param name="lastDate">The last date.</param>
        public ProxyMapping(
            ITimeSeriesRepositoryEntry timeSeriesKey,
            ITimeSeriesRepositoryEntry proxyTimeSeriesKey,
            double beta,
            DateTime firstDate,
            DateTime lastDate)
        {
            TimeSeriesKey = timeSeriesKey;
            ProxyTimeSeriesKey = proxyTimeSeriesKey;
            Beta = beta;
            FirstDate = firstDate;
            LastDate = lastDate;
        }

        #endregion

        #region Properties

        /// <summary>The time series key.</summary>
        public ITimeSeriesRepositoryEntry TimeSeriesKey { get;}

        /// <summary>The proxy time series key.</summary>
        public ITimeSeriesRepositoryEntry ProxyTimeSeriesKey { get; }

        /// <summary>The beta.</summary>
        public double Beta { get; }

        /// <summary>The first date.</summary>
        public DateTime FirstDate { get; }

        /// <summary>The last date.</summary>
        public DateTime LastDate { get; }

        #endregion

    }
}
