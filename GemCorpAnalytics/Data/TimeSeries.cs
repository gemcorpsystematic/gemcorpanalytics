﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using GemCorpAnalytics.Enums;
    using Data.Interfaces;

    /// <summary>The time series.</summary>
    public class TimeSeries : List<ITimeSeriesPoint>
    {
        #region Constructor

        public TimeSeries()
        {
        }

        /// <summary>Initialise an instance of the time series class.</summary>
        /// <param name="points">The points.</param>
        public TimeSeries(IEnumerable<ITimeSeriesPoint> points)
        {
            AddRange(points);
            // do we want to enforce a unique currency constraint on a time series. //yes we do

            // sort points by date
            this.Sort(delegate (ITimeSeriesPoint x, ITimeSeriesPoint y) {
                return x.Date.CompareTo(y.Date);
            });
        }

        #endregion

        #region Public Properties

        /// <summary>The currency.</summary>
        public string Currency { get; }

        #endregion

        #region Public Methods

        /// <summary>Checks if date is contained in time series.</summary>
        /// <param name="date">The date.</param>
        /// <returns>A flag to outline if date is contained.</returns>
        public bool ContainsDate(DateTime date)
        {
            var dates = new List<DateTime>();

            for(var i=0;i<Count;i++)
            {
                dates.Add(this[i].Date);
            }

            return dates.Count != 0 && dates.Contains(date);
        }

        /// <summary>Checks if date is contained and time series attribute.</summary>
        /// <param name="date">The date.</param>
        /// <param name="timeSeriesAttribute">The time series attribute.</param>
        /// <returns></returns>
        public bool ContainsDate(DateTime date, TimeSeriesAttributes timeSeriesAttribute)
        {
            var containsDate = this.ContainsDate(date);

            if (!containsDate) return false;

            var count = this.Count(p => p.TimeSeriesAttribute == timeSeriesAttribute);

            return count > 0;
        }

        #endregion

    }
}
