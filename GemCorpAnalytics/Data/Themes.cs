﻿namespace GemCorpAnalytics.Data
{
    using System.Runtime.Serialization;
    using Data.Interfaces;

    [DataContract]
    public class Themes : IThemes
    {

        #region Constructor

        /// <summary>Initialize an instance of the themes object.</summary>
        /// <param name="countryOfRisk">The country of risk.</param>
        /// <param name="theme">The theme.</param>
        /// <param name="region">The region.</param>
        private Themes(
            string countryOfRisk,
            string theme,
            string region)
        {
            CountryOfRisk = countryOfRisk;
            Theme = theme;
            Region = region;
        }

        #endregion

        #region Public Properties

        /// <summary>The country of risk.</summary>
        [DataMember]
        public string CountryOfRisk { get; protected set; }

        /// <summary>The theme.</summary>
        [DataMember]
        public string Theme { get; protected set; }

        /// <summary>The region.</summary>
        [DataMember]
        public string Region { get; protected set; }

        #endregion

        #region Public Methods

        /// <summary>Creates a builder.</summary>
        /// <returns>The <see cref="Builder"/>.</returns>
        public Builder ToBuilder()
        {
            return new Builder { 
                CountryOfRisk = CountryOfRisk,
                Theme = Theme,
                Region = Region
            };
        }
        
        /// <summary>Converts object to string.</summary>
        /// <returns>The string.</returns>
        public override string ToString()
        {
            return $"{CountryOfRisk}<{Theme}<{Region}";
        }

        /// <summary>Checks if object is equal to new object.</summary>
        /// <param name="themes">The themes.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        public bool Equals(Themes themes)
        {
            return themes.CountryOfRisk == CountryOfRisk && themes.Theme == Theme && themes.Region == Region;
        }

        /// <summary>The equals object.</summary>
        /// <param name="obj">The object.</param>
        /// <returns>The <see cref="bool"/>.</returns>
        public override bool Equals(object obj)
        {
            var theme = obj as Themes;

            return theme == null ? false : Equals(theme);
        }

        #endregion


        #region Builder

        /// <summary>The builder.</summary>
        public class Builder 
        {

            #region Public Properties

            /// <summary>The country of risk.</summary>
            public string CountryOfRisk { get; set; }

            /// <summary>The theme.</summary>
            public string Theme { get; set; }

            /// <summary>The region.</summary>
            public string Region { get; set; }

            #endregion

            #region Public Methods

            /// <summary>Build the themes object.</summary>
            /// <returns>The <see cref="Themes"/>.</returns>
            public IThemes Build()
            {
                return new Themes(CountryOfRisk, Theme, Region);
            }

            #endregion

        }

        #endregion

    }
}
