﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Collections.Generic;
    using Data.Interfaces;

    /// <summary>The conso risk report summary.</summary>
    public class ConsoRiskReportSummary : IConsoRiskReportSummary
    {

        #region Constructor

        /// <summary>Initialise an instance of the conso risk report summary.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="bondSpread">The bond spread.</param>
        /// <param name="commodity">The commodity.</param>
        /// <param name="commodityVolatility">The commodity volatility.</param>
        /// <param name="equity">The equity.</param>
        /// <param name="equityVolatility">The equity volatility.</param>
        /// <param name="forex">The forex.</param>
        /// <param name="fxBase">The fx base.</param>
        /// <param name="interest">The interest</param>
        public ConsoRiskReportSummary(
            DateTime valueDate,
            IDictionary<ITrade, double> bondSpread,
            IDictionary<ITrade, double> commodity,
            IDictionary<ITrade, double> commodityVolatility,
            IDictionary<ITrade, double> equity,
            IDictionary<ITrade, double> equityVolatility,
            IDictionary<ITrade, double> forex,
            IDictionary<ITrade, double> fxBase,
            IDictionary<ITrade, double> interest
            )
        {
            ValueDate = valueDate;
            BondSpread = bondSpread;
            Commodity = commodity;
            CommodityVolatility = commodityVolatility;
            Equity = equity;
            EquityVolatility = equityVolatility;
            Forex = forex;
            FxBase = fxBase;
            Interest = interest;
        }

        #endregion

        #region Public Properties

        /// <summary>The value date.</summary>
        public DateTime ValueDate {get;}

        /// <summary>The bond spread.</summary>
        public IDictionary<ITrade, double> BondSpread { get;}
        
        /// <summary>The commodity.</summary>
        public IDictionary<ITrade, double> Commodity { get; }

        /// <summary>The commodity volatility.</summary>
        public IDictionary<ITrade, double> CommodityVolatility { get; }

        /// <summary>The equity.</summary>
        public IDictionary<ITrade, double> Equity { get; }

        /// <summary>The equity volatility.</summary>
        public IDictionary<ITrade, double> EquityVolatility { get; }

        /// <summary>The equity volatility.</summary>
        public IDictionary<ITrade, double> Forex { get; }

        /// <summary>The equity volatility.</summary>
        public IDictionary<ITrade, double> FxBase { get; }

        /// <summary>The equity volatility.</summary>
        public IDictionary<ITrade, double> Interest { get; }

        #endregion

        #region

        public List<string> ToString()
        {
            var output = new List<string>();

            foreach(var bonds in BondSpread)
            {
                var bondSpread = $"{ValueDate:yyyy-MM-dd},{bonds.Key.Book},{bonds.Key.TradeId},{bonds.Key.Description},{bonds.Value},{bonds.Key.CountryOfRisk},{bonds.Key.Quantity}";
                output.Add(bondSpread);
            }


            foreach (var commod in Commodity)
            {
                var commodity = $"{ValueDate:yyyy-MM-dd},{commod.Key.Book},{commod.Key.TradeId},{commod.Key.Description},{commod.Value},{commod.Key.Product.OptionType}";
                output.Add(commodity);
            }

            return output;
        }

        #endregion

    }
}
