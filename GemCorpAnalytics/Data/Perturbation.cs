﻿namespace GemCorpAnalytics.Data
{
    using System.Collections.Generic;
    using Data.Interfaces;

    /// <summary>The perturbation.</summary>
    public class Perturbation : IPerturbation
    {
        #region Constructor

        /// <summary>Initialise an instance of the perturbation class.</summary>
        /// <param name="description">The description.</param>
        /// <param name="name">The name.</param>
        /// <param name="bumpType">The bump type.</param>
        /// <param name="perturbationType">The perturbation type.</param>
        /// <param name="quoteName">The quote name.</param>
        /// <param name="quoteValue">The quote value.</param>
        /// <param name="shiftValue">The shift value.</param>
        /// <param name="products">The products.</param>
        /// <param name="tradeValue">The trade value.</param>
        /// <param name="tradeDeltaBase">The trade delta base.</param>
        /// <param name="tradeGammaBase">The trade gamma base.</param>
        /// <param name="vegaByTrade">The vega by trade.</param>
        /// <param name="tradeBaseResults">The trade base results.</param>
        private Perturbation(
            string description,
            string name,
            string bumpType,
            string perturbationType,
            string quoteName,
            double quoteValue,
            double shiftValue,
            string curveCurrency,
            string curveDescription,
            long curveId,
            IList<IProduct> products,
            IDictionary<long, double> tradeValue,
            IDictionary<long, double> tradeDeltaBase,
            IDictionary<long, double> tradeGammaBase,
            IDictionary<long, double> vegaByTrade,
            IDictionary<long, double> tradeBaseResults)
        {
            Description = description;
            Name = name;
            BumpType = bumpType;
            PerturbationType = perturbationType;
            QuoteName = quoteName;
            QuoteValue = quoteValue;
            ShiftValue = shiftValue;
            CurveCurrency = curveCurrency;
            CurveDescription = curveDescription;
            CurveId = curveId;
            Products = products;
            TradeValue = tradeValue;
            TradeDeltaBase = tradeDeltaBase;
            TradeGammaBase = tradeGammaBase;
            VegaByTrade = vegaByTrade;
            TradeBaseResults = tradeBaseResults;
        }

        #endregion


        #region Public Properties

        /// <summary>The description.</summary>
        public string Description { get; protected set; }

        /// <summary>The name.</summary>
        public string Name { get; protected set; }

        /// <summary>The bump type.</summary>
        public string BumpType { get; protected set;} 

        /// <summary>The perturbation type.</summary>
        public string PerturbationType { get; protected set; }

        /// <summary>The quote name.</summary>
        public string QuoteName { get; protected set; }

        /// <summary>The quote value.</summary>
        public double QuoteValue { get; protected set; }

        /// <summary>The shift value.</summary>
        public double ShiftValue { get; protected set; }

        /// <summary>The curve currency.</summary>
        public string CurveCurrency { get; protected set; }

        /// <summary>The curve description.</summary>
        public string CurveDescription { get; protected set; }

        /// <summary>The curve id.</summary>
        public long CurveId { get; protected set; }

        /// <summary>The products.</summary>
        public IList<IProduct> Products { get; protected set; }

        /// <summary>The trade value.</summary>
        public IDictionary<long, double> TradeValue { get; protected set; } 

        /// <summary>The trade delta base.</summary>
        public IDictionary<long, double> TradeDeltaBase { get; protected set; }

        /// <summary>The trade gamma base.</summary>
        public IDictionary<long, double> TradeGammaBase { get; protected set; }

        /// <summary>The vega by trade.</summary>
        public IDictionary<long, double> VegaByTrade { get; protected set; }

        /// <summary>The trade base results.</summary>
        public IDictionary<long, double> TradeBaseResults { get; protected set; }

        #endregion

        #region Public Methods
        
        public string ToString()
        {
            return $"{Name},{PerturbationType}";
        }

        /// <summary>Clone an instance of the Perturbation object.</summary>
        /// <returns>The <see cref="Perturbation"/>.</returns>
        public Perturbation Clone()
        {
            return new Perturbation(Description, Name, BumpType, PerturbationType, 
                QuoteName, QuoteValue, ShiftValue, CurveCurrency, CurveDescription, CurveId, Products, 
                TradeValue, TradeDeltaBase, TradeGammaBase, VegaByTrade, TradeBaseResults);
        }

        #endregion

        #region Builder

        /// <summary>The builder class.</summary>
        public class Builder
        {
            #region Public Properties

            /// <summary>The description.</summary>
            public string Description { get; set; }

            /// <summary>The name.</summary>
            public string Name { get; set; }

            /// <summary>The bump type.</summary>
            public string BumpType { get; set; }

            /// <summary>The perturbation type.</summary>
            public string PerturbationType { get; set; }

            /// <summary>The quote name.</summary>
            public string QuoteName { get; set; }

            /// <summary>The quote value.</summary>
            public double QuoteValue { get; set; }

            /// <summary>The shift value.</summary>
            public double ShiftValue { get; set; }

            /// <summary>The curve currency.</summary>
            public string CurveCurrency { get; set; }

            /// <summary>The curve description.</summary>
            public string CurveDescription { get; set; }

            /// <summary>The curve id.</summary>
            public long CurveId { get; set; }

            /// <summary>The products.</summary>
            public IList<IProduct> Products { get; set; }

            /// <summary>The trade value.</summary>
            public IDictionary<long, double> TradeValue { get; set; }

            /// <summary>The trade delta base.</summary>
            public IDictionary<long, double> TradeDeltaBase { get; set; }

            /// <summary>The trade gamma base.</summary>
            public IDictionary<long, double> TradeGammaBase { get; set; }

            /// <summary>The vega by trade.</summary>
            public IDictionary<long, double> VegaByTrade { get; set; }

            /// <summary>The trade base results.</summary>
            public IDictionary<long, double> TradeBaseResults { get; set; }

            #endregion

            #region Public Methods

            /// <summary>Build a perturbation object.</summary>
            /// <returns>The <see cref="Perturbation"/>.</returns>
            public Perturbation Build()
            {
                return new Perturbation(Description, Name, BumpType, PerturbationType, 
                    QuoteName, QuoteValue, ShiftValue,CurveCurrency, CurveDescription, CurveId, Products, TradeValue, TradeDeltaBase, 
                    TradeGammaBase, VegaByTrade, TradeBaseResults);
            }

            #endregion

        }

        #endregion

    }
}
