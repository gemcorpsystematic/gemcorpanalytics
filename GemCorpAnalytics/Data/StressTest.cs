﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Data.Interfaces;

    [DataContract]
    [KnownType(typeof(IScenario))]
    public class StressTest : IStressTest
    {

        #region Constructor

        /// <summary>Initialize an instance of stress test.</summary>
        /// <param name="name">The name.</param>
        /// <param name="valueDate">The value date.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="scenarios">The scenarios.</param>
        private StressTest(
            string name,
            DateTime valueDate,
            DateTime startDate,
            DateTime endDate,
            IEnumerable<IScenario> scenarios) 
        {
            Name = name;
            ValueDate = valueDate;
            StartDate = startDate;
            EndDate = endDate;
            Scenarios = scenarios;
        }

        #endregion

        #region Public Properties

        [DataMember]
        /// <summary>The stress test name.</summary>
        public string Name { get; protected set; }

        [DataMember]
        /// <summary>The value date.</summary>
        public DateTime ValueDate { get; protected set; }

        [DataMember]
        /// <summary>The start date.</summary>
        public DateTime StartDate { get; protected set; }

        [DataMember]
        /// <summary>The end date.</summary>
        public DateTime EndDate { get; protected set; }

        [DataMember]
        /// <summary>The scenarios.</summary>
        public IEnumerable<IScenario> Scenarios { get; protected set; }

        #endregion

        #region Public Methods

        /// <summary>Creates builder.</summary>
        /// <returns>The <see cref="Builder"/>.</returns>
        public Builder ToBuilder()
        {
            return new Builder {
                Name = Name,
                ValueDate = ValueDate,
                StartDate = StartDate,
                EndDate = EndDate,
                Scenarios = Scenarios
            };
        }

        #endregion

        #region Builder

        /// <summary>The builder.</summary>
        public class Builder 
        {
            #region Public Properties

            /// <summary>The name.</summary>
            public string Name { get; set; }

            /// <summary>The value date.</summary>
            public DateTime ValueDate { get; set; }
            
            /// <summary>The start date.</summary>
            public DateTime StartDate { get; set; }

            /// <summary>The end date.</summary>
            public DateTime EndDate { get; set; }

            /// <summary>The scenarios.</summary>
            public IEnumerable<IScenario> Scenarios { get; set; }

            #endregion

            #region Public Methods

            /// <summary>Builds a stress test instance.</summary>
            /// <returns>The <see cref="StressTest"/>.</returns>
            public StressTest Build()
            {
                return new StressTest(Name, ValueDate, StartDate, EndDate, Scenarios);
            }

            #endregion

        }

        #endregion
    }
}
