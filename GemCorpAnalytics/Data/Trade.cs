﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Runtime.Serialization;
    using Data.Interfaces;

    [DataContract]
    [KnownType("Product")]
    public class Trade : IData, ITrade
    {
        #region Constructor

        /// <summary>Initialize an instance of the trade.</summary>
        /// <param name="date">The date.</param>
        /// <param name="quantity">The quantity.</param>
        /// <param name="price">The price.</param>
        /// <param name="name">The name.</param>
        /// <param name="ticker">The ticker.</param>
        /// <param name="type">The type.</param>
        /// <param name="tradeId">The trade id.</param>
        /// <param name="buySell">The buy sell flag.</param>
        /// <param name="product">The product.</param>
        /// <param name="book">The book.</param>
        /// <param name="bookId">The book id.</param>
        /// <param name="strategy">The strategy.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="parentFund">The parent fund.</param>
        /// <param name="countryOfRisk">The country of risk.</param>
        /// <param name="tradeSettleCurrency">The trade settle currency.</param>
        /// <param name="notional">The notional.</param>
        /// <param name="isPosition">The position flag.</param>
        private Trade(
            DateTime date,
            double quantity,
            double price, 
            string name,
            string description, 
            int tradeId,
            string buySell,
            IProduct product,
            string book,
            int bookId,
            string strategy,
            string entity,
            string parentFund,
            string countryOfRisk,
            string tradeSettleCurrency,
            double notional,
            bool isPosition
            )
        {
            TradeDate = date;
            Quantity = quantity;
            Price = price;
            Name = name;
            Description = description;
            TradeId = tradeId;
            BuySell = buySell;
            Product = product;
            Book = book;
            BookId = bookId;
            Strategy = strategy;
            Entity = entity;
            ParentFund = parentFund;
            CountryOfRisk = countryOfRisk;
            TradeSettleCurrency = tradeSettleCurrency;
            Notional = notional;
            IsPosition = isPosition;
        }

        #endregion

        #region Public Properties

        /// <summary>The date.</summary>
        [DataMember]
        public DateTime TradeDate { get; protected set; }

        /// <summary>The quantity.</summary>
        [DataMember]
        public double Quantity { get; protected set; }

        /// <summary>The price.</summary>
        [DataMember]
        public double Price { get; protected set; }

        /// <summary>The name.</summary>
        [DataMember]
        public string Name { get; protected set; }

        /// <summary>The type.</summary>
        [DataMember]
        public string Description { get; protected set; }

        /// <summary>The trade id.</summary>
        [DataMember]
        public int TradeId { get; protected set; }

        /// <summary>The buy sell flag.</summary>
        [DataMember]
        public string BuySell { get; protected set; }

        /// <summary>The product.</summary>
        [DataMember]
        public IProduct Product { get; protected set; }

        /// <summary>The book.</summary>
        [DataMember]
        public string Book { get; protected set; }

        /// <summary>The book id.</summary>
        [DataMember]
        public int BookId { get; protected set; }

        /// <summary>The strategy.</summary>
        [DataMember]
        public string Strategy { get; protected set; }

        /// <summary>The entity.</summary>
        [DataMember]
        public string Entity { get; protected set; }

        /// <summary>The parent fund.</summary>
        [DataMember]
        public string ParentFund { get; protected set; }

        /// <summary>The country of risk.</summary>
        [DataMember]
        public string CountryOfRisk { get; protected set; }

        /// <summary>The trade settle currency.</summary>
        [DataMember]
        public string TradeSettleCurrency { get; protected set; }

        /// <summary>The trade settle currency.</summary>
        [DataMember]
        public double Notional { get; protected set; }

        /// <summary>The is position.</summary>
        [DataMember]
        public bool IsPosition { get; protected set; }

        #endregion

        #region Public Methods

        /// <summary>A string representation of the object.</summary>
        /// <returns>A string representation of the object.</returns>
        public string ToString()
        {
            return $"{TradeId},{Name}, {Product.ProductId}";
        }

        /// <summary>Clone an instance of the trade class.</summary>
        /// <returns>The <see cref="ITrade"/>.</returns>
        public ITrade Clone() 
        {
            return new Trade(TradeDate, Quantity, Price, Name, Description, TradeId, BuySell, 
                Product, Book, BookId, Strategy, Entity, ParentFund, CountryOfRisk, TradeSettleCurrency, Notional, IsPosition);   
        }

        /// <summary>Creates a builder.</summary>
        /// <returns>The <see cref="Builder"/>.</returns>
        public Builder ToBuilder()
        {
            return new Builder {
                TradeDate = TradeDate,
                Quantity = Quantity,
                Price = Price,
                Name=  Name,
                Description = Description,
                TradeId = TradeId,
                BuySell = BuySell,
                Product= Product,
                Book = Book,
                BookId = BookId,
                Strategy = Strategy,
                Entity = Entity,
                ParentFund = ParentFund,
                CountryOfRisk = CountryOfRisk,
                TradeSettleCurrency = TradeSettleCurrency,
                Notional = Notional,
                IsPosition = IsPosition
            };
        }

        /// <summary>Checks if the trade object is equal to present object.</summary>
        /// <param name="trade">The trade.</param>
        /// <returns>A flag to indicate if trade objects are equivalent</returns>
        public bool Equals(ITrade trade)
        {
            return trade.TradeId == TradeId;
        }

        /// <summary>Checks if object equals present object.</summary>
        /// <param name="o">The object.</param>
        /// <returns>A flag to indicate if trade objects are equivalent</returns>
        public override bool Equals(Object o)
        {
            if (!(o is Trade trade))
                return false;

            return Equals(trade);
        }

        #endregion


        #region Builder

        /// <summary>The builder.</summary>
        public class Builder 
        {

            #region Public Properties

            /// <summary>The date.</summary>
            public DateTime TradeDate { get; set; }

            /// <summary>The quantity.</summary>
            public double Quantity { get; set; }

            /// <summary>The price.</summary>
            public double Price { get; set; }

            /// <summary>The name.</summary>
            public string Name { get; set; }

            /// <summary>The ticker.</summary>
            public string Description { get; set; }

            /// <summary>The trade id.</summary>
            public int TradeId { get; set;}

            /// <summary>The buy sell.</summary>
            public string BuySell { get; set; }

            /// <summary>The product.</summary>
            public IProduct Product { get; set; }

            /// <summary>The book.</summary>
            public string Book { get; set; }
            
            /// <summary>The book id.</summary>
            public int BookId { get; set; }

            /// <summary>The strategy.</summary>
            public string Strategy { get; set; }

            /// <summary>The entity.</summary>
            public string Entity { get; set; }

            /// <summary>The parent fund.</summary>
            public string ParentFund { get; set; }

            /// <summary>The country of risk.</summary>
            public string CountryOfRisk { get; set; }

            /// <summary>The trade settle currency.</summary>
            public string TradeSettleCurrency { get; set; }

            /// <summary>The trade notional.</summary>
            public double Notional { get; set; }

            /// <summary>The is position.</summary>
            public bool IsPosition { get; set; }

            #endregion

            #region Public Methods

            /// <summary>Builds the trade.</summary>
            /// <returns>The <see cref="Trade"/>.</returns>
            public Trade Build() 
            {
                return new Trade(TradeDate, Quantity, Price, Name, Description, TradeId, BuySell,
                    Product, Book, BookId, Strategy, Entity, ParentFund, CountryOfRisk, TradeSettleCurrency, Notional, IsPosition);
            }

            #endregion

        }


        #endregion

    }
}
