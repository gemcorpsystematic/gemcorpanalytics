﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Runtime.Serialization;
    using Data.Interfaces;

    /// <summary>The portfolio risk sensitivity.</summary>
    [DataContract]
    public class PortfolioRiskSensitivity : IData
    {

        #region Fields

        /// <summary>The underlier.</summary>
        [DataMember]
        private string _underlier;

        /// <summary>The book.</summary>
        [DataMember]
        private string _book;

        /// <summary>The entity.</summary>
        [DataMember]
        private string _entity;

        /// <summary>The strategy.</summary>
        [DataMember]
        private string _strategy;

        /// <summary>The asset class.</summary>
        [DataMember]
        private string _assetClass;

        /// <summary>The country of risk.</summary>
        [DataMember]
        private string _countryOfRisk;

        /// <summary>The curve currency.</summary>
        [DataMember]
        private string _curveCurrency;

        /// <summary>The underlying tenor.</summary>
        [DataMember]
        private string _underlyingTenor;

        /// <summary>The scenario name.</summary>
        [DataMember]
        private string _scenarioName;

        /// <summary>The risk type.</summary>
        [DataMember]
        private string _riskType;

        /// <summary>The product type.</summary>
        [DataMember]
        private string _productType;

        /// <summary>The delta.</summary>
        [DataMember]
        private double _delta;

        /// <summary>The deltabase.</summary>
        [DataMember]
        private double _deltaBase;

        /// <summary>The vega.</summary>
        [DataMember]
        private double _vega;

        /// <summary>The pl base.</summary>
        [DataMember]
        private double _plBase;

        /// <summary>The z spread.</summary>
        [DataMember]
        private double _zSpread;

        #endregion

        #region Constructor

        /// <summary>Initialise an instance of the portfolio risk sensitivity.</summary>
        /// <param name="underlier">The underlier.</param>
        /// <param name="book">The book.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="scenarioName">The scenarioName.</param>
        /// <param name="riskType">The risk type.</param>
        /// <param name="productType">The product type.</param>
        /// <param name="delta">The delta.</param>
        /// <param name="deltaBase">The delta base.</param>
        /// <param name="vega">The vega.</param>
        /// <param name="plBase">The plBase.</param>
        private PortfolioRiskSensitivity(
            string underlier,
            string book,
            string entity,
            string strategy,
            string assetClass,
            string countryOfRisk,
            string curveCurrency,
            string underlyingTenor,
            string scenarioName,
            string riskType,
            string productType,
            double delta,
            double deltaBase,
            double vega,
            double plBase,
            double zSpread)
        {
            _underlier = underlier;
            _book = book;
            _entity = entity;
            _strategy = strategy;
            _assetClass = assetClass;
            _countryOfRisk = countryOfRisk;
            _curveCurrency = curveCurrency;
            _underlyingTenor = underlyingTenor;
            _scenarioName = scenarioName;
            _riskType = riskType;
            _productType = productType;
            _delta = delta;
            _deltaBase = deltaBase;
            _vega = vega;
            _plBase = plBase;
            _zSpread = zSpread;
        }

        #endregion

        #region Properties
        /// <summary>The underlier.</summary>
        public string Underlier
        {
            get { return _underlier; }
            protected set { _underlier = value; }
        }

        /// <summary>The book.</summary>
        public string Book
        {
            get { return _book; }
            protected set { _book = value; }
        }

        /// <summary>The entity.</summary>
        public string Entity
        {
            get { return _entity; }
            protected set { _entity = value; }
        }

        /// <summary>The strategy.</summary>
        public string Strategy
        {
            get { return _strategy; }
            protected set { _strategy = value; }
        }

        /// <summary>The asset class.</summary>
        public string AssetClass
        {
            get { return _assetClass; }
            protected set { _assetClass = value; }
        }

        /// <summary>The country of risk.</summary>
        public string CountryOfRisk
        {
            get { return _countryOfRisk; }
            protected set { _countryOfRisk = value; }
        }

        /// <summary>The curve currency.</summary>
        public string CurveCurrency 
        {
            get { return _curveCurrency; }
            protected set { _curveCurrency = value; }
        }

        /// <summary>The underlying tenor.</summary>
        public string UnderlyingTenor 
        {
            get { return _underlyingTenor; }
            protected set { _underlyingTenor = value; }
        }

        /// <summary>The scenario name.</summary>
        public string ScenarioName 
        {
            get { return _scenarioName; }
            protected set { _scenarioName = value; }
        }

        /// <summary>The risk type.</summary>
        public string RiskType 
        {
            get { return _riskType; }
            protected set { _riskType = value; }
        }

        /// <summary>The product type.</summary>
        public string ProductType 
        {
            get { return _productType; }
            protected set { _productType = value; }
        }

        /// <summary>The delta.</summary>
        public double Delta 
        {
            get { return _delta; }
            protected set { _delta = value; }
        }

        /// <summary>The delta base.</summary>
        public double DeltaBase 
        {
            get { return _deltaBase; }
            protected set { _deltaBase = value; }
        }

        /// <summary>The vega.</summary>
        public double Vega 
        {
            get { return _vega; }
            protected set { _vega = value; }
        }

        /// <summary>The pl base.</summary>
        public double PlBase 
        {
            get { return _plBase; }
            protected set { _plBase = value; }
        }

        /// <summary>The z spread.</summary>
        public double ZSpread 
        {
            get { return _zSpread; }
            protected set { _zSpread = value; }
        }

        #endregion

        #region Public Methods

        /// <summary>To builder.</summary>
        /// <returns>The <see cref="Builder"/>.</returns>
        public Builder ToBuilder()
        {
            return new Builder {
                Underlier = _underlier,
                Book = _book,
                Entity = _entity,
                Strategy = _strategy,
                AssetClass = _assetClass,
                CountryOfRisk = _countryOfRisk,
                CurveCurrency = _curveCurrency,
                UnderlyingTenor = _underlyingTenor,
                ScenarioName = _scenarioName,
                RiskType = _riskType,
                ProductType = _productType,
                Delta = _delta,
                DeltaBase = _deltaBase,
                Vega = _vega,
                PlBase = _plBase,
                ZSpread = _zSpread
            };
        }

        #endregion

        #region Builder

        /// <summary>The builder.</summary>
        public class Builder 
        {

            #region Properties

            /// <summary>The underlier.</summary>
            public string Underlier { get; set; }

            /// <summary>The book.</summary>
            public string Book { get; set; }

            /// <summary>The entity.</summary>
            public string Entity { get; set; }

            /// <summary>The strategy.</summary>
            public string Strategy { get; set; }

            /// <summary>The asset class.</summary>
            public string AssetClass { get; set; }

            /// <summary>The country of risk.</summary>
            public string CountryOfRisk { get; set; }

            /// <summary>The curve currency.</summary>
            public string CurveCurrency { get; set; }

            /// <summary>The underlying tenor.</summary>
            public string UnderlyingTenor { get; set; }

            /// <summary>The scenario name.</summary>            
            public string ScenarioName { get; set; }

            /// <summary>The risk type.</summary>
            public string RiskType { get; set; }

            /// <summary>The product type.</summary>
            public string ProductType { get; set; }

            /// <summary>The delta.</summary>
            public double Delta { get; set; }

            /// <summary>The delta base.</summary>
            public double DeltaBase { get; set; }

            /// <summary>The vega.</summary>
            public double Vega { get; set; }

            /// <summary>The pl base.</summary>
            public double PlBase { get; set; }

            /// <summary>The z spread.</summary>
            public double ZSpread { get; set; }

            #endregion

            #region Public Methods

            /// <summary>Builds the portfolio risk sensitivity.</summary>
            /// <returns>The <see cref="PortfolioRiskSensitivity"/>.</returns>
            public PortfolioRiskSensitivity Build()
            {
                if (Underlier == null) throw new ArgumentNullException(nameof(Underlier));
                if (Book == null) throw new ArgumentNullException(nameof(Book));
                if (Entity == null) throw new ArgumentNullException(nameof(Entity));
                if (ScenarioName == null) throw new ArgumentNullException(nameof(ScenarioName));
                if (RiskType == null) throw new ArgumentNullException(nameof(RiskType));
                return new PortfolioRiskSensitivity(Underlier, Book, Entity, Strategy, AssetClass, CountryOfRisk, CurveCurrency, UnderlyingTenor, ScenarioName, RiskType, ProductType, Delta, DeltaBase, Vega, PlBase, ZSpread);
            }

            #endregion

        }

        #endregion

    }
}
