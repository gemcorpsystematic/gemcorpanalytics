﻿namespace GemCorpAnalytics.Data
{
    using System.Runtime.Serialization;
    using Data.Interfaces;

    /// <summary>The product class.</summary>
    [DataContract]
    public class Product : IProduct
    {

        #region Constructor

        /// <summary>Initialise an instance of the product class.</summary>
        /// <param name="productId">The product id.</param>
        /// <param name="productType">The product type.</param>
        /// <param name="name">The name.</param>
        /// <param name="description">The description.</param>
        /// <param name="assetClass">The asset class.</param>
        /// <param name="optionType">The option type.</param>
        /// <param name="currency">The currency.</param>
        /// <param name="countryOfRisk">The country of risk.</param>
        private Product(
            int productId,
            string productType,
            string name,
            string description,
            string assetClass,
            string optionType,
            string currency,
            string countryOfRisk
            )
        {
            ProductId = productId;
            ProductType = productType;
            Name = name;
            Description = description;
            AssetClass = assetClass;
            OptionType = optionType;
            Currency = currency;
            CountryOfRisk = countryOfRisk;
        }

        #endregion

        #region Public Properties

        /// <summary>The product id.</summary>
        [DataMember]
        public int ProductId { get; protected set; }

        /// <summary>The product type.</summary>
        [DataMember]
        public string ProductType { get; protected set; }

        /// <summary>The name.</summary>
        [DataMember]
        public string Name { get; protected set; }

        /// <summary>The description.</summary>
        [DataMember]
        public string Description { get; protected set; }

        /// <summary>The asset class.</summary>
        [DataMember]
        public string AssetClass { get; protected set; }

        /// <summary>The option type.</summary>
        [DataMember]
        public string OptionType { get; protected set; }

        /// <summary>The currency.</summary>
        [DataMember]
        public string Currency { get; protected set; }

        /// <summary>The country of risk.</summary>
        [DataMember]
        public string CountryOfRisk { get; protected set; }

        #endregion

        /// <summary>Produce a clone of the Product.</summary>
        /// <returns>The <see cref="Product"/>.</returns>
        public IProduct Clone()
        {
            return new Product(ProductId, ProductType, Name, Description, AssetClass, OptionType, Currency, CountryOfRisk);
        }

        /// <summary>The to builder method.</summary>
        /// <returns>The <see cref="Builder"/>.</returns>
        public Builder ToBuilder()
        {
            return new Builder
            {
                ProductId = ProductId,
                ProductType = ProductType,
                Name = Name,
                Description = Description,
                AssetClass = AssetClass,
                OptionType = OptionType,
                Currency = Currency,
                CountryOfRisk = CountryOfRisk
            };
        }

        #region Builder

        /// <summary>The builder.</summary>
        public class Builder
        {
            #region Public Properties

            /// <summary>The product id.</summary>
            public int ProductId { get; set; }

            /// <summary>The product type.</summary>
            public string ProductType { get; set; }

            /// <summary>The name.</summary>
            public string Name { get; set; }

            /// <summary>The description.</summary>
            public string Description { get; set; }

            /// <summary>The asset class.</summary>
            public string AssetClass { get; set; }

            /// <summary>The option type.</summary>
            public string OptionType { get; set; }

            /// <summary>The currency.</summary>
            public string Currency { get; set; }

            /// <summary>The country of risk.</summary>
            public string CountryOfRisk { get; set; }

            #endregion

            #region Public Methods

            /// <summary>Build a product object.</summary>
            /// <returns>The <see cref="Product"/>.</returns>
            public Product Build() 
            {
                return new Product(ProductId, ProductType, Name, Description, AssetClass, OptionType, Currency, CountryOfRisk);
            }

            #endregion

        }

        #endregion

    }
}
