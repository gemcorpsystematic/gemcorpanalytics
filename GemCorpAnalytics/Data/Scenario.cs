﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Runtime.Serialization;
    using Data.Interfaces;

    [DataContract]
    public class Scenario : IScenario
    {

        #region Constructor

        /// <summary>Initialize an instance of the scenario object.</summary>
        /// <param name="underlier">The underlier.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <param name="currentDate">The current date.</param>
        /// <param name="startValue">The start date.</param>
        /// <param name="endValue">The end value.</param>
        /// <param name="currentValue">The current value.</param>
        private Scenario(
            string underlier,
            DateTime startDate,
            DateTime endDate,
            DateTime currentDate,
            double startValue,
            double endValue,
            double currentValue)
        {
            Underlier = underlier;
            StartDate = startDate;
            EndDate = endDate;
            CurrentDate = currentDate;
            StartValue = startValue;
            EndValue = endValue;
            CurrentValue = currentValue;
        }

        #endregion


        #region Public Properties

        /// <summary>The underlier.</summary>
        [DataMember]
        public string Underlier { get; protected set; }

        /// <summary>The start date.</summary>
        [DataMember]
        public DateTime StartDate { get; protected set; }

        /// <summary>The end date.</summary>
        [DataMember]
        public DateTime EndDate { get; protected set; }

        /// <summary>The current date.</summary>
        [DataMember]
        public DateTime CurrentDate { get; protected set; }

        /// <summary>The start value.</summary>
        [DataMember]
        public double StartValue { get; protected set; }

        /// <summary>The end value.</summary>
        [DataMember]
        public double EndValue { get; protected set; }

        /// <summary>The current value.</summary>
        [DataMember]
        public double CurrentValue { get; protected set; }

        #endregion

        #region Builder

        /// <summary>The builder.</summary>
        public class Builder 
        {
            #region Public Properties

            /// <summary>The underlier.</summary>
            public string Underlier { get; set; }

            /// <summary>The start date.</summary>
            public DateTime StartDate { get; set; }

            /// <summary>The end date.</summary>
            public DateTime EndDate { get; set; }

            /// <summary>The current date.</summary>
            public DateTime CurrentDate { get; set; }

            /// <summary>The start value.</summary>
            public double StartValue { get; set; }

            /// <summary>The end value.</summary>
            public double EndValue { get; set; }

            /// <summary>The current value.</summary>
            public double CurrentValue { get; set; }

            #endregion

            #region Public Methods

            /// <summary>Build the scenario instance.</summary>
            /// <returns>The <see cref="Scenario"/>.</returns>
            public Scenario Build()
            {
                return new Scenario(Underlier, StartDate, EndDate, CurrentDate, StartValue, EndValue, CurrentValue);            
            }

            #endregion

        }

        #endregion

    }
}
