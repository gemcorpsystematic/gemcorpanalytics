﻿namespace GemCorpAnalytics.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using Data.Interfaces;

    /// <summary>The matrix.</summary>
    public class Matrix : IMatrix<double>
    {
        #region Fields

        /// <summary>The underlying data.</summary>
        private readonly double[,] _underlyingData;

        /// <summary>The column names.</summary>
        private readonly IEnumerable<string> _columnNames;

        /// <summary>The row ids.</summary>
        private readonly IEnumerable<string> _rowIds;

        #endregion

        #region Constructor

        /// <summary>Initialize an instance of a matrix.</summary>
        /// <param name="rowSize">The row size.</param>
        /// <param name="colSize">The column size.</param>
        public Matrix(int rowSize, int colSize)
        {
            _underlyingData = new double[rowSize, colSize];
            _columnNames = new List<string>();
            _rowIds = new List<string>();
        }


        #endregion

        #region Public Properties

        /// <summary>The column names.</summary>
        public IEnumerable<string> ColumnNames => _columnNames;

        /// <summary>The row ids.</summary>
        public IEnumerable<string> RowIds => _rowIds;

        /// <summary>The number of rows.</summary>
        public int NumberOfRows { get { return _rowIds.Count(); } }

        /// <summary>The number of columns.</summary>
        public int NumberOfColumns { get { return _columnNames.Count(); } }

        #endregion

        #region Public Methods

        /// <summary>Gets underlying data.</summary>
        /// <returns>The <see cref="<double[,]"/>.</returns>
        public double[,] GetUnderlyingData()
        {
            return _underlyingData;
        }

        /// <summary>Add column names.</summary>
        /// <param name="columnName">The column name.</param>
        public void AddColumnNames(string columnName) 
        {
            _columnNames.ToList().Add(columnName);
        }

        /// <summary>Adds range of columns names.</summary>
        /// <param name="columnsNames">The column names.</param>
        public void AddColumnNames(IEnumerable<string> columnsNames) 
        {
            _columnNames.ToList().AddRange(columnsNames);
        }

        /// <summary>Adds row ids.</summary>
        /// <param name="rowId">The row id.</param>
        public void AddRowIds(string rowId) 
        {
            _rowIds.ToList().Add(rowId);
        }

        /// <summary>Adds the </summary>
        /// <param name="rowIds"></param>
        public void AddRowIds(IEnumerable<string> rowIds) 
        {
            _rowIds.ToList().AddRange(rowIds);
        }
        
        /// <summary>Matrix indexers.</summary>
        /// <param name="i">The row ids.</param>
        /// <param name="j">The column ids.</param>
        /// <returns>The value.</returns>
        public double this[int i, int j] 
        {
            get { return _underlyingData[i, j]; }
            set { _underlyingData[i, j] = value; }
        }

        #endregion
    }
}
