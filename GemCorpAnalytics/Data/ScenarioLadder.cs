﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Runtime.Serialization;
    using Data.Interfaces;

    /// <summary>The scenario ladder.</summary>
    [DataContract]
    public class ScenarioLadder : IData
    {

        #region Constructor
        
        /// <summary>Initialize an instance of the scenario ladder.</summary>
        /// <param name="name">The name.</param>
        /// <param name="scenarioDate">The scenario date.</param>
        /// <param name="assetClass">The asset class.</param>
        /// <param name="ladder">The ladder.</param>
        private ScenarioLadder(
            string name,
            DateTime scenarioDate, 
            string riskType,
            string scenarioName,
            string strategy, 
            string entity,
            double scenarioValue)
        {
            Name = name ?? throw new ArgumentNullException(nameof(name));
            ScenarioDate = scenarioDate;
            RiskType = riskType ?? throw new ArgumentNullException(nameof(riskType));
            ScenarioName = scenarioName ?? throw new ArgumentNullException(nameof(scenarioName));
            Strategy = strategy ?? throw new ArgumentNullException(nameof(strategy));
            Entity = entity ?? throw new ArgumentNullException(nameof(entity));
            ScenarioValue = scenarioValue;
        }

        #endregion

        #region Public Properties

        /// <summary>The name.</summary>
        [DataMember]
        public string Name { get; protected set; }

        /// <summary>The scenario date.</summary>
        [DataMember]
        public DateTime ScenarioDate { get; protected set; }

        /// <summary>The asset class.</summary>
        [DataMember]
        public string RiskType { get; protected set; }

        /// <summary>The scenario name.</summary>
        [DataMember]
        public string ScenarioName { get; protected set; }

        /// <summary>The strategy.</summary>
        [DataMember]
        public string Strategy { get; protected set; }

        /// <summary>The entity.</summary>
        [DataMember]
        public string Entity { get; protected set; }

        /// <summary>The ladder.</summary>
        [DataMember]
        public double ScenarioValue { get; protected set; }

        #endregion

        #region Public Methods

        /// <summary>The to string method.</summary>
        /// <returns>The object string representation.</returns>
        public override string ToString()
        {
            return $"{Name},{RiskType},{ScenarioName},{Strategy},{Entity}";
        }

        #endregion


        #region Builder

        /// <summary>The builder.</summary>
        public class Builder 
        {
            #region Public Properties

            /// <summary>The name.</summary>
            public string Name { get; set; }

            /// <summary>The scenario date.</summary>
            public DateTime ScenarioDate { get; set; }

            /// <summary>The asset class.</summary>
            public string RiskType { get; set; }

            /// <summary>The scenario name.</summary>
            public string ScenarioName { get; set; }

            /// <summary>The strategy.</summary>
            public string Strategy { get; set; }

            /// <summary>The entity.</summary>
            public string Entity { get; set; }

            /// <summary>The ladder.</summary>
            public double ScenarioValue { get; set; }

            #endregion

            #region Public Methods

            /// <summary>Builds scenario ladder.</summary>
            /// <returns>The <see cref="ScenarioLadder"/>.</returns>
            public ScenarioLadder Build()
            {
                return new ScenarioLadder(Name, ScenarioDate, RiskType, ScenarioName, Strategy, Entity, ScenarioValue);
            }

            #endregion

        }

        #endregion
    }
}
