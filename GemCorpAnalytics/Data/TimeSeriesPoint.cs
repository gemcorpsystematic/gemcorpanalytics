﻿namespace GemCorpAnalytics.Data
{
    using System;
    using GemCorpAnalytics.Enums;
    using Data.Interfaces;

    /// <summary>The time series point.</summary>
    public class TimeSeriesPoint : ITimeSeriesPoint
    {

        #region Constructor

        /// <summary>Initializes an instance of the time series point.</summary>
        /// <param name="date">The date.</param>
        /// <param name="name">The name.</param>
        /// <param name="ticker">The ticker.</param>
        /// <param name="value">The value.</param>
        /// <param name="currency">The currency.</param>
        /// <param name="contributor">The contributor.</param>
        /// <param name="unit">The unit.</param>
        /// <param name="instrumentId">The instrument id.</param>
        /// <param name="timeSeriesAttribute">The time series attribute.</param>
        private TimeSeriesPoint(DateTime date,
            string name,
            string ticker,
            double value,
            string currency,
            string contributor,
            string unit,
            int? instrumentId,
            TimeSeriesAttributes? timeSeriesAttribute)
        {
            Date = date;
            Name = name;
            Ticker = ticker;
            Value = value;
            Currency = currency;
            Contributor = contributor;
            Unit = unit;
            InstrumentId = instrumentId;
            TimeSeriesAttribute = timeSeriesAttribute;
        }

        #endregion

        #region Public Properties

        /// <summary>Gets the date.</summary>
        public DateTime Date { get; }

        /// <summary>Gets the name.</summary>
        public string Name { get; }

        /// <summary>Gets the ticker.</summary>
        public string Ticker { get; }

        /// <summary>Gets the value.</summary>
        public double Value { get; }

        /// <summary>Gets the currency.</summary>
        public string Currency { get; }

        /// <summary>Gets the contributor.</summary>
        public string Contributor { get; }

        /// <summary>Gets the unit.</summary>
        public string Unit { get; }

        /// <summary>The instrument id.</summary>
        public int? InstrumentId { get; }

        public TimeSeriesAttributes? TimeSeriesAttribute { get; }

        #endregion

        #region Public Methods

        /// <summary>Overrides the equals method.</summary>
        /// <param name="p">The point.</param>
        /// <returns>The bool.</returns>
        public bool Equals(TimeSeriesPoint p)
        {
            return p.Date==Date && p.Name.ToLower()==Name.ToLower() && p.Ticker.ToLower()==Ticker.ToLower()
                && Math.Abs(p.Value-Value)<1e-6 && p.Currency.ToLower()==Currency.ToLower() && p.Contributor.ToLower()==Contributor.ToLower()
                && p.Unit.ToLower() == Unit.ToLower() && p.InstrumentId==InstrumentId && p.TimeSeriesAttribute==TimeSeriesAttribute;
        }

        /// <summary>Overrides the equals method.</summary>
        /// <param name="p">The point.</param>
        /// <returns>The bool.</returns>
        public override bool Equals(Object p)
        {
            var point = p as TimeSeriesPoint;
            return point != null && Equals(point);
        }

        #endregion

        #region Builder

        /// <summary>The builder.</summary>
        public class Builder
        {
            #region Public Properties

            /// <summary>The date.</summary>
            public DateTime Date { get; set; }

            /// <summary>The name.</summary>
            public string Name { get; set; }

            /// <summary>The ticker.</summary>
            public string Ticker { get; set; }

            /// <summary>The value.</summary>
            public double Value { get; set; }

            /// <summary>The currency.</summary>
            public string Currency { get; set; }

            /// <summary>The contributor.</summary>
            public string Contributor { get; set; }

            /// <summary>The unit.</summary>
            public string Unit { get; set; }

            /// <summary>The instrument id.</summary>
            public int? InstrumentId { get; set; }

            /// <summary>The time series attribute.</summary>
            public TimeSeriesAttributes? TimeSeriesAttribute { get; set; }

            #endregion

            #region Public Methods
            
            /// <summary>Builds the time series point.</summary>
            /// <returns>The <see cref="TimeSeriesPoint"/>.</returns>
            public TimeSeriesPoint Build()
            {
                return new TimeSeriesPoint(
                    Date, 
                    Name, 
                    Ticker, 
                    Value, 
                    Currency, 
                    Contributor, 
                    Unit, 
                    InstrumentId,
                    TimeSeriesAttribute);
            }

            #endregion

        }

        #endregion

    }
}
