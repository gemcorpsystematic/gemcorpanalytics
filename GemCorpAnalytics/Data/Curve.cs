﻿namespace GemCorpAnalytics.Data
{
    using System;
    using GemCorpAnalytics.Enums;
    using Data.Interfaces;

    /// <summary>The curve.</summary>
    public class Curve : ICurve
    {
        #region Constructor

        /// <summary>Initialise an instance of the Curve.</summary>
        /// <param name="name">The name.</param>
        /// <param name="dayCountConvention">The day count convention.</param>
        /// <param name="currency">The currency.</param>
        /// <param name="curveDate">The curve date.</param>
        /// <param name="x">The tenors.</param>
        /// <param name="y">The rates.</param>
        public Curve(string name,
            DayCountConvention dayCountConvention,
            string currency,
            DateTime curveDate,
            double[] x,
            double[] y)
        {
            Name = string.IsNullOrEmpty(name) ? throw new ArgumentNullException(nameof(name)) : name;
            DayCountConvention = dayCountConvention;
            Currency = string.IsNullOrEmpty(currency) ? throw new ArgumentNullException(nameof(currency)) : currency;
            CurveDate = curveDate;
            X = x;
            Y = y;
        }

        #endregion


        #region Properties

        /// <summary>The curve name.</summary>
        public string Name { get; }

        /// <summary>The day count convention.</summary>
        public DayCountConvention DayCountConvention { get; }

        /// <summary>The currency.</summary>
        public string Currency { get; }

        /// <summary>The curve date.</summary>
        public DateTime CurveDate { get; }

        /// <summary>The tenors.</summary>
        public double[] X { get; }

        /// <summary>The rates.</summary>
        public double[] Y { get;}

        #endregion



    }
}
