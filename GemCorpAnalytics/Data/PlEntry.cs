﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Runtime.Serialization;
    using Data.Interfaces;

    /// <summary>The pl entry.</summary>
    [DataContract]
    public class PlEntry : IPlEntry
    {
        #region Constructor
        
        /// <summary>Initialize an instance of the pl entry object.</summary>
        /// <param name="countryOfRisk">The country of risk.</param>
        /// <param name="description">The description.</param>
        /// <param name="underlierInfo">The underlier info.</param>
        /// <param name="book">The book.</param>
        /// <param name="currency">The currency.</param>
        /// <param name="productId">The product id.</param>
        /// <param name="tradeId">The trade id.</param>
        /// <param name="assetClass">The asset class.</param>
        /// <param name="parentFund">The parent fund.</param>
        /// <param name="tradeDate">The trade date.</param>
        /// <param name="plInception">The pl inception.</param>
        /// <param name="plDaily">The pl daily.</param>
        /// <param name="plWeek">The pl week.</param>
        /// <param name="plMonth">The pl month.</param>
        /// <param name="plYtd">The pl ytd.</param>
        /// <param name="theme">The theme.</param>
        /// <param name="entity">The entity.</param>
        /// <param name="strategy">The strategy.</param>
        /// <param name="startFxPrice">The strategy.</param>
        /// <param name="startQuote">The strategy.</param>
        /// <param name="endFxPrice">The strategy.</param>
        /// <param name="endQuote">The strategy.</param>
        /// <param name="startNominal">The start Nominal.</param>
        /// <param name="startPv">The start pv.</param>
        /// <param name="endNominal">The end nominal.</param>
        /// <param name="endPv">The end pv.</param>
        private PlEntry(
            string countryOfRisk,
            string description,
            string underlierInfo,
            string book,
            string currency,
            int productId,
            int tradeId,
            string assetClass,
            string parentFund,
            DateTime tradeDate,
            double plInception,
            double plDaily,
            double plWeek,
            double plMonth,
            double plYtd,
            string theme,
            string entity,
            string strategy,
            double startFxPrice,
            double startQuote,
            double endFxPrice,
            double endQuote,
            double startNominal,
            double startPv,
            double endNominal,
            double endPv
            )
        {
            CountryOfRisk = countryOfRisk;
            Description = description;
            UnderlierInfo = underlierInfo;
            Book = book;
            Currency = currency;
            ProductId = productId;
            TradeId = tradeId;
            AssetClass = assetClass;
            ParentFund = parentFund;
            TradeDate = tradeDate;
            PlInception = plInception;
            PlDaily = plDaily;
            PlWeek = plWeek;
            PlMonth = plMonth;
            PlYtd = plYtd;
            Theme = theme;
            Entity = entity;
            Strategy = strategy;
            StartFxPrice = startFxPrice;
            StartQuote = startQuote;
            EndFxPrice = endFxPrice;
            EndQuote = endQuote;
            StartNominal = startNominal;
            StartPv = startPv;
            EndNominal = endNominal;
            EndPv = endPv;
        }

        #endregion


        #region Public Properties

        /// <summary>The country of risk.</summary>
        [DataMember]
        public string CountryOfRisk { get; protected set; }

        /// <summary>The description.</summary>
        [DataMember]
        public string Description { get; protected set; }

        /// <summary>The underlier info.</summary>
        [DataMember]
        public string UnderlierInfo { get; protected set; }
        
        /// <summary>The book.</summary>
        [DataMember]
        public string Book { get; protected set; }
        
        /// <summary>The currency.</summary>
        [DataMember]
        public string Currency { get; protected set; }
        
        /// <summary>The product id.</summary>
        [DataMember]
        public int ProductId { get; protected set; }
        
        /// <summary>The trade id.</summary>
        [DataMember]
        public int TradeId { get; protected set; }
        
        /// <summary>The asset class.</summary>
        [DataMember]
        public string AssetClass { get; protected set; }
        
        /// <summary>The parent fund.</summary>
        [DataMember]
        public string ParentFund { get; protected set; }
        
        /// <summary>The trade date.</summary>
        [DataMember]
        public DateTime TradeDate { get; protected set; }
        
        /// <summary>The pl inception.</summary>
        [DataMember]
        public double PlInception { get; protected set; }
        
        /// <summary>The pl daily.</summary>
        [DataMember]
        public double PlDaily { get; protected set; }
        
        /// <summary>The pl week.</summary>
        [DataMember]
        public double PlWeek { get; protected set; }
        
        /// <summary>The pl month.</summary>
        [DataMember]
        public double PlMonth { get; protected set; }
        
        /// <summary>The pl ytd.</summary>
        [DataMember]
        public double PlYtd { get; protected set; }

        /// <summary>The theme.</summary>
        [DataMember]
        public string Theme { get; protected set; }

        /// <summary>The entity.</summary>
        [DataMember]
        public string Entity { get; protected set; }

        /// <summary>The strategy.</summary>
        [DataMember]
        public string Strategy { get; protected set; }

        /// <summary>The start fx price.</summary>
        [DataMember]
        public double StartFxPrice { get; protected set; }

        /// <summary>The start quote.</summary>
        [DataMember]
        public double StartQuote { get; protected set; }

        /// <summary>The end fx price.</summary>
        [DataMember]
        public double EndFxPrice { get; protected set; }

        /// <summary>The end quote.</summary>
        [DataMember]
        public double EndQuote { get; protected set; }

        /// <summary>The start nominal.</summary>
        [DataMember]
        public double StartNominal { get; protected set; }
        
        /// <summary>The start pv.</summary>
        [DataMember]
        public double StartPv { get; protected set; }
        
        /// <summary>The end nominal.</summary>
        [DataMember]
        public double EndNominal { get; protected set; }
        
        /// <summary>The end pv.</summary>
        [DataMember]
        public double EndPv { get; protected set; }

        #endregion

        #region Public Methods

        /// <summary>The to string method.</summary>
        /// <returns>The object string.</returns>
        public override string ToString()
        {
            var output = $"{CountryOfRisk}<{Description}<{UnderlierInfo}<{Book}<{Currency}<{ProductId}<{TradeId}<{AssetClass}<{ParentFund}<{TradeDate:dd/MM/yyyy}<{PlInception}<{PlDaily}<{PlWeek}<{PlMonth}<{PlYtd}<{Theme}";
            return output;
        }

        /// <summary>Creates builder.</summary>
        /// <returns>The <see cref="Builder"/>.</returns>
        public Builder ToBuilder()
        {
            return new Builder
            {
                CountryOfRisk = CountryOfRisk,
                Description = Description,
                UnderlierInfo = UnderlierInfo,
                Book = Book,
                Currency = Currency,
                ProductId = ProductId,
                TradeId = TradeId,
                AssetClass = AssetClass,
                ParentFund = ParentFund,
                TradeDate = TradeDate,
                PlInception = PlInception,
                PlDaily = PlDaily,
                PlWeek = PlWeek,
                PlMonth = PlMonth,
                PlYtd = PlYtd,
                Theme = Theme,
                Entity = Entity,
                Strategy = Strategy,
                StartFxPrice = StartFxPrice,
                StartQuote = StartQuote,
                EndFxPrice = EndFxPrice,
                EndQuote = EndQuote,
                StartNominal = StartNominal,
                StartPv = StartPv,
                EndNominal = EndNominal,
                EndPv = EndPv
            };
        }

        #endregion

        #region Builder

        /// <summary>The builder.</summary>
        public class Builder 
        {

            #region Properties

            /// <summary>The asset class.</summary>
            public string AssetClass { get; set; }

            /// <summary>The country of risk.</summary>
            public string CountryOfRisk { get; set; }

            /// <summary>The description.</summary>
            public string Description { get; set; }
            
            /// <summary>The entity</summary>
            public string Entity { get; set; }

            /// <summary>The underlier info.</summary>
            public string UnderlierInfo { get; set; }

            /// <summary>The book.</summary>
            public string Book { get; set; }

            /// <summary>The currency.</summary>
            public string Currency { get; set; }

            /// <summary>The product id.</summary>
            public int ProductId { get; set; }

            /// <summary>The trade id.</summary>
            public int TradeId { get; set; }

            /// <summary>The parent fund.</summary>
            public string ParentFund { get; set; }

            /// <summary>The strategy.</summary>
            public string Strategy { get; set; }

            /// <summary>The trade date.</summary>
            public DateTime TradeDate { get; set; }

            /// <summary>The pl inception.</summary>
            public double PlInception { get; set; }

            /// <summary>The pl daily.</summary>
            public double PlDaily { get; set; }

            /// <summary>The pl week.</summary>
            public double PlWeek { get; set; }

            /// <summary>The pl month.</summary>
            public double PlMonth { get; set; }

            /// <summary>The pl ytd.</summary>
            public double PlYtd { get; set; }

            /// <summary>The theme.</summary>
            public string Theme { get; set; }

            /// <summary>The start fx price.</summary>
            public double StartFxPrice { get; set; }

            /// <summary>The start quote.</summary>
            public double StartQuote { get; set; }

            /// <summary>The end fx price.</summary>
            public double EndFxPrice { get; set; }

            /// <summary>The end quote.</summary>
            public double EndQuote { get; set; }

            /// <summary>The start nominal.</summary>
            public double StartNominal { get; set; }

            /// <summary>The start pv.</summary>
            public double StartPv { get; set; }

            /// <summary>The end nominal.</summary>
            public double EndNominal { get; set; }

            /// <summary>The end pv.</summary>
            public double EndPv { get; set; }

            #endregion


            #region Public Methods

            /// <summary>Build the pl entry object.</summary>
            /// <returns>The <see cref="PlEntry"/>.</returns>
            public IPlEntry Build() 
            {
                return new PlEntry(
                    CountryOfRisk, Description, 
                    UnderlierInfo, Book, 
                    Currency, ProductId, 
                    TradeId, AssetClass, 
                    ParentFund, TradeDate, 
                    PlInception, PlDaily, 
                    PlWeek, PlMonth,
                    PlYtd, Theme, Entity, Strategy, StartFxPrice, StartQuote, EndFxPrice, EndQuote,
                    StartNominal, StartPv, EndNominal, EndPv);
            }

            #endregion
        }

        #endregion

    }
}
