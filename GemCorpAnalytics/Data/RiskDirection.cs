﻿namespace GemCorpAnalytics.Data
{

    using GemCorpAnalytics.Enums;
    using System;
    using System.Runtime.Serialization;

    /// <summary>The risk direction.</summary>
    [DataContract]
    public class RiskDirection
    {

        #region Constructor

        /// <summary>Initialize an instance of the risk direction.</summary>
        /// <param name="assetClass">The asset class.</param>
        /// <param name="riskGreekDirection">The risk greek direction.</param>
        /// <param name="riskAmount">The risk amount.</param>
        /// <param name="name">The name.</param>
        /// <param name="timeSeriesMapping">The time series mapping.</param>
        private RiskDirection(
            AssetClass assetClass,
            RiskGreekDirection riskGreekDirection,
            double riskAmount,
            string name,
            string timeSeriesMapping)
        {
            AssetClass = assetClass;
            RiskGreekDirection = riskGreekDirection;
            RiskAmount = riskAmount;
            Name = name;
            TimeSeriesMapping = timeSeriesMapping ?? string.Empty;
            //Include time series repository entry building logic in this class....

            if (RiskGreekDirection == RiskGreekDirection.Vega && TimeSeriesMapping.ToLower() == "spx")
                TimeSeriesMapping = "ES1";

        }

        #endregion

        #region Properties

        /// <summary>The asset class.</summary>
        [DataMember]
        public AssetClass AssetClass { get; protected set; }

        /// <summary>The greek.</summary>
        [DataMember]
        public RiskGreekDirection RiskGreekDirection { get; protected set; }

        /// <summary>The risk amount.</summary>
        [DataMember]
        public double RiskAmount { get; protected set; }

        /// <summary>The name.</summary>
        [DataMember]
        public string Name { get; protected set; }

        /// <summary>The time series mapping.</summary>
        [DataMember]
        public string TimeSeriesMapping { get; protected set; }

        #endregion

        #region Public Methods

        /// <summary>Equals.</summary>
        /// <param name="rd">The risk direction.</param>
        /// <returns>The result of the equals check.</returns>
        public bool Equals(RiskDirection rd)
        {
            var flag = true;
            if(!(rd.AssetClass == AssetClass)) return false;
            if (!(rd.RiskGreekDirection == RiskGreekDirection)) return false;
            if(!(Math.Abs(rd.RiskAmount - RiskAmount) < 1e-6)) return false;
            if(!string.Equals(rd.Name, Name)) return false;
            if(!string.Equals(rd.TimeSeriesMapping, TimeSeriesMapping)) return false;
            
            return flag;
        }
        
        /// <summary>Equals.</summary>
        /// <param name="o">The object.</param>
        /// <returns>The result of equals check.</returns>
        public override bool Equals(object o)
        {
            var riskDirection = o as RiskDirection;

            if (riskDirection == null)
                return false;

            return Equals(riskDirection);
        }

        /// <summary>The string representation of the object.</summary>
        /// <returns>The string representation.</returns>
        public override string ToString()
        { 
            return $"{AssetClass},{RiskGreekDirection},{Name},{RiskAmount},{TimeSeriesMapping}";
        }

        /// <summary>Returns the builder.</summary>
        /// <returns>The <see cref="Builder"/>.</returns>
        public Builder ToBuilder()
        {
            return new Builder 
            { 
                AssetClass = AssetClass,
                RiskGreekDirection = RiskGreekDirection,
                RiskAmount = RiskAmount,
                Name = Name,
                TimeSeriesMapping = TimeSeriesMapping
            };

        }

        //[OnDeserializing]
        //public void OnDeserializing(StreamingContext riskDirection)
        //{
        //    var tokens = riskDirection.ToString().Split(',');
        //    AssetClass = AssetClass.Fx;
        //    RiskGreekDirection = RiskGreekDirection.Delta;
        //    RiskAmount = Convert.ToDouble(tokens[3]);
        //    Name = tokens[2];
        //    TimeSeriesMapping = tokens[4];
        //}

        #endregion

        #region Builder

        public class Builder 
        {

            /// <summary>The asset class.</summary>
            public AssetClass AssetClass { get; set; }

            /// <summary>The risk greek direction</summary>
            public RiskGreekDirection RiskGreekDirection { get; set; }
        
            /// <summary>The risk amount.</summary>
            public double RiskAmount { get; set; }

            /// <summary>The name.</summary>
            public string Name { get; set; }

            /// <summary>The time series mapping.</summary>
            public string TimeSeriesMapping { get; set; }

            /// <summary>Build an instance of risk direction.</summary>
            /// <returns></returns>
            public RiskDirection Build()
            {
                return new RiskDirection(AssetClass, RiskGreekDirection, RiskAmount, Name, TimeSeriesMapping);
            }

        }
        
        #endregion

    }
}
