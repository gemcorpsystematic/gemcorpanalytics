﻿namespace GemCorpAnalytics.Data
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using GemCorpAnalytics.Data.Interfaces;

    /// <summary>The compliance limit.</summary>
    [DataContract]
    public class ComplianceLimit : IComplianceLimit
    {
        #region Constructor
        
        /// <summary>Initialise an instance of the compliance limit object.</summary>
        /// <param name="countryOfRisk">The country of risk.</param>
        /// <param name="description">The description.</param>
        /// <param name="output">The output.</param>
        /// <param name="limit">The limit.</param>
        /// <param name="remaining">The remaining.</param>
        /// <param name="tradeIds">The tradeIds.</param>
        public ComplianceLimit(
            string countryOfRisk,
            string description,
            double output,
            double limit,
            double remaining,
            List<int> tradeIds)
        {
            CountryOfRisk = string.IsNullOrEmpty(countryOfRisk) ? throw new ArgumentNullException(nameof(countryOfRisk)) : countryOfRisk;
            Description = string.IsNullOrEmpty(description) ? throw new ArgumentNullException(nameof(description)) : description;
            Output = output;
            Limit = limit;
            Remaining = remaining;
            TradeIds = tradeIds ?? throw new ArgumentNullException(nameof(tradeIds));
        }

        #endregion

        #region Properties

        /// <summary>The country of risk.</summary>
        [DataMember]
        public string CountryOfRisk { get; }

        /// <summary>The description.</summary>
        [DataMember]
        public string Description { get; }

        /// <summary>The output.</summary>
        [DataMember]
        public double Output { get; }

        /// <summary>The limit.</summary>
        [DataMember]
        public double Limit { get; }

        /// <summary>The remaining.</summary>
        [DataMember]
        public double Remaining { get; }

        /// <summary>The trade ids.</summary>
        [DataMember]
        public List<int> TradeIds { get; }

        #endregion

        #region Public Methods

        /// <summary>The string representation of the object.</summary>
        /// <returns>A string.</returns>
        public string ToString()
        {
            return $"{CountryOfRisk},{Description}";
        }

        #endregion

    }
}
