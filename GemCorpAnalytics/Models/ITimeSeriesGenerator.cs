﻿namespace GemCorpAnalytics.Models
{
    using System;
    using GemCorpAnalytics.Data;

    /// <summary>The time series generator interface.</summary>
    /// <typeparam name="T"></typeparam>
    public interface ITimeSeriesGenerator<T>  where T : TimeSeries
    {

        /// <summary>Generates a time series.</summary>
        /// <param name="primaryTimeSeries">The primary time series.</param>
        /// <param name="secondaryTimeSeries">The secondary time series.</param>
        /// <param name="mergingRule">The merging rule.</param>
        /// <returns>The <see cref="TimeSeries"/>.</returns>
        T GenerateTimeSeries(
            T primaryTimeSeries, 
            T secondaryTimeSeries, 
            Func<T,T, bool> mergingRule);

    }
}
