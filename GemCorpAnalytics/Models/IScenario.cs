﻿namespace GemCorpAnalytics.Models
{
    /// <summary>The scenario.</summary>
    public interface IScenario
    {
        /// <summary>The scenario name.</summary>
        string ScenarioName { get; }

    }
}
