﻿namespace GemCorpAnalytics.Models
{
    using System;
    using System.Collections.Generic;


    /// <summary>The calendar.</summary>
    public interface ICalendar
    {
        /// <summary>The calendar code.</summary>
        string CalendarCode { get; }

        /// <summary>The holidays.</summary>
        List<DateTime> Holidays { get; }

        /// <summary>Checks if date is a holiday.</summary>
        /// <param name="date">The date.</param>
        /// <returns>A flag to determine if date is a holiday.</returns>
        bool IsHoliday(DateTime date);

    }
}
