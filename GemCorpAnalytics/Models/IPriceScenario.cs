﻿namespace GemCorpAnalytics.Models
{
    public interface IPriceScenario : IScenario
    {
        /// <summary>The instrument id.</summary>
        int InstrumentId { get; }

        /// <summary>The instrument name.</summary>
        string InstrumentName { get; }

        /// <summary>The lower bound.</summary>
        double LowerBound { get; }

        /// <summary>The upper bound.</summary>
        double UpperBound { get; }

    }
}
