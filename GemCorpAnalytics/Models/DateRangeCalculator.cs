﻿namespace GemCorpAnalytics.Models
{
    using System;


    public class DateRangeCalculator : IDateRangeCalculator
    {
        /// <summary>Calculates business days between dates.</summary>
        /// <param name="d1">The first date.</param>
        /// <param name="d2">The second date.</param>
        /// <param name="calendarCode">The calendar code.</param>
        /// <returns>The business days between dates.</returns>
        public int CalculateBusinessDaysBetweenDates(DateTime d1, DateTime d2, string calendarCode)
        {
            // To do this we need a calendar, ignore public holidays and weekends
            throw new NotImplementedException();
        }

        

        /// <summary>Calculates days between dates.</summary>
        /// <param name="d1">The first date.</param>
        /// <param name="d2">The second date.</param>
        /// <returns>The number of days between dates.</returns>
        public int CalculateDaysBetweenDates(DateTime d1, DateTime d2)
        {
            if (d1 > d2)
                throw new Exception($"d1:{d1:yyyy-MM-dd} is after d2:{d2:yyyy-MM-dd}");
            return Convert.ToInt32((d2 - d1).TotalDays);
        }

    }
}
