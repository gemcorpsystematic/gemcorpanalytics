﻿namespace GemCorpAnalytics.Models
{
    using System;
    using System.Collections.Generic;
    using GemCorpAnalytics.Data;

    /// <summary>The proxy time series generator.</summary>
    public interface IProxyTimeSeriesGenerator
    {
        /// <summary>Generates the proxy time series.</summary>
        /// <param name="proxySeries">The proxy series.</param>
        /// <param name="projectionSeries">The projection series.</param>
        /// <param name="dates">The dates.</param>
        /// <param name="beta">The beta.</param>
        /// <returns></returns>
        TimeSeries GenerateProxySeries(TimeSeries proxySeries, TimeSeries projectionSeries, IEnumerable<DateTime> dates, double beta);

    }
}
