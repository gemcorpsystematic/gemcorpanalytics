﻿namespace GemCorpAnalytics.Models
{
    using System;
    
    /// <summary>The date range calculator</summary>
    public interface IDateRangeCalculator
    {

        /// <summary>Calculates days between dates.</summary>
        /// <param name="d1">The first date.</param>
        /// <param name="d2">The second date.</param>
        /// <returns></returns>
        int CalculateDaysBetweenDates(DateTime d1, DateTime d2);

        /// <summary>Calculates business days between dates.</summary>
        /// <param name="d1">The first date.</param>
        /// <param name="d2">The second date.</param>
        /// <param name="calendarCode">The calendar code.</param>
        /// <returns></returns>
        int CalculateBusinessDaysBetweenDates(DateTime d1, DateTime d2, string calendarCode);

    }
}
