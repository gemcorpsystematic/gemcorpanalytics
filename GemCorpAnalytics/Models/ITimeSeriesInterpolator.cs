﻿namespace GemCorpAnalytics.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>The time series interpolator</summary>
    public interface ITimeSeriesInterpolator
    {

        /// <summary>Interpolates value.</summary>
        /// <param name="date">The date.</param>
        /// <returns>The interpolated value.</returns>
        double Interpolate(DateTime date);

        /// <summary>Interpolates value.</summary>
        /// <param name="date">The date.</param>
        /// <returns>The interpolated value.</returns>
        double Interpolate(double date);

    }
}
