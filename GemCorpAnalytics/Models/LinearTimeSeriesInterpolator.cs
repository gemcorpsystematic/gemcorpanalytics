﻿namespace GemCorpAnalytics.Models
{
    using System;
    using GemCorpAnalytics.Data;
    using System.Linq;
    
    /// <summary>The linear time series interpolator.</summary>
    public class LinearTimeSeriesInterpolator : ITimeSeriesInterpolator
    {

        #region Fields

        /// <summary>The time series.</summary>
        private readonly TimeSeries _timeSeries;

        /// <summary>The date range calculator.</summary>
        private readonly IDateRangeCalculator _dateRangeCalculator;

        #endregion

        #region Constructor

        /// <summary>Create an instance of the linear time series interpolator.</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <param name="dateRangeCalculator">The date range calculator.</param>
        public LinearTimeSeriesInterpolator(
            TimeSeries timeSeries,
            IDateRangeCalculator dateRangeCalculator)
        {
            _timeSeries = timeSeries;
            _dateRangeCalculator = dateRangeCalculator;
        }

        #endregion

        /// <summary>Interpolates for a given date.</summary>
        /// <param name="date">The date for interpolation.</param>
        /// <returns>The value.</returns>
        public double Interpolate(DateTime date)
        {
            // if date exists return date
            if (_timeSeries.ContainsDate(date))
                return _timeSeries.First(t => t.Date == date).Value;

            if (_timeSeries.First().Date <= date)
                return _timeSeries.First().Value;

            if (_timeSeries.Last().Date >= date)
                return _timeSeries.Last().Value;

            var previousPoint = _timeSeries.Last(t => t.Date < date);
            var followingPoint = _timeSeries.First(t => t.Date > date);

            var currency = _timeSeries.Select(p => p.Currency.ToLower()).Distinct();

            if (currency.Count() > 1) {
                var ccyMappings = String.Join(",",currency);
                throw new Exception($"Multiple currencies identified in time series: {ccyMappings}");
            }

            var calendarCcy = currency.First();
            return previousPoint.Value + _dateRangeCalculator.CalculateBusinessDaysBetweenDates(previousPoint.Date, date, calendarCcy)/
                _dateRangeCalculator.CalculateBusinessDaysBetweenDates(previousPoint.Date, followingPoint.Date, calendarCcy) * (followingPoint.Value - previousPoint.Value);
        }

        public double Interpolate(double date)
        {
            throw new NotImplementedException();
        }
    }
}
