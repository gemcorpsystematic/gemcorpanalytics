﻿namespace GemCorpAnalytics.Models
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.Enums;

    /// <summary>The time series filter.</summary>
    public static class TimeSeriesExtensions
    {
        /// <summary>Filters the time series by bounds.</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <param name="lowerBound">The lower boound.</param>
        /// <param name="upperBound">The upper bound.</param>
        /// <returns>The <see cref="IEnumerable{TimeSeries}"/>.</returns>
        public static IEnumerable<TimeSeries> FilterTimeSeriesByBounds(
            this TimeSeries timeSeries,
            double lowerBound,
            double upperBound)
        {
            /// what happens if there are multiple periods in the bounds.
            /// this will have to be a list of time series.
            var filteredTimeSeries = new List<TimeSeries>();
            var firstBoundsBreachIndex = timeSeries.FindIndex(p => p.Value >= lowerBound);

            for (var i = firstBoundsBreachIndex; i < timeSeries.Count; i++)
            {
                var tmpTimeSeriesPoints = new List<ITimeSeriesPoint>();
                var tmpIndex = i;

                var timeSeriesCapture = timeSeries[i].Value >= lowerBound;

                while (tmpIndex <= timeSeries.Count && timeSeries[i].Value <= upperBound && timeSeriesCapture)
                {
                    tmpTimeSeriesPoints.Add(timeSeries[i]);
                    tmpIndex++;
                }
                i = tmpIndex;

                if (tmpTimeSeriesPoints.Count > 0) filteredTimeSeries.Add(new TimeSeries(tmpTimeSeriesPoints));
            }

            return filteredTimeSeries;
        }

        /// <summary>Filters the time series by date.</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns>The <see cref="TimeSeries"/>.</returns>
        public static TimeSeries FilterTimeSeriesByDate(
            this TimeSeries timeSeries,
            DateTime startDate,
            DateTime endDate)
        {
            return new TimeSeries(timeSeries.Where(p => p.Date >= startDate && p.Date <= endDate));
        }

        /// <summary>Filter time series by week day.</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <param name="dayOfWeek">The day of week.</param>
        /// <returns></returns>
        public static TimeSeries FilterByWeekDay(
            this TimeSeries timeSeries,
            DayOfWeek dayOfWeek)
        {
            var timeSeriesPoints = timeSeries.Where(p => p.Date.DayOfWeek == dayOfWeek);
            return new TimeSeries(timeSeriesPoints);
        }

        /// <summary>Calculates daily return</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <param name="timeSeriesReturnType">The time series return type.</param>
        /// <returns>A time series of returns.</returns>
        public static TimeSeries CalculateDailyReturn(
            this TimeSeries timeSeries, TimeSeriesReturnType timeSeriesReturnType)
        {

            Func<double, double, double> returnCalculator;

            switch (timeSeriesReturnType)
            {
                case TimeSeriesReturnType.Absolute:
                    returnCalculator = (x, y) => y - x;
                    break;
                case TimeSeriesReturnType.Relative:
                    returnCalculator = (x, y) => y / x - 1.0;
                    break;
                case TimeSeriesReturnType.Log:
                    returnCalculator = (x, y) => Math.Log(y / x);
                    break;
                default:
                    throw new NotImplementedException($"Unable to handle return type: {timeSeriesReturnType}");
            }

            var timeSeriesPoints = new List<ITimeSeriesPoint>();


            for (var i = 1; i < timeSeries.Count; i++)
            {
                var x = timeSeries[i - 1];
                var y = timeSeries[i];

                timeSeriesPoints.Add(new TimeSeriesPoint.Builder
                {
                    Date = y.Date,
                    Contributor = y.Contributor,
                    Currency = y.Currency,
                    Name = y.Name,
                    Ticker = y.Ticker,
                    TimeSeriesAttribute = y.TimeSeriesAttribute,
                    InstrumentId = y.InstrumentId,
                    Unit = y.Unit,
                    Value = returnCalculator(x.Value, y.Value)
                }.Build()); ;
            }

            return new TimeSeries(timeSeriesPoints);
        }

        /// <summary>Scales the time series.</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>The time series.</returns>
        public static TimeSeries ScaleTimeSeries(this TimeSeries timeSeries, double scale)
        {

            var timeSeriesPoints = timeSeries.Select(p => new TimeSeriesPoint.Builder 
            { 
                Date =p.Date,
                Currency = p.Currency,
                Contributor = p.Contributor,
                Unit = p.Unit,
                InstrumentId = p.InstrumentId,
                Value = p.Value*scale,
                Ticker = p.Ticker,
                Name = p.Name,
                TimeSeriesAttribute = p.TimeSeriesAttribute
            }.Build());
            return new TimeSeries(timeSeriesPoints);
        }

        /// <summary>Calculates the average.</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <returns>The average.</returns>
        public static double Average(this TimeSeries timeSeries)
        {
            return timeSeries.Select(p => p.Value).Average();
        }

        /// <summary>Calculates the standard deviation.</summary>
        /// <param name="timeSeries">The time series.</param>
        /// <returns>The standard deviation.</returns>
        public static double StdDeviation(this TimeSeries timeSeries)
        {
            var mu = timeSeries.Average();
            var numerator = timeSeries.Select(p => p.Value).Aggregate(0.0, (total, next) => total + Math.Pow(next - mu, 2));
            return Math.Sqrt(numerator / (timeSeries.Count - 1));
        }

    }
}
