﻿namespace GemCorpAnalytics.Models
{
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.Interpolators;
    using System;
    using System.Linq;
    using System.Collections.Generic;

    /// <summary>The proxy time series generator.</summary>
    public class ProxyTimeSeriesGenerator : IProxyTimeSeriesGenerator
    {

        #region Fields

        /// <summary>The interpolator.</summary>
        private readonly IInterpolator _interpolator;

        #endregion

        #region Constructor

        /// <summary>Initialize an instance of the proxy time series generator.</summary>
        /// <param name="proxySeries">The proxy series.</param>
        /// <param name="interpolator">The interpolator.</param>
        public ProxyTimeSeriesGenerator(
            IInterpolator interpolator)
        {
            _interpolator = interpolator;
        }

        #endregion

        #region Public Methods

        /// <summary>Genrates the proxy series.</summary>
        /// <param name="projectionSeries">The projection series.</param>
        /// <param name="dates">The dates.</param>
        /// <param name="beta">The beta.</param>
        /// <returns></returns>
        public TimeSeries GenerateProxySeries(TimeSeries proxySeries, TimeSeries projectionSeries, IEnumerable<DateTime> dates, double beta)
        {
            // are we extending from the back ... most likely
            // if dates are included in the projection series then return values from projection series

            // px_last * (1 + return)^-beta
            
            var projectionLevel = projectionSeries.First();
            var proxyNumerator = proxySeries.First(p => p.Date == projectionLevel.Date);

            var timeSeriesPoints = new List<ITimeSeriesPoint>();
            var firstProjectionDate = projectionSeries.First().Date;

            foreach (var date in dates)
            {
                var value = 0.0;
                var contributor = "Proxy";
                if (date < firstProjectionDate)
                {
                    var proxyDenominator = proxySeries.Count(p => p.Date == date) > 0 ? proxySeries.First(p => p.Date == date).Value
                    : _interpolator.GetValue(proxySeries.Last(p => p.Date <= date), proxySeries.First(p => p.Date >= date), date);
                    value = projectionLevel.Value * Math.Pow(1.0 + (proxyNumerator.Value / proxyDenominator - 1.0), -beta);
                }
                else
                {
                    value = projectionSeries.Count(p => p.Date == date) > 0 ? projectionSeries.First(p => p.Date == date).Value :
                        _interpolator.GetValue(projectionSeries.Last(p => p.Date <= date), projectionSeries.First(p => p.Date >= date), date);
                    contributor = projectionSeries.First().Contributor;
                }


                timeSeriesPoints.Add(new TimeSeriesPoint.Builder
                {
                    Date = date,
                    Name = projectionLevel.Name,
                    Ticker = projectionLevel.Ticker,
                    Value = value,
                    Currency = projectionLevel.Currency,
                    Contributor = contributor,
                    Unit = projectionLevel.Unit,
                    InstrumentId = projectionLevel.InstrumentId,
                    TimeSeriesAttribute = projectionLevel.TimeSeriesAttribute
                }.Build());
            }

            return new TimeSeries(timeSeriesPoints);
            
        }

        #endregion

    }
}
