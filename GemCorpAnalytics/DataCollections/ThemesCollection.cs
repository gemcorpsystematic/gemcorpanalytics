﻿namespace GemCorpAnalytics.DataCollections
{
    using System.Collections.Generic;
    using GemCorpAnalytics.Data.Interfaces;

    /// <summary>The themes collection.</summary>
    public class ThemesCollection : List<IThemes>
    {
    }
}
