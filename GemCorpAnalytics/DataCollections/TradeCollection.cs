﻿namespace GemCorpAnalytics.DataCollections
{
    using GemCorpAnalytics.Data;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    [CollectionDataContract]
    [KnownType(typeof(Trade))]
    public class TradeCollection : List<Trade>
    {
    }
}
