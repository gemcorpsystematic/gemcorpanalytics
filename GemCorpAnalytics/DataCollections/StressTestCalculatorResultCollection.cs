﻿namespace GemCorpAnalytics.DataCollections
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using GemCorpAnalytics.Calculators.Interfaces;

    [CollectionDataContract]
    [KnownType(typeof(IStressTestCalculatorResult))]

    public class StressTestCalculatorResultCollection : List<IStressTestCalculatorResult>
    {
    }
}
