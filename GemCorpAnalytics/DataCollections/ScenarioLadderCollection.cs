﻿namespace GemCorpAnalytics.DataCollections
{
    using GemCorpAnalytics.Data;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>The scenario ladder collection.</summary>
    [CollectionDataContract]
    [KnownType(typeof(ScenarioLadder))]
    public class ScenarioLadderCollection : List<ScenarioLadder>
    {

        /// <summary>Adds range of scenario ladder collection.</summary>
        /// <param name="scenarioLadderCollection">The scenario ladder collection.</param>
        public void AddRange(ScenarioLadderCollection scenarioLadderCollection)
        {
            foreach(var scenarioLadder in scenarioLadderCollection)
            {
                this.Add(scenarioLadder);
            }
        }

    }
}
