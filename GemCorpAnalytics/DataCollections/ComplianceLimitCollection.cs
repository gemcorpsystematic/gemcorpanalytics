﻿using System;
namespace GemCorpAnalytics.DataCollections
{
    using System.Collections.Generic;
    using GemCorpAnalytics.Data.Interfaces;
    using System.Runtime.Serialization;

    [DataContract]
    [KnownType(typeof(IComplianceLimit))]
    public class ComplianceLimitCollection : List<IComplianceLimit>
    {
        // can just have a generic class and make life easier
    }
}
