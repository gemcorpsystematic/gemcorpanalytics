﻿
namespace GemCorpAnalytics.DataCollections
{
    using System.Collections.Generic;
    using GemCorpAnalytics.Data;
    using System.Runtime.Serialization;

    [CollectionDataContract]
    [KnownType(typeof(Nav))]
    public class NavCollection : List<Nav>
    {
    }
}
