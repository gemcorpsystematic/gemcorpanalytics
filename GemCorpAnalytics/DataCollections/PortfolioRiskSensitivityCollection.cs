﻿namespace GemCorpAnalytics.DataCollections
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using GemCorpAnalytics.Data;


    /// <summary>The portfolio risk sensitivity collection.</summary>
    [CollectionDataContract]
    [KnownType(typeof(PortfolioRiskSensitivity))]
    public class PortfolioRiskSensitivityCollection : List<PortfolioRiskSensitivity>
    {
    }
}
