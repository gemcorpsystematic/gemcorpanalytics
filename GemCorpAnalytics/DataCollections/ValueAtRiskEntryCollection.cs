﻿namespace GemCorpAnalytics.DataCollections
{
    using System.Collections.Generic;
    using GemCorpAnalytics.Data.Interfaces;

    /// <summary>The value at risk entry collection.</summary>
    public class ValueAtRiskEntryCollection : List<IValueAtRiskEntry>
    {
    }
}
