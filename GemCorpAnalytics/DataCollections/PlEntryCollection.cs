﻿
namespace GemCorpAnalytics.DataCollections
{
    using GemCorpAnalytics.Data.Interfaces;
    using System.Collections.Generic;

    /// <summary>The pl entry collection.</summary>
    public class PlEntryCollection : List<IPlEntry>
    {

    }
}
