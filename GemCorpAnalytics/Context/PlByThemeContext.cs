﻿namespace GemCorpAnalytics.Context
{
    using System;
    using System.Collections.Generic;
    using GemCorpAnalytics.Enums;
    
    /// <summary>The pl by theme context.</summary>
    public class PlByThemeContext
    {
        #region Constructor

        /// <summary>Initialize an instance of the pl by theme context.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="environment">The environment.</param>
        /// <param name="fundConfig">The weekly config.</param>
        private PlByThemeContext(
            DateTime valueDate,
            string environment,
            Dictionary<FundNames, Dictionary<FundStrategies, IReadOnlyList<string>>> fundConfigs
            )
        {
            ValueDate = valueDate;
            Environment = environment;
            FundConfigs = fundConfigs;
        }

        #endregion

        #region Public Properties

        /// <summary>The value date.</summary>
        public DateTime ValueDate { get; }

        /// <summary>The environment.</summary>
        public string Environment { get; }

        /// <summary>The weekly config.</summary>
        public Dictionary<FundNames, Dictionary<FundStrategies, IReadOnlyList<string>>> FundConfigs { get; }


        #endregion

        #region Builder

        public class Builder
        {

            #region Public Properties

            /// <summary>The value date.</summary>
            public DateTime ValueDate { get; set; }

            /// <summary>The environment.</summary>
            public string Environment { get; set; }

            /// <summary>The weekly config.</summary>
            public Dictionary<FundNames, Dictionary<FundStrategies, IReadOnlyList<string>>> FundConfigs { get; set; }

            
            #endregion

            #region Public Method

            /// <summary>Build pl by theme context.</summary>
            /// <returns>The <see cref="PlByThemeContext"/>.</returns>
            public PlByThemeContext Build()
            {
                return new PlByThemeContext(
                    ValueDate,
                    Environment,
                    FundConfigs);
            }

            #endregion

        }

        #endregion
    }
}
