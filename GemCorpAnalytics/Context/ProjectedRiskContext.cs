﻿namespace GemCorpAnalytics.Context
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Enums;


    /// <summary>The projected risk context.</summary>
    [DataContract]
    public class ProjectedRiskContext
    {
        
        #region Constructor

        /// <summary>Initialise an instance of the projected risk context.</summary>
        /// <param name="environment">The environment.</param>
        /// <param name="projectionEntries">The projection entries.</param>
        /// <param name="valueDate">The value date.</param>
        /// <param name="seriesToProxy">The series to proxy.</param>
        /// <param name="seriesToExclude">The series to exclude.</param>
        /// <param name="seriesMapping">The series mapping.</param>
        /// <param name="seriesComposition">The series composition.</param>
        /// <param name="riskType">The risk type.</param>
        /// <param name="riskMapping">The risk mapping.</param>
        /// <param name="riskValues">The risk values.</param>
        /// <param name="timeSeries">The time series.</param>
        /// <param name="riskDirection">The risk direction.</param>
        /// <param name="entity">The risk direction.</param>
        public ProjectedRiskContext(
            string environment,
            Dictionary<ITimeSeriesRepositoryEntry, AssetClass> projectionEntries,
            DateTime valueDate,
            ITimeSeriesRepositoryEntry[] seriesToProxy,
            ITimeSeriesRepositoryEntry[] seriesToExclude,
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, double>> seriesMapping,
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, ITimeSeriesRepositoryEntry>> seriesComposition,
            IReadOnlyDictionary<string, string> riskType,
            IReadOnlyDictionary<string, string> riskMapping,
            IReadOnlyDictionary<string, double> riskValues,
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> timeSeries,
            IEnumerable<RiskDirection> riskDirection,
            IEnumerable<Nav> nav,
            string entity
            )
        {
            Environment = environment;
            ProjectionEntries = projectionEntries;
            ValueDate = valueDate;
            SeriesToProxy = seriesToProxy;
            SeriesToExclude = seriesToExclude;
            SeriesMapping = seriesMapping;
            SeriesComposition = seriesComposition;
            RiskType = riskType;
            RiskMapping = riskMapping;
            RiskValues = riskValues;
            TimeSeries = timeSeries;
            RiskDirection = riskDirection;
            Nav = nav;
            Entity = entity;
        }

        #endregion

        #region Public Properties

        /// <summary>The environment.</summary>
        [DataMember]
        public string Environment { get; }

        /// <summary>The projection series.</summary>
        [DataMember]
        public Dictionary<ITimeSeriesRepositoryEntry, AssetClass> ProjectionEntries { get; }

        /// <summary>The value date.</summary>
        [DataMember]
        public DateTime ValueDate { get; }

        /// <summary>The series to proxy.</summary>
        [DataMember]
        public ITimeSeriesRepositoryEntry[] SeriesToProxy { get; }

        /// <summary>The series to exclude.</summary>
        [DataMember]
        public ITimeSeriesRepositoryEntry[] SeriesToExclude { get; }

        /// <summary>The proxy series mapping and beta amount.</summary>
        [DataMember]
        public IReadOnlyDictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, double>> SeriesMapping { get; }

        /// <summary>The composition of time series.</summary>
        [DataMember]
        public IReadOnlyDictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, ITimeSeriesRepositoryEntry>> SeriesComposition { get; }

        /// <summary>The risk types.</summary>
        [DataMember]
        public IReadOnlyDictionary<string, string> RiskType { get; }

        /// <summary>The risk mapping.</summary>
        [DataMember]
        public IReadOnlyDictionary<string, string> RiskMapping { get; }

        /// <summary>The risk values.</summary>
        [DataMember]
        public IReadOnlyDictionary<string, double> RiskValues { get; }

        /// <summary>The time series.</summary>
        [DataMember]
        public IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> TimeSeries { get; }

        [DataMember]
        public IEnumerable<RiskDirection> RiskDirection { get; }

        [DataMember]
        public IEnumerable<Nav> Nav { get; }

        [DataMember]
        public string Entity { get; }

        #endregion

    }
}
