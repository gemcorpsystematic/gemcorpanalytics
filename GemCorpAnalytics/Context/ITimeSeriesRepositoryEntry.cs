﻿namespace GemCorpAnalytics.Context
{
    
    using GemCorpAnalytics.Enums;

    /// <summary>The time series repository entry.</summary>
    public interface ITimeSeriesRepositoryEntry
    {
        /// <summary>The name.</summary>
        string Name { get; }

        /// <summary>The time series attribute</summary>
        TimeSeriesAttributes TimeSeriesAttribute { get; }

    }
}
