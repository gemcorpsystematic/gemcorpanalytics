﻿namespace GemCorpAnalytics.Context
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataCollections;

    /// <summary>The stress test calculator context.</summary>
    [DataContract]
    [KnownType(typeof(ScenarioLadderCollection))]
    [KnownType(typeof(StressTestCollection))]
    [KnownType(typeof(TimeSeries))]
    public class StressTestCalculatorContext
    {

        #region Constructor

        /// <summary>Initialize an instance of the stress test calculator context.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="scenarioLadderCollection">The scenario ladder collection.</param>
        /// <param name="stressTestCollection">The stress test collection.</param>
        /// <param name="riskDirectionCollection">The risk direction collection.</param>
        /// <param name="timeSeriesRepository">The time series repository.</param>
        /// <param name="riskToleranceAmount">The risk tolerance amount.</param>
        public StressTestCalculatorContext(
            DateTime valueDate,
            ScenarioLadderCollection scenarioLadderCollection,
            StressTestCollection stressTestCollection,
            IEnumerable<RiskDirection> riskDirectionCollection,
            ITimeSeriesRepository timeSeriesRepository,
            double riskToleranceAmount = 100.0)
        {
            ValueDate = valueDate;
            ScenarioLadderCollection = scenarioLadderCollection;
            StressTestCollection = stressTestCollection;
            RiskDirectionCollection = riskDirectionCollection;
            TimeSeriesRepository = timeSeriesRepository;
            RiskToleranceAmount = riskToleranceAmount;
        }

        #endregion

        #region Public Properties

        /// <summary>The value date.</summary>
        [DataMember]
        public DateTime ValueDate { get; protected set; }

        /// <summary>The scenario ladder collection.</summary>
        [DataMember]
        public ScenarioLadderCollection ScenarioLadderCollection { get; protected set; }

        /// <summary>The stress test collection.</summary>
        [DataMember]
        public StressTestCollection StressTestCollection { get; protected set; }
        
        /// <summary>The risk direction collection.</summary>
        [DataMember]
        public IEnumerable<RiskDirection> RiskDirectionCollection { get; protected set; }
        
        /// <summary>The time series repository.</summary>
        [DataMember]
        public ITimeSeriesRepository TimeSeriesRepository { get; protected set; }

        /// <summary>The risk tolerance amount.</summary>
        [DataMember]
        public double RiskToleranceAmount { get; protected set; }

        #endregion

    }
}
