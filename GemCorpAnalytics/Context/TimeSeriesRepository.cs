﻿namespace GemCorpAnalytics.Context
{
    using GemCorpAnalytics.Data;
    using System.Collections.Generic;
    
    /// <summary>The time series repository.</summary>
    public class TimeSeriesRepository : ITimeSeriesRepository
    {

        #region Constructor
        
        /// <summary>Initialize an instance of the time series repository.</summary>
        /// <param name="timeSeries">The time series.</param>
        public TimeSeriesRepository(
            IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> timeSeries)
        {
            TimeSeries = timeSeries;
        }

        #endregion

        #region Public Properties

        /// <summary>The time series.</summary>
        public IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> TimeSeries { get; }

        #endregion

        #region Public Methods

        /// <summary>Get the time series.</summary>
        /// <param name="timeSeriesRepositoryEntry">The time series repository entry.</param>
        /// <returns>The <see cref="TimeSeries"/>.</returns>
        public TimeSeries GetTimeSeries(ITimeSeriesRepositoryEntry timeSeriesRepositoryEntry)
        {
            if (!TimeSeries.ContainsKey(timeSeriesRepositoryEntry))
                return null;

            return TimeSeries[timeSeriesRepositoryEntry];
        }

        #endregion
    }
}
