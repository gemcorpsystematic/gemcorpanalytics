﻿namespace GemCorpAnalytics.Context
{
    using GemCorpAnalytics.Enums;
    
    /// <summary>The time series repository entry.</summary>
    public class TimeSeriesRepositoryEntry : ITimeSeriesRepositoryEntry
    {

        #region Constructor

        /// <summary>Initialize an instance of the time series repository entry.</summary>
        /// <param name="name">The name.</param>
        /// <param name="timeSeriesAttribute">The time series attribute.</param>
        public TimeSeriesRepositoryEntry(
            string name,
            TimeSeriesAttributes timeSeriesAttribute
            )
        {
            Name = name;
            TimeSeriesAttribute = timeSeriesAttribute;
        }

        #endregion


        #region Properties

        /// <summary>The name.</summary>
        public string Name { get; }

        /// <summary>The time series attribute.</summary>
        public TimeSeriesAttributes TimeSeriesAttribute { get; }

        #endregion

        #region Public Methods

        /// <summary>A string representation of the time series repository entry.</summary>
        /// <returns>A string.</returns>
        public override string ToString()
        {
            return $"{Name},{TimeSeriesAttribute}";
        }

        /// <summary>Checks if time series repository entry object is equal to this instance.</summary>
        /// <param name="timeSeriesRepositoryEntry">The time series repository entry.</param>
        /// <returns>A bool.</returns>
        public bool Equals(TimeSeriesRepositoryEntry timeSeriesRepositoryEntry)
        {
            return timeSeriesRepositoryEntry.Name.ToLower() == Name.ToLower()
                && timeSeriesRepositoryEntry.TimeSeriesAttribute == TimeSeriesAttribute;
        }

        /// <summary>Checks if object is equal to this instance of time series repository entry.</summary>
        /// <param name="obj">The object.</param>
        /// <returns>A bool.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is TimeSeriesRepositoryEntry timeSeriesRepositoryEntry))
                return false;

            return Equals(timeSeriesRepositoryEntry);
        }

        /// <summary>Computes objects hash code</summary>
        /// <returns>The hash code.</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode() + TimeSeriesAttribute.GetHashCode();
        }

        #endregion

    }
}
