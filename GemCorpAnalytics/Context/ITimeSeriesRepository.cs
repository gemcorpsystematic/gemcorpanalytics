﻿namespace GemCorpAnalytics.Context
{
    using System.Collections.Generic;
    using GemCorpAnalytics.Data;
    
    /// <summary>The time series repository interface.</summary>
    public interface ITimeSeriesRepository
    {
        /// <summary>The time series.</summary>
        IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> TimeSeries { get; }


        /// <summary>Gets the time series.</summary>
        /// <param name="timeSeriesRepositoryEntry">The time series repository entry..</param>
        /// <returns>The <see cref="TimeSeries"/>.</returns>
        TimeSeries GetTimeSeries(ITimeSeriesRepositoryEntry timeSeriesRepositoryEntry);

    }
}
