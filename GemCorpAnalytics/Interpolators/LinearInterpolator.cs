﻿namespace GemCorpAnalytics.Interpolators
{
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using System;
    using System.Linq;

    /// <summary>The linear interpolator.</summary>
    public class LinearInterpolator : IInterpolator
    {
        #region Public Methods

        /// <summary>Gets value.</summary>
        /// <param name="t1">The time series point.</param>
        /// <param name="t2">The time series point.</param>
        /// <param name="d">The date.</param>
        /// <returns>The value.</returns>
        public double GetValue(ITimeSeriesPoint t1, ITimeSeriesPoint t2, DateTime d)
        {
            if (t2.Date < t1.Date)
                throw new Exception($"{t1.Date:yyyy/MM/dd} is before {t2.Date:yyyy/MM/dd}");

            if (t1.Date > d || t2.Date < d)
                throw new Exception($"{d:yyyy/MM/dd} is outside the range {t1.Date:yyyy/MM/dd}, {t2.Date:yyyy/MM/dd}");

            var totalDays = (t2.Date - t1.Date).TotalDays;
            var d1 = (d - t1.Date).TotalDays;

            return totalDays > 0 ? t1.Value + d1 / totalDays * (t2.Value - t1.Value) : t1.Value;
        }

        /// <summary>Gets value.</summary>
        /// <param name="t">The time series.</param>
        /// <param name="d">The date.</param>
        /// <returns>The value.</returns>
        public double GetValue(TimeSeries t, DateTime d)
        {
            if (t[0].Date >= d)
                return t[0].Value;

            if (t.Last().Date <= d)
                return t.Last().Value;

            return GetValue(t.Last(p => p.Date <= d), t.First(p => p.Date >= d), d);

        }

        /// <summary>Gets value.</summary>
        /// <param name="xs">The xs.</param>
        /// <param name="ys">The ys.</param>
        /// <param name="x">The x.</param>
        /// <returns>The interpolated value.</returns>
        public double GetValue(double[] xs, double[] ys, double x, bool extrapolate=false)
        {
            if (xs.Length != ys.Length) throw new Exception("The length of x's and y's must be the same");

            if (x <= xs[0]) 
                return extrapolate ? Extrapolate(xs, ys, x) : ys[0];

            if (x >= xs[xs.Length - 1]) 
                return extrapolate ? Extrapolate(xs, ys, x) : ys[ys.Length - 1];

            var lb = xs.Last(p => p <= x);
            var ub = xs.First(p => p >= x);

            var lbIndex = Array.IndexOf(xs, lb);
            var ubIndex = Array.IndexOf(xs, ub);

            if (lbIndex == ubIndex) return ys[lbIndex];

            var denom = xs[ubIndex] - xs[lbIndex];

            return ys[lbIndex] + (x - xs[lbIndex]) / denom * (ys[ubIndex] - ys[lbIndex]);
        }

        #endregion

        #region 

        private double Extrapolate(double[] xs, double[] ys, double x)
        {
            var size = xs.Length;
            if(x >= xs[size-1])
            {
                var output = ys[size - 2] + (ys[size - 1] - ys[size - 2]) / (xs[size - 1] - xs[size - 2]) * (x - xs[size - 1]);
                return output;
            }

            var lowerOutput = ys[0] + (ys[1] - ys[0]) / (xs[1] - xs[0]) * (x - xs[0]);

            return lowerOutput;
        }

        #endregion

    }
}
