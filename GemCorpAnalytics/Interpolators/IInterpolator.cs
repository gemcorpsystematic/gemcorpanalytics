﻿
namespace GemCorpAnalytics.Interpolators
{
    using System;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;

    /// <summary>The interpolator.</summary>
    public interface IInterpolator
    {
        /// <summary>Gets value.</summary>
        /// <param name="t1">The time series point.</param>
        /// <param name="t2">The time series point.</param>
        /// <param name="d">The date.</param>
        /// <returns>The interpolated value.</returns>
        double GetValue(ITimeSeriesPoint t1, ITimeSeriesPoint t2, DateTime d);

        /// <summary>Gets value.</summary>
        /// <param name="t">The time series.</param>
        /// <param name="d">The date.</param>
        /// <returns>The interpolated value.</returns>
        double GetValue(TimeSeries t, DateTime d);


        /// <summary>Gets value.</summary>
        /// <param name="xs">The xs.</param>
        /// <param name="ys">The ys.</param>
        /// <param name="x">The x.</param>
        /// <param name="extrapolate">The extrapolate flag.</param>
        /// <returns>The interpolated value.</returns>
        double GetValue(double[] xs, double[] ys, double x, bool extrapolate=false);

    }
}
