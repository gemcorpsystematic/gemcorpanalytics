﻿namespace GemCorpAnalytics.XlDna
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Xml;
    using ExcelDna.Integration;
    using GemCorpAnalytics.Calculators;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.DataAccess.Factories;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.Enums;

    public static class GemCorpAnalyticsFunctions
    {

        private static Dictionary<string, object> GlobalDictionary;


        /// <summary>Gets the version of the addin</summary>
        /// <returns>The version.</returns>
        [ExcelFunction(Description = "Analytics functions version")]
        public static double GCA_GetVersion()
        {
            return 0.3;
        }


        [ExcelFunction(Description = "Initializes Add-In")]
        public static string GCA_Initialize()
        {
            GlobalDictionary = new Dictionary<string, object>();

            return "Addin Initialized";
        }

        /// <summary>Removes a given key from global dictionary.</summary>
        /// <param name="key">The key of the object to be removed</param>
        /// <returns>A message to indicate if removal was successful.</returns>
        [ExcelFunction(Description = "Removes stored values from global dictionary")]
        public static string GCA_RemoveValuesFromGlobalDictionary(string key)
        {
            if (!GlobalDictionary.ContainsKey(key))
                return $"Global dictionary does not contain key: {key}";

            var removeFlag = GlobalDictionary.Remove(key);

            return removeFlag ? $"Removed {key}" : $"Unable to remove {key}";
        }


        /// <summary>Loads the risk direction time series.</summary>
        /// <param name="name">This will be the name of the time series dictionary.</param>
        /// <param name="timeSeriesDictionary">This will be an mxn matrix.</param>
        /// <returns>A key used for storing the risk direction time series.</returns>
        [ExcelFunction(Description = "Stores time series dictionary")]
        public static string GCA_StoreTimeSeriesDictionary(string name, object timeSeriesDictionary)
        {
            // Add the proxy calcultion for the time series here to
            // should this be in a separate method

            if (GlobalDictionary == null)
                return "Add-in has not been initialized, please run GCA_Initialize()";

            // This method will be used to store the risk direction and risk projection time series.
            var timeSeriesMatrix = timeSeriesDictionary as object[,];

            // the first row will be names and first column will be dates

            if (timeSeriesMatrix == null)
                return "Unable to store time series matrix";

            var rows = timeSeriesMatrix.GetLength(0);
            var columns = timeSeriesMatrix.GetLength(1);

            var timeSeriesDates = new List<DateTime>();
            var timeSeriesLabels = new List<string>();
            var timeSeriesDict = new Dictionary<ITimeSeriesRepositoryEntry, TimeSeries>();

            // start from i=1 because timeSeriesMatrix[0, 0] is an empty cell
            for (var i = 1; i < rows; i++)
            {
                timeSeriesDates.Add(DateTime.FromOADate((double)timeSeriesMatrix[i, 0]));
            }

            for (var i = 1; i < columns; i++)
            {
                timeSeriesLabels.Add((string)timeSeriesMatrix[0, i]);
            }

            // start from j=1, because first column is for dates
            for (var j = 1; j < columns; j++)
            {
                var tsLabel = timeSeriesLabels[j - 1];
                var tsPoints = new List<TimeSeriesPoint>();
                for (var i = 1; i < rows; i++)
                {
                    var tsPoint = new TimeSeriesPoint.Builder
                    {
                        Date = timeSeriesDates[i - 1],
                        Name = tsLabel,
                        Value = (double)timeSeriesMatrix[i, j]
                    }.Build();
                    tsPoints.Add(tsPoint);
                }
                var timeSeriesRepositoryEntry = new TimeSeriesRepositoryEntry(tsLabel, TimeSeriesAttributes.Close);
                timeSeriesDict[timeSeriesRepositoryEntry] = new TimeSeries(tsPoints);
            }

            var timeSeriesDictKey = $"#{timeSeriesDict.GetHashCode()}";

            GlobalDictionary[timeSeriesDictKey] = timeSeriesDict;

            return timeSeriesDictKey;
        }

        /// <summary>Stores time series</summary>
        /// <param name="timeSeriesDictKey"></param>
        /// <param name="timeSeriesKey"></param>
        /// <param name="timeSeriesObject"></param>
        /// <returns></returns>
        [ExcelFunction(Description = "Adds time series to risk dictionary")]
        public static string GCA_StoreTimeSeries(string timeSeriesDictKey, string timeSeriesKey, object timeSeriesObject)
        {
            // add time series to time series dict key
            var timeSeriesMatrix = timeSeriesObject as object[,];

            if (timeSeriesMatrix == null)
                return "Unable to handle time series";

            var timeSeriesDict = (Dictionary<ITimeSeriesRepositoryEntry, TimeSeries>)GlobalDictionary[timeSeriesDictKey];

            var timeSeries = ConvertMatrixToTimeSeries(timeSeriesKey, timeSeriesMatrix);

            timeSeriesDict[new TimeSeriesRepositoryEntry(timeSeriesKey, TimeSeriesAttributes.Close)] = timeSeries;

            return $"Added {timeSeriesKey} to time series dictionary";
        }

        [ExcelFunction(Description = "Updates time series using proxy mappings.")]
        public static string GCA_UpdateTimeSeriesUsingProxy(
            string timeSeriesDictKey, 
            string timeSeriesToAddKey,
            string proxyTimeSeriesKey,
            object timeSeriesToAdd, 
            object proxyTimeSeriesObject, 
            object proxyMappingsObject)
        {
            // proxy mappings -> TimeSeriesKey, ProxyKey, Beta, FirstDate, LastDate

            // do we create proxy values upfront or only when needed?

            var proxyMappingsMatrix = proxyMappingsObject as object[,];
            var timeSeriesToAddMatrix = timeSeriesToAdd as object[,];
            var proxyTimeSeriesMatrix = proxyTimeSeriesObject as object[,]; // convert this into a time series

            if (proxyMappingsMatrix == null)
                return "Unable to handle proxy mappings, expecting a 2-d array";

            var rows = proxyMappingsMatrix.GetLength(0);
            var columns = proxyMappingsMatrix.GetLength(1);

            var proxyTimeSeries = ConvertMatrixToTimeSeries(proxyTimeSeriesKey, proxyTimeSeriesMatrix);
            var timeSeriesToProxy = ConvertMatrixToTimeSeries(timeSeriesToAddKey, timeSeriesToAddMatrix);
            var timeSeriesDict = (Dictionary<ITimeSeriesRepositoryEntry, TimeSeries>)GlobalDictionary[timeSeriesDictKey];

            // a time series can have more than 1 proxy....need a proxy mapping object

            var proxyMappings = new List<IProxyMapping>();

            for (var i = 0; i < rows; i++)
            {
                var timeSeriesKey = (string)proxyMappingsMatrix[i, 0];
                var timeSeriesEntryKey = new TimeSeriesRepositoryEntry(timeSeriesKey, TimeSeriesAttributes.Close);
                var proxySeriesKey = (string)proxyMappingsMatrix[i, 1];
                var proxyTimeSeriesEntryKey = new TimeSeriesRepositoryEntry(proxySeriesKey, TimeSeriesAttributes.Close);
                var beta = (double)proxyMappingsMatrix[i, 2];
                var firstDate = DateTime.FromOADate((double)proxyMappingsMatrix[i, 3]);
                var lastDate = DateTime.FromOADate((double)proxyMappingsMatrix[i, 4]);
                // tricky because sometimes, we can have more than 1 proxies
                var proxyMapping = new ProxyMapping(timeSeriesEntryKey, proxyTimeSeriesEntryKey, beta, firstDate, lastDate);
                proxyMappings.Add(proxyMapping);
            }

            // need a time series of dates starting from start date

            var linearInterpolator = new Interpolators.LinearInterpolator();
            var proxyTimeSeriesCalculators = new ProxyTimeSeriesCalculator(proxyMappings, timeSeriesToProxy, linearInterpolator);

            var proxyDates = proxyTimeSeries.Select(ts => ts.Date);

            var proxyKey = new TimeSeriesRepositoryEntry(timeSeriesToAddKey, TimeSeriesAttributes.Close);

            var result = proxyTimeSeriesCalculators.Calculate(proxyDates, proxyTimeSeries);

            timeSeriesDict[proxyKey] = result;

            return $"Time Series Dict: Added Key {timeSeriesToAddKey}";
        }


        [ExcelFunction(Description ="Stores scenario ladders")]
        public static string GCA_StoreScenarioLadders(DateTime valueDate, string assetClass, string entity, object scenarioLadders)
        {
            var scenarioLaddersMatrix = scenarioLadders as object[,];

            if (scenarioLaddersMatrix == null)
                return "Unable to handle scenario ladders";

            var rows = scenarioLaddersMatrix.GetLength(0);
            var columns = scenarioLaddersMatrix.GetLength(1);

            var ladderCollection = new ScenarioLadderCollection();

            // first column is scenario name
            // first row is name of scenario ladder series

            for(int j=1;j<columns;j++)
            {
                var scenarioLadder = new ScenarioLadderCollection();
                var name = (string)scenarioLaddersMatrix[0, j];
                for (int i=1;i<rows;i++)
                {
                    var scenarioLadderPoint = new ScenarioLadder.Builder{
                        Name = name,
                        ScenarioDate = valueDate,
                        ScenarioName = (string)scenarioLaddersMatrix[i,0],
                        RiskType = $"{assetClass} Ladder",
                        Strategy = "",
                        Entity = entity,
                        ScenarioValue = (double)scenarioLaddersMatrix[i,j]
                    }.Build();

                    scenarioLadder.Add(scenarioLadderPoint);
                }
                ladderCollection.AddRange(scenarioLadder);
            }

            var timeSeriesDictKey = $"ScenarioLadder_{assetClass}_{ladderCollection.GetHashCode()}";

            GlobalDictionary[timeSeriesDictKey] = ladderCollection;

            return timeSeriesDictKey;
        }


        public static string GCA_StoreRiskDirections(object riskDirectionEntries)
        {

            var riskDirectionsMatrix = riskDirectionEntries as object[,];

            if (riskDirectionsMatrix == null)
                return "Unable to handle risk directions";

            var riskDirectionRows = riskDirectionsMatrix.GetLength(0);

            var riskDirections = new List<RiskDirection>();

            for (int i = 0; i < riskDirectionRows; i++)
            {
                var riskAmount = (double)riskDirectionsMatrix[i, 0];
                var name = (string)riskDirectionsMatrix[i, 1];
                var timeSeriesMapping = name;
                var riskReturnType = (string)riskDirectionsMatrix[i, 2];

                var riskDirection = new RiskDirection.Builder
                {
                    AssetClass = DataAccess.Utils.Utils.ParseAssetClass(riskReturnType),
                    RiskGreekDirection = RiskGreekDirection.Delta,
                    Name = name,
                    RiskAmount = riskAmount,
                    TimeSeriesMapping = timeSeriesMapping,
                }.Build();

                riskDirections.Add(riskDirection);
            }

            var riskDirectionsKey = $"#RiskDirections_{riskDirections.GetHashCode()}";

            GlobalDictionary[riskDirectionsKey] = riskDirections;

            return riskDirectionsKey;
        }


        [ExcelFunction(Description = "Calculates historical scenarios")]
        public static object GCA_CalculateHistScenarios(DateTime valueDate, object riskIdentifiers, object scenarioStartPoints, object scenarioEndPoints,
            string timeSeriesKey,
            string riskDirectionKey)
        {
            var riskIdentifiersArr = riskIdentifiers as object[,];
            var scenarioStartPointsMat = scenarioStartPoints as object[,];
            var scenarioEndPointsMat = scenarioEndPoints as object[,];


            var startRows = scenarioStartPointsMat.GetLength(0);
            var startCols = scenarioStartPointsMat.GetLength(1);
            var endRows = scenarioEndPointsMat.GetLength(0);
            var endCols = scenarioEndPointsMat.GetLength(1);

            if ((startRows != endRows) && (startCols != endCols))
                return "Scenario start and end data is not of the same dimension";

            //each col represents a stress test and each row is a scenario

            var identifiers = new List<string>();
            var identifierValue = new Dictionary<string, double>();

            var riskDirectionTimeSeries = (Dictionary<ITimeSeriesRepositoryEntry, TimeSeries>)GlobalDictionary[timeSeriesKey];
            var riskDirections = (List<RiskDirection>)GlobalDictionary[riskDirectionKey];

            foreach (var riskObj in riskIdentifiersArr)
            {
                var identifier = (string)riskObj;
                identifiers.Add(identifier);
                var timeSeries = riskDirectionTimeSeries[new TimeSeriesRepositoryEntry(identifier, TimeSeriesAttributes.Close)];
                var currValue = timeSeries.First(p => p.Date == valueDate).Value;
                identifierValue[identifier] = currValue;
            }


            var stressTests = new List<StressTest>();

            for (var j = 0; j < endCols; j++)
            {
                var stressTestStartDate = DateTime.FromOADate((double)scenarioStartPointsMat[0, j]);
                var stressTestEndDate = DateTime.FromOADate((double)scenarioEndPointsMat[0, j]);
                var scenarios = new List<Scenario>();
                for (var i = 1; i < endRows; i++) // first rows are dates
                {
                    var underlier = identifiers[i - 1];
                    var startScenValue = (double)scenarioStartPointsMat[i, j];
                    var endScenValue = (double)scenarioEndPointsMat[i, j];
                    var currValue = identifierValue[underlier];
                    scenarios.Add(new Scenario.Builder
                    {
                        Underlier = underlier,
                        StartDate = stressTestStartDate,
                        EndDate = stressTestEndDate,
                        CurrentDate = valueDate,
                        StartValue = startScenValue,
                        EndValue = endScenValue,
                        CurrentValue = identifierValue[underlier]
                    }.Build());
                }

                var stressTest = new StressTest.Builder {
                    Name = $"StressTest_{j}",
                    ValueDate = valueDate,
                    StartDate = stressTestStartDate,
                    EndDate = stressTestEndDate,
                    Scenarios = scenarios
                }.Build();

                stressTests.Add(stressTest);
            }

            var scenarioLadderKeys = GlobalDictionary.Keys.Where(k => k.Contains("ScenarioLadder"));

            if (scenarioLadderKeys.Count() == 0)
                return "No scenario ladders have been provided";

            var scenarioLadderCollection = new ScenarioLadderCollection();

            foreach (var ladderKey in scenarioLadderKeys)
            {

                var ladder = (ScenarioLadderCollection)GlobalDictionary[ladderKey];
                scenarioLadderCollection.AddRange(ladder);
            }

            var stressTestCollection = new StressTestCollection();
            stressTestCollection.AddRange(stressTests);

            var timeSeriesRepository = new TimeSeriesRepository(riskDirectionTimeSeries);

            var context = new StressTestCalculatorContext(valueDate, scenarioLadderCollection, stressTestCollection, riskDirections, timeSeriesRepository);
            var stressTestCalculator = new StressTestCalculator(new Interpolators.LinearInterpolator());
            var stressTestCalculatorResults = stressTestCalculator.Calculate(context);

            var outputObject = new object[stressTestCalculatorResults.Count, 11];

            var counter = 0;
            foreach (var stressTestCalculatorResult in stressTestCalculatorResults)
            {
                outputObject[counter, 0] = stressTestCalculatorResult.ValueDate;
                outputObject[counter, 1] = stressTestCalculatorResult.ScenarioName;
                outputObject[counter, 2] = stressTestCalculatorResult.UnderlierName;
                outputObject[counter, 3] = stressTestCalculatorResult.AssetClass;
                outputObject[counter, 4] = stressTestCalculatorResult.RiskDirectionAmount;
                outputObject[counter, 5] = stressTestCalculatorResult.StartDate;
                outputObject[counter, 6] = stressTestCalculatorResult.EndDate;
                outputObject[counter, 7] = stressTestCalculatorResult.StartValue;
                outputObject[counter, 8] = stressTestCalculatorResult.EndValue;
                outputObject[counter, 9] = stressTestCalculatorResult.Shock;
                outputObject[counter, 10] = stressTestCalculatorResult.PlAmount;
                counter++;
            }

            return outputObject;
        }

        /// <summary>Gets fund sensitivities</summary>
        /// <param name="environment">The environment.</param>
        /// <param name="funds">The funds.</param>
        /// <param name="valueDate">The value date.</param>
        /// <returns>A list of fund risks.</returns>
        [ExcelFunction(Description = "Gets the fund risk sensitivities")]
        public static object GCA_GetFundRisks(string environment, string funds, DateTime valueDate)
        {

            var riskType = "None";

            var getRiskSensitivityCollection = new GetRiskSensitivityCollection();
            getRiskSensitivityCollection.AddRange(new[] {
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LIMITED", riskType, "Forex"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LIMITED", riskType, "Credit"), // there is no risk type called credit
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LIMITED", riskType, "Equity"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LIMITED", riskType, "Interest"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LIMITED", riskType, "Commodity"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LIMITED", riskType, "Commodity Volatility"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LIMITED", riskType, "Equity Volatility"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LIMITED", "CS01", "BondSpread"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LP", riskType, "Forex"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LP", riskType, "Credit"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LP", riskType, "Equity"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LP", riskType, "Interest"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LP", riskType, "Commodity"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LP", riskType, "Commodity Volatility"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LP", riskType, "Equity Volatility"),
                new GetRiskSensitivityRequest(valueDate, "", "GEMCORP FUND I LP", "CS01", "BondSpread")
            });

            var getRiskSensitivityCollectionHandler = RiskSensitivityRequestHandlerFactory.GetHandler(environment);
            var riskSensitivityCollection = getRiskSensitivityCollectionHandler.Handle(getRiskSensitivityCollection);


            var portfolioRiskSensitivityCollection = new PortfolioRiskSensitivityCollection();
            portfolioRiskSensitivityCollection.AddRange(riskSensitivityCollection);

            var portfolioRiskCalculator = new PortfolioRiskFactorCalculator(portfolioRiskSensitivityCollection, null);
            var sensitivities = portfolioRiskCalculator.Calculate();

            var riskDirectionCollections = sensitivities.RiskDirectionCollection.Select(x =>
            {
                return new RiskDirection.Builder
                {
                    AssetClass = x.AssetClass,
                    Name = x.Name,
                    RiskAmount = x.RiskAmount,
                    RiskGreekDirection = x.RiskGreekDirection,
                    TimeSeriesMapping = string.Empty
                }.Build();
            });


            //var riskDirectionCollections = CollectionDeserializer<IEnumerable<RiskDirection>>("C:\\temp\\RiskDirectionCollection_20221230.xml");

            var output = new object[riskDirectionCollections.Count(), 4];

            var i = 0;

            foreach (var riskDirection in riskDirectionCollections)
            {
                output[i, 0] = riskDirection.AssetClass.ToString(); // have to convert enums to string
                output[i, 1] = riskDirection.Name;
                output[i, 2] = riskDirection.RiskAmount;
                output[i, 3] = riskDirection.RiskGreekDirection.ToString();
                i++;
            }

            return output;
        }

        /// <summary>Gets scenario ladders.</summary>
        /// <param name="environment">The environment.</param>
        /// <param name="funds">The funds.</param>
        /// <param name="valueDate">The value date.</param>
        /// <param name="names">The names.</param>
        /// <param name="riskType">The risk type.</param>
        /// <returns></returns>
        [ExcelFunction(Description = "Gets scenario ladders.")]
        public static object GCA_GetScenarioLadders(string environment, string funds, DateTime valueDate, object targetNames, string riskType)
        {
            // deserialize from the xml file
            // Can have a flag that includes first column
            var objectLength = targetNames as object[,];

            if (objectLength == null)
                return "Unable to transform string names into a list of strings";

            var names = new List<string>();

            foreach (var obj in objectLength)
            {
                names.Add((string)obj);
            }

            var scenarioLadderCollection = CollectionDeserializer<ScenarioLadderCollection>("C:\\temp\\ScenarioLadderCollection_20221230.xml");

            var scenarioLadders = scenarioLadderCollection.Where(s => s.RiskType == riskType);
            var ladderLength = scenarioLadderCollection.Count();
            var output = new object[ladderLength, names.Count() + 1];

            var j = 0;
            foreach (var name in names)
            {
                var scenarioLadder = scenarioLadders.Where(s => s.Name == name);
                var i = 0;
                if (j < 1)
                {
                    foreach (var data in scenarioLadder)
                    {
                        if (i == 0)
                        {
                            output[i, j] = string.Empty;
                            output[i+1, j] = data.ScenarioName;
                            i++;
                            continue;
                        }

                        output[i, j] = data.ScenarioName;
                        i++;
                    }
                    j++;
                }
                i = 0;
                foreach (var data in scenarioLadder)
                {
                    if (i == 0)
                    {
                        output[i, j] = data.Name;
                        output[i+1, j] = data.ScenarioValue;
                        i++;
                        continue;
                    }

                    output[i, j] = data.ScenarioValue;
                    i++;
                }
                j++;
            }
            return output;
        }


        [ExcelFunction(Description ="Gets the time series stored in time series dictionary.")]
        public static object GCA_GetTimeSeries(string timeSeriesDictKey, string timeSeriesKey)
        {
            var timeSeriesDict = (Dictionary<ITimeSeriesRepositoryEntry, TimeSeries>)GlobalDictionary[timeSeriesDictKey];
            var timeSeriesEntry = new TimeSeriesRepositoryEntry(timeSeriesKey, TimeSeriesAttributes.Close);

            if (!timeSeriesDict.ContainsKey(timeSeriesEntry))
                return $"Unable to find {timeSeriesKey}";

            var timeSeries = timeSeriesDict[timeSeriesEntry];

            var output = new object[timeSeries.Count(), 2];

            var i = 0;
            foreach (var data in timeSeries)
            {
                output[i, 0] = data.Date; // have to convert enums to string
                output[i, 1] = data.Value;
                i++;
            }

            return output;

        }

        /// <summary>Calculates betas.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="timeSeriesKey">The time series key.</param>
        /// <param name="riskDirectionsKey">The risk directions key.</param>
        /// <param name="projectionDirections">The projection directions.</param>
        /// <param name="lowerBound">The lower bound.</param>
        /// <param name="upperBound">The upper bound.</param>
        /// <returns></returns>
        [ExcelFunction(Description = "Calculates betas")]
        public static object GCA_CalculateBetas(DateTime valueDate, string timeSeriesKey, string riskDirectionsKey, object projectionDirections, double lowerBound, double upperBound)
        {
            // given a set of risk directions and a list of projection directions, will return beta

            string environment = "PROD";
            string entity = "Gemcorp Fund I limited";
            var riskDirectionTimeSeries = (Dictionary<ITimeSeriesRepositoryEntry, TimeSeries>)GlobalDictionary[timeSeriesKey];
            var riskDirections = (List<RiskDirection>)GlobalDictionary[riskDirectionsKey];

            if (!GlobalDictionary.ContainsKey(timeSeriesKey))
                return "Unable to find time series key";

            if (!GlobalDictionary.ContainsKey(riskDirectionsKey))
                return "Unable to find risk directions key";

            var projectionDirectionsMatrix = projectionDirections as object[,];

            if (projectionDirectionsMatrix == null)
                return "Unable to hanlde projection directions";

            var projectionRows = projectionDirectionsMatrix.GetLength(0);
            var projectionColumns = projectionDirectionsMatrix.GetLength(1);

            if (projectionColumns < 2)
                return $"Unable to handle projection columns, expected 2 columns received {projectionColumns} columns";

            var seriesToProxy = new List<ITimeSeriesRepositoryEntry>();
            ITimeSeriesRepositoryEntry[] seriesToExclude = null;
            var seriesMapping = new Dictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, double>>();
            var seriesComposition = new Dictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, ITimeSeriesRepositoryEntry>>();
            var projectionEntries = new Dictionary<ITimeSeriesRepositoryEntry, AssetClass>();
            IReadOnlyDictionary<string, string> riskType = null; // redundant
            IReadOnlyDictionary<string, string> riskMapping = null; // redundant
            IReadOnlyDictionary<string, double> riskValues = null; // redundant
            var navs = new List<Nav>();

            for (int i=0;i< projectionRows;i++) 
            {
                var timeSeriesEntry = new TimeSeriesRepositoryEntry((string)projectionDirectionsMatrix[i, 0], TimeSeriesAttributes.Close);                
                var assetClass = DataAccess.Utils.Utils.ParseAssetClass((string)projectionDirectionsMatrix[i,1]);
                projectionEntries[timeSeriesEntry] = assetClass;
            }

            var nav = new Nav.Builder {
                Date = valueDate,
                Entity =entity,
                Amount = 651891093.0
            }.Build();

            navs.Add(nav);

            var minDate = new DateTime(2015, 11, 5);

            var projectedRiskContext = new ProjectedRiskContext(environment, projectionEntries, valueDate, seriesToProxy.ToArray(), seriesToExclude, seriesMapping,
                seriesComposition, riskType, riskMapping, riskValues, riskDirectionTimeSeries, riskDirections, navs, entity);

            var minVarHedgeCalculator = new MinVarHedgeBetaCalculator(projectedRiskContext, null, null);

            var minVarHedgeCalculatorResult = minVarHedgeCalculator.Calculate(valueDate, minDate, lowerBound, upperBound);

            var output = new object[minVarHedgeCalculatorResult.Beta.Keys.Count(), 2];

            var idx = 0;
            foreach(var kvp in minVarHedgeCalculatorResult.Beta)
            {
                output[idx, 0] = kvp.Key;
                output[idx, 1] = kvp.Value;
                idx++;
            }

            return output;
        }

        /// <summary>Gets Pl by theme data from orchestrade.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="config">The config.</param>
        /// <param name="filter">The filter.</param>
        /// <returns>Value by theme output.</returns>
        [ExcelFunction("")]
        public static object GCA_GetPlByTheme(DateTime valueDate, string config, string filter)
        {
            // create an OT connection 
            // run the 

            var userName = "orchestra";
            var env = "msprod";
            var pwd = "orchestra";

            var otBlotter = new OrchestradeLayer.OtBlotterPl(userName, env, pwd);
            otBlotter.ConnnectToOrchestrade();

            var plByTheme = otBlotter.RunReport(valueDate, config, filter);

            return plByTheme;
        }


        public static T CollectionDeserializer<T>(string fileName)
        {
            var fs = new FileStream(fileName, FileMode.Open);
            var reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            var serialiser = new DataContractSerializer(typeof(T));
            var riskSensitivityCollection = (T)serialiser.ReadObject(reader, true);
            reader.Close();
            fs.Close();
            return riskSensitivityCollection;
        }

        private static TimeSeries ConvertMatrixToTimeSeries(string timeSeriesKey, object[,] timeSeriesMatrix)
        {
            var rows = timeSeriesMatrix.GetLength(0);
            var columns = timeSeriesMatrix.GetLength(1);

            var timeSeriesPoints = new List<TimeSeriesPoint>();

            for (var i = 0; i < rows; i++)
            {
                for (var j = 1; j < columns; j++)
                {
                    var timeSeriesPoint = new TimeSeriesPoint.Builder
                    {
                        Name = timeSeriesKey,
                        Ticker = timeSeriesKey,
                        Date = DateTime.FromOADate((double)timeSeriesMatrix[i, 0]),
                        Value = (double)timeSeriesMatrix[i, j],
                        TimeSeriesAttribute = TimeSeriesAttributes.Close
                    }.Build();
                    timeSeriesPoints.Add(timeSeriesPoint);
                }
            }

            return new TimeSeries(timeSeriesPoints);
        }

    }
}
