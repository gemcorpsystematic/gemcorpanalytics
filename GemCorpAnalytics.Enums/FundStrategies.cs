﻿namespace GemCorpAnalytics.Enums
{

    /// <summary>The fund strategies.</summary>
    public enum FundStrategies
    {
        Macro,

        PrivateCredit,

        PublicCredit,

        Systematic
    }
}
