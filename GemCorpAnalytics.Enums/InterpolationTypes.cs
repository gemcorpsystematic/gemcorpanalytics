﻿namespace GemCorpAnalytics.Enums
{
    /// <summary>The interpolation types.</summary>
    public enum InterpolationTypes
    {
        /// <summary>Linear interpolation.</summary>
        Linear

    }
}
