﻿namespace GemCorpAnalytics.Enums
{
    /// <summary>The day count convention.</summary>
    public enum DayCountConvention
    {

        // we are missing the ISMA and ICMA day count conventions, as well as a few others.

        /// <summary>The act 365.</summary>
        Act365,

        /// <summary>The act 360.</summary>
        Act360,

        /// <summary>The act act.</summary>
        ActAct

    }
}
