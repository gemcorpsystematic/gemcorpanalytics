﻿namespace GemCorpAnalytics.Enums
{
    /// <summary>The time series type.</summary>
    public enum TimeSeriesType
    {
        /// <summary>The fair value 16:30 snap.</summary>
        FairValue1630Snap,

        /// <summary>The daily value.</summary>
        Daily

    }
}
