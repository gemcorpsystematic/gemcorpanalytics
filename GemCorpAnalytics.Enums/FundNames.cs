﻿namespace GemCorpAnalytics.Enums
{
    public enum FundNames
    {
        /// <summary>The primary fund.</summary>
        Fund,

        /// <summary>The multi fund.</summary>
        Multi,

        /// <summary>The africa fund.</summary>
        Africa,

        /// <summary>The macro strategy</summary>
        Macro
    }
}
