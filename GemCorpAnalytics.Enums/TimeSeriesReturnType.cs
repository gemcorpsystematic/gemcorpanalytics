﻿namespace GemCorpAnalytics.Enums
{
    public enum TimeSeriesReturnType
    {
        /// <summary>The relative return</summary>
        Relative, 

        /// <summary>The absolute return.</summary>
        Absolute,

        /// <summary>The log return.</summary>
        Log

    }
}
