﻿namespace GemCorpAnalytics.Enums
{
    /// <summary>The instrument types.</summary>
    public enum InstrumentTypes
    {
        /// <summary>Bond.</summary>
        Bond, 

        /// <summary>Bond future.</summary>
        BondFuture,

        /// <summary>Credit default swaps.</summary>
        CDS,
        
        /// <summary>Commodity future listed option.</summary>
        CommodityFutureListedOption,

        /// <summary>Equity listed option.</summary>
        EquityListedOption,

        /// <summary>Exchange traded fund.</summary>
        ETF,

        /// <summary>FX option.</summary>
        FXOption,
        
        /// <summary>FX swap.</summary>
        FXSwap,

        /// <summary>Loan.</summary>
        Loan,

        /// <summary>Loan deposit.</summary>
        LoanDeposit,

        /// <summary>Non deliverable swap.</summary>
        NDS,

        /// <summary>Repo.</summary>
        Repo,

        /// <summary>Swap.</summary>
        Swap,
        
        /// <summary>Total return swap.</summary>
        TRS
    }
}
