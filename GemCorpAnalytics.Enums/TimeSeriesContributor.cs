﻿namespace GemCorpAnalytics.Enums
{
    /// <summary>The time series contributor.</summary>
    public enum TimeSeriesContributor
    {
        /// <summary>Bloomberg</summary>
        Bloomberg,

        /// <summary>Platts</summary>
        Platts
    }
}
