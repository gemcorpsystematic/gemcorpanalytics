﻿namespace GemCorpAnalytics.Enums
{
    /// <summary>The time series attributes.</summary>
    public enum TimeSeriesAttributes
    {
        /// <summary>The open.</summary>
        Open,

        /// <summary>The high.</summary>
        High,

        /// <summary>The low.</summary>
        Low,

        /// <summary>The close.</summary>
        Close,

        /// <summary>The open high low close.</summary>
        OHLC,

        /// <summary>The volume.</summary>
        Volume,

        /// <summary>The aum.</summary>
        Aum,

        /// <summary>The nav.</summary>
        Nav,

        /// <summary>The shares outstanding.</summary>
        Shares_Outstanding,

        /// <summary>The short ineterest.</summary>
        Short_Interest,

        /// <summary>The z spread mid.</summary>
        Z_Sprd_Mid,

        /// <summary>The volatility 30 day, standard deviation.</summary>
        Volatility_30D,

        /// <summary>The volatility 30 day, 50 delta, call option</summary>
        Volatility_30D_50Delta,

        /// <summary>The volatility 60 day, standard deviation</summary>
        Volatility_60D,

        /// <summary>The volatiity 90 day, standard deviation.</summary>
        Volatility_90D,

        /// <summary>The volatility 180 day. standard deviation</summary>
        Volatility_180D,

        /// <summary>The 30 day implied vol at 100% moneyness.</summary>
        Volatility_30D_ImpVol_100_Mny_Df,

        /// <summary>The 60 day implied vol at 100% moneyness.</summary>
        Volatility_60D_ImpVol_100_Mny_Df,

        /// <summary>The 180 day implied vol at 100% moneyness.</summary>
        Volatility_180D_ImpVol_100_Mny_Df

    }
}
