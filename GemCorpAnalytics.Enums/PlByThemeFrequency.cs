﻿namespace GemCorpAnalytics.Enums
{
    public enum PlByThemeFrequency
    {
        Base,

        Week,

        Month,

        Year,

        Inception
    }
}
