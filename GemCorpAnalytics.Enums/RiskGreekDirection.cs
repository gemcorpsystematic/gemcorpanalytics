﻿namespace GemCorpAnalytics.Enums
{
    /// <summary>The risk greek direction.</summary>
    public enum RiskGreekDirection
    {
        /// <summary>The delta.</summary>
        Delta, 

        /// <summary>The vega.</summary>
        Vega
    }
}
