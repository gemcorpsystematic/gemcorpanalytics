﻿namespace GemCorpAnalytics.Enums
{
    /// <summary>The asset class.</summary>
    public enum AssetClass
    {
        /// <summary>Bond Spread.</summary>
        BondSpread,

        /// <summary>Commodity.</summary>
        Commodity,

        /// <summary>Credit.</summary>
        Credit, 

        /// <summary>Interest.</summary>
        Interest,

        /// <summary>Equity.</summary>
        Equity,

        /// <summary>Fx.</summary>
        Fx

    }
}
