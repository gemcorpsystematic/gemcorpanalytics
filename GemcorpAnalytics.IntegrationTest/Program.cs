﻿namespace GemcorpAnalytics.IntegrationTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using GemcorpAnalytics.IntegrationTest.TestCase;
    using GemCorpAnalytics.Data.Interfaces;
    using GemcorpAnalytics.IntegrationTest.TestCaseContext;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.DataAccess.Factories;
    using GemCorpAnalytics.Enums;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.Calculators;
    using GemCorpAnalytics.OrchestradeLayer;
    using System.Data;
    using System.Xml;
    using System.Runtime.Serialization;
    using GemCorpAnalytics.DataCollections;
    using System.IO;
    using System.Globalization;
    using GemCorpAnalytics.DataAccess.ContextBuilders;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataAccess.Utils;

    class Program
    {
        static void Main(string[] args)
        {

            //var file = JObject.Parse(File.ReadAllText("C:\\GemcorpSystematic\\GemCorpAnalytics\\ProjectedRiskThemeMappings.json"));
            //var test = file["FX:EUR:USD"].Value<string>();
            //StressTest();
            //BetaTest();
            OrchestradeConnectivityTest();
            //GetOrchestradeForwardCurves();
            //SummaryByThemeEmailTest();
            //ProjectedRiskTest();
            //RiskSensitivityTest();
            //BuildRiskSensitivityMapping();
            //TradePopulationTest();
            //StressTest();
            //GetNavTest();
        }

        private static void OrchestradeConnectivityTest()
        {
            //var userName = "OPTIONS-IT\\aspwebsrv_gmc";
            var userName = "orchestra";
            var env = "msprod";
            //var pwd = "Uj5Ae7ctn2";
            var pwd = "orchestra";

            var otBlotter = new OtBlotterPl(userName, env, pwd);
            otBlotter.ConnnectToOrchestrade();
            
            var reportType = "PlBlotterPanel";
            var configName = "PL by Themes";
            var varConfigName = "Default";
            var filterName = "CVaR Global Strategy ex TSHARES";
            var varConfig = "1D95";
            var consoRiskConfig = "ConsoRisk";
            var consoRiskFilter = "Global Strategy - Fund I";
            //var otOutput = otBlotter.RunReport(new DateTime(2023, 9, 29, 23, 59, 59), reportType, configName);
            //var vaROutput = otBlotter.RunVaRReport(new DateTime(2023, 9, 29, 23, 59, 59), varConfigName, filterName, varConfig);
            //var complianceLimitName = "Sector Exposure - GCF1 - Net"; // Single Issuer Non-sovereign - Private

            
            var tmpValueDates = new List<DateTime>();
            var holidays = new[] { new DateTime(2020, 5, 25, 23, 59, 59), new DateTime(2020, 6, 19, 23, 59, 59), new DateTime(2020, 7, 3, 23, 59, 59),
                new DateTime(2020,9,7, 23,59,59),new DateTime(2020,11,11, 23,59,59), new DateTime(2020,11,26, 23,59,59), new DateTime(2020,12,25, 23,59,59),
            new DateTime(2021,12,25, 23,59,59), new DateTime(2022,12,25, 23,59,59), new DateTime(2022,1,1, 23,59,59)};
            var tmpDate = new DateTime(2023,9, 29, 23, 59, 59);
            var endDate = new DateTime(2023,10,31, 23, 59, 59);

            while (tmpDate <= endDate)
            {
                //tmpValueDates.Add(tmpDate);
                //tmpDate = tmpDate.AddMonths(1);
                //var daysInMonth = DateTime.DaysInMonth(tmpDate.Year, tmpDate.Month);
                //tmpDate = tmpDate.AddDays(daysInMonth - tmpDate.Day);
                //if (tmpDate.DayOfWeek == DayOfWeek.Saturday)
                //{
                //    tmpDate = tmpDate.AddDays(-1);
                //}
                //if (tmpDate.DayOfWeek == DayOfWeek.Sunday)
                //{
                //    tmpDate = tmpDate.AddDays(-2);
                //}
                if (tmpDate.DayOfWeek == DayOfWeek.Saturday || tmpDate.DayOfWeek == DayOfWeek.Sunday || holidays.Contains(tmpDate))
                {
                    tmpDate = tmpDate.AddDays(1);
                    continue;
                }
                tmpValueDates.Add(tmpDate);
                tmpDate = tmpDate.AddDays(1);

            }

            //var complianceLimitName = "Country Of Risk - GCF1 - Net - PrivateCredit"; // Single Issuer Non-sovereign - Private
            var complianceLimitName = "Net Exposure - PrivateCredit Fund I - PrivateCredit"; // Single Issuer Non-sovereign - Private

            //using (var complianceLimitFile = new StreamWriter("C:\\temp\\NetExposure_PrivateCredit_v2.txt")) {
                foreach (var limitDate in tmpValueDates)
                {
                    Console.WriteLine($"Working: {limitDate:yyyy-MM-dd}");
                    Console.WriteLine($"StartTime: {DateTime.Now:yyyy-MM-dd hh:mm:ss}");
                    //var complianceLimits = otBlotter.RunComplianceLimits(new DateTime(2015, 1, 30, 23, 59, 59), new DateTime(2015, 1, 30, 23, 59, 59), complianceLimitName);
                    //var complianceLimits = otBlotter.RunComplianceLimits(limitDate, limitDate, complianceLimitName);
                    //var relevantComplianceLimits = complianceLimits.Where(c => Math.Abs(c.Output) > 1e-6).ToList();

                    //foreach (var relevantComplianceLimit in relevantComplianceLimits)
                    //{
                    //    complianceLimitFile.WriteLine($"{limitDate:yyyy/M/d},{relevantComplianceLimit.CountryOfRisk},{relevantComplianceLimit.Output}");
                    //}
                    var vaROutput = otBlotter.RunVaRReport(limitDate, varConfigName, filterName, varConfig);
                }
            //}

            var consoRiskResults = new Dictionary<DateTime, IConsoRiskReportSummary>();

            var valueDates = new[] {new DateTime(2020,2,28,23,59,59), new DateTime(2020, 3, 31, 23, 59, 59) , new DateTime(2020, 4, 30, 23, 59, 59),
            new DateTime(2020,5,29,23,59,59), new DateTime(2020, 6, 30, 23, 59, 59) , new DateTime(2020, 7, 31, 23, 59, 59) , new DateTime(2020, 8, 28, 23, 59, 59),
                new DateTime(2020, 9, 30, 23, 59, 59), new DateTime(2020,10,30,23,59,59), new DateTime(2020, 11, 30, 23, 59, 59) , new DateTime(2020, 12, 31, 23, 59, 59)};

            //var valueDates = new[] { new DateTime(2020, 2, 28, 23, 59, 59) };

            //var startDate = new DateTime(2020, 2, 1, 23, 59, 59);
            //var endDate = new DateTime(2020, 12, 31, 23, 59, 59);
            //var tmpValueDates = new List<DateTime> { new DateTime(2023,8,31,23,59,59)};
            /*
            var tmpValueDates = new List<DateTime>();
            var holidays = new[] { new DateTime(2020, 5, 25, 23, 59, 59), new DateTime(2020, 6, 19, 23, 59, 59), new DateTime(2020, 7, 3, 23, 59, 59),
                new DateTime(2020,9,7, 23,59,59),new DateTime(2020,11,11, 23,59,59), new DateTime(2020,11,26, 23,59,59), new DateTime(2020,12,25, 23,59,59)};
            var tmpDate = startDate;

            while (tmpDate <= endDate)
            {
                if (tmpDate.DayOfWeek == DayOfWeek.Saturday || tmpDate.DayOfWeek == DayOfWeek.Sunday || holidays.Contains(tmpDate))
                {
                    tmpDate = tmpDate.AddDays(1);
                    continue;
                }
                tmpValueDates.Add(tmpDate);
                tmpDate = tmpDate.AddDays(1);
            }
            */
            var books = new List<string> { "Angola Hedge", ""};
            Func<ITrade, bool> tradeFilter = (t) => t.CountryOfRisk != "United States";
            foreach (var valueDate in tmpValueDates)
            {
                Console.WriteLine($"Running: {valueDate:yyyy-MM-dd}");
                var consoRiskReportResult = otBlotter.RunConsoRiskReport(valueDate, varConfigName, consoRiskFilter, consoRiskConfig);
                var summaryCalculator = new ConsoRiskReportSummaryCalculator();
                var summary = summaryCalculator.Calculate(consoRiskReportResult);
                //var summary = summaryCalculator.CalculateUsingFilter(consoRiskReportResult, books, tradeFilter);
                consoRiskResults[valueDate] = summary;
                File.AppendAllLines("C:\\temp\\ConsoRiskResults_v3.txt", summary.ToString());
                Console.WriteLine($"Finished Running: {valueDate:yyyy-MM-dd}");
            }

            otBlotter.CloseConnection();
        }

        private static void GetOrchestradeForwardCurves()
        {
            var userName = "orchestra";
            var env = "msprod";
            var pwd = "orchestra";

            var otBlotter = new OtBlotterPl(userName, env, pwd);
            otBlotter.ConnnectToOrchestrade();

            var varConfigName = "Default";
            var consoRiskConfig = "ConsoRisk";
            var consoRiskFilter = "Global Strategy - Fund I";

            var curveIds = new List<KeyValuePair<DateTime, int>> { new KeyValuePair<DateTime, int>(new DateTime(2023,7,25), 11051),
                new KeyValuePair<DateTime, int>(new DateTime(2023, 8, 18), 11051) };
            var otCurves = otBlotter.GetCurves(curveIds, varConfigName, consoRiskFilter, consoRiskConfig);

            var fwdCurves = new List<ICurve>();

            var fwdCurveBuilder = new ForwardCurveBuilder();

            var fwdRateTenor = 7.0 / 360.0;
            var tenorCutOff = 10.0;

            var zScore = 2.5;

            foreach(var otCurve in otCurves)
            {
                var filteredCurve = fwdCurveBuilder.RemoveOutliersFromCurve(otCurve, zScore);
                var fwdCurve = fwdCurveBuilder.BuildForwardCurve(otCurve, fwdRateTenor, tenorCutOff);
                fwdCurves.Add(fwdCurve);
            }

        }

        private static void RegexTest()
        {
            var mapString = "%GOLD%";
            var subString = mapString.Trim(new[] { '%' });
            var targetString = "GOLD";
            string pattern = string.Format(@"^{0}$", targetString);
            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            var matches = rgx.Match(subString);
        }

        private static void TradePopulationTest()
        {
            var testResult = new CalculateTradePopulationTestCase().RunTest(null);
        }

        private static void SummaryByThemeEmailTest()
        {
            var calculateSummaryByTheme = new CalculateSummaryByTheme();
            var result = calculateSummaryByTheme.RunTest(null);
        }

        private static void GetNavTest()
        {
            var navRequest = new GetNavRequest(DateTime.Today, "Macro");
            var navRequestHandler = GetNavRequestHandlerFactory.GetHandler("PROD");
            var navCollection = navRequestHandler.Handle(navRequest);
        }

        private static void BetaTest()
        {
            var hedgingVals = new Dictionary<string, string> {
                {"CO1","Commodity"},
                {"SPX","Equity"},
               //{"EMB_US","Equity"},
                {"DXY","Fx"},
                {"FXJPEMCS","Equity"},
                {"SPBDU10T","Equity"},
            };

            var valueDate = new DateTime(2022, 10, 14);
            var minDate = new DateTime(2015, 11, 6);

            var context = new GetBetaTestCaseContext(valueDate, minDate, "PROD", hedgingVals,"Macro");
            var test = new CalculateBetaTestCase();
            var result = test.RunTest(context);
        }

        private static void ProjectedRiskTest()
        {

            //var valueDates = new[] { new DateTime(2021,5,7),
            //    new DateTime(2021,5,10), new DateTime(2021, 5, 11), new DateTime(2021, 5, 12), new DateTime(2021, 5, 13), new DateTime(2021, 5, 14),
            //    new DateTime(2021,5,17), new DateTime(2021,5,18),new DateTime(2021,5,19),new DateTime(2021,5,20),new DateTime(2021,5,21),
            //    new DateTime(2021,5,25),new DateTime(2021,5,26), new DateTime(2021,5,27), new DateTime(2021,5,28),
            //    new DateTime(2021,5,31), new DateTime(2021,6,1)
            //};

            var output = new Dictionary<DateTime, double>();

            var getTimeSeriesRequest = new GetTimeSeriesRequest(new DateTime(1970, 1, 1), DateTime.Today, TimeSeriesType.Daily, TimeSeriesAttributes.Close);
            getTimeSeriesRequest.Names.AddRange(new List<KeyValuePair<string, TimeSeriesAttributes>> {
                new KeyValuePair<string, TimeSeriesAttributes>("BBG00BD31G34", TimeSeriesAttributes.Z_Sprd_Mid),
                new KeyValuePair<string, TimeSeriesAttributes>("BBG0073Y9X68", TimeSeriesAttributes.Z_Sprd_Mid),
                new KeyValuePair<string, TimeSeriesAttributes>("BBG00G41HPF7", TimeSeriesAttributes.Z_Sprd_Mid),
                new KeyValuePair<string, TimeSeriesAttributes>("BBG0000GXSD1", TimeSeriesAttributes.Z_Sprd_Mid),
                new KeyValuePair<string, TimeSeriesAttributes>("CDS_COL_5Y", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("CDS_REPSOU_5Y", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("CDS_TUR_5Y", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("EGH GN", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("EMB_US", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("TIP US", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("SPAC", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("ARVL US", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("EURCZK", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("EURUSD_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("GBPUSD_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDBRL_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDCLP_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDCNH_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDILS_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDMXN_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDZAR_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDJPY_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("CO1", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("CO1", TimeSeriesAttributes.Volatility_30D),
                new KeyValuePair<string, TimeSeriesAttributes>("OD9", TimeSeriesAttributes.Low),
                new KeyValuePair<string, TimeSeriesAttributes>("USGG5YR", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>(".US3Y2Y U", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("NQ1", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("JPSSGGH", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USGG3YR", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USGG2YR", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("SPGSCI", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("JPEIGLBL", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("DXY", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("CDX_EM_5Y", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("LBUTTRUU", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("SPBDU10T", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("EMLCTRUU", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("SPX", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("SPX", TimeSeriesAttributes.Volatility_30D_50Delta),
                new KeyValuePair<string, TimeSeriesAttributes>("TLT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("TLT", TimeSeriesAttributes.Volatility_30D),
                new KeyValuePair<string, TimeSeriesAttributes>("GC1", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("GC1", TimeSeriesAttributes.Volatility_30D),
                new KeyValuePair<string, TimeSeriesAttributes>("VG1", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("VG1", TimeSeriesAttributes.Volatility_30D)
            });

            // config file
            var riskType = new Dictionary<string, string> {
                { "BBG00BD31G34", "spread" },
                { "BBG0073Y9X68", "spread" },
                { "BBG00G41HPF7", "spread" },
                { "BBG0000GXSD1", "spread" },
                { "CDS_COL_5Y", "cds" },
                { "CDS_REPSOU_5Y", "cds" },
                { "CDS_TUR_5Y", "cds" },
                { "EGH GN", "equity" },
                { "EMB_US", "equity" },
                { "TIP US", "equity" },
                { "TLT", "equity" },
                { "SPAC", "equity" },
                { "ARVL US", "equity" },
                { "SPX", "equity"},
                { "VG1", "equity"},
                { "EURCZK", "fx" },
                { "EURUSD_SPOT", "fx" },
                { "GBPUSD_SPOT", "fx" },
                { "USDBRL_SPOT", "fx" },
                { "USDCLP_SPOT", "fx" },
                { "USDCNH_SPOT", "fx" },
                { "USDILS_SPOT", "fx" },
                { "USDMXN_SPOT", "fx" },
                { "USDZAR_SPOT", "fx" },
                { "USDJPY_SPOT", "fx" },
                { "CO1", "oil" },
                { "GC1", "oil" },
                { "CO1 VOL", "vol" },
                { "GC1 VOL", "vol" },
                { "OD9", "rate" },
                { "USGG5YR", "rate" },
                { "US3Y2Y", "rate" },
                { "CDX_EM_5Y","spread"},
                { "LBUTTRUU","spread"},
                { "SPBDU10T","spread"},
                { "EMLCTRUU","spread"},
                { "JPEIGLBL","fx"}
            };

            // risk mapping
            var riskMapping = new Dictionary<string, string> {
                {"LAUCA", "BBG00BD31G34"},
                {"ANG25", "BBG00BD31G34"},
                {"ANG RCF", "BBG00BD31G34"},
                {"GHAN26", "BBG0073Y9X68"},
                {"OMAN32", "BBG00G41HPF7"},
                {"Telconet", "BBG0000GXSD1"},
                {"CO CDS", "CDS_COL_5Y"},
                {"ZA CDS", "CDS_REPSOU_5Y"},
                {"TR CDS", "CDS_TUR_5Y"},
                {"BMA", "EGH GN"},
                {"EMB", "EMB_US"},
                {"TIPS", "TIP US"},
                {"SPAC", "SPAC"},
                {"ARVL", "ARVL US"},
                {"EURCZK", "EURCZK"},
                {"EURUSD", "EURUSD_SPOT"},
                {"GBPUSD", "GBPUSD_SPOT"},
                {"USDBRL", "USDBRL_SPOT"},
                {"USDCLP", "USDCLP_SPOT"},
                {"USDCNH", "USDCNH_SPOT"},
                {"USDILS", "USDILS_SPOT"},
                {"USDMXN", "USDMXN_SPOT"},
                {"USDZAR", "USDZAR_SPOT"},
                {"CO1", "CO1"},
                {"CO1 VOL", "CO1"},
                {"BR RATE", "OD9"},
                {"US RATE", "USGG5YR"},
                {"US3Y2Y", "US3Y2Y"},
                { "SPX","SPX"},
                { "SPX VOL","SPX"},
                { "TLT","TLT"},
                { "TLT VOL","TLT"},
                { "VG1","VG1"},
                { "VG1 VOL","VG1"},
            };

            //var riskMapping = new Dictionary<string, string> {
            //    {"JPEIGLBL", "JPEIGLBL"}
            //};

            // risk values
            var riskValues = new Dictionary<string, double> {
                {"LAUCA", -23390.947},
                {"ANG25", 10447.138},
                {"ANG RCF", -8293.291},
                {"GHAN26", -581.704},
                {"OMAN32", 7004.383},
                {"Telconet", -936.258},
                {"CO CDS", -9092.629},
                {"ZA CDS", 17248.907},
                {"TR CDS", 7593.418},
                {"BMA", 3711583.068},
                {"EMB", 17188156.968},
                {"TIPS", -15702400.142},
                {"SPAC", 27251933.657},
                {"ARVL", 4748882.628},
                {"EURCZK", -20367198.903},
                {"EURUSD", 221346.715},
                {"USDBRL", 1216192.627},
                {"USDCLP", -327158.446},
                {"USDCNH", -7581972.622},
                {"USDILS", -7550699.417},
                {"USDMXN", -10576584.751},
                {"USDZAR", 6232585.875},
                {"CO1", -782.415},
                {"CO1 VOL", 343.055},
                {"BR RATE", -11296.089},
                {"US RATE", -62440.190},
                {"US3Y2Y", 51372.000}
            };

            //var riskValues = new Dictionary<string, double>
            //{
            //    { "JPEIGLBL",1.0}
            //};

            var environment = "UAT";
            //var projectionSeries = "SPX";
            var projectionSeries = "CO1";
            var seriesToProxy = new[] { 
                new TimeSeriesRepositoryEntry("SPAC",TimeSeriesAttributes.Close),
                new TimeSeriesRepositoryEntry("ARVL US",TimeSeriesAttributes.Close),
                new TimeSeriesRepositoryEntry("BBG00G41HPF7",TimeSeriesAttributes.Z_Sprd_Mid)
            };
            var seriesToExclude = new[] {
                new TimeSeriesRepositoryEntry("NQ1",TimeSeriesAttributes.Close),
                new TimeSeriesRepositoryEntry("JPSSGGH",TimeSeriesAttributes.Close),
                new TimeSeriesRepositoryEntry("USGG3YR",TimeSeriesAttributes.Close),
                new TimeSeriesRepositoryEntry("USGG2YR",TimeSeriesAttributes.Close),
                new TimeSeriesRepositoryEntry("SPGSCI",TimeSeriesAttributes.Close),
                new TimeSeriesRepositoryEntry("JPEIGLBL",TimeSeriesAttributes.Close),
                new TimeSeriesRepositoryEntry("DXY",TimeSeriesAttributes.Close) 
            };

            var seriesMapping = new Dictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, double>> {
                    { new TimeSeriesRepositoryEntry("SPAC", TimeSeriesAttributes.Close), new Tuple<ITimeSeriesRepositoryEntry, double>(new TimeSeriesRepositoryEntry("NQ1", TimeSeriesAttributes.Close), 0.5)},
                    { new TimeSeriesRepositoryEntry("ARVL US", TimeSeriesAttributes.Close), new Tuple<ITimeSeriesRepositoryEntry, double>(new TimeSeriesRepositoryEntry("NQ1", TimeSeriesAttributes.Close), 0.75)},
                    { new TimeSeriesRepositoryEntry("BBG00G41HPF7", TimeSeriesAttributes.Z_Sprd_Mid), new Tuple<ITimeSeriesRepositoryEntry, double>(new TimeSeriesRepositoryEntry("NQ1", TimeSeriesAttributes.Close), 0.5)},
                };

            var seriesComposition = new Dictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, ITimeSeriesRepositoryEntry>> {
                    { new TimeSeriesRepositoryEntry("US3Y2Y", TimeSeriesAttributes.Close), new Tuple<ITimeSeriesRepositoryEntry, ITimeSeriesRepositoryEntry>(new TimeSeriesRepositoryEntry("USGG3YR",TimeSeriesAttributes.Close),new TimeSeriesRepositoryEntry("USGG2YR",TimeSeriesAttributes.Close))}
                };

            var timeSeriesRequestHandler = TimeSeriesRequestHandlerFactory.GetHandler(environment, TimeSeriesType.Daily);
            var timeSeriesRepository = timeSeriesRequestHandler.Handle(getTimeSeriesRequest);

            var navRequestHandler = GetNavRequestHandlerFactory.GetHandler(environment);
            var navCollection = navRequestHandler.Handle(new GetNavRequest(DateTime.Today, "Macro"));

            var d = new DirectoryInfo("C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\Fund");
            var files = d.GetFiles("*.xml");

            var multiFilesFolder = new DirectoryInfo("C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\Multi");
            var multiFiles = multiFilesFolder.GetFiles("*.xml");

            var macroFiles = files.Zip(multiFiles, (a, b) => new Tuple<FileInfo, FileInfo>(a, b));

            Func<string, PortfolioRiskSensitivityCollection> fileReader = (f) =>
            {
                var fs = new FileStream(f, FileMode.Open);
                var reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
                var serialiser = new DataContractSerializer(typeof(PortfolioRiskSensitivityCollection));
                var riskSensitivityCollection = (PortfolioRiskSensitivityCollection)serialiser.ReadObject(reader, true);
                reader.Close();
                fs.Close();
                return riskSensitivityCollection;
            };

            var themeContextBuilder = PlByThemeContextBuilder.Build(DateTime.Now, "prod");
            var macroBooks = themeContextBuilder.FundConfigs[FundNames.Macro][FundStrategies.Macro];

            var tmpFiles = new[] { new Tuple<string, string>("PortfolioRiskSensitivityCollection_20210910.xml", "PortfolioRiskSensitivityCollection_Gemcorp Multi Strategy Master Fund SICAV SCS_20210910.xml") };

            foreach (var file in tmpFiles)
            {
                var fileName = $"C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\Fund\\{file.Item1}";

                var tmp = file.ToString().Split('.')[0].Split('_')[1];

                var valueDate = DateTime.ParseExact(tmp, "yyyyMMdd", CultureInfo.InvariantCulture);

                var multiSensitivitiesFileName = $"C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\Multi\\{file.Item2}";

                var riskSensitivityTable = fileReader(fileName);
                var multiSensitivities = fileReader(multiSensitivitiesFileName);

                riskSensitivityTable.AddRange(multiSensitivities);

                var macroRiskSensitivityTable = riskSensitivityTable.Where(r => macroBooks.Contains(r.Book));

                var macroRiskSensitivityCollection = new PortfolioRiskSensitivityCollection();
                macroRiskSensitivityCollection.AddRange(macroRiskSensitivityTable);

                var riskSensitivityTimeSeriesMapper = new RiskSensitivityTimeSeriesMapper();

                var portfolioRiskCalculator = new PortfolioRiskFactorCalculator(macroRiskSensitivityCollection, riskSensitivityTimeSeriesMapper);
                var sensitivities = portfolioRiskCalculator.Calculate();

                var riskDirectionCollection = sensitivities.RiskDirectionCollection.Select(x => new GemCorpAnalytics.Data.RiskDirection.Builder
                {
                    AssetClass = x.AssetClass,
                    Name = x.Name,
                    RiskAmount = x.RiskAmount,
                    RiskGreekDirection = x.RiskGreekDirection,
                    TimeSeriesMapping = TimeSeriesMapping(x)
                }.Build());

                //using (var tmpFile = new StreamWriter($"C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\Diagnostics\\Macro_RiskSensitivities_{tmp}.txt"))
                //{
                //    foreach (var riskDirection in riskDirectionCollection)
                //    {
                //        var outString = $"{riskDirection.AssetClass},{riskDirection.Name},{riskDirection.RiskGreekDirection},{riskDirection.RiskAmount},{riskDirection.TimeSeriesMapping}";
                //        tmpFile.WriteLine(outString);
                //    }
                //}

                var hedgingSeries = new Dictionary<ITimeSeriesRepositoryEntry, AssetClass>
                {
                    {new TimeSeriesRepositoryEntry("CO1",TimeSeriesAttributes.Close),AssetClass.Commodity},
                };

                var projectedRiskContext = new ProjectedRiskContext(environment, hedgingSeries, valueDate, seriesToProxy, 
                    seriesToExclude, seriesMapping, seriesComposition,
                    riskType, riskMapping, riskValues, timeSeriesRepository, riskDirectionCollection, navCollection,"GEMCORP FUND 1 LIMITED");

                var minVarHedgeBetaCalculator = new MinVarHedgeBetaCalculator(projectedRiskContext, null, null);
                var result = minVarHedgeBetaCalculator.Calculate(valueDate, DateTime.MinValue, null, null);

                var riskAllocationCalculator = new RiskAllocationCalculator(new ProjectedRiskThemeMapper());
                var riskAllocation = riskAllocationCalculator.Calculate(result);

                output[valueDate] = result.Beta[projectionSeries];

            }


            using (var outFile = new StreamWriter($"C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\Diagnostics\\Macro_FundBetas_{projectionSeries}_ByNAV.txt"))
            {
                foreach (var result in output)
                {
                    outFile.WriteLine($"{result.Key:dd/MM/yyyy},{result.Value}");
                }
            }
            //var getTimeSeriesTestCase = new GetTimeSeriesTestCase();
            //var getTimeSeriesTestCaseContext = new GetTimeSeriesTestCaseContext(getTimeSeriesRequest, riskType, riskMapping, riskValues);
            //var getTimeSeriesTestCaseContext = new GetTimeSeriesTestCaseContext(projectedRiskContext);

            //var getTimeSeriesTestCaseResult = getTimeSeriesTestCase.RunTest(getTimeSeriesTestCaseContext);
            //Console.WriteLine($"Get time series test case: {getTimeSeriesTestCaseResult.Success}");
        }


        public static void StressTest()
        {

            // GetStressTestRequest followed by handler
            // GetScenarioLadderRequest followed by handler

            var valueDate = new DateTime(2022, 12, 30);
            var environment = "PROD";
            var stressTestRequest = new GetStressTestRequest(valueDate);
            var stressTestRequestHandler = StressTestRequestHandlerFactory.GetHandler(environment);
            var stressTestCollection = stressTestRequestHandler.Handle(stressTestRequest);

            var scenarioLadderRequest = new GetScenarioLadderRequest(new[] { valueDate }, new[] { "GEMCORP FUND I LIMITED", "GEMCORP FUND I LP" });
            var scenarioLadderRequestHandler = ScenarioLadderRequestHandlerFactory.GetHandler(environment);
            var scenarioLadderCollection = scenarioLadderRequestHandler.Handle(scenarioLadderRequest);

            CollectionSerializer<ScenarioLadderCollection>("C:\\temp\\ScenarioLadderCollection_20221230.xml", scenarioLadderCollection);

            var riskDirectionCollection = GetRiskDirectionCollectionFromDb(valueDate, environment); // get risk directions from database and not read from file.

            CollectionSerializer<IEnumerable<RiskDirection>>("C:\\temp\\RiskDirectionCollection_20221230.xml", riskDirectionCollection);

            //var scenarioLadderCollection = CollectionDeserializer<ScenarioLadderCollection>("C:\\temp\\ScenarioLadderCollection_v2.xml");
            //var riskDirectionCollection = CollectionDeserializer<IEnumerable<RiskDirection>>("C:\\temp\\RiskDirectionCollection_v2.xml");

            //var stressTestCollection = CollectionDeserializer<StressTestCollection>("C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\StressTestCollection_20210910.xml");
            //var scenarioLadderCollection = CollectionDeserializer<ScenarioLadderCollection>("C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\ScenarioLadderCollection_20210910.xml");

            var timeSeriesRequests = Utils.TimeSeriesRequestBuilder(riskDirectionCollection, DateTime.MinValue, valueDate,
                TimeSeriesType.Daily, TimeSeriesAttributes.Close);

            var timeSeriesRequestHandler = TimeSeriesRequestHandlerFactory.GetHandler(environment, TimeSeriesType.Daily);
            var timeSeries = timeSeriesRequestHandler.Handle(timeSeriesRequests);

            var timeSeriesRepository = new TimeSeriesRepository(timeSeries);

            //var timeSeriesRepository = new TimeSeriesRepository(GetTimeSeries(environment));
            // risk direction, time series and value date

            var context = new StressTestCalculatorContext(valueDate, scenarioLadderCollection, stressTestCollection, riskDirectionCollection, timeSeriesRepository);
            var stressTestCalculator = new StressTestCalculator(new GemCorpAnalytics.Interpolators.LinearInterpolator());
            var stressTestCalculatorResult = stressTestCalculator.Calculate(context);

            //var scenarios = stressTestCalculatorResult.Select(r => r.ScenarioName).Distinct();

            using(var outFile = new StreamWriter($"C:\\temp\\ScenarioDetails_fund_{valueDate:yyyyMMdd}.txt"))
            {
                foreach (var scen in stressTestCalculatorResult)
                {
                    outFile.WriteLine($"{scen.ScenarioName},{scen.StartDate:dd/MM/yyyy},{scen.EndDate:dd/MM/yyyy},{scen.UnderlierName},{scen.StartValue},{scen.EndValue},{scen.Shock},{scen.PlAmount},{scen.RiskDirectionAmount},{scen.AssetClass}");
                    //Console.WriteLine($"{scen},{stressTestCalculatorResult.Where(d => d.ScenarioName == scen).Sum(p => p.PlAmount)}");
                }
            }

        }

        public static void RiskSensitivityTest()
        {
            var environment = "UAT";
            var date = new DateTime(2022, 3, 24);
            var getRiskSensitivityCollection = new GetRiskSensitivityCollection();
            getRiskSensitivityCollection.AddRange(new[] {
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED","None","Forex"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED","None","Credit"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED","None","Equity"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED","None","Interest"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED","None","Commodity"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED","None","Commodity Volatility"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED","None","Equity Volatility")
            });

            var context = new GetRiskSensitivityTestCaseContext(environment, getRiskSensitivityCollection);
            var testCase = new GetRiskSensitivityTestCase();
            var testCaseResult = testCase.RunTest(context);

        }

        public static void BuildRiskSensitivityMapping()
        {
            /*
             * Get risk sensitivity table
             * For FX and 
             * this needs to return a dictionary{string, double}
             */

            var tmpDate = new DateTime(2021, 9, 9);
            var endDate = new DateTime(2021, 9, 10);
            var tmpDates = new List<DateTime>();

            while (tmpDate <= endDate)
            {
                var dayDelta = 1;

                if (tmpDate.DayOfWeek == DayOfWeek.Friday)
                    dayDelta = 3;

                if (tmpDate.DayOfWeek == DayOfWeek.Saturday)
                    dayDelta = 2;

                tmpDate = tmpDate.AddDays(dayDelta);

                tmpDates.Add(tmpDate);
            };


            foreach (var date in tmpDates)
            {

                Console.WriteLine($"{DateTime.Now}, Working: {date:yyy-MM-dd}");
                //var valueDate = date;
                //var entity = "GEMCORP FUND I LIMITED";
                //var entity = "Gemcorp Multi Strategy Master Fund SICAV SCS";
                //var riskType = "None";
                //var getRiskSensitivityCollection = new GetRiskSensitivityCollection();
                //getRiskSensitivityCollection.AddRange(new[] {
                //new GetRiskSensitivityRequest(valueDate, "", entity, riskType, "Forex"),
                //new GetRiskSensitivityRequest(valueDate, "", entity, riskType, "Credit"),
                //new GetRiskSensitivityRequest(valueDate, "", entity, riskType, "Equity"),
                //new GetRiskSensitivityRequest(valueDate, "", entity, riskType, "Interest"),
                //new GetRiskSensitivityRequest(valueDate, "", entity, riskType, "Commodity"),
                //new GetRiskSensitivityRequest(valueDate, "", entity, riskType, "Commodity Volatility"),
                //new GetRiskSensitivityRequest(valueDate, "", entity, riskType, "Equity Volatility")
                //});

                var request = new GetStressTestRequest(date);

                //var fileName = $"C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\Multi v2\\PortfolioRiskSensitivityCollection_{entity.Trim()}_{date:yyyyMMdd}.xml";
                var fileName = $"C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\StressTestCollection_{date:yyyyMMdd}.xml";

                var getRiskSensitivityCollectionHandler = StressTestRequestHandlerFactory.GetHandler("UAT");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine($"{DateTime.Now}, Retrieving data");
                var riskSensitivityTable = getRiskSensitivityCollectionHandler.Handle(request);
                Console.WriteLine($"{DateTime.Now}, Data successfully retrieved");
                Console.ResetColor();
                var filePath = fileName;
                var xmlWriter = XmlWriter.Create(filePath);
                var dataContractSerializer = new DataContractSerializer(typeof(StressTestCollection));
                dataContractSerializer.WriteObject(xmlWriter, riskSensitivityTable);
                xmlWriter.Close();
                Console.WriteLine($"{DateTime.Now}, Finished working: {date:yyyy-MM-dd}");
            };

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();

        }

        public static T CollectionDeserializer<T>(string fileName)
        {
            var fs = new FileStream(fileName, FileMode.Open);
            var reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            var serialiser = new DataContractSerializer(typeof(T));
            var riskSensitivityCollection = (T)serialiser.ReadObject(reader, true);
            reader.Close();
            fs.Close();
            return riskSensitivityCollection;
        }

        public static bool CollectionSerializer<T>(string fileName, T collection)
        {
            var xmlWriter = XmlWriter.Create(fileName);
            var dataContractSerializer = new DataContractSerializer(typeof(T));
            dataContractSerializer.WriteObject(xmlWriter, collection);
            xmlWriter.Close();
            return true;
        }

        public static IEnumerable<RiskDirection> GetRiskDirectionCollection()
        {

            var themeContextBuilder = PlByThemeContextBuilder.Build(DateTime.Now, "prod");
            var macroBooks = themeContextBuilder.FundConfigs[FundNames.Macro][FundStrategies.Macro];

            var fileName = $"C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\Fund\\PortfolioRiskSensitivityCollection_20210910.xml";
            var tmp = "20210910";
            var valueDate = DateTime.ParseExact(tmp, "yyyyMMdd", CultureInfo.InvariantCulture);

            var multiSensitivitiesFileName = $"C:\\Gemcorp\\Fund Risk Calculation\\ConsoRiskData\\Multi\\PortfolioRiskSensitivityCollection_Gemcorp Multi Strategy Master Fund SICAV SCS_20210910.xml";

            var riskSensitivityTable = CollectionDeserializer<PortfolioRiskSensitivityCollection>(fileName);
            var multiSensitivities = CollectionDeserializer<PortfolioRiskSensitivityCollection>(multiSensitivitiesFileName);

            riskSensitivityTable.AddRange(multiSensitivities);

            var macroRiskSensitivityTable = riskSensitivityTable.Where(r => macroBooks.Contains(r.Book));

            var macroRiskSensitivityCollection = new PortfolioRiskSensitivityCollection();
            macroRiskSensitivityCollection.AddRange(macroRiskSensitivityTable);

            var riskSensitivityTimeSeriesMapper = new RiskSensitivityTimeSeriesMapper();

            var portfolioRiskCalculator = new PortfolioRiskFactorCalculator(macroRiskSensitivityCollection, riskSensitivityTimeSeriesMapper);
            var sensitivities = portfolioRiskCalculator.Calculate();

            var riskDirectionCollection = sensitivities.RiskDirectionCollection.Select(x => new GemCorpAnalytics.Data.RiskDirection.Builder
            {
                AssetClass = x.AssetClass,
                Name = x.Name,
                RiskAmount = x.RiskAmount,
                RiskGreekDirection = x.RiskGreekDirection,
                TimeSeriesMapping = TimeSeriesMapping(x)
            }.Build());

            return riskDirectionCollection;

        }

        // <summary>Gets the risk direction collection.</summary>
        /// <param name="date">The date.</param>
        /// <param name="environment">The environment.</param>
        /// <returns>The <see cref="RiskDirection"/>.</returns>
        private static IEnumerable<RiskDirection> GetRiskDirectionCollectionFromDb(DateTime date, string environment)
        {
            var themeContextBuilder = PlByThemeContextBuilder.Build(date, environment);
            var macroBooks = themeContextBuilder.FundConfigs[FundNames.Macro][FundStrategies.Macro];

            var riskType = "None";

            // This is a bit of a hack for macro

            var getRiskSensitivityCollection = new GetRiskSensitivityCollection();
            getRiskSensitivityCollection.AddRange(new[] {
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Forex"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Credit"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Equity"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Interest"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Commodity"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Commodity Volatility"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Equity Volatility"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LP", riskType, "Forex"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LP", riskType, "Credit"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LP", riskType, "Equity"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LP", riskType, "Interest"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LP", riskType, "Commodity"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LP", riskType, "Commodity Volatility"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LP", riskType, "Equity Volatility"),
                //new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Forex"),
                //new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Credit"),
                //new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Equity"),
                //new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Interest"),
                //new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Commodity"),
                //new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Commodity Volatility"),
                //new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Equity Volatility"),
            });

            var getRiskSensitivityCollectionHandler = RiskSensitivityRequestHandlerFactory.GetHandler(environment);
            var riskSensitivityCollection = getRiskSensitivityCollectionHandler.Handle(getRiskSensitivityCollection);

            //var macroRiskSensitivityCollection = riskSensitivityCollection.Where(r => macroBooks.Contains(r.Book));

            var riskSensitivityTimeSeriesMapper = new RiskSensitivityTimeSeriesMapper();

            var portfolioRiskSensitivityCollection = new PortfolioRiskSensitivityCollection();
            //portfolioRiskSensitivityCollection.AddRange(macroRiskSensitivityCollection);
            portfolioRiskSensitivityCollection.AddRange(riskSensitivityCollection);

            var portfolioRiskCalculator = new PortfolioRiskFactorCalculator(portfolioRiskSensitivityCollection, riskSensitivityTimeSeriesMapper);
            var sensitivities = portfolioRiskCalculator.Calculate();

            var riskDirectionCollection = sensitivities.RiskDirectionCollection.Select(x => {
                return new RiskDirection.Builder
                {
                    AssetClass = x.AssetClass,
                    Name = x.Name,
                    RiskAmount = x.RiskAmount,
                    RiskGreekDirection = x.RiskGreekDirection,
                    TimeSeriesMapping = Utils.TimeSeriesMapping(x.AssetClass, x.Name, x.TimeSeriesMapping)
                }.Build();
            });

            return riskDirectionCollection;
        }


        public static IReadOnlyDictionary<ITimeSeriesRepositoryEntry, TimeSeries> GetTimeSeries(string environment="UAT")
        {

            var getTimeSeriesRequest = new GetTimeSeriesRequest(new DateTime(1970, 1, 1), DateTime.Today, TimeSeriesType.Daily, TimeSeriesAttributes.Close);
            getTimeSeriesRequest.Names.AddRange(new List<KeyValuePair<string, TimeSeriesAttributes>> {
                new KeyValuePair<string, TimeSeriesAttributes>("BBG00BD31G34", TimeSeriesAttributes.Z_Sprd_Mid),
                new KeyValuePair<string, TimeSeriesAttributes>("BBG0073Y9X68", TimeSeriesAttributes.Z_Sprd_Mid),
                new KeyValuePair<string, TimeSeriesAttributes>("BBG00G41HPF7", TimeSeriesAttributes.Z_Sprd_Mid),
                new KeyValuePair<string, TimeSeriesAttributes>("BBG0000GXSD1", TimeSeriesAttributes.Z_Sprd_Mid),
                new KeyValuePair<string, TimeSeriesAttributes>("CDS_COL_5Y", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("CDS_REPSOU_5Y", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("CDS_TUR_5Y", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("EGH GN", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("EMB_US", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("TIP_US", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("TIP_US", TimeSeriesAttributes.Volatility_30D),
                new KeyValuePair<string, TimeSeriesAttributes>("SPAC", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("ARVL US", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("EURCZK", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("EURUSD_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("GBPUSD_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDBRL_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDCLP_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDCNH_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDILS_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDMXN_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDZAR_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USDJPY_SPOT", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("CO1", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("CO1", TimeSeriesAttributes.Volatility_30D),
                new KeyValuePair<string, TimeSeriesAttributes>("OD9", TimeSeriesAttributes.Low),
                new KeyValuePair<string, TimeSeriesAttributes>("USGG5YR", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>(".US3Y2Y U", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("NQ1", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("JPSSGGH", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USGG3YR", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("USGG2YR", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("SPGSCI", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("JPEIGLBL", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("DXY", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("CDX_EM_5Y", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("LBUTTRUU", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("SPBDU10T", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("EMLCTRUU", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("SPX", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("SPX", TimeSeriesAttributes.Volatility_30D),
                new KeyValuePair<string, TimeSeriesAttributes>("TLT_US", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("TLT", TimeSeriesAttributes.Volatility_30D),
                new KeyValuePair<string, TimeSeriesAttributes>("GC1", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("GC1", TimeSeriesAttributes.Volatility_30D),
                new KeyValuePair<string, TimeSeriesAttributes>("VG1", TimeSeriesAttributes.Close),
                new KeyValuePair<string, TimeSeriesAttributes>("VG1", TimeSeriesAttributes.Volatility_30D)
            });

            var timeSeriesRequestHandler = TimeSeriesRequestHandlerFactory.GetHandler(environment, TimeSeriesType.Daily);
            var timeSeriesRepository = timeSeriesRequestHandler.Handle(getTimeSeriesRequest);

            return timeSeriesRepository;
        }

        public static string TimeSeriesMapping(RiskDirection riskDirection)
        { 
            switch(riskDirection.AssetClass)
            {
                case AssetClass.Fx:
                    return riskDirection.TimeSeriesMapping=="EURCZK" ? riskDirection.TimeSeriesMapping : $"{riskDirection.TimeSeriesMapping}_SPOT";
                case AssetClass.Equity:

                    var equityTimeSeriesMapping = riskDirection.TimeSeriesMapping;

                    if (riskDirection.Name.ToLower().Contains("emb"))
                        return "EMB_US";

                    if (riskDirection.Name.ToLower().Contains("bond etf"))
                        return "TLT";

                    if (riskDirection.Name.ToLower().Contains("euro stoxx"))
                        return "VG1";

                    return equityTimeSeriesMapping;
                case AssetClass.Credit:
                    var mapping = riskDirection.TimeSeriesMapping;
                    if (riskDirection.Name.ToLower() == "colombia")
                        mapping = "CDS_COL_5Y";
                    if (riskDirection.Name.ToLower() == "turkey")
                        mapping = "CDS_TUR_5Y";
                    if (riskDirection.Name.ToLower().Contains("south africa"))
                        mapping = "CDS_REPSOU_5Y";
                    return mapping;
                case AssetClass.Interest:
                    var interestMapping = riskDirection.TimeSeriesMapping;

                    if (riskDirection.Name.ToLower().Contains("brl"))
                        interestMapping = "OD9";

                    if (riskDirection.Name.ToLower().Contains("treasury"))
                        interestMapping = "USGG5YR";

                    return interestMapping;

                case AssetClass.Commodity:
                    var commodityInterestMapping = riskDirection.TimeSeriesMapping;

                    if (riskDirection.Name.ToLower().Contains("gold"))
                        commodityInterestMapping = "GC1";

                    return commodityInterestMapping;

                default:
                    return riskDirection.TimeSeriesMapping;
            }
        }

    }
}
