﻿namespace GemcorpAnalytics.IntegrationTest.TestCase
{
    using GemcorpAnalytics.IntegrationTest.TestCaseContext;
    using GemcorpAnalytics.IntegrationTest.TestCaseResults;
    using System.Threading.Tasks;

    /// <summary>The test case.</summary>
    public interface ITestCase
    {
        /// <summary>Runs the test.</summary>
        /// <param name="testContext">The test context.</param>
        /// <returns>The test case results.</returns>
        Task<ITestCaseResults> RunTest(ITestCaseContext testContext);

    }
}
