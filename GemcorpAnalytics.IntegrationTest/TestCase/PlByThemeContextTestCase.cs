﻿namespace GemcorpAnalytics.IntegrationTest.TestCase
{
    using GemcorpAnalytics.IntegrationTest.TestCaseContext;
    using GemcorpAnalytics.IntegrationTest.TestCaseResults;
    using GemCorpAnalytics.DataAccess.ContextBuilders;
    using System;
    using System.Threading.Tasks;

    public class PlByThemeContextTestCase : ITestCase
    {
        public async Task<ITestCaseResults> RunTest(ITestCaseContext testContext)
        {
            var context = testContext as PlByThemeContextTestCaseContext;
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            var contextOutput = PlByThemeContextBuilder.Build(context.AsOfDate, context.Environment);
            throw new NotImplementedException();
        }
    }
}
