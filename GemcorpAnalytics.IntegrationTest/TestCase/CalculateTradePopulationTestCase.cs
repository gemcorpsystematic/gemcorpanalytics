﻿namespace GemcorpAnalytics.IntegrationTest.TestCase
{
    using GemcorpAnalytics.IntegrationTest.TestCaseContext;
    using GemcorpAnalytics.IntegrationTest.TestCaseResults;
    using GemCorpAnalytics.DataAccess.Factories;
    using GemCorpAnalytics.DataAccess.Requests;
    using System;
    using System.Threading.Tasks;

    /// <summary>Calculate the trade population test case.</summary>
    public class CalculateTradePopulationTestCase : ITestCase
    {
        /// <summary>Run the test.</summary>
        /// <param name="testContext">The test case context.</param>
        /// <returns></returns>
        public async Task<ITestCaseResults> RunTest(ITestCaseContext testContext)
        {
            var tradeCollectionRequestHandler = GetTradeCollectionRequestFactory.GetHandler("UAT");
            var tradePopulationRequest = new GetTradePopulationRequest(DateTime.Today);
            var tradeCollection = tradeCollectionRequestHandler.Handle(tradePopulationRequest);
            throw new NotImplementedException();
        }
    }
}
