﻿namespace GemcorpAnalytics.IntegrationTest.TestCase
{
    using GemcorpAnalytics.IntegrationTest.TestCaseContext;
    using GemcorpAnalytics.IntegrationTest.TestCaseResults;
    using GemCorpAnalytics.Calculators;
    using GemCorpAnalytics.Context;
    using GemCorpAnalytics.Data;
    using GemCorpAnalytics.DataAccess.ContextBuilders;
    using GemCorpAnalytics.DataAccess.Factories;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.DataAccess.Utils;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.Enums;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Threading.Tasks;
    using System.Xml;

    /// <summary>Calculate the beta test case.</summary>
    public class CalculateBetaTestCase : ITestCase
    {
        
        #region Public Methods

        public async Task<ITestCaseResults> RunTest(ITestCaseContext testContext)
        {
            var context = (GetBetaTestCaseContext)testContext;

            var seriesToProxy = new[] { new TimeSeriesRepositoryEntry("SPAC",TimeSeriesAttributes.Close), 
                new TimeSeriesRepositoryEntry("ARVL US",TimeSeriesAttributes.Close), 
                new TimeSeriesRepositoryEntry("BBG00G41HPF7",TimeSeriesAttributes.Z_Sprd_Mid)};
            //var seriesToExclude = new[] { "NQ1", "JPSSGGH", "USGG3YR", "USGG2YR", "SPGSCI", "JPEIGLBL", "DXY" };
            var seriesToExclude = new[] { new TimeSeriesRepositoryEntry("JPSSGGH", TimeSeriesAttributes.Close), 
                new TimeSeriesRepositoryEntry("USGG3YR", TimeSeriesAttributes.Close), new TimeSeriesRepositoryEntry("USGG2YR",TimeSeriesAttributes.Close)};

            var seriesMapping = new Dictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, double>>{};
            var seriesComposition = new Dictionary<ITimeSeriesRepositoryEntry, Tuple<ITimeSeriesRepositoryEntry, ITimeSeriesRepositoryEntry>>{};

            // 20103, 20023
            // Retrieve time series and calculate beta

            var navRequest = new GetNavRequest(context.ValueDate, "Macro");
            var navRequestHandler = GetNavRequestHandlerFactory.GetHandler(context.Environment);
            var nav = navRequestHandler.Handle(navRequest);

            //var riskDirectionCollection = GetRiskDirectionCollection(context.ValueDate, context.Environment);
            var fileName = $"C:\\temp\\RiskDirectionCollection_{context.ValueDate:yyyyMMdd}.xml";

            //var flag = fileSerializer(fileName, riskDirectionCollection);

            var riskDirectionCollection = fileReader(fileName);

            // Pull time series with all attributes
            var timeSeries = riskDirectionCollection.Select(r => r.TimeSeriesMapping).Distinct(); 

            var timeSeriesRepository = GetTimeSeriesRepository(context.HedgingVals, riskDirectionCollection, context.ValueDate, context.Environment);
            var riskType = CreateRiskType(riskDirectionCollection);

            foreach(var projSeries in context.HedgingVals)
            {
                if (riskType.ContainsKey(projSeries.Key))
                    continue;
                riskType[$"{projSeries.Key},Delta"] = projSeries.Value;
            }

            var projectionSeries = new Dictionary<ITimeSeriesRepositoryEntry, AssetClass>
            {
                { new TimeSeriesRepositoryEntry("CO1", TimeSeriesAttributes.Close), AssetClass.Commodity },
                { new TimeSeriesRepositoryEntry("SPX", TimeSeriesAttributes.Close), AssetClass.Equity },
                { new TimeSeriesRepositoryEntry("SPBDU10T", TimeSeriesAttributes.Close), AssetClass.Equity },
                { new TimeSeriesRepositoryEntry("FXJPEMCS", TimeSeriesAttributes.Close), AssetClass.Equity },
                { new TimeSeriesRepositoryEntry("DXY", TimeSeriesAttributes.Close), AssetClass.Fx },
            };

            var projectedRiskContext = new ProjectedRiskContext(context.Environment, projectionSeries, 
                context.ValueDate, seriesToProxy, seriesToExclude, seriesMapping, seriesComposition,
                riskType, null, null, timeSeriesRepository.TimeSeries, riskDirectionCollection, nav, context.Entity);

            var minVarHedgeCalculator = new MinVarHedgeBetaCalculator(projectedRiskContext, null, null);
            var result = minVarHedgeCalculator.Calculate(context.ValueDate, context.MinDate, null, null);

            var projRiskThemeMapper = new ProjectedRiskThemeMapper();
            var riskAllocCalculator = new RiskAllocationCalculator(projRiskThemeMapper);
            var riskAllocation = riskAllocCalculator.Calculate(result);
            return new CalculateBetaTestCaseResults(true, result);
        }

        #endregion

        #region Methods

        /// <summary>Gets the risk direction collection.</summary>
        /// <param name="date">The date.</param>
        /// <param name="environment">The environment.</param>
        /// <returns>The <see cref="RiskDirection"/>.</returns>
        private RiskDirectionCollection GetRiskDirectionCollection(DateTime date, string environment)
        {
            var proxyEnvironment = "PROD";
            var themeContextBuilder = PlByThemeContextBuilder.Build(date, proxyEnvironment);
            var macroBooks = themeContextBuilder.FundConfigs[FundNames.Macro][FundStrategies.Macro];

            var riskType = "None";

            // This is a bit of a hack for macro

            var getRiskSensitivityCollection = new GetRiskSensitivityCollection();
            getRiskSensitivityCollection.AddRange(new[] {
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Forex"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Credit"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Equity"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Interest"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Commodity"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Commodity Volatility"),
                new GetRiskSensitivityRequest(date, "", "GEMCORP FUND I LIMITED", riskType, "Equity Volatility"),
                new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Forex"),
                new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Credit"),
                new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Equity"),
                new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Interest"),
                new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Commodity"),
                new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Commodity Volatility"),
                new GetRiskSensitivityRequest(date, "", "Gemcorp Multi Strategy Master Fund SICAV SCS", riskType, "Equity Volatility"),
            });

            var getRiskSensitivityCollectionHandler = RiskSensitivityRequestHandlerFactory.GetHandler(proxyEnvironment);
            var riskSensitivityCollection = getRiskSensitivityCollectionHandler.Handle(getRiskSensitivityCollection);

            var macroRiskSensitivityCollection = riskSensitivityCollection.Where(r => macroBooks.Contains(r.Book));

            var riskSensitivityTimeSeriesMapper = new RiskSensitivityTimeSeriesMapper();

            var portfolioRiskSensitivityCollection = new PortfolioRiskSensitivityCollection();
            portfolioRiskSensitivityCollection.AddRange(macroRiskSensitivityCollection);

            var portfolioRiskCalculator = new PortfolioRiskFactorCalculator(portfolioRiskSensitivityCollection, riskSensitivityTimeSeriesMapper);
            var sensitivities = portfolioRiskCalculator.Calculate();

            var riskDirections = new RiskDirectionCollection();

            var riskDirectionCollection = sensitivities.RiskDirectionCollection.Select(x => {
                return new RiskDirection.Builder
                {
                    AssetClass = x.AssetClass,
                    Name = x.Name,
                    RiskAmount = x.RiskAmount,
                    RiskGreekDirection = x.RiskGreekDirection,
                    TimeSeriesMapping = Utils.TimeSeriesMapping(x.AssetClass, x.Name, x.TimeSeriesMapping)
                }.Build();
            });

            riskDirections.AddRange(riskDirectionCollection);

            return riskDirections;
        }


        /// <summary>Gets the time series repository.</summary>
        /// <param name="riskDirections">The risk directions.</param>
        /// <param name="valueDate">The value date.</param>
        /// <param name="environment">The environment.</param>
        /// <returns>The <see cref="TimeSeriesRepository"/>.</returns>
        private TimeSeriesRepository GetTimeSeriesRepository(Dictionary<string, string> hedgingVals, IEnumerable<RiskDirection> riskDirections, DateTime valueDate, string environment)
        {
            var timeSeriesRequest = Utils.TimeSeriesRequestBuilder(riskDirections, new DateTime(1970, 1, 1), valueDate, TimeSeriesType.Daily, TimeSeriesAttributes.Close);

            var proxyTimeSeriesRequestNames = new List<KeyValuePair<string, TimeSeriesAttributes>>();

            foreach (var request in timeSeriesRequest.Names)
            {
                var key = request.Key;
                var value = request.Value;
                if (request.Key == "OD9")
                    value = TimeSeriesAttributes.Low;
                proxyTimeSeriesRequestNames.Add(new KeyValuePair<string, TimeSeriesAttributes>(key, value));
            }

            foreach (var hedgeVal in hedgingVals)
            {
                proxyTimeSeriesRequestNames.Add(new KeyValuePair<string, TimeSeriesAttributes>(hedgeVal.Key, TimeSeriesAttributes.Close));
            }

            var proxyTimeSeriesRequest = new GetTimeSeriesRequest(timeSeriesRequest.FromDate, timeSeriesRequest.ToDate);
            proxyTimeSeriesRequest.Names.AddRange(proxyTimeSeriesRequestNames);

            var timeSeriesRequestHandler = TimeSeriesRequestHandlerFactory.GetHandler(environment, TimeSeriesType.Daily);
            var timeSeries = timeSeriesRequestHandler.Handle(proxyTimeSeriesRequest);
            var timeSeriesRepository = new TimeSeriesRepository(timeSeries);
            return timeSeriesRepository;
        }

        private static Func<string, RiskDirectionCollection> fileReader = (f) =>
        {
            var fs = new FileStream(f, FileMode.Open);
            var reader = XmlDictionaryReader.CreateTextReader(fs, new XmlDictionaryReaderQuotas());
            var serialiser = new DataContractSerializer(typeof(RiskDirectionCollection));
            var collection = serialiser.ReadObject(reader, true);
            var riskDirectionCollection = (RiskDirectionCollection)collection;
            reader.Close();
            fs.Close();
            return riskDirectionCollection;
        };


        private static Func<string, RiskDirectionCollection, bool> fileSerializer = (f, c) => 
        {
            var xmlWriter = XmlWriter.Create(f);
            var dataContractSerializer = new DataContractSerializer(typeof(RiskDirectionCollection));
            dataContractSerializer.WriteObject(xmlWriter, c);
            xmlWriter.Close();
            return true;
        };

        private static Dictionary<string, string> CreateRiskType(RiskDirectionCollection riskDirectionCollection)
        {

            var output = new Dictionary<string, string>();

            foreach(var riskDirection in riskDirectionCollection)
            {
                var returnType = string.Empty;

                if (riskDirection.RiskGreekDirection == RiskGreekDirection.Vega)
                {
                    output[$"{riskDirection.TimeSeriesMapping},{riskDirection.RiskGreekDirection}"] = "vol";
                    continue;
                }

                if(riskDirection.AssetClass==AssetClass.Fx)
                    returnType = "fx";

                if (riskDirection.AssetClass == AssetClass.Equity)
                    returnType = "equity";

                if (riskDirection.AssetClass == AssetClass.Commodity)
                    returnType = "oil";

                if (riskDirection.AssetClass == AssetClass.Interest || riskDirection.AssetClass == AssetClass.Credit)
                    returnType = "spread";

                output[$"{riskDirection.TimeSeriesMapping},{riskDirection.RiskGreekDirection}"] = returnType;
            }

            return output;
        }

        #endregion

    }
}
