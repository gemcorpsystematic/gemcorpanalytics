﻿namespace GemcorpAnalytics.IntegrationTest.TestCase
{
    using GemCorpAnalytics.Data.Interfaces;
    using GemCorpAnalytics.Calculators;
    using GemcorpAnalytics.IntegrationTest.TestCaseContext;
    using GemcorpAnalytics.IntegrationTest.TestCaseResults;
    using System;
    using System.IO;
    using System.Data;
    using System.Linq;
    using System.Collections.Generic;
    using GemCorpAnalytics.DataCollections;
    using GemCorpAnalytics.DataAccess.Factories;
    using GemCorpAnalytics.DataAccess.Requests;
    using GemCorpAnalytics.Enums;
    using Newtonsoft.Json;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;

    public class CalculateSummaryByTheme : ITestCase
    {
        #region Constructor

        public CalculateSummaryByTheme()
        {

        }

        #endregion

        #region Public Methods

        public async Task<ITestCaseResults> RunTest(ITestCaseContext testContext)
        {
            // Step 1: connect to GMCAPP04P
            // Step 2: do the same for Var Data and theme

            var dateRange = new List<DateTime>();
            var finalDate = new DateTime(2022, 10, 6, 23, 59, 59);
            var tmpDate = new DateTime(2022, 10, 6, 23, 59, 59);

            while(tmpDate <= finalDate)
            {
                dateRange.Add(tmpDate);
                tmpDate = tmpDate.AddDays(1);
                if(tmpDate.DayOfWeek == DayOfWeek.Saturday)
                    tmpDate = tmpDate.AddDays(2);
                if (tmpDate.DayOfWeek == DayOfWeek.Sunday)
                    tmpDate = tmpDate.AddDays(1);
            }


            var environment = "PROD";

            var themesRequestHandler = ThemesRequestHandlerFactory.GetHandler(environment);
            var themesRequest = new GetThemesRequest("ThemeOverrides");
            var macroThemesRequest = new GetThemesRequest("MacroThemeOverrides");
            var themesTable = themesRequestHandler.Handle(themesRequest);
            var macroThemesTable = themesRequestHandler.Handle(macroThemesRequest);

            var macroDailyPnlRequestHandler = DailyPnLDataRequestHandlerFactory.GetHandler(environment, macroThemesTable, MapThemes);
            var macroVarRequestHandler = VaRDataRequestHandlerFactory.GetHandler(environment, macroThemesTable, MapThemes);

            
            foreach (var date in dateRange)
            {
                try
                {
                    //var valueDate = new DateTime(2022, 4, 20, 23, 59, 59);
                    var valueDate = date;
                    Console.WriteLine($"Running for: {valueDate:yyyy-MM-dd}");

                    var dailyPnlRequestHandler = DailyPnLDataRequestHandlerFactory.GetHandler(environment, themesTable, MapThemes);
                    var request = new GetPnLDataRequest(valueDate);
                    var pnlTable = dailyPnlRequestHandler.Handle(request);

                    var missingThemes = pnlTable.Where(p => p.Theme == "Missing Theme").ToList();

                    var macroPnLTable = macroDailyPnlRequestHandler.Handle(request);

                    //var varRequestHandler = VaRDataRequestHandlerFactory.GetHandler(environment, themesTable, MapThemes);
                    var varRequest = new GetVaRDataRequest(valueDate);
                    //var varDataTable = varRequestHandler.Handle(varRequest);


                    var macroVarDataTable = macroVarRequestHandler.Handle(varRequest);

                    // map lines daily pnl table to themes in themesTable
                    var plByThemeContext = GemCorpAnalytics.DataAccess.ContextBuilders.PlByThemeContextBuilder.Build(valueDate, environment);
                    //var plByThemeCalculator = new PlByThemeCalculator(pnlTable, varDataTable, plByThemeContext);


                    var macroBooks = plByThemeContext.FundConfigs[FundNames.Macro][FundStrategies.Macro];

                    var macroPlCollection = macroPnLTable.Where(m => macroBooks.Contains(m.Book)).ToList();

                    var macroPlByThemeCalculator = new PlByThemeCalculator(macroPlCollection, macroVarDataTable, plByThemeContext);

                    //var fundPlByThemes = plByThemeCalculator.Calculate(FundNames.Fund);
                    //var multiPlByThemes = plByThemeCalculator.Calculate(FundNames.Multi);
                    //var africaPlByThemes = plByThemeCalculator.Calculate(FundNames.Africa);
                    // Musing -> should Macro be treated as a fund or strategy
                    var macroPlByThemes = macroPlByThemeCalculator.Calculate(FundNames.Macro);

                    //var macroPlByThemesResult = JsonConvert.SerializeObject(macroPlByThemes);

                    //var dataSet = new DataSet("PlByThemes");
                    //dataSet.Tables.AddRange(fundPlByThemes.ResultsTable.ToArray());
                    //dataSet.Tables.AddRange(multiPlByThemes.ResultsTable.ToArray());
                    //dataSet.Tables.AddRange(africaPlByThemes.ResultsTable.ToArray());
                    //dataSet.Tables.AddRange(macroPlByThemes.ResultsTable.ToArray());

                    //var dataTableJson = JsonConvert.SerializeObject(dataSet, Formatting.Indented);

                    //OutputDataTable(varDataTable.Select(r=>r.ToString()), $"PlByTheme_VaRDataTable_{valueDate:yyyyMMdd}");
                    //OutputDataTable(pnlTable.Select(r => r.ToString()), $"PlByTheme_PnLDataTable_{valueDate:yyyyMMdd}");
                    //OutputDataTable(macroPnLTable.Select(r => r.ToString()), $"PlByTheme_MacroPnLDataTable_{valueDate:yyyyMMdd}");

                    //OutputDataTable(fundPlByThemes.ResultsTable[1].AsEnumerable().Select(r=>r.ToString()), $"PlByTheme_Fund_{valueDate:yyyyMMdd}");
                    //OutputDataTable(multiPlByThemes.ResultsTable[1].AsEnumerable().Select(r => r.ToString()), $"PlByTheme_Multi_{valueDate:yyyyMMdd}");
                    //OutputDataTable(africaPlByThemes.ResultsTable[1].AsEnumerable().Select(r => r.ToString()), $"PlByTheme_Africa_{valueDate:yyyyMMdd}");
                    //OutputDataTable(macroPlByThemes.ResultsTable[0], $"PlByTheme_Macro_{valueDate:yyyyMMdd}", $"{valueDate:yyyy-MM-dd}");
                    Console.WriteLine($"{valueDate:yyyy-MM-dd}, {macroPlByThemes.ResultsTable[0].Rows[0].ItemArray[5]}");
                    OutputDataTable(macroPlByThemes.ResultsTable[0], $"PlByTheme_Macro_TimeSeries", $"{valueDate:yyyy-MM-dd}");

                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error: {e.Message}");
                }
            }
            
            throw new NotImplementedException();
        }

        #endregion

        #region Methods

        public string MapThemes(string book, string countryOfRisk, string underlierInfo, ThemesCollection themes)
        {
            /* Order of dependency
             * 1. Book
             * 2. Strategy
             * 3. Country Of Risk
             * 4. UnderlierInfo
             */

            book = TrimName(book);

            //var bookMatch = from DataRow themeRow in themes.Rows
            //                where themeRow.Field<string>("countryofrisk") == book
            //                select themeRow;

            var bookMatch = themes.Where(r => r.CountryOfRisk == book).ToList();

            var regexUnderlierPrecedence = themes.Where(r => {
                var precedenceString = r.CountryOfRisk;
                if (string.IsNullOrEmpty(precedenceString) || !precedenceString.Contains("%")) return false;

                var trimString = precedenceString.Trim(new[] { '%' });
                var stringPattern = string.Format(@"{0}", trimString);
                var rgx = new Regex(stringPattern, RegexOptions.IgnoreCase);
                var matches = rgx.Matches(underlierInfo);

                return matches.Count > 0;
            });

            if (regexUnderlierPrecedence.Count() > 0)
                return regexUnderlierPrecedence.First().Theme;

            if(bookMatch.Count()==0)
            {
                bookMatch = themes.Where(r => r.CountryOfRisk == $"{book} ()").ToList();
            }


            if (bookMatch.Count() == 0)
            {
                if (string.IsNullOrEmpty(countryOfRisk))
                {
                    
                    var underlierMatch = themes.Where(r => r.CountryOfRisk == underlierInfo);

                    if (underlierMatch.Count() != 0)
                    {
                        return underlierMatch.First().Theme;
                    }

                    var regexUnderlierMatch = themes.AsEnumerable().Where(r=> 
                    {
                        var proxyString = r.CountryOfRisk;
                        if (string.IsNullOrEmpty(proxyString)) return false;
                        var trimProxyString = proxyString.Contains('%') ? proxyString.Trim(new[] { '%' }) : proxyString;
                        var stringPattern = string.Format(@"{0}", trimProxyString);
                        var rgx = new Regex(stringPattern, RegexOptions.IgnoreCase);
                        var matches = rgx.Matches(underlierInfo);
                        return matches.Count > 0;
                    });

                    if(regexUnderlierMatch.Count() > 0)
                        return regexUnderlierMatch.First().Theme;
                    
                }
            }
            else {
                return bookMatch.First().Theme;                
            }

            return "Missing Theme";    
        }


        public string TrimName(string name)
        {
            if(name.Contains('('))
            {
                var splitName = name.Split('(');
                return $"{splitName[0]}()";
            }
            return name;
        }


        #endregion

        #region Method 

        private void OutputDictionary(Dictionary<string, Tuple<double,double>> results, string fileName)
        {
            using (var outputFile = new StreamWriter($"C:\\temp\\{fileName}.txt")) 
            {

                foreach(var kvp in results)
                {
                    outputFile.WriteLine($"{kvp.Key},{kvp.Value.Item2},{kvp.Value.Item1}");
                }

            }
        }

        private void OutputDataTable(IEnumerable<string> data, string fileName)
        {

            using (var outFile = new StreamWriter($"C:\\temp\\{fileName}.txt")) 
            {
                foreach(var row in data)
                {
                    outFile.WriteLine(row);
                }
            }

        }
        private void OutputDataTable(DataTable data, string fileName, string valueDate="")
        {
            using (var outFile = File.AppendText($"C:\\temp\\{fileName}.txt"))
            {
                foreach (var row in data.AsEnumerable())
                {
                    var outputString = string.Empty;
                    foreach(var output in row.ItemArray)
                    {
                        outputString += $"{output},";
                    }
                    outputString = outputString.TrimEnd(',');
                    outputString = $"{valueDate},{outputString}";
                    outFile.WriteLine(outputString);
                }
            }

        }


        #endregion

    }
}
