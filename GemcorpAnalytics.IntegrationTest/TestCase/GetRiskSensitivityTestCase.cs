﻿namespace GemcorpAnalytics.IntegrationTest.TestCase
{
    using GemcorpAnalytics.IntegrationTest.TestCaseContext;
    using GemcorpAnalytics.IntegrationTest.TestCaseResults;
    using GemCorpAnalytics.DataAccess.Factories;
    using System;
    using System.Linq;
    using System.Threading.Tasks;


    /// <summary>The get risk sensitivity test case.</summary>
    public class GetRiskSensitivityTestCase : ITestCase
    {

        #region Constructor

        public GetRiskSensitivityTestCase()
        {

        }

        #endregion

        #region Public Methods

        public async Task<ITestCaseResults> RunTest(ITestCaseContext testContext)
        {
            var context = (GetRiskSensitivityTestCaseContext)testContext;
            var handler = RiskSensitivityRequestHandlerFactory.GetHandler(context.Environment);
            var resultTable = handler.Handle(context.GetRiskSensitivityCollection);
            var filteredResultTable = resultTable.Select(r=>r.Underlier).Distinct().ToList();
            throw new NotImplementedException();
        }

        #endregion
    }
}
