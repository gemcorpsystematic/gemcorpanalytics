﻿namespace GemcorpAnalytics.IntegrationTest.TestCaseResults
{
    /// <summary>The test case results.</summary>
    public interface ITestCaseResults
    {
        /// <summary>The success flag.</summary>
        bool Success { get; }
    }
}
