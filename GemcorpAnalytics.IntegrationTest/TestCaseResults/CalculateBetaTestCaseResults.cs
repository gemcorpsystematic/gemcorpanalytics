﻿namespace GemcorpAnalytics.IntegrationTest.TestCaseResults
{
    using GemCorpAnalytics.Calculators.Interfaces;
    
    /// <summary>Calculates the beta test case results.</summary>
    public class CalculateBetaTestCaseResults : ITestCaseResults
    {
        #region Constructor

        /// <summary>Initialize an instance of the calculate beta test case result.</summary>
        /// <param name="success">The success flag.</param>
        /// <param name="minVarHedgeCalculatorResult">The min var hedge calculator result.</param>
        public CalculateBetaTestCaseResults(
            bool success,
            IMinVarHedgeBetaCalculatorResult minVarHedgeCalculatorResult)
        {
            Success = success;
            MinVarHedgeBetaCalculatorResult = minVarHedgeCalculatorResult;
        }

        #endregion

        #region Public Properties

        /// <summary>The success flag.</summary>
        public bool Success {get;}

        /// <summary>The min var hedge beta calculator result.</summary>
        public IMinVarHedgeBetaCalculatorResult MinVarHedgeBetaCalculatorResult { get; } 

        #endregion
    }
}
