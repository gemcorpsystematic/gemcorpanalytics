﻿namespace GemcorpAnalytics.IntegrationTest.TestCaseContext
{
    using System;
    using System.Collections.Generic;

    /// <summary>The get beta test case context.</summary>
    public class GetBetaTestCaseContext : ITestCaseContext
    {
        #region Constructor

        /// <summary>Initialize an instance of the get beta test case context.</summary>
        /// <param name="valueDate">The value date.</param>
        /// <param name="minDate">The min date.</param>
        /// <param name="environment">The environment.</param>
        /// <param name="projectionSeries">The projection series.</param>
        /// <param name="projectionSeriesReturnType">The projection series return type.</param>
        /// <param name="entity">The entity.</param>
        public GetBetaTestCaseContext(
            DateTime valueDate,
            DateTime minDate,
            string environment,
            Dictionary<string, string> hedgingVals,
            string entity)
        {
            ValueDate = valueDate;
            MinDate = minDate;
            Environment = environment;
            HedgingVals = hedgingVals;
            Entity = entity;
        }

        #endregion

        #region Public Properties

        /// <summary>The value date.</summary>
        public DateTime ValueDate { get; }

        /// <summary>The min date.</summary>
        public DateTime MinDate { get; }

        /// <summary>The environment.</summary>
        public string Environment { get; }

        /// <summary>The projection series.</summary>
        public Dictionary<string,string> HedgingVals { get; }

        /// <summary>The entity.</summary>
        public string Entity { get; }

        #endregion

    }
}
