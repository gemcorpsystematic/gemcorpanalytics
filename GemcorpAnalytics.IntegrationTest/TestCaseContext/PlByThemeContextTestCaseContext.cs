﻿namespace GemcorpAnalytics.IntegrationTest.TestCaseContext
{
    using System;

    /// <summary>The pl by theme context test case context.</summary>
    public class PlByThemeContextTestCaseContext : ITestCaseContext
    {
        #region Constructor

        /// <summary>Initialize an instance of the constructor.</summary>
        /// <param name="asOfDate">The as of date.</param>
        /// <param name="toDate">The to date.</param>
        /// <param name="environment">The environment.</param>
        public PlByThemeContextTestCaseContext(
            DateTime asOfDate,
            DateTime toDate,
            string environment)
        {
            AsOfDate = asOfDate;
            ToDate = toDate;
            Environment = environment ?? throw new ArgumentNullException(nameof(environment));
        }

        #endregion

        #region Public Properties

        /// <summary>The as of date.</summary>
        public DateTime AsOfDate { get; }

        /// <summary>The to date.</summary>
        public DateTime ToDate { get; }

        /// <summary>The environment.</summary>
        public string Environment { get; }

        #endregion
    }
}
