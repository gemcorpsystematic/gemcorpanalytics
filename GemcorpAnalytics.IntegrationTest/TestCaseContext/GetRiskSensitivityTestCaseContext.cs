﻿namespace GemcorpAnalytics.IntegrationTest.TestCaseContext
{
    using GemCorpAnalytics.DataAccess.Requests;

    /// <summary>The get risk sensitivity test case context.</summary>
    public class GetRiskSensitivityTestCaseContext : ITestCaseContext
    {

        #region Constructor

        /// <summary>Initialize an instance of the get risk sensitivity test case context.</summary>
        /// <param name="environment">The environment.</param>
        /// <param name="valueDate">The value date.</param>
        public GetRiskSensitivityTestCaseContext(
            string environment,
            GetRiskSensitivityCollection getRiskSensitivityCollection)
        {
            Environment = environment;
            GetRiskSensitivityCollection = getRiskSensitivityCollection;
        }

        #endregion

        #region Public Properties

        /// <summary>The environment.</summary>
        public string Environment { get; }

        /// <summary>The value date.</summary>
        public GetRiskSensitivityCollection GetRiskSensitivityCollection { get; }

        #endregion

    }
}
